<?php

/* OOPSIES */
/**
/*
==============================================================================
CMS Name    :   HUMMINGBIRD
CMS Version :   v3
Available   :   www.hummingbirdcms.com / www.tickboxmarketing.co.uk
Copyright   :   Copyright (C) 2006 - 2014 Hummingbird. All rights reserved.
Description :   Hummingbird 3.0 is a Content Management System for websites 
                developed by Tickbox Marketing. It is built using open source 
                frameworks and designed from the bottom up to support 
                responsive design for mobiles and other devices. 

                Hummingbird 3.0 is based around a centralised core Content 
                Management System and a core library of key functions called 
                plug-ins.

==============================================================================
 */

return array
(
    array(
        'is_directory'  => true,
        'location'      => '/assets copy'
    ),
    array(
        'is_directory'  => false,
        'location'      => '/update2 copy.php'
    )
);
