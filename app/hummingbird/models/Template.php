<?php

class Template extends LaravelBook\Ardent\Ardent {

    public $table = 'templates';
    public $cms_url = '';
    protected $fillable = array('name', 'description', 'html', 'locked');
    
    protected $guarded = array();
    
    public static function get_selection($add_new = true)
    {
        $selection = array();
        
        $templates = Template::whereNull('deleted_at')->get();
        
        $selection[0] = "Select template";
        
        foreach($templates as $template) {
            $selection[$template->id] = $template->name;
        }
        
        return $selection;
    }
    
    public static function get_types()
    {
        return array ('page' => 'Page');
    }
    
    public static function get_statuses()
    {
        return array (0 => 'Inactive', 1 => 'Active');
    }
    
    private function get_relational_data($path = null)
    {            
        $path = ($path != null) ? $path : __DIR__ . '/../views/templates/emails/';
        $templates = scandir($path);
        $all_templates = array();

        foreach ($templates as $template) {

            if ($template == '.' || $template == '..')
                continue;
            
            $view = str_replace(".blade.php", "", $template);            
            $all_templates[$view] = $view;
        }
        
        $this->data['views'] = $all_templates;    
    }
    
    public static function get_layouts($path = null)
    {
        $available_layouts = array();
        
        $path = ($path != null) ? $path : (__DIR__ . '/../../themes/public/'.Config::get('HummingbirdBase::hummingbird.activeTheme').'/templates/');
        $layouts = scandir($path);
        foreach ($layouts as $layout) {

            if ($layout == '.' || $layout == '..')
                continue;
            
            $view = str_replace(".blade.php", "", $layout);            
            $available_layouts[strtolower($view)] = ucfirst($view);
        }
        
        return $available_layouts;
    }
    
    public function get_module_areas()
    {
        $html = $this->html;
        echo $html;
        
        // regex on {{$modules[area name]}}
        return array('AREA-ONE', 'AREA-TWO');
    }
    
    /*
     * html to be rendered for a page on the frontend
     */
    public function getHtmlFromStructure()
    {
        // coming from frontend, so won't have path
        View::addLocation(app_path() . "/hummingbird/pages/views");
        
        $data['structure'] = json_decode($this->structure);
        $data['elements'] = array();
        
        return View::make('HummingbirdBase::cms.template-structure', $data);
    }

    public function setDeletedAtAttribute($value)
    {
        $this->attributes['deleted_at'] = date("Y-m-d H:i:s");
    }

    public function scopeFind($query, $value)
    {
        return $query->where('id', '=', $value)->whereNull('deleted_at');
    }
}
