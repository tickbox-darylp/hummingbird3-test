<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Post extends LaravelBook\Ardent\Ardent
{
    use SoftDeletingTrait;

    public $table = 'news';
    public $cms_url = 'blog';
    
    protected $fillable = array('title', 'summary', 'content', 'permalink', 'post_date', 'post_end_date', 'feature', 'status', 'author', 'author_email', 'showcase', 'searchable', 'search_image', 'featured_image');

    protected $permission_type = 'blog';

    public static $rules = array(
        'title' => 'required'
    );

    public static function get_statuses()
    {
        return array('draft' => 'Draft', 'public' => 'Public');
    }
    
    public function getPermissionType()
    {
        return $this->permission_type;
    }

    public function setPostDateAttribute($value)
    {
        $this->attributes['post_date'] = date("Y-m-d H:i:s", strtotime(str_replace("/", "-", $value)));
    }

    public function setPostEndDateAttribute($value)
    {
        if($value == '') $this->attributes['post_end_date'] = '0000-00-00 00:00:00';
        $this->attributes['post_end_date'] = date("Y-m-d H:i:s", strtotime(str_replace("/", "-", $value)));
    }

    public function getPostDateAttribute($value)
    {
        if($value == '0000-00-00 00:00:00') return time();
        return strtotime($value);
    }

    public function getPostEndDateAttribute($value)
    {
        if($value == '0000-00-00 00:00:00') return '';
        return strtotime($value);
    }

    public function storeTaxonomy($tags, $post, $class)
    {
        DB::table('taxonomy_relationships')->where('tax_type', '=', $class)->where('item_id', '=', $post->id)->delete();

        $input = array();
        foreach($tags as $tag)
        {
            $input[] = array('term_id' => $tag, 'tax_type' => $class, 'item_id' => $post->id);
        }

        DB::table('taxonomy_relationships')->insert($input);
    }

    public function taxonomy()
    {
        $data = array('category' => array(), 'tag' => array());
        $terms = DB::table('taxonomy_relationships')->leftJoin('taxonomy', 'taxonomy_relationships.term_id', '=', 'taxonomy.id')->where('item_id', '=', $this->id)->where('tax_type', '=', get_class($this))->select('taxonomy.id', 'taxonomy.type')->get();

        foreach($terms as $term)
        {
            $data[$term->type][] = $term->id;
        }

        return $data;
    }

    public function taxonomy_relationships()
    {
        return $this->belongsToMany('Categories', 'taxonomy_relationships', 'item_id', 'term_id');
    }

    public function scopeLive($query)
    {
        return $query->where('status', '=', 'public');
    }

    public function getImage($field, $temp_image = '')
    {
        if($this->$field != '') return $this->$field;
        else if($temp_image != '') return $temp_image;
    }

    public function scopeFuture($query, $from_date = null, $to_date = null)
    {
        if(null !== $from_date OR null !== $to_date)
        {
        }
        else
        {
            return $query->where(function($query)
            {
                $query->where('post_date', '<=', date("Y-m-d H:i:s"))
                    ->orWhere(function($query)
                    {
                        $query->whereNull('post_end_date')
                            ->orWhere('post_end_date', '>=', date("Y-m-d H:i:s"))
                            ->orWhereNotIn('post_end_date', array('0000-00-00 00:00:00', '1970-01-01 01:00:00', '1970-01-01 00:00:00'));
                    });
            });
        }
    }


    public static function boot()
    {
        parent::boot();
    }
    
}
