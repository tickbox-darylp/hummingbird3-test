<?php

class BlogController extends CmsbaseController 
{
    public $code_location = 'blog';

    public $pagination = 15;

    /**
     * Inject the models.
     * @param Post $post
     * @param User $user
     */
    public function __construct(Post $post)
    {
        parent::__construct();

        $this->post = $post;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($pagelist = '')
    {
        if($this->user->hasAccess('read', get_class($this->post)))
        {
            $this->data['tag'] = 'Manage Blog Articles';

            if(Input::get('s') AND Input::get('s') != '')
            {
                $this->data['posts'] = $this->cms_search();
            } 
            elseif($pagelist == 'trash')
            {
                $this->data['posts'] = Post::where('status', 'trash')->paginate($this->pagination);
            }
            else
            {
                $this->data['posts'] = Post::orderBy('post_date', 'DESC')->where('status', '!=', 'trash')->whereNull('deleted_at')->paginate($this->pagination);
            }

            return View::make('HummingbirdBase::cms/blog', $this->data);
        }
        else
        {
            return View::make('HummingbirdBase::cms.errors.unauthorised'); //replace with CMS Error/Unauthorised controller
        }
    }

     public function getTrash()
    {
        return $this->index($pagelist = 'trash');
    }

    public function getRestore($id)
    {
        
        $post           = Post::find($id);
        $post->status   = 'draft';
        $post->deleted_at = NULL;
        $post->save();

        return Redirect::to(App::make('backend_url').'/blog/'.$id);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    public function getMetaData($id, $version_id = null)
    {
        $content = array();

        $metasets = DB::table('meta_content')->where('item_id', '=', $id)->where('item_type', '=', get_class(new Post))->get();

        if(count($metasets) > 0)
        {
            foreach($metasets as $meta)
            {
                $content[$meta->key] = $meta->value;
            }
        }

        return $content;
    }

    public function setMetaData($id, $data = array(), $version_id = null)
    {
        if(!empty($data))
        {
            $metasets = DB::table('meta_content')->where('item_id', '=', $id)->where('item_type', '=', get_class(new Post))->delete();

            $insert_array = array();

            $insert_array[] = array('item_id' => $id, 'item_type' => get_class(new Post), 'key' => 'meta_title', 'value' => $data['meta_title']);
            $insert_array[] = array('item_id' => $id, 'item_type' => get_class(new Post), 'key' => 'meta_keywords', 'value' => $data['meta_keywords']);
            $insert_array[] = array('item_id' => $id, 'item_type' => get_class(new Post), 'key' => 'meta_description', 'value' => $data['meta_description']);

            $metasets = DB::table('meta_content')->insert($insert_array);
        }
    }

    public function show($id = false)
    {
        $version = (!Input::get('version')) ? false:Input::get('version'); //get the version
        $this->data['post_version'] ='';

        if(!$id AND !$version) return Redirect::to(App::make('backend_url').'/blog/');

        if($version)
        {
            $this->data['post'] = PostVersion::where('id', '=', $version)->where('post_id', '=', $id)->first();
            $this->data['post_id'] = $this->data['post']->post_id;
            $this->data['post_version'] = $this->data['post']->id;
            $this->data['taxonomy'] = Post::find($id)->taxonomy();
        }

        if(!isset($this->data['post']))
        {
            $this->data['post'] = Post::find($id);
            $this->data['post_id'] = $this->data['post']->id;
            $this->data['taxonomy'] = $this->data['post']->taxonomy();
            $this->data['versions'] = PostVersion::where('post_id', '=', $id)->whereNull('deleted_at')->orderBy('id', 'DESC')->get();
        }

        $this->data['meta'] = $this->getMetaData($id);

        return View::make('HummingbirdBase::cms/blog-edit', $this->data);
    }
    
    public function update($id)
    {
        $version = (!Input::get('version')) ? false:Input::get('version'); //get the version

        $input  = Input::except('_token');
        $post   = Post::find($id);
        
        if($input['status'] == 'public')
        {
            if($this->user->hasAccess('publish', get_class($post)))
            {
                $post = $post->fill($input);
                $post->status = 'public';
                $post->permalink = $post->url;

                /* Create new search record - change to be an event listener */
                (new Searcher)->addToSearch($post);

                if (!$post->save()) {
                    return Redirect::to(App::make('backend_url').'/blog/'.$id)->withErrors($post->errors());
                }
            }
        }

        /* Create version */

        if($version)
        {
            $post_version = PostVersion::where('id', '=', $version)->where('post_id', '=', $id)->first();
        }
        else
        {
            $post_version = new PostVersion;
        }

        $post_version = $post_version->fill(Input::all());
        $post_version['title'] = $post['title'];
        $post_version['hash'] = $post['url'].time();
        $post_version['post_id'] = $id;
        $post_version['url'] = $post['url'];
        $post_version['permalink'] = $post['permalink'];
        $post_version['user_id'] = Auth::user()->id;
        $post_version->save();

        /* Store meta */
        $this->setMetaData($id, $input);

        /* Store cats and tags */
        $input['taxonomy'] = (isset($input['taxonomy'])) ? $input['taxonomy']:array();
        $post->storeTaxonomy($input['taxonomy'], $post, get_class($post));

        /* Store activity */
        Activitylog::log([
            'action' => 'UPDATED',
            'type' => get_class($post),
            'link_id' => $id,
            'description' => 'Updated article',
            'notes' => Auth::user()->username . " has updated an article - &quot;$post_version->title&quot;"
        ]);     
        
        if($input['status'] == 'public')
        {
            return Redirect::to(App::make('backend_url').'/blog/'.$id)->with('success', 'Article has been updated and marked as public.'); 
        }
        else
        {
            return Redirect::to(App::make('backend_url').'/blog/'.$id.'/?version='.$post_version->id)->with('success', 'Article has been updated and saved as a draft.');
            // return Redirect::to(Route::)  
            // return Redirect::to(URL::action('BlogController@show', $id));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $post = (new Post)->fill(Input::all());
        $post['url'] = Str::slug($post['title']);
        $post['permalink'] = $post['url'];
        $post['user_id'] = Auth::user()->id;
        $post['status'] = 'draft';

        if(!$post->save()){
            return \Redirect::to(App::make('backend_url').'/blog')->withErrors($post->errors());
        }

        /* Create version */
        $post_version = (new PostVersion)->fill(Input::all());
        $post_version['hash'] = $post['url'].time();
        $post_version['post_id'] = $post->id;
        $post_version['url'] = $post['url'];
        $post_version['permalink'] = $post['permalink'];
        $post_version['user_id'] = Auth::user()->id;
        $post_version['status'] = 'draft';
        $post_version->save();

        Activitylog::log([
            'action' => 'CREATED',
            'type' => get_class($post),
            'link_id' => $post->id,
            'description' => 'Created blog post',
            'notes' => Auth::user()->username . " created a new article - &quot;$post->title&quot;"
        ]);

        return \Redirect::to(App::make('backend_url').'/blog/'.$post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        /* Delete search record - change to be an event listener */
        (new Searcher)->deleteSearch($post->id, get_class($post));

        Activitylog::log([
            'action' => 'DELETED',
            'type' => get_class($post),
            'link_id' => null,
            'description' => 'Deleted blog article',
            'notes' => Auth::user()->username . " deleted the article &quot;$post->title&quot;"
        ]);

        $post->status = 'trash';
        $post->save();

        $post->delete();

        return \Redirect::to(App::make('backend_url').'/blog')->with('success', 'Article has been moved to trash.');

    }

    /**
     *
     *
     * @param  
     * @return 
     */
    public function cms_search($ajax = false)
    {
        if(Input::get('ajax')) $ajax = true;

        $words = preg_replace('/[^a-zA-Z0-9\s]/', '', trim(Input::get('s')));
        $words = explode(" ", $words);

        $to_search = array();

        foreach($words as $word)
        {
            // $word = trim($word);
            // $to_search[] = Porter::Stem($word);
            $to_search[] = trim($word);
        }

        $to_search_words = trim(implode(" ", $to_search));

        return $this->post->select(DB::raw("news.*, 
            (
                (
                    1 * (
                            MATCH(title) AGAINST(?)
                        )
                )
                +
                (
                    1 * (
                            MATCH(url) AGAINST(?)
                        )
                )
                +
                (
                    1 * (
                            MATCH(content) AGAINST(?)
                        )
                )
            ) AS `score`"))
        // $search will NOT be bind here
        // it will be bind when calling setBindings
        ->whereRaw("MATCH(title, url, content) AGAINST(?)")
        // I want to keep only published quotes
        // ->where('approved', '=', 1)
        // Order by the rank column we got with our FULLTEXT index
        ->orWhere(function($query) use ($words)
        {
            foreach($words as $word)
            {
                $query->orWhereRaw("(title LIKE '%%$word%%' OR url LIKE '%%$word%%' OR content LIKE '%%$word%%')");
            }
        })
        ->havingRaw('score > 0')
        ->orderBy('score', 'DESC')
        // Bind variables here
        // We really need to bind ALL variables here
        // question marks will be replaced in the query
        ->setBindings([$to_search_words, $to_search_words, $to_search_words, $to_search_words])
        ->get();

        DatabaseHelper::getDatabaseQueries();
        die();
    }
}
