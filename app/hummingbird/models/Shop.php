<?php 
use Plugins\Ecommerce\Traits\ShopItemTrait;


class ShopItem extends LaravelBook\Ardent\Ardent
{
    use ShopItemTrait;
        

    public $table  = 'shop_products';
    public $options_table   = 'shop_optionsets';

    public $cms_url = 'shop';
    protected $fillable = array();
    protected $guarded = array();
    protected static $main_category_taxonomy = 'shop_category';

    public static function boot()
    {
        parent::boot();
    }

    public static function get_products()
    {
        $selection = array();
        
        $products = Products::all();
        
        foreach($products as $product) {
            $selection[$product->id] = $product;
        }
        
        return $selection;
    }
}
