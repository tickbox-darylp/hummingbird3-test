@extends('HummingbirdBase::cms.layout')

@section('content')

<?php 
$even = false; 
$first = true;?>

<h1>Previous versions of pages</h1>

{{ Form::open(array('url' => '/cms/pageversions/search')) }}
    {{ Form::label('search', 'Search: ') }}
    {{ Form::text('search', Request::get('search')) }}
    {{ Form::submit('Go') }}
{{ Form::close() }}

<hr/>

<table class='results' cellpadding='5' cellspacing='0'>
    <thead>
        <th>Title</th>
        <th>Url</th>
        <th>Parent page</th>
        <th>Status</th>
        <th>Version saved</th>
        <th>Actions</th>
    </thead>
    <tbody>
        @foreach($pageversions as $page)
        
            @if ($first)
                {{ $first = false;
                continue; }}
            @endif
        
        <tr <?php echo ($even) ? 'class="even"': '';$even = !$even;?>>
            <td>{{$page->title}}</td>
            <td>{{$page->url}}</td>
            <td>{{($page->parentpage_id > 0) ? $page->parentpage->title : ''}}</td>
            <td>{{$page->status}}</td>
            <td>{{$page->created_at}}</td>
            <td>
                <a target='_blank' href='/{{$page->permalink}}'>View</a> | 
                <a href='/{{url(App::make('backend_url')}}/pageversions/restoreVersion/{{$page->id}}'>Restore this version</a> | 
                <a href='/{{url(App::make('backend_url')}}/pageversions/delete/{{$page->id}}'>Delete</a>
            </td>
        </tr>
        
        @endforeach
    </tbody>
</table>

@stop
