@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />
@stop

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                {{ Session::get('success') }}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('message'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-info fade in">
                {{ Session::get('message') }}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('error'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                {{ Session::get('error') }}
            </div>
        </div>
    </div>  
@endif

<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <div class="clearfix">

                @if(Request::segment(3) == 'trash')
                    <h1 class="pull-left">Pages &raquo; Trash</h1>
                @else
                    <h1 class="pull-left">Page Content</h1>
                @endif

                <!-- <a href="/{{App::make('backend_url')}}/pages/re-order/" class="pull-right btn btn-default"><i class="fa fa-sort"></i> Re-order pages</a> -->

                @if(Auth::user()->hasAccess('create', get_class(new Page)))
                    <button type="button" class="pull-right btn btn-info" data-toggle="modal" data-target="#add-page"style="margin-right:20px;"><i class="fa fa-plus-circle"></i> Add Page</button>
                @endif
                
                <a href="/{{App::make('backend_url')}}/pages/trash/" class="pull-right btn btn-info"style="margin-right:20px;">View Trash</a>
            </div>

            @if(Input::get('s') AND Input::get('s') != '')
                <div class="clearfix">
                    <h3 class="normal">Searching for: {{Input::get('s')}}</h3>
                </div>
            @endif

            <div class="table">
                <table class="table table-striped">
                    <thead>
                        <th scope="row">Title</th>
                        <!-- <th>Url</th> -->
                        <th>Parent page</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        @foreach($pages as $page)
                        
                            <tr>
                                <?php 
                                    $link = ($page->permalink[0] != '/') ? '/'.$page->permalink:$page->permalink;
                                    $link = str_replace("//", "/", $link);
                                ?>
                                
                                <td>
                                    @if(Auth::user()->hasAccess('update', get_class(new Page)))<a href='/{{App::make('backend_url')}}/pages/edit/{{$page->id}}'>@endif
                                        {{$page->title}}
                                    @if(Auth::user()->hasAccess('update', get_class(new Page)))</a>@endif
                                </td>
                                <td>{{($page->parentpage_id > 0) ? $page->parentpage->title : '-'}}</td>
                                <td>
                                    @if($page->isLive())
                                        @if(Auth::user()->hasAccess('update', get_class(new Page)))
                                            <a href="/{{App::make('backend_url')}}/pages/publish/page:{{$page->id}}/0/" class="toggle-publish"><i class="fa fa-toggle-on"></i></a>
                                        @else
                                            {{$page->status}}
                                        @endif
                                    @else
                                        @if(Auth::user()->hasAccess('update', get_class(new Page)))
                                            <a href="/{{App::make('backend_url')}}/pages/publish/page:{{$page->id}}/1/" class="toggle-publish"><i class="fa fa-toggle-off"></i></a>
                                        @else
                                            {{$page->status}}
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if($page->status == 'public')
                                        <a target="_blank" href="{{$link}}" class="btn btn-xs" style="background-color:black;color:white;"><i class="fa fa-globe"></i> Preview</a>
                                    @endif

                                    @if(Request::segment(3) == 'trash')
                                        <a href='/{{App::make('backend_url')}}/pages/restore/{{$page->id}}' class="btn btn-xs btn-info">Remove from Trash</a>
                                    @else

                                    @if(Auth::user()->hasAccess('update', get_class(new Page)))<a href='/{{App::make('backend_url')}}/pages/edit/{{$page->id}}' class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>@endif
                                    @if(Auth::user()->hasAccess('delete', get_class(new Page)) AND !$page->locked)<a href='/{{App::make('backend_url')}}/pages/delete/{{$page->id}}' class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>@endif
                                    
                                    @endif                                    

                                </td>
                            </tr>
                        
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="text-center"> 
                @if(null === Input::get('s') AND Input::get('s') == '')
                    {{ $pages->appends(Request::except('page'))->links(); }}
                @endif
            </div>
        </section>
    </div>
</div>

@if(Auth::user()->hasAccess('create', get_class(new Page)))
    <div class="modal fade" id="add-page">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new page</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12"> 
                            {{ Form::open(array('url' => '/'.General::backend_url().'/pages/add/', 'class' => 'form-horizontal')) }}
                                <div class="form-group">
                                    {{ Form::label('title', 'Title: ', array('class' => 'col-sm-3')) }}
                                    <div class="col-sm-9">
                                        {{ Form::text('title', '', array('class' => 'form-control')) }}
                                    </div>
                                </div>

                                @if(count($page_list) > 0)
                                    <div class="form-group">
                                        {{ Form::label('parentpage_id', 'Parent Page: ', array('class' => 'col-sm-3')) }}
                                        <div class="col-sm-9">
                                            {{ Form::select('parentpage_id', $page_list, array('id' => 'parentpage_id', 'class' => 'selectpicker form-control')) }}
                                        </div>
                                    </div>                            
                                @else
                                    <input type="hidden" name="parentpage_id" value="1" />
                                @endif

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <button type="submit" class="btn btn-default btn-primary">Add Page</button>
                                    </div>
                                </div> 
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endif

@stop

@section('scripts')
    <script src="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>

    <script>
        $(document).ready(function()
        {
            $("#parentpage_id").data('live-search', 'true');
            $('#parentpage_id').selectpicker('render');

            // if($(".toggle-publish").length > 0)
            // {
                // $(".toggle-publish").click(function(e)
                // {
                    // e.preventDefault();

                    // $(this).find('.fa').toggleClass('fa-toggle-off fa-toggle-on');

                    // return true;
                // });
            // }
        });
    </script>
@stop
