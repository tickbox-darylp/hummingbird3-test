@extends('HummingbirdBase::cms.layout')

@section('content')

<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <h1 class="normal pull-left">Manage Templates</h1>

            <button type="button" class="pull-right btn btn-info" data-toggle="modal" data-target="#add-template"><i class="fa fa-plus-circle"></i> New Template</button>

            <div class="clearfix">&nbsp;</div> 
            @if(count($templates) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($templates as $template)
                                <tr>
                                    <td>{{$template->name}} @if($template->locked) <span class="text-danger bold">(locked)</span> @endif</td>
                                    <td>
                                        <a href="/{{App::make('backend_url').'/templates-new/edit/'.$template->id}}" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>

                                        @if(!$template->locked)
                                            <a href="/{{App::make('backend_url').'/templates-new/edit/'.$template->id}}?lock=1" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="Lock this template"><i class="fa fa-unlock"></i></a>
                                        @endif

                                        @if(!$template->locked)
                                            <a href="/{{App::make('backend_url').'/templates-new/delete/'.$template->id}}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            @else
                <div class="alert alert-info alert-info fade in text-center">
                    No templates created.
                </div>
            @endif
        </div>
    </section>
</div>


<div class="modal fade" id="add-template">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add new template</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12"> 
                        {{ Form::open(array('url' => '/'.General::backend_url().'/templates-new/add', 'class' => 'form-horizontal')) }}
                            <div class="form-group">
                                {{ Form::label('title', 'Title: ', array('class' => 'col-sm-3')) }}
                                <div class="col-sm-9">
                                    {{ Form::text('name', '', array('class' => 'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-10">
                                    <button type="submit" class="btn btn-default btn-primary">Add Template</button>
                                </div>
                            </div> 
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@stop

@section('scripts')
    <script>
        $(document).ready(function()
        {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop