<?php

class TagsController extends CmsbaseController
{
	public function __construct()
	{
		parent::__construct();

		$this->checkDeletedTags(base_path().'/delete-tags.csv');
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->data['tags'] = Tags::type()->whereNull('deleted_at')->orderBy('name', 'ASC')->get();
		$this->data['deleted_tags'] = Tags::deletedTags();
		$this->data['alphabetical'] = $this->generateAlphabetical();

		/* Build breadcrumb */
		$breadcrumbs = array(
			0 => array(
	            'classes' => '',
	            'icon' => '',
	            'title' => 'Tags',
	            'url' => ''
			)
		);

		foreach($breadcrumbs as $crumb)
		{
			$this->addToBreadcrumb($crumb);
		}

        return View::make('HummingbirdBase::cms/taxonomy-tags', $this->data);
	}

	/**
	 * Return an array of A-Z listing of items
	 *
	 * @return Data (array)
	 */
	public function generateAlphabetical()
	{
		$listing = array();
		$other = array();

		if(count($this->data['tags']) > 0)
		{
			foreach($this->data['tags'] as $tag)
			{
				$first_letter = ucfirst( strtolower( $tag->name[0] ) );

				switch($first_letter)
				{
					case ctype_alpha($first_letter):
						// is alphabetical
						$listing[ $first_letter ][] = $tag;

						break;
					case is_numeric($first_letter):
						// is numeric
						$other['0-9'][] = $tag;

						break;
					default:
						// something else
						$other['Other'][] = $tag;

						break;
				}
			}
		}

		if(isset($other['0-9']))
		{
			$listing['0-9'] = $other['0-9'];
		}

		if(isset($other['Other']))
		{
			$listing['Other'] = $other['Other'];
		}

		return $listing;
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		dd("create");
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array();

		$input = Input::except('_token');

		if($input['tags'] != '')
		{
			$tags = explode(",", $input['tags']);

			$added = array();

			if(count($tags) > 0)
			{
				foreach($tags as $tag_name)
				{
					if(!(new Tags)->exists(trim($tag_name)))
					{
						$tag = new Tags;
						$tag->name = trim($tag_name);
						$tag->slug = $tag->name;
						$tag->parent = NULL;
						$tag->type = $tag->getType();

						if(!$tag->save())
						{
							$messages['error'][] = 'Tag &quot;' . $tag->name . '&quot; could not be saved.';
						}
						else
						{
							$added[] = $tag->name;
							$messages['success'][] = 'Tag &quot;' . $tag->name . '&quot; has been added.';
						}
					}
					else
					{
						$messages['messages'][] = 'Tag &quot;' . $tag_name . '&quot; already exists.';
					}
				}

				if(count($added) > 0)
				{
					if(count($added) == 1)
					{
			        	/* record activity */
				        Activitylog::log([
				            'action' => 'CREATED',
				            'type' => get_class(new Tags),
				            'link_id' => $tag->id,
				            'description' => 'Created taxonomy term',
				            'notes' => Auth::user()->username . " created a new tag - &quot;$tag->name&quot;"
				        ]);
					}
					else
					{
				        /* record activity */
				        Activitylog::log([
				            'action' => 'CREATED',
				            'type' => get_class(new Tags),
				            'link_id' => null,
				            'description' => 'Created multiple taxonomy terms',
				            'notes' => Auth::user()->username . " created the following tags (" . implode(", ", $added) . ")"
				        ]);
					}

					return Redirect::to(App::make('backend_url').'/tags/')->with('messages', $messages);
				}
				else
				{
					return Redirect::to(App::make('backend_url').'/tags/')->with('error', 'There was a problem processing your tags.');
				}
			}
			else
			{
				//no tags
				return Redirect::to(App::make('backend_url').'/tags/')->with('error', 'There was a problem processing your tags.');
			}
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if(is_numeric($id) AND $id > 0)
		{
			$tag = Tags::whereNull('deleted_at')->where('id', '=', $id)->first();

			if(null !== $tag)
			{
				$this->data['tag'] = $tag;
				// $this->data['references'] = Tags::find($id)->take(10)->get();
				$this->data['references'] = DB::table('taxonomy_relationships')->where('term_id', '=', $id)->get();

				/* Build breadcrumb */
				$breadcrumbs = array(
					0 => array(
			            'classes' => '',
			            'icon' => '',
			            'title' => 'Tags',
			            'url' => '/' . General::backend_url() . '/tags/'
					),
					1 => array(
			            'classes' => '',
			            'icon' => '',
			            'title' => $this->data['tag']->name,
			            'url' => ''
					)
				);

				foreach($breadcrumbs as $crumb)
				{
					$this->addToBreadcrumb($crumb);
				}

				return View::make('HummingbirdBase::cms/taxonomy-tags-edit', $this->data);
			}
			else
			{
				return Redirect::to(App::make('backend_url').'/tags/')->with('error', 'Tag could not be found. Please try again.');
			}
		}

		return Redirect::to(App::make('backend_url').'/tags/')->with('error', 'Please select a valid tag to edit.');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		dd("edit");
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(is_numeric($id) AND $id > 0)
		{
			$input = Input::except('_token');
			$tag = Tags::find($id);

			if(null !== $tag)
			{
				$tag->fill($input);
				$tag->save();

				if($tag->save())
				{
		        	/* record activity */
			        Activitylog::log([
			            'action' => 'UPDATED',
			            'type' => get_class($tag),
			            'link_id' => $tag->id,
			            'description' => "Tag has been updated",
			            'notes' => Auth::user()->username . " has updated the tag &quot;$tag->name&quot;"
			        ]);

					return Redirect::to(App::make('backend_url').'/tags/')->with('success', 'Tag &quot;' . $tag->name . '&quot; has been updated.');
				}
				else
				{
					return Redirect::to(App::make('backend_url').'/tags/'.$tag->id)->with('error', 'Tag could not be updated.');
				}
			}
			else
			{
				return Redirect::to(App::make('backend_url').'/tags/')->with('errors', 'Tag could not be found');
			}
		}

		return Redirect::to(App::make('backend_url').'/tags/'.$tag->id)->with('errors', 'There was a problem updating this tag.');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(is_numeric($id) AND $id > 0)
		{
			$tag = Tags::find($id);
			$tag_name = $tag->name;

			if(null !== $tag)
			{
				if($tag->delete())
				{
					// successfully deleted tag - add activity
		        	/* record activity */
			        Activitylog::log([
			            'action' => 'DELETED',
			            'type' => get_class(new Tags),
			            'link_id' => $tag->id,
			            'description' => "Tag has been deleted",
			            'notes' => Auth::user()->username . " deleted the tag &quot;$tag->name&quot;"
			        ]);

			        return Redirect::to(App::make('backend_url').'/tags/')->with('success', 'Tag has been deleted.');
				}
				else
				{
			        return Redirect::to(App::make('backend_url').'/tags/')->with('error', 'Tag could not be deleted.');
				}
			}
			else
			{
				return Redirect::to(App::make('backend_url').'/tags/')->with('error', 'Tag could not be found. Please try again.');
			}
		}

		return Redirect::to(App::make('backend_url').'/tags/')->with('error', 'Please select a tag to delete.');
	}

	public function import()
	{
		
	}


	/**
	 * Show the soft deleted items
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showDeleted()
	{
		$this->data['tags'] = Tags::whereNotNull('deleted_at')->type()->orderBy('name', 'ASC')->get();
		$this->data['alphabetical'] = $this->generateAlphabetical();

		/* Build breadcrumb */
		$breadcrumbs = array(
			0 => array(
	            'classes' => '',
	            'icon' => '',
	            'title' => 'Tags',
	            'url' => '/' . General::backend_url() . '/tags/'
			),
			1 => array(
	            'classes' => '',
	            'icon' => '',
	            'title' => 'Trash',
	            'url' => ''
			)
		);

		foreach($breadcrumbs as $crumb)
		{
			$this->addToBreadcrumb($crumb);
		}

        return View::make('HummingbirdBase::cms/taxonomy-tags-deleted', $this->data);
	}

	/**
	 * Physically remove the item
	 * POST will send "all" OR "single"
	 *
	 * @param  int  $id (default = null)
	 * @return Response
	 */
	public function purge($id = null)
	{	
		if($id == 'all' OR (is_numeric($id) AND $id > 0))
		{
			$deleted = array();
			$delete_array = array(); //delete from taxonomy table and all references

			switch($id)
			{
				case 'all':
					$tags = Tags::whereNotNull('deleted_at')->get();

					if(count($tags) > 0)
					{
						foreach($tags as $tag)
						{
							$deleted[] = $tag;
							$delete_array[] = $tag->id;
						}
					}
					else
					{
						return Redirect::to(App::make('backend_url').'/tags/deleted/')->with('error', 'No tags could be found. Please try again.');
					}
					break;
				default:
					$tag = Tags::whereNotNull('deleted_at')->where('id', '=', $id)->first();

					if(null !== $tag)
					{
						$deleted[] = $tag;
						$delete_array[] = $tag->id;
					}
					else
					{
						return Redirect::to(App::make('backend_url').'/tags/deleted/')->with('error', 'That tag could not be found. Please try again.');
					}
					break;
			}

			if(count($delete_array) > 0)
			{
				DB::table('taxonomy')->whereIn('id', $delete_array)->delete(); //remove taxonomy
				DB::table('taxonomy_relationships')->whereIn('term_id', $delete_array)->delete(); //remove relationships

				foreach($deleted as $tag)
				{
		        	/* record activity */
			        Activitylog::log([
			            'action' => 'PURGED',
			            'type' => get_class(new Tags),
			            'link_id' => $tag->id,
			            'description' => 'Purged tag',
			            'notes' => Auth::user()->username . " permanently deleted &quot;$tag->name&quot;"
			        ]);
				}
			}
			else
			{
				return Redirect::to(App::make('backend_url').'/tags/deleted/')->with('error', 'No tags could be removed. Please try again.');
			}
		}
		else
		{
			return Redirect::to(App::make('backend_url').'/tags/deleted/')->with('error', 'Please select a tag to delete.');
		}

		return Redirect::to(App::make('backend_url').'/tags/deleted/');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function reinstate($id)
	{
		if($id == 'all' OR (is_numeric($id) AND $id > 0))
		{
			switch($id)
			{
				case 'all':
					$tags = Tags::whereNotNull('deleted_at')->get();

					if(count($tags) > 0)
					{
						foreach($tags as $tag)
						{
							$tag->deleted_at = NULL;
							$tag->save();

				        	/* record activity */
					        Activitylog::log([
					            'action' => 'UPDATED',
					            'type' => get_class(new Tags),
					            'link_id' => $tag->id,
					            'description' => 'Restored tag',
					            'notes' => Auth::user()->username . " restored the tag &quot;$tag->name&quot;"
					        ]);
						}
					}
					else
					{
						dd("NO TAGS FOUND");
					}
					break;
				default:
					$tag = Tags::whereNotNull('deleted_at')->where('id', '=', $id)->first();

					if(null !== $tag)
					{
						$tag->deleted_at = NULL;
						$tag->save();

			        	/* record activity */
				        Activitylog::log([
				            'action' => 'UPDATED',
				            'type' => get_class(new Tags),
				            'link_id' => $tag->id,
				            'description' => 'Restored tag',
				            'notes' => Auth::user()->username . " restored the tag &quot;$tag->name&quot;"
				        ]);
					}
					else
					{
						dd("NO TAG $id FOUND");
					}
					break;
			}
		}
		else
		{
			dd("NO ID GIVEN");
		}

		$this->data['tags'] = Tags::whereNotNull('deleted_at')->orderBy('name', 'ASC')->get();
		$this->data['alphabetical'] = $this->generateAlphabetical();

		/* Build breadcrumb */
		$breadcrumbs = array(
			0 => array(
	            'classes' => '',
	            'icon' => '',
	            'title' => 'Tags',
	            'url' => '/' . General::backend_url() . '/tags/'
			),
			1 => array(
	            'classes' => '',
	            'icon' => '',
	            'title' => 'Trash',
	            'url' => ''
			)
		);

		foreach($breadcrumbs as $crumb)
		{
			$this->addToBreadcrumb($crumb);
		}

        return View::make('HummingbirdBase::cms/taxonomy-tags-deleted', $this->data);
	}

	public function checkDeletedTags($filename = '', $delimiter = ",")
	{
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;
     
        $header = NULL;
        $data = array();

        ini_set('auto_detect_line_endings', true);
		if (($handle = fopen($filename, "r")) !== FALSE) {
		    while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {

                if(!$header)
                    $header = $row;
                else
                    $data[] = $row[0];
            }

            fclose($handle);
		}

        if(count($data) > 0)
        {

        	DB::table('taxonomy')
        		->whereNotIn('name', $data)
        		->where('type', '=', 'tag')
        		->delete();
        }

        // File::delete($filename);
    }
}
