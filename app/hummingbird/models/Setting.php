<?php

class Setting extends LaravelBook\Ardent\Ardent {

    public $table = 'settings';
    public $timestamps = false;
    public $cms_url = '';
    protected $fillable = array();
    
    protected $guarded = array();

    public function getValueAttribute($value)
    {
        if(is_array($value)) return;
        
        if($value != '') return unserialize($value);

        return;
    }

    public static function getWhere($where, $condition, $value)
    {
        $setting = Setting::where($where, $condition, $value)->first();
        if ($setting) {
            return $setting->value;
        }
        
        return false;
    }
    
    // Override method of Illuminate\Database\Eloquent\Model so we can unserialize the results
    public static function all($columns = array('*'))
    {
        $data = array();
        $all = parent::all($columns);
        
        foreach($all as $result)
        {
            $data[$result->key] = $result->value;
        }
        
        return $data;
    }
    
    public function updateSettings($key, $value) {
        $setting = Setting::where('key', '=', $key)->first();
        $setting->value = serialize($value);
        return $setting->save();
    }
    
    public function newSetting($key, $value)
    {
        $setting = new Setting;
        $setting->key = $key;
        $setting->value = serialize($value);
        return $setting->save();
    }
    
}
