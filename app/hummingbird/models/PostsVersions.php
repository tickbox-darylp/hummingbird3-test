    <?php

class PostVersion extends LaravelBook\Ardent\Ardent {

    public $table = 'news_versions';
    public $cms_url = 'blog';
    
    protected $fillable = array('hash', 'post_id', 'title', 'summary', 'content', 'permalink', 'post_date', 'post_end_date', 'feature', 'status', 'author', 'author_email', 'featured_image', 'search_image', 'showcase');

    protected $permission_type = 'blog';

    public static $rules = array(
        'title' => 'required'
    );

    public function post()
    {
        return $this->belongsTo('Post');
    }

    public function setHashAttribute($value)
    {
        $this->attributes['hash'] = Hash::make($value);
    }

    public static function get_statuses()
    {
        return array('draft' => 'Draft', 'public' => 'Public');
    }
    
    public function getPermissionType()
    {
        return $this->permission_type;
    }

    public function setPostDateAttribute($value)
    {
        $this->attributes['post_date'] = date("Y-m-d H:i:s", strtotime(str_replace("/", "-", $value)));
    }

    public function setPostEndDateAttribute($value)
    {
        if($value == '') $this->attributes['post_end_date'] = '0000-00-00 00:00:00';
        $this->attributes['post_end_date'] = date("Y-m-d H:i:s", strtotime(str_replace("/", "-", $value)));
    }

    public function getPostDateAttribute($value)
    {
        if($value == '0000-00-00 00:00:00') return time();
        return strtotime($value);
    }

    public function getPostEndDateAttribute($value)
    {
        if($value == '0000-00-00 00:00:00') return '';
        return strtotime($value);
    }

    public static function boot()
    {
        parent::boot();
    }
    
}
