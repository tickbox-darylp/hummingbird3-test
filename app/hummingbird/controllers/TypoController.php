<?php

class TypoController extends CmsbaseController {
    
    public function getIndex()
    {
        $this->data['tag'] = 'Typography for Hummingbird 3.0';
        return View::make('HummingbirdBase::cms/typography', $this->data);
    }
}
