@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/datetimepicker/datetimepicker.css" />
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />
    <link href="/app/hummingbird/views/gridster/jquery.gridster.min.css" rel="stylesheet" />

    <style>
        .nav-tabs {
            padding-right: 0;
        }

        .nav-tabs > li:last-child {
            margin-top: 13px;
        }

        .nav-tabs > li > a[href="#add-slides"] {
            padding: 5px 10px 5px 5px;
            margin-right:0;
        }

        .nav-tabs > li.active > a[href="#add-slides"], .nav-tabs > li > a[href="#add-slides"]:hover {
            color: #fff;
            background-color: #449d44;
            border-color: #398439;
        }
    </style>
@stop

@section('breadcrumbs')
    @if (count($breadcrumbs) > 0)
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <?php $i = 0;?>
                    @foreach ($breadcrumbs as $breadcrumb)
                        <?php $i++;?>
                        <li>
                        @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                            <a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                        @else
                            @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                        @endif
                        </li>
                    @endforeach
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
    @endif
@stop

@section('content')
    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {{ Session::get('error') }}
                </div>
            </div>
        </div>  
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel" style="background-color:white;padding:20px;">
                {{ Form::open(array('url' => App::make('backend_url').'/lc-sliders/edit/' . $slider->id, 'class' => 'form-horizontal')) }}   
                    <div class="form-group hidden-xs">
                        <div class="col-sm-12">
                            <button type="submit" class="update-all btn btn-default btn-primary pull-right" style="display:none;">Save slider</button>
                        </div>
                    </div>

                    <!-- Nav tabs -->
                    <ul id="tabs" class="nav nav-tabs" role="tablist">
                        <li class=""><a href="#details" role="tab" data-toggle="tab">Slider Details</a></li>
                        @if(count($slider->slider_data) > 0)
                            @foreach($slider->slider_data as $key => $slide)
                                <li><a class="show-slide" href="#tab-slide-{{$key}}" role="tab" data-toggle="tab">{{$slide['title']}}</a></li>  
                            @endforeach
                        @endif
                        <li class="pull-right active"><a href="#add-slides" role="tab" data-toggle="tab" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add slide</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content" style="margin:15px 0;">
                        <div class="tab-pane" id="details">
                            <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">Title:</label>
                                <div class="col-sm-10">
                                    <input name="title" id="title" type="text" class="form-control" placeholder="Slider Title" value="{{$slider->title}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-sm-2 control-label">Description:</label>
                                <div class="col-sm-10">
                                    {{ Form::textarea('description', $slider->description, array('class' => 'form-control textareas')) }}
                                </div>
                            </div>
                        </div>

                        @if(count($slider->slider_data) > 0)
                            @foreach($slider->slider_data as $key => $slide)
                                <?php $details = $slider->calculate($slide['template']);?>
                                <div class="tab-pane slider-pane" id="tab-slide-{{$key}}">
                                    <div class="form-group">
                                        <label for="slide_title" class="col-sm-2 control-label">Slide title:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="slide_titles[{{$key}}]" id="slide_title" type="text" class="form-control" placeholder="Slider Title" value="{{$slide['title']}}" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="slide_title" class="col-sm-2 control-label">Enable/Disable:</label>
                                        <div class="col-sm-10">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="slide_enabled[{{$key}}]" value="1" @if((isset($slide['enabled']) AND $slide['enabled'] == 1) OR !isset($slide['enabled'])) checked="checked" @endif> Enable
                                                </label>
                                            </div>
                                            <span class="help-block">To turn this slide off, simply uncheck the box</span>
                                        </div>
                                    </div>

                                    <div class="demo col-md-12">
                                        <div id="slide-{{$key}}" class="item-gridster gridster">
                                            <ul>
                                                <?php 
                                                    $counter = 0;
                                                    $col = 1;
                                                    $row = 1;
                                                
                                                    $has_children = array();
                                                ?>

                                                @for($i = 1; $i <= $details['total-modules']; $i++)
                                                    <?php 
                                                    /*
                                                        1. Calculate if the region has slides
                                                        2. Calculate if the region has children
                                                            * If has children:
                                                                - Iterate over the child and echo out
                                                                    - Needs: row, column and size

                                                            * Else:
                                                                - Just output
                                                    */

                                                    $slider_regions = (isset($slide['regions']['region-' . $i])) ? $slide['regions']['region-' . $i]:false; //regions

                                                    /* Set children for slide and region */
                                                    if($slider_regions !== false AND isset($slider_regions['regions']))
                                                    {
                                                        $has_children['region-' . $i] = $slider_regions['regions'];
                                                    }

                                                    $parent_id = ArrayHelper::get_array_key_by_value($has_children, 'region-' . $i);
                                                    $is_child = (!$parent_id) ? FALSE:TRUE;
                                                    $counter++;

                                                    $col = $slider->calculateNextColumn($col, $counter, $slide['template']);
                                                    $row = $slider->calculateNextRow($row, $counter, $slide['template']);
                                                    $sizes = $slider->calculateSize($row, $col, $counter, $slide['template']);

                                                    $col_x = $slider->calculateNextColumn($col, $counter, $slide['template']);
                                                    $row_y = $slider->calculateNextRow($row, $counter, $slide['template']);

                                                    ?>

                                                    @if(!$is_child)
                                                        <li id="region-{{$counter}}" class="item @if(isset($slider_regions['type'])) active @endif" data-col="{{$col}}" data-row="{{$row}}" data-sizex="{{$sizes['x']}}" data-sizey="{{$sizes['y']}}" data-content-type="{{$slider_regions['type']}}">
                                                            @if(isset($slider_regions['type']))
                                                                <div style="top:5px;right:10px;" class="settings absolute"><a style="color:#004756;" class="setting" href="#"><i class="fa fa-cog"></i></a></div>
                                                            @endif
                                                            <h3>Region-{{$i}}</h3>
                                                        </li>
                                                    @else
                                                        <li id="region-{{$counter}}" class="item @if($is_child) selected-as-another @endif" @if($is_child) data-parent="{{$parent_id}}" @endif data-col="{{$col}}" data-row="{{$row}}" data-sizex="{{$sizes['x']}}" data-sizey="{{$sizes['y']}}">
                                                            <div style="top:5px;right:10px;" class="settings absolute"><a style="color:#004756;" class="setting" href="#"><i class="fa fa-cog"></i></a></div>
                                                            <h3>Region-{{$i}} @if($is_child) (belongs to <i>Region-{{str_replace("region-", "", $parent_id)}}</i>) @endif</h3>
                                                        </li>
                                                    @endif
                                                @endfor                                                 
                                            </ul>
                                        </div>
                                    </div>

                                    <input type="hidden" id="tab-slide-{{$key}}-data" class="hidden-slide-data" name="slides[]" value="{{htmlspecialchars(json_encode($slide))}}" />
                                
                                    <div class="form-group">
                                        <div class="col-sm-10 pull-right">
                                            <button type="button" class="remove-slide btn btn-danger pull-right"><i class="fa fa-trash"></i> Remove slide</button>
                                        </div>
                                    </div>
                                </div> 
                            @endforeach
                        @endif

                        <div class="tab-pane active" id="add-slides">
                            <div class="form-group hide">
                                <div class="col-sm-2 control-label">Options:</div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="rows" class="col-sm-2 control-label">Rows:</label>
                                        <div class="col-sm-10">
                                            <select name="rows" id="rows" class="form-control">
                                                @for($i = 1; $i <= 2; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="cols" class="col-sm-2 control-label">Cols:</label>
                                        <div class="col-sm-10">
                                            <select name="cols" id="cols" class="form-control">
                                                @for($i = 1; $i <= 12; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button id="gridster-add" class="btn btn-default"><i class="fa fa-plus"></i> Add slide</button>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="slide_title" class="col-sm-2 control-label">Slide title:</label>
                                <div class="col-sm-10">
                                    <input name="add_slide_title" id="slide_title" type="text" class="form-control" placeholder="Slider Title">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="template" class="col-sm-2 control-label">Options:</label>
                                <div class="col-sm-8">
                                    <select name="template" id="template" class="form-control">
                                        <option value="">---</option>
                                        <option value="full">Full width</option>
                                        <optgroup label="Homepage specific">
                                            <option value="1">8 small modules</option>
                                            <option value="2">1 large, 4 small modules</option>
                                            <option value="3">1 large, 2 small modules</option>
                                        </optgroup>
                                        <optgroup label="Internal specific">
                                            <option value="4">6 small modules</option>
                                            <option value="5">1 large, 2 small modules</option>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="col-sm-2 hide">
                                    <button id="gridster-add-template" class="btn btn-default"><i class="fa fa-plus"></i> Add slide template</button>
                                    <button id="gridster-remove-template" class="btn btn-warning"><i class="fa fa-plus"></i> Remove template</button>
                                </div>
                            </div>

                            <div class="demo col-md-12">
                                <div class="gridster" id="main">
                                    <div class="alert alert-warning alert-box text-center">Please add an option to the slider</div>
                                    <ul></ul>
                                </div>
                            </div>

                            <div class="form-group hidden-xs clearfix">
                                <div class="col-sm-12">
                                    <button id="save-slide" type="button" class="btn btn-default pull-right" style="display:none;">Create slide</button>
                                </div>
                            </div>

                            <input type="hidden" name="slide_data" id="slide_data" value="" />
                        </div>
                    </div>
                {{Form::close()}}
            </section>  
        </div>
    </div>

    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal settings &raquo; <span class="span-modal-title">[modal-title]</span></h4>
                </div>
                <div class="modal-body form-horizontal"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <style>
        /* =============================================================================
           NORMALIZATION
        ========================================================================== */

        ul, ol {
            list-style: none;
        }

        /* Gridster styles */
        .demo {
            margin: 3em 0;
            padding: 2em 1.5em;
            background: #004756;
        }

        .gridster {
            width: 100%;
            margin: 0 auto;
        }

        .gridster > ul 
        {
            width:100% !important;
        }

        .gridster > ul li {padding:10px;}
        .gridster > ul li h3 {margin-top:0;font-size:12px;}

        .gridster .gs-w 
        {
            background: #FFF;
            cursor: pointer;
            -webkit-box-shadow: 0 0 5px rgba(0,0,0,0.3);
            box-shadow: 0 0 5px rgba(0,0,0,0.3);
        }

        .gridster:hover .gs-w:not(:hover) {
            background: rgba(255,255,255,.25)!important;
            transition: background-color 0.5s ease;
        }

        .gridster .gs-w.active:not(.selected-as-another)
        {
            background: rgba(255,255,255,.5)!important;
            cursor: pointer;
            -webkit-box-shadow: 0 0 5px rgba(0,0,0,0.3);
            box-shadow: 0 0 5px rgba(0,0,0,0.3);
        }

        .gridster .gs-w:before
        {
            position: absolute;
            font-family: FontAwesome;
            font-size: 32px;
            color: white;
            top: 50%;
            left: 50%;
            opacity: 0.50;
            margin-left: -14px;
            margin-top: -24px;
        }

        .gridster .gs-w.selected-as-another:before {font-size:24px;margin-left:-8px;margin-top:-18px;}

        .gridster .gs-w:not(.selected-as-another)[data-content-type="custom"]:before { content: "\f14b"; }
        .gridster .gs-w:not(.selected-as-another)[data-content-type="events"]:before { content: "\f073"; }
        .gridster .gs-w.selected-as-another:before { content: "\f023"; }

        .gridster .gs-w.selected-as-another:hover:before { color:#004756; }
        .gridster .gs-w.selected-as-another:not(:hover):before { color: #004756; }
        .gridster:hover .gs-w.selected-as-another:not(:hover):before { color: white; }

        .gridster .player {
            -webkit-box-shadow: 3px 3px 5px rgba(0,0,0,0.3);
            box-shadow: 3px 3px 5px rgba(0,0,0,0.3);
        }

        .gridster .preview-holder {
            border: none!important;
            border-radius: 0!important;
            background: rgba(255,255,255,.2)!important;
        }

        .gridster .gs-w .content-holder {}
        .gridster .gs-w .content-holder .content-title {}
        .gridster .gs-w .content-holder .content-summary {}
        .gridster .gs-w .content-holder .content-image {float:right;width:50px;}
        .gridster .gs-w .content-holder .content-image img {width:50px;}

        .gridster .gs-w.selected-as-another {
            background: #D6F9FF !important;
            cursor: pointer;
            -webkit-box-shadow: 0 0 5px rgba(0,0,0,0.3);
            box-shadow: 0 0 5px rgba(0,0,0,0.3);
        }
        
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
    </style>
@stop

@section('scripts')
    <script src="/themes/hummingbird/default/lib/moment/moment.js"></script>
    <script src="/themes/hummingbird/default/lib/datetimepicker/datetimepicker.js"></script>
    <script src="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>
    <script src="/app/hummingbird/views/gridster/jquery.gridster.min.js"></script>
    <script src="/themes/hummingbird/default/lib/media-library/media-library.js"></script>

    <script>
        var gridster;
        var clicked_item;
        var modal;

        function init_gridster()
        {
            var modal_width = $("#main.gridster").parent().outerWidth() - 42;
            modal_width = (modal_width / 12) - (10 * 2);

            //DOM Ready
            gridster = $("#main.gridster > ul").gridster({
                namespace: "#main",
                widget_margins: [10, 10],
                widget_base_dimensions: [modal_width, modal_width+30], // (1015 / 12) - (10 * 2) = 60 
                max_cols: [12],
                max_rows: [2]
            }).data('gridster').disable();
        }

        function resetFields(el)
        {
            el.find('option:gt(0)').remove();
        }

        function resetFormFields()
        {
            $("#add-slides #slide_title, #add-slides #template, #add-slides #slide_data").val('');
            gridster.remove_all_widgets();
            
            $("#gridster-add-template").parent().addClass('hide');

            if($("#main.gridster .alert").length <= 0)
            {
                $("#main.gridster").append('<div class="alert alert-warning alert-box text-center">Please add an option to the slider</div>').find('ul').css('height', ''); 
            }
        }

        function init_date_ranges(start_date, finish_date)
        {
            $('.modal .datetimepicker').each(function()
            {
                var _minDate = false;

                if($(this).hasClass("to") && $('.modal .datetimepicker.from').val() != '')
                {
                    _minDate = $('.modal .datetimepicker.from').val();
                }

                $(this).datetimepicker({
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    format: "DD/MM/YYYY",
                    showClose: true,
                    minDate: _minDate
                });
            });

            $(".modal .datetimepicker.from").on("dp.change", function (e)
            {
                var el_to_date = $(this).closest('.date-select').find('.datetimepicker.to');
                var _from_date = $(this).data("DateTimePicker").date();
                var _to_date = el_to_date.data("DateTimePicker").date();

                // Set the minimum date for the "to" datepicker 
                el_to_date.data("DateTimePicker").minDate(e.date);

                /* Set the _to_date to be the from date if set in the past */
                if(_from_date > _to_date)
                {
                    el_to_date.data("DateTimePicker").date(_from_date);
                }
            });

            $(".modal .datetimepicker.to").on("dp.show", function (e)
            {
                var el_to_date = $(this).closest('.date-select').find('.datetimepicker.from');
                var _to_date = $(this).data("DateTimePicker").date();
                var _from_date = el_to_date.data("DateTimePicker").date();

                // Set the minimum date for the "to" datepicker 
                $(this).data("DateTimePicker").minDate(_from_date);

                /* Set the _to_date to be the from date if set in the past */
                if(_from_date > _to_date)
                {
                    $(this).data("DateTimePicker").date(_from_date);
                }
            });
        }

        function init_events(el)
        {
            el.selectpicker();
        }

        function processEventsRegions(data)
        {
            var items = []; //root array
            items['all'] = []; //all regions used

            if(data != '')
            {
                data = $.parseJSON(data);

                $.each(data.regions, function(index, new_data)
                {
                    if(new_data['regions'])
                    {
                        items[index] = new_data['regions'];
                        items['all'] = $.merge(items['all'], new_data['regions']);
                        // items = $.merge(items, new_data['regions']);
                    }
                });
            }

            return items;
        }

        function setParentRegion(modal_el)
        {
            return modal_el.find('.regions:checked:first').val(); //the regions we are applying this too.
        }

        $(document).ready(function()
        { 
            $(document).on('shown.bs.tab', 'a[href="#add-slides"]', function (e)
            {
                if(typeof $(".btn.update-all").data('action') === 'undefined') $(".btn.update-all").slideUp();

                init_gridster();
            });

            $(document).on('shown.bs.tab', '#tabs li a:not([href="#add-slides"])', function (e)
            {
                if(typeof $(".btn.update-all").data('action') === 'undefined') $(".btn.update-all").slideDown();

                if($(this).hasClass('show-slide'))
                {
                    var id = $(this).attr('href');
                    id = id.replace("tab-", ""); //id of slider
                    var el = $(id + ".item-gridster > ul");

                    var modal_width = el.parent().parent().outerWidth() - 42;
                    modal_width = (modal_width / 12) - (10 * 2);

                    //DOM Ready
                    el.gridster({
                        namespace: id,
                        widget_margins: [10, 10],
                        widget_base_dimensions: [modal_width, modal_width+30], // (1015 / 12) - (10 * 2) = 60 
                        max_cols: [12],
                        max_rows: [2]
                    }).data('gridster').disable();
                }
            })

            init_gridster();

            if($("#gridster-add-template").length > 0)
            {
                $("#gridster-add-template").click(function(e)
                {
                    e.preventDefault();

                    var template = $("#template");

                    gridster.remove_all_widgets();
                    resetFields($(".modal #fields"));

                    if(template.val() == '')
                    {
                        $("#main.gridster .gridster-info").addClass('hide');
                    }
                    else
                    {
                        if($("#main.gridster .gridster-info").hasClass('hide'))
                        {
                            $("#main.gridster .gridster-info").removeClass('hide');
                        }
                    }

                    if($("#template").val() == 'full')
                    {
                        gridster.add_widget.apply(gridster, ['<li id="region-1" class="item"><h3>Full width slider</h3>', 12, 2]);
                    }
                    else
                    {
                        switch($("#template").val())
                        {
                            case '1':
                                for(var i = 0; i < 8; i++)
                                {
                                    gridster.add_widget.apply(gridster, ['<li id="region-'+(i+1)+'" class="item"><h3>1x1 module</h3>', 3, 1]);
                                }
                                break;
                            case '2':
                                gridster.add_widget.apply(gridster, ['<li id="region-1" class="item"><h3>2x2 Module</h3>', 6, 2]);
                                for(var i = 0; i < 4; i++)
                                {
                                    gridster.add_widget.apply(gridster, ['<li id="region-'+(i+2)+'" class="item"><h3>1x1 module</h3>', 3, 1]);
                                }
                                break;
                            case '3':
                                gridster.add_widget.apply(gridster, ['<li id="region-1" class="item"><h3>4x2 Module</h3>', 10, 2]);
                                for(var i = 0; i < 2; i++)
                                {
                                    gridster.add_widget.apply(gridster, ['<li id="region-'+(i+2)+'" class="item"><h3>1x1 module</h3>', 2, 1]);
                                }
                                break;
                            case '4':
                                for(var i = 1; i <= 6; i++)
                                {
                                    gridster.add_widget.apply(gridster, ['<li id="region-' + i + '" class="item"><h3>4x1 module</h3>', 4, 1]);
                                }
                                break;
                            case '5':
                                gridster.add_widget.apply(gridster, ['<li id="region-1" class="item"><h3>4x2 Module</h3>', 8, 2]);
                                for(var i = 2; i <= 3; i++)
                                {
                                    gridster.add_widget.apply(gridster, ['<li id="region-' + i + '" class="item"><h3>1x1 module</h3>', 4, 1]);
                                }
                                break;
                        }
                    }

                    if($("#main.gridster").find('.alert').length > 0 && $("#main.gridster > ul li").length > 0)
                    {
                        $("#main.gridster").find('.alert').remove();
                    }

                    if($("#save-slide").length > 0)
                    {
                        $("#save-slide").slideDown();
                    }
                });
            }

            if($("#template").length > 0)
            {
                $("#template").change(function()
                {
                    if($(this).val() == '')
                    {
                        $("#gridster-add-template").parent().addClass("hide");
                    }
                    else
                    {
                        if($("#gridster-add-template").parent().hasClass("hide"))
                        {
                            $("#gridster-add-template").parent().removeClass("hide");
                        }
                    }
                });
            }

            $(document).on('click', ".item.selected-as-another", function(e)
            {
                e.preventDefault();
                if($(this).data('parent') != '')
                {   
                    $(this).closest('.tab-pane').find("#" + $(this).data('parent')).trigger('click');
                }
            });

            $(document).on("click", ".item .settings .setting", function(e)
            {
                e.preventDefault();

                //get modal for showing text + number of events to show + merge fields for showing events
                modal = $(".modal");
                clicked_item = $(this).closest('.item');

                var data = {};
                data.get = 'list-mode';
                data.region = clicked_item.attr('id');
                data.json = (clicked_item.closest('.tab-pane').attr('id') == 'add-slides') ? clicked_item.closest('.tab-pane').find('#slide_data').val():clicked_item.closest('.tab-pane').find('.hidden-slide-data').val();

                /* Load modal and defaults - http://www.smashingmagazine.com/2010/02/some-things-you-should-know-about-ajax/ */
                $.ajax({
                    cache: false,
                    url: '/hummingbird/lc-sliders/modal-data/',
                    timeout:5000,
                    data: data,
                    success: function(data)
                    {
                        if(data.charAt(0) == '*')
                        {
                            data = data.substring(1); //remove '*' from response
                            data = $.parseJSON(data);

                            modal.find('h4.modal-title').html(data.title);
                            modal.find('.modal-body').html('<div class="alert alert-box alert-danger text-center">There was a problem loading the settings for this region</div>');
                        }
                        else
                        {  
                            modal.html(data);
                        
                            var slide_title = clicked_item.closest('.tab-pane').find("#slide_title").val();
                            slide_title = (slide_title == '') ? clicked_item.attr('id'):slide_title + ' [' + clicked_item.attr('id') + ']';

                            modal.find(".span-modal-title").text(slide_title + ' settings');

                            modal.data('id', clicked_item.attr('id'));
                            if(clicked_item.closest('.tab-pane').hasClass('slider-pane'))
                            {
                                modal.data('slide-id', clicked_item.closest('.tab-pane').attr('id'));
                            }
                        }

                        modal.modal('show');
                    },
                    beforeSend: function(data)
                    {
                        modal.find('h4.modal-title').html('Processing');
                        modal.find('.modal-footer').remove();
                        modal.find('.modal-body').html('<p>Loading...</p>');
                    },
                    error: function(req,error){
                        if(error === 'error') { error = req.statusText; }
                        var errormsg = 'There was a communication error: '+error;
                        modal.find('.modal-body').html(errormsg);
                    },
                }, this);

                return false; //do not click the item!
            });

            $(document).on('click', '.modal .modal-footer .btn.save-region-settings', function(e)
            {
                e.preventDefault();

                /* We only save the data for events */

                console.log('saving the region settings');

                /* set local vars */
                var modal_el = $('.modal');
                    console.log(modal_el);
                var item_id = modal_el.data('id');
                    console.log(item_id);
                var is_current_tab = (typeof modal_el.data('slide-id') != 'undefined') ? true:false;
                    console.log(is_current_tab);
                var listing = (modal.find("#display_by").prop('checked')) ? true:false;
                    console.log(listing);

                var item = (!is_current_tab) ? $("#main.gridster > ul li#" + item_id):$('#' + modal_el.data('slide-id') + ' .gridster > ul li#' + item_id);
                    console.log(item);
                var item_data = (!is_current_tab) ? {}:$.parseJSON($('#' + modal_el.data('slide-id') + '-data').val());
                    console.log(item_data);

                if(typeof item_data['appearance'] === 'undefined')
                {
                    console.log("SETTING REGION-DATA");
                    item_data['appearance'] = {};
                }

                if(listing)
                {
                    console.log("LISTING ENABLED");
                    item_data['appearance'][item_id] = {};
                    item_data['appearance'][item_id]['title'] = modal_el.find("#display_title").val();
                    item_data['appearance'][item_id]['summary'] = modal_el.find("#display_summary").val();
                    item_data['appearance'][item_id]['limit'] = modal_el.find("#display_limit").val();
                }
                else
                {
                    if(typeof item_data['appearance'][item_id] !== 'undefined')
                    {
                        console.log("LISTING DISABLED");
                        /* Remove item */
                        delete item_data['appearance'][item_id];
                    }
                }

                if(!is_current_tab)
                {
                    $("#slide_data").val(JSON.stringify(item_data));
                }
                else
                {
                    $('#' + modal_el.data('slide-id') + '-data').val(JSON.stringify(item_data));
                }

                $(".modal").modal('hide');
            });

            $(document).on("click", ".item:not(.selected-as-another)", function()
            {
                modal = $(".modal");
                modal.removeData('slide-id'); //remove this ID
                clicked_item = $(this);

                var data = {};
                data.get = 'by-content';

                if(clicked_item.hasClass('active'))
                {
                    data.get = 'content-type'; //we have activated the details
                    data.content_type = clicked_item.data('content-type');
                }

                data.region = clicked_item.attr('id');
                data.json = (clicked_item.closest('.tab-pane').attr('id') == 'add-slides') ? clicked_item.closest('.tab-pane').find('#slide_data').val():clicked_item.closest('.tab-pane').find('.hidden-slide-data').val();

                /* Load modal and defaults - http://www.smashingmagazine.com/2010/02/some-things-you-should-know-about-ajax/ */
                $.ajax({
                    cache: false,
                    url: '/hummingbird/lc-sliders/modal-data/',
                    timeout:5000,
                    data: data,
                    success: function(data)
                    {
                        if(data.charAt(0) == '*')
                        {
                            data = data.substring(1); //remove '*' from response
                            data = $.parseJSON(data);

                            modal.find('h4.modal-title').html(data.title);
                            modal.find('.modal-body').html('<div class="alert alert-box alert-danger text-center">' + data.message + '</div>');
                        }
                        else
                        {  
                            modal.html(data);

                            //deactivate toggle content
                            if(clicked_item.hasClass('active'))
                            {
                                modal.find("#content_type").attr('disabled', true);
                            }

                            //adding settings to this option
                            var num_areas = clicked_item.closest('.tab-pane').find(".gridster > ul li").length;
                            var slide_title = clicked_item.closest('.tab-pane').find("#slide_title").val();
                            slide_title = (slide_title == '') ? clicked_item.attr('id'):slide_title;

                            modal.find(".span-modal-title").text(slide_title);
                            modal.data('id', clicked_item.attr('id'));

                            if(clicked_item.closest('.tab-pane').hasClass('slider-pane'))
                            {
                                modal.data('slide-id', clicked_item.closest('.tab-pane').attr('id'));
                            }

                            /* Set up number of fields */
                            resetFields($(".modal #fields"));

                            var fields_select = modal.find("#fields");
                            var event_groups = modal.find('.modal-data .region-checkboxes');
                            var prepended = false;
                            var events_regions = (clicked_item.closest('.tab-pane').attr('id') == 'add-slides') ? clicked_item.closest('.tab-pane').find('#slide_data').val():clicked_item.closest('.tab-pane').find('.hidden-slide-data').val();
                            events_regions = processEventsRegions(events_regions);

                            clicked_item.closest('.tab-pane').find(".gridster > ul li").each(function()
                            {
                                var available = ($(this).hasClass('active')) ? 'disabled':'';
                                available += ($(this).attr('id') == clicked_item.attr('id')) ? ' selected':'';

                                if(modal.find('.modal-data').data('content-type') == 'custom')
                                {
                                    if(($(this).attr('id') == clicked_item.attr('id') && $(this).hasClass('active')) || $.inArray($(this).attr('id'), events_regions['all']) !== -1)
                                    {
                                        fields_select.attr('disabled', true);
                                    }

                                    fields_select.append(
                                        $('<option '+available+'></option>').val($(this).attr('id')).html($(this).attr('id'))
                                    );
                                }
                                else if(modal.find('.modal-data').data('content-type') == 'events')
                                {
                                    var class_name = '';
                                    var available = ($(this).hasClass('active')) ? 'disabled':'';
                                    available += ($(this).attr('id') == modal.data('id')) ? ' checked':'';

                                    if(clicked_item.closest('.tab-pane').find("#" + $(this).attr('id')).hasClass("selected-as-another"))
                                    {
                                        available = 'checked';
                                    }

                                    if($.inArray($(this).attr('id'), events_regions['all']) !== -1)
                                    {
                                        available = 'checked';
                                        class_name = 'light';

                                        if($(this).attr('id') != modal.data('id'))
                                        {
                                            available += ' disabled';
                                            class_name += ' disabled';
                                        }

                                        /* Removed disabled(s) as this is for the region we are editing */
                                        if($.inArray($(this).attr('id'), events_regions[modal.data('id')]) !== -1)
                                        {
                                            available = available.replace('disabled','');
                                            class_name = '';
                                        }
                                        else
                                        {
                                            available = available.replace('checked','');
                                        }
                                    }
                                    else
                                    {
                                        if($(this).hasClass('active') && $(this).attr('id') != modal.data('id'))
                                        {
                                            available = 'disabled';
                                            class_name = 'light disabled';
                                        }
                                    }

                                    var checkbox = '<div class="col-md-3"><label class="checkbox-inline ' + class_name + '"><input type="checkbox" ' + available + ' class="regions" name="regions" value="' + $(this).attr('id') + '"> ' + $(this).attr('id') + '</label></div>';

                                    event_groups.append(checkbox);

                                    if(clicked_item.closest('.tab-pane').find(".gridster > ul li").length <= 1)
                                    {
                                        event_groups.find('.regions').attr('checked', true).attr('disabled', true);
                                        event_groups.parent().parent().find('.help-block').remove();
                                    }
                                }
                            });

                            if(modal.find('.modal-data').data('content-type') == 'events')
                            {
                                var events_select = $(".modal #events-select");
                                var tax_select = $(".modal #taxonomy-select");

                                if(events_select.find('option').length > 0 || tax_select.find('option').length > 0)
                                {
                                    init_events(events_select);
                                    init_events(tax_select);
                                }

                                init_date_ranges();
                            }
                        }

                        modal.modal('show');
                    },
                    beforeSend: function(data)
                    {
                        modal.find('h4.modal-title').html('Processing');
                        modal.find('.modal-footer').remove();
                        modal.find('.modal-body').html('<p>Loading...</p>');
                    },
                    error: function(req,error){
                        if(error === 'error') { error = req.statusText; }
                        var errormsg = 'There was a communication error: '+error;
                        modal.find('.modal-body').html(errormsg);

                        console.log(error);
                    },
                }, this);
            });

            $(document).on("change", ".modal #content_type", function()
            {
                if($(this).val() == '')
                {
                    $(".modal .modal-data").addClass("hide");
                    $(".modal .content-picker").removeClass('hide');
                }
                else
                {
                    $(".modal .content-picker").addClass("hide");
                    modal = $(".modal");

                    var changed_val = $(this).val();

                    var data = {};
                    data.get = 'content-type';
                    data.content_type = $(this).val();

                    data.region = clicked_item.attr('id');
                    data.json = (clicked_item.closest('.tab-pane').attr('id') == 'add-slides') ? clicked_item.closest('.tab-pane').find('#slide_data').val():clicked_item.closest('.tab-pane').find('.hidden-slide-data').val();

                    /* Load modal and defaults - http://www.smashingmagazine.com/2010/02/some-things-you-should-know-about-ajax/ */
                    $.ajax({
                        cache: false,
                        url: '/hummingbird/lc-sliders/modal-data/',
                        timeout:5000,
                        data: data,
                        success: function(data)
                        {
                            modal.html(data);

                            modal.data('content-type', changed_val);

                            //adding settings to this option
                            var num_areas = clicked_item.closest('.tab-pane').find(".gridster > ul li").length;

                            var slide_title = clicked_item.closest('.tab-pane').find("#slide_title").val();
                            slide_title = (slide_title == '') ? modal.data('id'):slide_title;

                            modal.find(".span-modal-title").text(slide_title);

                            /* Set up number of fields */
                            resetFields($(".modal #fields"));
                            var fields_select = modal.find("#fields");

                            var event_groups = modal.find('.modal-data .region-checkboxes > div');
                            var prepended = false;
                            var events_regions = (clicked_item.closest('.tab-pane').attr('id') == 'add-slides') ? clicked_item.closest('.tab-pane').find('#slide_data').val():clicked_item.closest('.tab-pane').find('.hidden-slide-data').val();
                            events_regions = processEventsRegions(events_regions);

                            clicked_item.closest('.tab-pane').find(".gridster > ul li").each(function()
                            {
                                var available = ($(this).hasClass('active')) ? 'disabled':'';
                                available += ($(this).attr('id') == modal.data('id')) ? ' selected':'';

                                if(modal.find('.modal-data').data('content-type') == 'custom')
                                {
                                    if(($(this).attr('id') == modal.data('id') && $(this).hasClass('active')) || clicked_item.closest('.tab-pane').find(".gridster > ul li").length <= 1 || $.inArray($(this).attr('id'), events_regions['all']) !== -1)
                                    {
                                        fields_select.attr('disabled', true);

                                        if(clicked_item.closest('.tab-pane').find(".gridster > ul li").length <= 1)
                                        {
                                            modal.parent().parent().find('.help-block').remove();
                                        }
                                    }

                                    fields_select.append(
                                        $('<option '+available+'></option>').val($(this).attr('id')).html($(this).attr('id'))
                                    );
                                }
                                else if(modal.find('.modal-data').data('content-type') == 'events')
                                {
                                    var class_name = ($(this).hasClass('active')) ? 'light':'';
                                    var available = ($(this).hasClass('active')) ? 'disabled':'';
                                    available += ($(this).attr('id') == modal.data('id')) ? ' checked':'';

                                    if($.inArray($(this).attr('id'), events_regions['all']) !== -1)
                                    {
                                        available = 'disabled';
                                        class_name = 'light disabled';

                                        if($.inArray($(this).attr('id'), events_regions[modal.data('id')]) !== -1)
                                        {
                                            available = available.replace('disabled', ''); //remove disabled property
                                            class_name = class_name.replace('light', ''); //remove light class
                                                class_name = class_name.replace('disabled', ''); //remove disabled class
                                        }
                                    }

                                    var checkbox = '<div class="col-md-3"><label class="checkbox-inline ' + class_name + '"><input type="checkbox" ' + available + ' class="regions" name="regions" value="' + $(this).attr('id') + '"> ' + $(this).attr('id') + '</label></div>';

                                    event_groups.append(checkbox);

                                    if(clicked_item.closest('.tab-pane').find(".gridster > ul li").length <= 1)
                                    {
                                        event_groups.find('.regions').attr('checked', true).attr('disabled', true);
                                        event_groups.parent().parent().find('.help-block').remove();
                                    }
                                }
                            });
                        },
                        beforeSend: function(data)
                        {
                            modal.find('h4.modal-title').html('Processing');
                            modal.find('.modal-footer').remove();
                            modal.find('.modal-body').html('<p>Loading...</p>');
                        },
                        error: function(req,error){
                            if(error === 'error') { error = req.statusText; }
                            var errormsg = 'There was a communication error: '+error;
                            modal.find('.modal-body').html(errormsg);
                        },
                    }, this);
                }
            });

            $("#gridster-remove-template").click(function(e)
            {
                e.preventDefault();

                resetFormFields();

                if($("#save-slide").length > 0)
                {
                    $("#save-slide").slideUp();
                }
            });

            if($("#save-slide").length > 0)
            {
                $("#save-slide").click(function()
                {
                    var slide_title = $(this).closest('.tab-pane').find('#slide_title').val();
                    slide_title = (slide_title != '') ? slide_title:'Undefined';

                    // Get tab 
                    var nextTab = $('#tabs li').size()+1;

                    // create the tab
                    $('<li><a href="#tab-slide-'+nextTab+'" data-toggle="tab">'+slide_title+'</a></li>').insertBefore('#tabs li:last');
                    
                    // create the tab content
                    $('<div class="tab-pane" id="tab-slide-'+nextTab+'"><h3>Content will be loaded when you save the slider</h3></div>').appendTo('.tab-content');
                    
                    // make the new tab active
                    $('#tabs a:last').tab('show');

                    var item_data = {};
                    item_data['title'] = slide_title;
                    item_data['template'] = $(this).closest('.tab-pane').find('#template').val();

                    var slides = $("#slide_data").val();
                    var slide_data = (slides != '') ? $.parseJSON(slides):{};
                    item_data['regions'] = (slides != '') ? slide_data.regions:{};

                    $("#tab-slide-"+nextTab).append("<input type='hidden' id='tab-slide-"+nextTab+"-data' name='slides[]' value='" + JSON.stringify(item_data) + "' />")

                    resetFormFields();

                    $(".update-all").data('action', true).slideDown();

                    $(this).slideUp();
                });
            }

            $(document).on('click', '.tab-pane .remove-slide', function(e)
            {
                e.preventDefault();
                    
                var tab = $(this).closest('.tab-pane');
                var tab_link = $('#tabs li a[href="#' + tab.attr('id') + '"]');

                /* Remove */
                tab.remove();
                tab_link.parent().remove();

                /* Open last link */
                $('#tabs li:last-child').prev('li').find('a').trigger('click');
            });

            $(document).on('click', '.modal .modal-footer .btn:not(.create-image):not(.delete-region):not(.save-region-settings)', function(e)
            {
                e.preventDefault();

                var modal_el = $('.modal');
                var item_id = modal_el.data('id');
                var type = modal_el.find('.modal-data').data('content-type');
                var is_current_tab = (typeof modal_el.data('slide-id') != 'undefined') ? true:false;

                if(type == 'custom')
                {
                    if($("#fields").val() != item_id && !is_current_tab)
                    {
                        item_id = $("#fields").val();
                    }
                }

                clicked_item.data('content-type', type);

                var item = (!is_current_tab) ? $("#main.gridster > ul li#" + item_id):$('#' + modal_el.data('slide-id') + ' .gridster > ul li#' + item_id);
                var item_data = (!is_current_tab) ? {}:$.parseJSON($('#' + modal_el.data('slide-id') + '-data').val());

                switch(type)
                {
                    case 'custom':
                        var modal_title = modal_el.find('#modal_title').val();
                        var modal_summary = modal_el.find('#modal_summary').val();
                        modal_summary = modal_summary.replace("'" , "&#39;");

                        var modal_link = modal_el.find('#modal_link').val();
                        var modal_image = modal_el.find('.media-image').val();

                        if(!is_current_tab)
                        {
                            if($("#slide_data").val() != '') item_data = $.parseJSON($("#slide_data").val());
                            if(Object.keys(item_data).length == 0) item_data['regions'] = {};
                            if(item_data['regions'].length == 0) item_data['regions'] = {};

                            item_data['regions'][modal_el.data('id')] = {};
                            item_data['regions'][modal_el.data('id')]['title'] = modal_title;
                            item_data['regions'][modal_el.data('id')]['summary'] = modal_summary;
                            item_data['regions'][modal_el.data('id')]['link'] = modal_link;
                            item_data['regions'][modal_el.data('id')]['image'] = modal_image;
                            
                            $("#slide_data").val(JSON.stringify(item_data));
                        }
                        else
                        {
                            if(Object.keys(item_data).length == 0) item_data['regions'] = {};
                            if(item_data['regions'].length == 0) item_data['regions'] = {};

                            item_data['regions'][modal_el.data('id')] = {};
                            item_data['regions'][modal_el.data('id')]['title'] = modal_title;
                            item_data['regions'][modal_el.data('id')]['summary'] = modal_summary;
                            item_data['regions'][modal_el.data('id')]['link'] = modal_link;
                            item_data['regions'][modal_el.data('id')]['image'] = modal_image;

                            $('#' + modal_el.data('slide-id') + '-data').val(JSON.stringify(item_data));
                        }
                        break;
                    case 'events':
                        var _id_updated = false;
                        var select_by = modal_el.find('#events-select-by').val();
                        var venue_id = modal_el.find('#events-venue').val();
                        var old_id = item_id;
                        modal_el.data('id', setParentRegion(modal_el));

                        if(modal_el.data('id') != item_id)
                        {
                            _id_updated = true;
                            item_id = modal_el.data('id');
                            item = (!is_current_tab) ? $("#main.gridster > ul li#" + item_id):$('#' + modal_el.data('slide-id') + ' .gridster > ul li#' + item_id);
                            item.data('content-type', 'events');

                            var old_item = (!is_current_tab) ? $("#main.gridster > ul li#" + old_id):$('#' + modal_el.data('slide-id') + ' .gridster > ul li#' + old_id);
                            old_item.removeData('content-type');
                        }

                        if(!is_current_tab)
                        {
                            if($("#slide_data").val() != '') item_data = $.parseJSON($("#slide_data").val());
                            if(Object.keys(item_data).length == 0) item_data['regions'] = {};
                            if(item_data['regions'].length == 0) item_data['regions'] = {};

                            if(_id_updated)
                            {
                                /* Remove item */
                                delete item_data['regions'][old_id];
                            }

                            item_data['regions'][modal_el.data('id')] = {};
                            item_data['regions'][modal_el.data('id')]['type'] = 'events';
                            item_data['regions'][modal_el.data('id')]['select'] = select_by;
                            item_data['regions'][modal_el.data('id')]['venue'] = venue_id;

                            if(select_by == 'custom')
                            {
                                item_data['regions'][modal_el.data('id')]['events'] = [];

                                var event_select = modal_el.find('#events-select');
                                var events_selected = event_select.parent().find('.dropdown-menu.selectpicker li.selected');

                                if(events_selected.length > 0)
                                {
                                    events_selected.each(function()
                                    {
                                        var index = $(this).data('original-index');

                                        item_data['regions'][modal_el.data('id')]['events'].push(event_select.find('option:eq(' + index + ')').val());
                                    });
                                }                               
                            }

                            if(select_by == 'filter')
                            {
                                item_data['regions'][modal_el.data('id')]['filter_by'] = modal_el.find('#event-filter-by').val();

                                if(item_data['regions'][modal_el.data('id')]['filter_by'] == 'categories')
                                {
                                    item_data['regions'][modal_el.data('id')]['categories'] = [];

                                    var tax_select = modal_el.find('#taxonomy-select');
                                    var taxs_selected = tax_select.parent().find('.dropdown-menu.selectpicker li.selected');

                                    if(taxs_selected.length > 0)
                                    {
                                        taxs_selected.each(function()
                                        {
                                            var index = $(this).data('original-index');

                                            item_data['regions'][modal_el.data('id')]['categories'].push(tax_select.find('option:eq(' + index + ')').val());
                                        });
                                    }
                                }

                                if(item_data['regions'][modal_el.data('id')]['filter_by'] == 'tags')
                                {
                                    item_data['regions'][modal_el.data('id')]['tags'] = [];

                                    var tax_select = modal_el.find('#taxonomy-select');
                                    var taxs_selected = tax_select.parent().find('.dropdown-menu.selectpicker li.selected');

                                    if(taxs_selected.length > 0)
                                    {
                                        taxs_selected.each(function()
                                        {
                                            var index = $(this).data('original-index');

                                            item_data['regions'][modal_el.data('id')]['tags'].push(tax_select.find('option:eq(' + index + ')').val());
                                        });
                                    }
                                }

                                if(item_data['regions'][modal_el.data('id')]['filter_by'] == 'dates')
                                {
                                    item_data['regions'][modal_el.data('id')]['dates'] = {};
                                    item_data['regions'][modal_el.data('id')]['dates']['dates_from'] = modal_el.find('#post_date').val();
                                    item_data['regions'][modal_el.data('id')]['dates']['dates_to'] = modal_el.find('#post_end_date').val();
                                }
                            }

                            var regions = modal_el.find('.regions:checked'); //the regions we are applying this too.
                            var regions_set = false;
                            clicked_item.closest('.tab-pane').find('.selected-as-another[data-parent="' + modal_el.data('id') + '"]').removeClass('selected-as-another').removeData('parent');

                            regions.each(function()
                            {
                                var _val = $(this).val();
                                if(_val != item_id)
                                {
                                    if(regions_set === false)
                                    {
                                        item_data['regions'][modal_el.data('id')]['regions'] = [];
                                        regions_set = true;
                                    }

                                    item_data['regions'][modal_el.data('id')]['regions'].push(_val);
                                    clicked_item.closest('.tab-pane').find("#" + $(this).val()).addClass("selected-as-another").data('parent', item_id);
                                }
                            });

                            item_data['regions'][modal_el.data('id')]['order_by'] = modal_el.find('#event-order-by').val();
                            item_data['regions'][modal_el.data('id')]['ignore_dupes'] = (modal_el.find('.events-ignore-dupes').is(':checked')) ? 1:0;
                            
                            $("#slide_data").val(JSON.stringify(item_data));
                        }
                        else
                        {
                            if(item_data['regions'].length == 0) item_data['regions'] = {};

                            if(_id_updated)
                            {
                                /* Remove item */
                                delete item_data['regions'][old_id];
                            }

                            item_data['regions'][modal_el.data('id')] = {};
                            item_data['regions'][modal_el.data('id')]['type'] = 'events';
                            item_data['regions'][modal_el.data('id')]['select'] = select_by;
                            item_data['regions'][modal_el.data('id')]['venue'] = venue_id;

                            if(select_by == 'custom')
                            {
                                item_data['regions'][modal_el.data('id')]['events'] = [];

                                var event_select = modal_el.find('#events-select');
                                var events_selected = event_select.parent().find('.dropdown-menu.selectpicker li.selected');

                                if(events_selected.length > 0)
                                {
                                    events_selected.each(function()
                                    {
                                        var index = $(this).data('original-index');

                                        item_data['regions'][modal_el.data('id')]['events'].push(event_select.find('option:eq(' + index + ')').val());
                                    });
                                }                               
                            }

                            if(select_by == 'filter')
                            {
                                item_data['regions'][modal_el.data('id')]['filter_by'] = modal_el.find('#event-filter-by').val();

                                if(item_data['regions'][modal_el.data('id')]['filter_by'] == 'categories')
                                {
                                    item_data['regions'][modal_el.data('id')]['categories'] = [];

                                    var tax_select = modal_el.find('#taxonomy-select');
                                    var taxs_selected = tax_select.parent().find('.dropdown-menu.selectpicker li.selected');

                                    if(taxs_selected.length > 0)
                                    {
                                        taxs_selected.each(function()
                                        {
                                            var index = $(this).data('original-index');

                                            item_data['regions'][modal_el.data('id')]['categories'].push(tax_select.find('option:eq(' + index + ')').val());
                                        });
                                    }
                                }

                                if(item_data['regions'][modal_el.data('id')]['filter_by'] == 'tags')
                                {
                                    item_data['regions'][modal_el.data('id')]['tags'] = [];

                                    var tax_select = modal_el.find('#taxonomy-select');
                                    var taxs_selected = tax_select.parent().find('.dropdown-menu.selectpicker li.selected');

                                    if(taxs_selected.length > 0)
                                    {
                                        taxs_selected.each(function()
                                        {
                                            var index = $(this).data('original-index');

                                            item_data['regions'][modal_el.data('id')]['tags'].push(tax_select.find('option:eq(' + index + ')').val());
                                        });
                                    }
                                }

                                if(item_data['regions'][modal_el.data('id')]['filter_by'] == 'dates')
                                {
                                    item_data['regions'][modal_el.data('id')]['dates'] = {};
                                    item_data['regions'][modal_el.data('id')]['dates']['dates_from'] = modal_el.find('#post_date').val();
                                    item_data['regions'][modal_el.data('id')]['dates']['dates_to'] = modal_el.find('#post_end_date').val();
                                }
                            }

                            item_data['regions'][modal_el.data('id')]['order_by'] = modal_el.find('#event-order-by').val();
                            item_data['regions'][modal_el.data('id')]['ignore_dupes'] = (modal_el.find('.events-ignore-dupes').is(':checked')) ? 1:0;

                            var regions = modal_el.find('.regions:checked'); //the regions we are applying this too.
                            clicked_item.closest('.tab-pane').find('.selected-as-another[data-parent="' + modal_el.data('id') + '"]').removeClass('selected-as-another').removeData('parent');

                            var regions_set = false;
                            regions.each(function()
                            {
                                var _val = $(this).val();
                                if(_val != item_id)
                                {
                                    if(!regions_set)
                                    {
                                        item_data['regions'][modal_el.data('id')]['regions'] = [];
                                        regions_set = true;
                                    }

                                    item_data['regions'][modal_el.data('id')]['regions'].push(_val);
                                    clicked_item.closest('.tab-pane').find("#" + $(this).val()).addClass("selected-as-another").data('parent', item_id);
                                }
                            });

                            $('#' + modal_el.data('slide-id') + '-data').val(JSON.stringify(item_data));
                        }
                        break;
                }
                
                item.removeAttr('data-content-type');
                item.attr('data-content-type', type);

                item.data('has-data', 1);
                item.addClass('active');

                $(".modal").modal('hide');
            });

            $(document).on('click', '.modal .modal-footer .btn.delete-region', function(e)
            {
                e.preventDefault();

                var modal_el = $('.modal');
                var item_id = modal_el.data('id');
                var is_current_tab = (typeof modal_el.data('slide-id') != 'undefined') ? true:false;

                /* Current slide */
                var slide_id = modal_el.data('slide-id');
                var slide = (is_current_tab) ? $("#" + slide_id):$("#add-slides");

                /* Delete value from array */
                var slide_data = (is_current_tab) ? $("#" + slide_id).find('.hidden-slide-data').val():slide.find('#slide_data').val();
                var item_data = (slide_data != '') ? $.parseJSON(slide_data):'';
                
                /* Remove classes and children from Gridster */
                if(typeof item_data.regions != 'undefined' && typeof item_data.regions[item_id] != 'undefined' && typeof item_data.regions[item_id].regions != 'undefined')
                {
                    if(item_data.regions[item_id].regions)
                    {
                        $.each(item_data.regions[item_id].regions, function(index, value)
                        {
                            slide.find('#' + value).removeClass('selected-as-another').removeData('parent');
                        });
                    }

                    /* Remove main item active class from Gridster */
                    slide.find('#' + item_id).removeClass('active').removeData('content-type');

                    /* Remove item */
                    delete item_data.regions[item_id];

                    /* Store item back */
                    slide.find('.hidden-slide-data').val(JSON.stringify(item_data));
                }

                $(".modal").modal('hide');
            });

            $(document).on('change', '.modal #events-select-by', function(e)
            {
                $(".modal .modal-content .events-select, .modal .modal-content .events-filter-by, .modal .modal-content .taxonomy-select, .modal .modal-content .date-select").addClass('hide'); //remove them from view

                if($(this).val() == 'filter')
                {
                    $(".modal .modal-content .events-filter-by").removeClass("hide");
                }
                else
                {
                    if($(this).val() == 'custom')
                    {
                        var data = {};
                        data.get = 'get-events-data';
                        data.data = 'custom';

                        /* Load modal and defaults - http://www.smashingmagazine.com/2010/02/some-things-you-should-know-about-ajax/ */
                        $.ajax({
                            dataType: 'json',
                            cache: false,
                            url: '/hummingbird/lc-sliders/modal-data/',
                            timeout:5000,
                            data: data,
                            success: function(data)
                            {
                                var events_select = $(".modal #events-select");
                                events_select.find('option').remove();

                                events_select.closest('.form-group').removeClass("hide");

                                if(events_select.length > 0)
                                {
                                    $.each( data.events, function( key, value )
                                    {
                                        var date = moment(eval(value.start_date*1000)); // *1000 because of date takes milliseconds
                                        var myDate = date.format("D MMM YYYY, h:mm:ss a");
                                        myDate = myDate.replace(':00','');
                                        myDate = myDate.replace(':00 ','');

                                        var event_title = value.title + ' (' + myDate + ')';
                                        events_select.append(
                                            $('<option></option>').val(value.id).html(event_title)
                                        );
                                    });

                                    events_select.selectpicker('refresh');
                                    $(".loading-events").remove();
                                }
                                else
                                {
                                    $(".loading-events").html('No events could be found.');
                                }
                            },
                            complete: function()
                            {
                                $(".modal #events-select").selectpicker('refresh');
                                $(".loading-events").remove();
                            },
                            beforeSend: function(data)
                            {
                                $('<div class="alert alert-box alert-info loading-events text-center">Loading events...</div>').insertAfter($(".modal #events-select-by").closest('.form-group'));
                            },
                            error: function(req,error){
                                if(error === 'error') { error = req.statusText; }
                                var errormsg = 'There was a communication error: '+error;
                                $('<div class="alert alert-box alert-info loading-events text-center">' + errormsg + '</div>').insertAfter($(".modal #events-select-by").closest('.form-group'));
                            },
                        }, this);
                    }
                    else
                    {
                        $(".modal #events-select").addClass('hide');
                    }
                }
            });

            $(document).on('change', '.modal #event-filter-by', function(e)
            {
                var select_val = $(this).val();

                if(select_val == '')
                {

                }
                else
                {
                    switch(select_val)
                    {
                        case 'categories':
                        case 'tags':
                            var taxonomy = $(".modal #taxonomy-select");
                            resetFields(taxonomy);
                            taxonomy.find('option').remove();

                            var data = {};
                            data.get = 'taxonomy';
                            data.taxonomy = select_val;

                            /* Load modal and defaults - http://www.smashingmagazine.com/2010/02/some-things-you-should-know-about-ajax/ */
                            $.ajax({
                                dataType: 'json',
                                cache: false,
                                url: '/hummingbird/lc-sliders/modal-data/',
                                timeout:5000,
                                data: data,
                                success: function(data)
                                {
                                    if(data.taxonomy.length > 0)
                                    {
                                        $.each( data.taxonomy, function( key, value )
                                        {
                                            taxonomy.append(
                                                $('<option></option>').val(value.id).html(value.name)
                                            );
                                        });

                                        taxonomy.closest('.form-group').removeClass("hide");

                                        taxonomy.selectpicker('refresh');
                                        $(".loading-taxonomy").remove();
                                    }
                                    else
                                    {
                                        $(".loading-taxonomy").html('No ' + select_val + ' could be found.');
                                    }
                                },
                                beforeSend: function(data)
                                {
                                    $('<div class="alert alert-box alert-info loading-taxonomy text-center">Loading ' + select_val + '...</div>').insertAfter($(".modal #event-filter-by").closest('.form-group'));
                                },
                                error: function(req,error){
                                    if(error === 'error') { error = req.statusText; }
                                    var errormsg = 'There was a communication error: '+error;
                                    $('<div class="alert alert-box alert-info loading-taxonomy text-center">' + errormsg + '</div>').insertAfter($(".modal #event-filter-by").closest('.form-group'));
                                },
                            }, this);
                            break;
                        case 'dates':
                            $(".modal .taxonomy-select").addClass('hide');
                            $(".modal .date-select").removeClass('hide');

                            init_date_ranges(null, null);

                            break;
                    }
                }
            });

            $(document).on('click', '.modal #display_by', function(e)
            {
                if($(this).prop('checked'))
                {
                    $(".modal .display_by_list").slideDown();
                }
                else
                {
                    $(".modal .display_by_list").slideUp();
                }
            });
        });
    </script>
@stop