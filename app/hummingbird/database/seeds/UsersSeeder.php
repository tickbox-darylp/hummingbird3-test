<?php namespace Hummingbird\Users\Seeds;

class UsersSeeder extends \Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Eloquent::unguard();
        
        // ordering is important here
        $this->call('EntrustTableSeeder');
        $this->call('PermissionSeeder');
        $this->call('PermissionOverridesTableSeeder');
        $this->call('UsersTableSeeder');
    }

}
