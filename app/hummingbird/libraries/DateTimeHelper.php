<?php

/**
 * 
 *
 * @package hummingbird
 * @author Daryl Phillips @ Tickbox Marketing
 *
 */
class DateTimeHelper
{
    protected $timezone;

    protected static $days = array(
        1 => array('short' => 'Mon', 'full' => 'Monday'),
        2 => array('short' => 'Tue', 'full' => 'Tuesday'),
        3 => array('short' => 'Wed', 'full' => 'Wednesday'),
        4 => array('short' => 'Thu', 'full' => 'Thursday'),
        5 => array('short' => 'Fri', 'full' => 'Friday'),
        6 => array('short' => 'Sat', 'full' => 'Saturday'),
        7 => array('short' => 'Sun', 'full' => 'Sunday')
    );

    public function __construct()
    {
        $this->timezone = Config::get('app.timezone', 'UTC');
    }

    /**
     *
     * Produce a list of timezones
     *
     * @return   Array Timezones
     *
     */  
    public static function timezone_select_list()
    {
        $list = array();
        
        $timezones = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        
        foreach($timezones as $timezone)
        {
            $d = new DateTime;
            $d->setTimezone(new DateTimeZone($timezone));
            $list[$timezone] = $timezone;
        }

        return $list;
    }

    /**
     *
     * Return the date in a particular format
     *
     * @param    integer $timestamp timestamp to convert
     * @param    string $format Format required
     * @return   String Date format
     *
     */  
    public static function date_format($timestamp = NULL, $format = 'd/m/Y')
    {
        $timestamp = (!$timestamp) ? time():$timestamp;
        return date($format, $timestamp);
    }


    /**
     *
     * Return days
     *
     * @return  Array of week days (with short and long)
     *
     */  
    public static function get_days()
    {
        return self::$days;
    }

    /**
     *
     * Return day name
     *
     * @param    integer Date needed
     * @param    Boolean Full name
     * @return   URL
     *
     */  
    public static function day_name($day = 1, $full_name = false)
    {
        $name = ($full_name) ? 'full':'short';
        return self::$days[$day][$name];
    }


    /**
     *
     * Generate a list of time increments
     *
     * @param    integer $increment value to increment time by (in minutes)
     * @return   Array of times with specified increment
     *
     */  
    public static function get_time_increments($increment = 15, $use_key = false)
    {
        $time = 0; //used to break the loop when the date has been reached
        $times = array();

        // protect against infinite loops
        if(!isset($increment) OR $increment <= 0)
        {
            $increment = 15;
        }

        $increment = $increment * 60; // turn increment seconds into minutes
        $base_time = strtotime(self::date_format(time(), "Y-m-d"));

        while($time < (24*60*60))
        {
            $now = self::date_format($base_time + $time, "H:i");

            if($use_key)
            {
                $times[$now] = $now;
            }
            else
            {
                $times[] = $now;
            }

            $time = $time + $increment;
        }

        return $times;
    }
}

?>
