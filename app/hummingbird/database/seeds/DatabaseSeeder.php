<?php 

use Illuminate\Database\Seeder as Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        /* Make sure all permissions are installed */
        $this->call('PermissionSeeder'); // DONE

        // ordering is important here
        $this->call('EntrustTableSeeder'); // DONE
        $this->call('UsersTableSeeder'); // DONE

        // ordering can be importnat here
        $this->call('DashboardTableSeeder'); // DONE
        $this->call('SystemUpdateTableSeeder');
        $this->call('ErrorsTableSeeder'); // DONE
        $this->call('ActivityLogTableSeeder'); // DONE
        $this->call('SettingTableSeeder'); // DONE
        $this->call('RedirectionTableSeeder'); // DONE
        $this->call('WidgetTableSeeder');

        // // ordering is important here
        $this->call('ModuleTableSeeder');
        $this->call('ModuletemplateTableSeeder');
        $this->call('TemplateTableSeeder'); // DONE
        $this->call('PagesTableSeeder'); // DONE
        $this->call('MenusTableSeeder'); // DONE
        $this->call('MediaLibrarySeeder');
        
        // ordering can be important here
        // $this->call('TaxonomySeeder');
        $this->call('CategoriesSeeder');
        $this->call('TagsSeeder');
        
        // ordering can be importnat here
        $this->call('PluginsTableSeeder');
        
        // ordering can be important here
        $this->call('ThemeInfoSeeder');
    }

}
