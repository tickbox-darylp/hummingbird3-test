<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class LCSlider extends LaravelBook\Ardent\Ardent
{
    use SoftDeletingTrait;

    public $table = 'lc_sliders';
    public $cms_url = 'lc-sliders';
    public $dash_icon = 'fa-object-group';
    
    protected $fillable = array('title', 'description');
    
    public $search_fields = array();
    public $export_fields = array();
    protected $shortcode = '[HB::LCSLIDERS()]';

    protected $permission_type = 'lc-sliders';

    protected $post_types = array('Custom', 'Events');

    protected $min_slides = 1;
    protected $rows = array('min' => 1, 'max' => 2);
    protected $columns = array('min' => 1, 'max' => 12);

    public function getChildren($slider_data = array())
    {
        if(!empty($slider_data) AND count($slider_data) > 0)
        {
            // return items
            return $slider_data;
        }

        return false;
    }


    public function randomImage($image = '')
    {
        if($image != '' AND File::exists(base_path() . $image)) return $image;

        $images = array(
            'https://www.priorityguestrewards.com.au/wp-content/uploads/2014/07/1140x400-logged-out-home-slides-single.jpg', 
            'http://two25peoria.com/wp-content/uploads/2013/10/130810-Two25-224-1140x400.jpg',
            'https://www.priorityguestrewards.com.au/wp-content/uploads/2014/07/1140x400-logged-out-home-slides-couple.jpg',
            'http://www.elephantandcastle-lendlease.com/wp-content/uploads/slide-history-1-1140x400-e1396539499277.jpg',
            'http://two25peoria.com/wp-content/uploads/2013/10/Two25-036-1140x400.jpg',
            'http://www.lakehighlandsyouthorchestra.com/wp-content/uploads/2014/04/lake-highlands-youth-orchestra-concert-1140x400.jpg'
        );

        return $images[array_rand($images)];
    }

    public function calculateNextColumn($col, $counter, $template)
    {
        if($template == 'full') return $col;

        switch($template)
        {
            case 1:
                if($counter == 1) return 1;

                return ($col + 3) % 12;
                break;
            case 2:
                if($counter == 1)
                {
                    return 1;
                }
                else
                {
                    if($counter % 2 == 0) return 7;
                    return 10;
                }
            case 3:
                if($counter == 1) return 1;
                return 11;
                break;
            case 4:
                if($counter % 3 == 1) return 1;
                if($counter % 3 == 2) return 5;
                if($counter % 3 == 0) return 9;
                break;
            case 5:
                if($counter % 1 == 0) return 1;
                return 9;
                break;
        }
    }

    public function calculateNextRow($row, $counter, $template)
    {
        if($template == 'full') return $row;

        switch($template)
        {
            case 1:
                if($counter <= 4) return 1;

                return 2;
                break;
            case 2:
                if($counter == 1 OR ($counter > 1 AND $counter < 4))
                {
                    return 1;
                }

                return 2;
                break;
            case 4:
                return ($counter < 4) ? 1:2;
                break;
            case 3:
            case 5:
                return ($counter < 3) ? 1:2;
                break;
        }
    }

    public function calculateSize($row, $col, $counter, $template)
    {
        if($template == 'full') return array('x' => 12, 'y' => 2);

        switch($template)
        {
            case 1:
                return array('x' => 3, 'y' => 1);
                break;
            case 2:
                if($counter == 1) return array('x' => 6, 'y' => 2);
                
                return array('x' => 3, 'y' => 1);

                break;
            case 3:
                if($counter == 1) return array('x' => 10, 'y' => 2);
                
                return array('x' => 2, 'y' => 1);

                break;
            case 4:
                return array('x' => 4, 'y' => 1);
                break;
            case 5:
                if($counter == 1) return array('x' => 8, 'y' => 2);
                
                return array('x' => 4, 'y' => 1);

                break;
        }
    }

    public function calculate($template)
    {
        $data = array('rows' => '', 'columns' => '', 'total-modules' => '');

        switch($template)
        {
            case 'full':
                $data['rows'] = $this->rows['max'];
                $data['columns'] = $this->columns['max'];
                $data['total-modules'] = 1;
                break;
            case 1:
                $data['rows'] = 1;
                $data['columns'] = 3;
                $data['total-modules'] = ($this->columns['max'] / $data['columns']) * $this->rows['max'];
                break;
            case 2:
                $data['rows'] = 1;
                $data['columns'] = 1;
                $data['total-modules'] = 5;
                break;
            case 3:
                $data['rows'] = 1;
                $data['columns'] = 1;
                $data['total-modules'] = 3;
                break;
            case 4:
                $data['rows'] = 1;
                $data['columns'] = 4;
                $data['total-modules'] = ($this->columns['max'] / $data['columns']) * $this->rows['max'];
                break;
            case 5:
                $data['rows'] = 1;
                $data['columns'] = 1;
                $data['total-modules'] = 3;
                break;
        }

        return $data;
    }

    public function fakeSliderData()
    {
        /* Now set up the slides */
        return array(
            0 => array(
                'title' => 'Stockwood',
                'slide' => array(
                    'rows' => 2,
                    'cols' => 12, //full width
                    'content' => array(
                        'title' => 'HELLO WORLD',
                        'summary' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                        'image' => 'http://www.lutonculture.com/uploads/images/raw/1436800353.jpg',
                        'link' => 'http://www.tickboxmarketing.co.uk'
                    )
                )
            ),
            1 => array(
                'title' => 'Children\'s Activities',
                'slide' => array(
                    'rows' => 1,
                    'cols' => 3, //full width
                    'total-mods' => 8,
                    'content' => array(
                        'post_type' => 'Events',
                        'filters' => array(
                            'category' => 8
                        ),
                        'by' => 'featured',
                        'order' => 'start_date',
                        'order_by' => 'ASC'
                    )
                )
            ),
            2 => array(
                'title' => 'Google',
                'slide' => array(
                    'rows' => 2,
                    'cols' => 12, //full width
                    'content' => array(
                        'title' => 'HELLO WORLD #2',
                        'summary' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                        'image' => 'http://www.geant.net/Resources/PartnerResources/Partner%20Resources%20Image%20Library/Image%2010_958642.jpg',
                        'link' => 'http://www.tickboxmarketing.co.uk'
                    )
                )
            ),
            3 => array(
                'title' => 'Robots',
                'slide' => array(
                    'rows' => 2,
                    'cols' => 12, //full width
                    'content' => array(
                        'title' => 'HELLO WORLD #2',
                        'summary' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                        'image' => 'http://topwalls.net/wallpapers/2012/01/Nature-sea-scenery-travel-photography-image-768x1366.jpg',
                        'link' => 'http://www.tickboxmarketing.co.uk'
                    )
                )
            ),
            4 => array(
                'title' => 'Falling over',
                'slide' => array(
                    'rows' => 2,
                    'cols' => 12, //full width
                    'content' => array(
                        'title' => 'HELLO WORLD #3',
                        'summary' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                        'image' => 'http://loremflickr.com/1000/300',
                        'link' => 'http://www.tickboxmarketing.co.uk'
                    )
                )
            ),
            5 => array(
                'title' => 'Stockwood',
                'slide' => array(
                    'rows' => 2,
                    'cols' => 12, //full width
                    'content' => array(
                        'title' => 'HELLO WORLD',
                        'summary' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                        'image' => 'http://www.lutonculture.com/uploads/images/raw/1436800353.jpg',
                        'link' => 'http://www.tickboxmarketing.co.uk'
                    )
                )
            ),
            6 => array(
                'title' => 'Children\'s Activities',
                'slide' => array(
                    'rows' => 1,
                    'cols' => 3, //full width
                    'total-mods' => 8,
                    'content' => array(
                        'post_type' => 'Events',
                        'filters' => array(
                            'category' => 8
                        ),
                        'by' => 'featured',
                        'order' => 'start_date',
                        'order_by' => 'ASC'
                    )
                )
            ),
            7 => array(
                'title' => 'Google',
                'slide' => array(
                    'rows' => 2,
                    'cols' => 12, //full width
                    'content' => array(
                        'title' => 'HELLO WORLD #2',
                        'summary' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                        'image' => 'http://www.geant.net/Resources/PartnerResources/Partner%20Resources%20Image%20Library/Image%2010_958642.jpg',
                        'link' => 'http://www.tickboxmarketing.co.uk'
                    )
                )
            ),
            8 => array(
                'title' => 'Robots',
                'slide' => array(
                    'rows' => 2,
                    'cols' => 12, //full width
                    'content' => array(
                        'title' => 'HELLO WORLD #2',
                        'summary' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                        'image' => 'http://topwalls.net/wallpapers/2012/01/Nature-sea-scenery-travel-photography-image-768x1366.jpg',
                        'link' => 'http://www.tickboxmarketing.co.uk'
                    )
                )
            ),
            9 => array(
                'title' => 'Falling over',
                'slide' => array(
                    'rows' => 2,
                    'cols' => 12, //full width
                    'content' => array(
                        'title' => 'HELLO WORLD #3',
                        'summary' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                        'image' => 'http://loremflickr.com/1000/300',
                        'link' => 'http://www.tickboxmarketing.co.uk'
                    )
                )
            )
        );
    }

    public function shortcode($title = FALSE)
    {
        if($title !== FALSE) return str_replace("()", "(" . $title . ")", $this->shortcode);
    	return str_replace("()", "(" . $this->attributes['id'] . ")", $this->shortcode);
    }

    public function getSliderDataAttribute($value)
    {
        if($value == '') return array();

        return unserialize($value);
    }

    public function setSliderDataAttribute($value)
    {
        if(!is_array($value)) $this->attributes['slider_data'] = array();

        $this->attributes['slider_data'] = serialize($value);
    }

    public function renderRegions()
    {
    }

}