<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TaxonomyTable extends Migration 
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('taxonomy', function($table) 
        {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('slug', 255);
            $table->text('description');
            $table->string('type', '255');
            $table->integer('parent')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('taxonomy');
	}

}