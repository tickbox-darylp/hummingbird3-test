<?php


// todo: accommodation filtering too

class Search {
    
    public $modelname;
    public $searchterm = '';
    public $results = array();
    public $which_end;
    
    public static function make($data)
    {
        $search = new Search;
        
        $search->modelname = $data['model'];        
        $search->searchterm = $data['searchterm'];
        $search->which_end = (isset($data['which_end'])) ? $data['which_end'] : 'front' ;
        
        return $search;
    }
    
    
    public function run()
    {
        return ($this->which_end == 'front') ? $this->runFrontendSearch() : $this->runBackendSearch();
    }
    
    private function runBackendSearch()
    {
        $modelname = $this->modelname;
        $model = new $modelname;
        
        $this->results = $modelname::where($model->search_field, 'like', "%$this->searchterm%")->get();        
    }
    
    private function runFrontendSearch()
    {
        // more complicated searching using weighting etc
    }
    
    
}
