<div class="quick_box">
    <div class="icon">
        <h2><a href="/{{App::make('backend_url')}}/pages">Pages</a></h2>
    </div>

    <form method="post" action="/{{App::make('backend_url')}}/pages/add">
        <p>
            {{ Form::text('title','',array('placeholder'=>'New page name')) }}
        </p>
        <p>					
            {{ Form::select('parentpage_id', $pages) }}
        </p>
        <p><input type="submit" value="Add Page"></p>
    </form>
</div>