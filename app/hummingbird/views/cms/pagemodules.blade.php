@extends('HummingbirdBase::cms.layout')

@section('content')

<?php $even = false; ?>

<h1>All Page Modules</h1>

<table class='results' cellpadding='5' cellspacing='0'>
    <thead>        
        <th>Page</th>
        <th>Module</th>
        <th>Area</th>
        <th>Order</th>
        <th>Live</th>
        <th>Actions</th>
    </thead>
    <tbody>
        @foreach($pagemodules as $pagemodule)
        
        <tr <?php echo ($even) ? 'class="even"': '';$even = !$even;?>>
            <td>{{$pagemodule->page->title}}</td>
            <td>{{$pagemodule->module->name}}</td>
            <td>{{$pagemodule->area}}</td>
            <td>{{$pagemodule->position}}</td>
            <td>{{($pagemodule->live)?'Active':'Not live'}}</td>
            <td>
                <a href='/{{App::make('backend_url')}}/page-modules/edit/{{$pagemodule->id}}'>Edit</a> | 
                <a href='/{{App::make('backend_url')}}/page-modules/delete/{{$pagemodule->id}}'>Delete</a>
            </td>
        </tr>
        
        @endforeach
    </tbody>
</table>

<h1>Add new page module</h1>
<?php echo Form::open(array('url' => App::make('backend_url').'/page-modules/add', 'method' => 'post')) ?>
    <table cellpadding="5" cellspacing="0" id="add-mini">
        <tbody>
            <tr>
                <td class="row_name">Page: <strong>*</strong></td>
                <td>{{Form::select('page_id', $pages)}}</td>
            </tr>
            <tr>
                <td class="row_name">Module template: <strong>*</strong></td>
                <td>{{Form::select('module_id', $modules)}}</td>
            </tr>
            <tr>
                <td class="row_name">Area (in page's template): <strong>*</strong></td>
                <td>{{Form::text('area')}}</td>
            </tr>
            <tr>
                <td class="row_name">Position in area: <strong>*</strong></td>
                <td>{{Form::text('position')}}</td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="submit" value="Add Page Module" name="add">
                </td>
            </tr>
        </tbody>
    </table>
<?php echo Form::close()?>

@stop
