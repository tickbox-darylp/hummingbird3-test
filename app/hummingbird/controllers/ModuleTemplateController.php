<?php

class ModuleTemplateController extends CmsbaseController
{
    public function __construct(Moduletemplate $Moduletemplate)
    {
        parent::__construct();

        $this->model = $Moduletemplate;

        $this->data['breadcrumbs'][] = array(
            'classes' => '',
            'icon' => '',
            'title' => 'Module templates',
            'url' => '/' . General::backend_url() . '/module-templates/'
        ); //breadcrumb manager        
    }
    
    public function getIndex()
    {
        if($this->user->hasAccess('read', get_class($this->model)))
        {
            $this->data['tag'] = 'Module Templates';
            $this->data['moduletemplates'] = Moduletemplate::whereNull('deleted_at')->paginate($this->pagination);
            return View::make('HummingbirdBase::cms/moduletemplates', $this->data);
        }
        else
        {
            return parent::forbidden();
        }
    }
    
    public function getEdit($id)
    {
        if($this->user->hasAccess('update', get_class($this->model)))
        {
            $this->data['moduletemplate'] = Moduletemplate::whereNull('deleted_at')->where('id', '=', $id)->first();
            
            if(null !== $this->data['moduletemplate'])
            {
                $this->data['tag'] = 'Edit '.$this->data['moduletemplate']->title;
                
                $this->data['breadcrumbs'][] = array(
                    'classes' => '',
                    'icon' => '',
                    'title' => 'Edit: ' . $this->data['moduletemplate']->name,
                    'url' => '/' . General::backend_url() . '/module-templates/edit/' . $this->data['moduletemplate']->id
                ); //breadcrumb manager   

                return View::make('HummingbirdBase::cms/moduletemplates-edit', $this->data);
            }
            else
            {
                //return error message
                return Redirect::to(App::make('backend_url').'/module-templates/?fail');
            }
        }
        else
        {
            return parent::forbidden();
        }
    }
    
    public function postEdit($id)
    {
        if($this->user->hasAccess('update', get_class($this->model)))
        {
            $input = Input::except('_token');
            $this->model = Moduletemplate::whereNull('deleted_at')->where('id', '=', $id)->first();

            if(null !== $this->model)
            {
                $messages = [
                    'name.required' => 'The :attribute field is required.',
                    'name.unique' => 'The :attribute must be unique.',
                    'name.max' => 'The :attribute must be shorter than 255 characters'
                ];

                $validator = Validator::make($input, [
                    'name' => "required|unique:moduletemplates,name,$id|max:255"
                ], $messages);

                if ($validator->fails())
                {
                    //return save error
                    return Redirect::to(App::make('backend_url').'/module-templates/?postEdit=Val_fail');
                }
                else
                {
                    $this->model->update($input);
                    $this->model->save();

                    Activitylog::log([
                        'action' => 'UPDATED',
                        'type' => get_class($this->model),
                        'link_id' => $this->model->id,
                        'description' => 'Updated module template',
                        'notes' => Auth::user()->username . " updated the module template - &quot;$this->model->name&quot;"
                    ]);            
                    
                    return Redirect::to(App::make('backend_url').'/module-templates/edit/'.$id);
                }
            }
            else
            {
                //return save error
                return Redirect::to(App::make('backend_url').'/module-templates/?postEdit=fail');
            }
        }
        else
        {
            return parent::forbidden();
        }
    }

    public function postLock($id)
    {
        if($this->user->hasAccess('lock', get_class($this->model)))
        {
            $this->model = Moduletemplate::whereNull('deleted_at')->where('id', '=', $id)->first();

            if(null !== $this->model)
            {
                $this->model->editable = 0;

                if($this->model->save())
                {
                    Activitylog::log([
                        'action' => 'LOCKED',
                        'type' => get_class($this->model),
                        'link_id' => $this->model->id,
                        'description' => 'Module template locked',
                        'notes' => Auth::user()->username . " locked the module template &quot;" . $this->model->name . "&quot;"
                    ]);            
                    
                    return Redirect::to(App::make('backend_url').'/module-templates/');
                }
                else
                {
                    //return save error
                    return Redirect::to(App::make('backend_url').'/module-templates/');
                }
            }

            return Redirect::to(App::make('backend_url').'/module-templates/');
        }
        else
        {
            return parent::forbidden();
        }
    }
    
    public function postAdd()
    {
        if($this->user->hasAccess('create', get_class($this->model)))
        {
            $input = Input::except('_token', 'add');        
            $moduletemplate = (new Moduletemplate)->fill($input);

            $messages = [
                'name.required' => 'The :attribute field is required.',
                'name.unique' => 'The :attribute must be unique.',
                'name.max' => 'The :attribute must be shorter than 255 characters'
            ];

            $validator = Validator::make($input, [
                'name' => 'required|unique:moduletemplates|max:255'
            ], $messages);

            if ($validator->fails())
            {
                return Redirect::to(App::make('backend_url').'/module-templates')->withErrors($validator->errors());
            }
            else
            {
                if(!$moduletemplate->save())
                {
                    return Redirect::to(App::make('backend_url').'/module-templates')->withErrors($moduletemplate->errors());
                }

                Activitylog::log([
                    'action' => 'CREATED',
                    'type' => get_class($this->model),
                    'link_id' => $moduletemplate,
                    'description' => 'Created a new module template',
                    'notes' => Auth::user()->username . " created a new module template - &quot;$moduletemplate->name&quot;"
                ]);  

                return Redirect::to(App::make('backend_url').'/module-templates/edit/'.$moduletemplate->id);
            }
        }
        else
        {
            return parent::forbidden();
        }
    }
    
    public function getDelete($id)
    {
        if($this->user->hasAccess('delete', get_class($this->model)))
        {
            $this->model = Moduletemplate::where('id', '=', $id)->whereNull('deleted_at')->first();

            if(null !== $this->model)
            {
                $name = $this->model->name;
                $this->model->delete($id);

                Activitylog::log([
                    'action' => 'DELETED',
                    'type' => get_class($this->model),
                    'link_id' => $id,
                    'description' => 'Deleted module template',
                    'notes' => Auth::user()->username . " deleted the module template &quot;$name&quot; role"
                ]);

                return Redirect::to(App::make('backend_url').'/module-templates');
            }
            else
            {
                //return errors too
                return Redirect::to(App::make('backend_url').'/module-templates');
            }
        }
        else
        {
            return parent::forbidden();
        }
    }

    public function import()
    {
        DB::unprepared(file_get_contents(base_path() . '/import/sql/moduletemplates.sql'));

        $templates = DB::table('old_moduletemplates')->get();

        if(count($templates) > 0)
        {
            foreach($templates as $template)
            {
                if($template->id > 1)
                {
                    $new_template = new Moduletemplate;
                    $new_template['name'] = $template->name;
                    $new_template['css'] = $template->css;
                    $new_template['type'] = $template->type;
                    $new_template['notes'] = $template->notes;
                    $new_template['editable'] = $template->editable;
                    $new_tempalte['created_at'] = $template->datetime;

                    if($template->deleted == 1)
                    {
                        $new_template['deleted_at'] = date("Y-m-d H:i:s");
                    }

                    /* replace old module template items */
                    $template->html = str_replace("<!--", "{{", $template->html);
                    $template->html = str_replace("-->", "}}", $template->html);
                    $new_template['html'] = $template->html;

                    /* Store for adding */
                    $new_template->save();

                    Activitylog::log([
                        'action' => 'CREATED',
                        'type' => get_class(new Moduletemplate),
                        'link_id' => $new_template->id,
                        'description' => 'Created Module template',
                        'notes' => "System created the module template &quot;" . $new_template->name . " &quot;"
                    ]);
                }
            }

            /* Insert and Drop */
            Schema::dropIfExists('old_moduletemplates');
        } 

        return Redirect::to(General::backend_url() . '/module-templates/');
    }

}
