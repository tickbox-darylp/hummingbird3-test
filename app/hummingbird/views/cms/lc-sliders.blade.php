@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    @if (count($breadcrumbs) > 0)
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <?php $i = 0;?>
                    @foreach ($breadcrumbs as $breadcrumb)
                        <?php $i++;?>
                        <li>
                        @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                            <a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                        @else
                            @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                        @endif
                        </li>
                    @endforeach
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
    @endif
@stop

@section('content')

    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('error'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    {{ Session::get('error') }}
                </div>
            </div>
        </div>  
    @endif

    @if(isset($errors) AND count($errors) > 0)
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    @foreach ($errors->all('<li>:message</li>') as $error)
                        {{$error}}
                    @endforeach
                </div>
            </div>
        </div> 
    @endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel" style="background-color:white;padding:20px;">
                <h1 class="pull-left normal">Sliders</h1>

                @if(Auth::user()->hasAccess('create', get_class(new LCSlider)))
                    <button type="button" class="pull-right btn btn-info" data-toggle="modal" data-target="#add-lc-slider"><i class="fa fa-plus-circle"></i> Add Slider</button>
                @endif

                @if(count($sliders) > 0)
                    <div class="row clearfix" style="margin-top:10px;">
                        <div class="col-md-12"> 
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($sliders as $slider)
                                            <tr>
                                                <td>{{$slider->title}}</td>
                                                <td>{{$slider->description}}</td>
                                                <td>
                                                    @if(Auth::user()->hasAccess('update', get_class(new LCSlider)))<a href='/{{App::make('backend_url')}}/lc-sliders/edit/{{$slider->id}}' class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>@endif
                                                    @if(Auth::user()->hasAccess('delete', get_class(new LCSlider)))<a href='/{{App::make('backend_url')}}/lc-sliders/delete/{{$slider->id}}' class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>@endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="text-center"> 
                                @if(null === Input::get('s') AND Input::get('s') == '')
                                    {{ $sliders->appends(Request::except('page'))->links(); }}
                                @endif
                            </div>
                        </div>
                    </div>
                @else
                    <div class="row clearfix">
                        <div class="col-md-12" style="margin-top:10px;">
                            <div class="alert alert-block alert-info text-center">No sliders have been created.</div>
                        </div>
                    </div>
                @endif
            </section>
        </div>
    </div>

    @if(Auth::user()->hasAccess('create', get_class(new LCSlider)))
        <div class="modal fade" id="add-lc-slider">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add new slider</h4>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12"> 
                                {{ Form::open(array('url' => '/'.General::backend_url().'/lc-sliders/add/', 'class' => 'form-horizontal')) }}
                                    <div class="form-group">
                                        {{ Form::label('title', 'Title: ', array('class' => 'col-sm-3')) }}
                                        <div class="col-sm-9">
                                            {{ Form::text('title', '', array('class' => 'form-control')) }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="description" class="col-sm-3">Description:</label>
                                        <div class="col-sm-9">
                                            <textarea name="description" id="description" class="form-control textareas"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-10">
                                            <button type="submit" class="btn btn-default btn-primary">Add slider</button>
                                        </div>
                                    </div> 
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif

@stop

@section('scripts')
    <script>
        $(document).ready(function()
        {
            if($('.form-control.textareas').length > 0)
            {
                $(".form-control.textareas").autogrow();
            }
        });
    </script>
@stop
