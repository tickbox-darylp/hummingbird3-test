@extends('HummingbirdBase::install.layout')

@section('content')
<div class='step'>
    <h2>Step {{$step}}</h2>
    <p>Database credentials</p>
    <form method='post' action=''>
        <fieldset>
            <label>Database Host:</label>
            <input type="text" name='db_host' placeholder="localhost or 127.0.0.1" value="{{Input::old('db_host')}}">
            {{$errors->first('db_host')}}
        </fieldset>
        <fieldset>
            <label>Database Name:</label>
            <input type="text" name='db_name' placeholder="YOUR_DATABASE_NAME" value="{{Input::old('db_name')}}">
            {{$errors->first('db_name')}}
        </fieldset>
        <fieldset>
            <label>Database User:</label>
            <input type="text" name='db_user' placeholder="YOUR_DATABASE_USER" value="{{Input::old('db_user')}}">
            {{$errors->first('db_user')}}
        </fieldset>
        <fieldset>
            <label>Password:</label>
            <input type="password" name='db_password' placeholder="" value="">
            {{$errors->first('db_password')}}
        </fieldset>
        <fieldset>
            <label>Prefix to tables:</label>
            <input type="text" name='db_prefix' placeholder="hb_" value="{{Input::old('db_prefix')}}">
            {{$errors->first('db_prefix')}}
        </fieldset>
        
        <input type='submit' value='Continue' />
    </form>
</div>
@stop
