<?php

/*
 * Parent class to organise all seed installs
 */

abstract class HbSeeder extends Seeder
{
    protected $PermissionManager;
    
    public abstract function add_seed_data();    
    public abstract function add_cmsnav_items();
    public abstract function register_permissions();
    public abstract function register_widgets();
    public abstract function register_modules();
    public abstract function register_blocks();

    public function run()
    {
        // $this->PermissionManager = new \Hummingbird\Libraries\PermissionManager();

        $this->add_seed_data();
        $this->add_cmsnav_items();
        $this->register_permissions();
        $this->register_widgets();
        $this->register_modules();
        $this->register_blocks();
    }

}

?>
