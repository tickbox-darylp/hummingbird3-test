<?php 

/**
 * Authentication checker and manager
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */
class AuthenticationManager
{
    /**
     * @var array List of registered items.
     */
    protected $menu_items;

    protected $backend_url;

    /* construct main menu item */
    public function __construct()
    {
        $this->menu_items = array(
            '' => array(
                'Dashboard' => array(
                    'label' => 'Dashboard',
                    'link' => General::backend_url(),
                    'icon' => 'fa fa-home'
                )
            ),
            'content' => array(
                'Pages' => array(
                    'label' => 'Pages',
                    'link' => General::backend_url() . '/pages/',
                    'icon' => 'fa fa-edit'
                ),
                'Template Manager' => array(
                    'label' => 'Template Manager',
                    'link' => General::backend_url() . '/templates-new/',
                    'icon' => 'fa fa-edit'
                ),
                'Media' => array(
                    'label' => 'Manage Media',
                    'link' => General::backend_url() . '/media/',
                    'icon' => 'fa fa-image',
                    'children' => array(
                        1 => array(
                            'label' => 'Galleries',
                            'link' => '#',
                            'icon' => ''
                        )
                    )
                )
            ),
            'users' => array(
                'Users' => array(
                    'label' => 'Users',
                    'link' => General::backend_url() . '/users/',
                    'icon' => 'fa fa-users'
                ),
                'Roles' => array(
                    'label' => 'Roles',
                    'link' => General::backend_url() . '/roles/',
                    'icon' => 'fa fa-eye-slash'
                )
            ),
            'settings' => array(
                'Website Settings' => array(
                    'label' => 'Website Settings',
                    'link' => General::backend_url() . '/settings/',
                    'icon' => 'fa fa-gears'
                ),
                'Error Logs' => array(
                    'label' => 'Error Log',
                    'link' => General::backend_url() . '/errors/',
                    'icon' => 'fa fa-warning'
                ),
                'Redirects' => array(
                    'label' => 'Redirects',
                    'link' => General::backend_url() . '/redirections/',
                    'icon' => 'fa fa-repeat'
                ),
                'Activity' => array(
                    'label' => 'Activity Logs',
                    'link' => General::backend_url() . '/activity-logs/',
                    'icon' => 'fa fa-history'
                )
            )
        );

        $this->backend_url = General::backend_url();
    }

    /* return list items */
    public function getMenuItems()
    {
        return $this->menu_items;
    }

    /* check active state */
    public function itemIsActive($item)
    {
    }

    /* additem to list */
    public function addToList($type = '', $item)
    {
        if(!isset($this->menu_items[$type][$item['name']]))
        {
            $this->menu_items[$type][$item['name']] = array
            (
                'label' => (isset($item['label'])) ? $item['label']:$item['name'],
                'link' => $this->formatURL($item['link']),
                'icon' => $item['icon']
            );
        }
    }

    /* user has access to navigation item */
    public function userHasAccess($user, $item, $action = 'read')
    {
    }

    public function formatURL($url)
    {
        return $this->backend_url . '/' . $url;
    }
}

?>