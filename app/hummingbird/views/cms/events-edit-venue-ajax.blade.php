<div id="pane_venue{{$venue}}" class="tab-pane">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" id="venue{{$venue}}-tabs">
        @if($tmp_venue)
            <li role="presentation" class="active"><a href="#new_venue{{$venue}}" data-toggle="tab">Venue Details</a></li>
        @endif

        <li role="presentation" @if(!$tmp_venue) class="active" @endif><a href="#contact_venue{{$venue}}" data-toggle="tab">Contact</a></li>
        <li role="presentation"><a href="#dates_venue{{$venue}}" data-toggle="tab">Dates</a></li>
        @if($event->ticketsLive())<li role="presentation"><a href="#tickets_venue{{$venue}}" data-toggle="tab">Tickets</a></li>@endif
        <li role="presentation"><a href="#booking_venue{{$venue}}" data-toggle="tab">Booking &amp; Information</a></li>
        <li role="presentation" class="pull-right"><a href="#remove" data-id="{{$venue}}" class="remove-venue btn-xs btn btn-danger"><i class="fa fa-trash"></i> Remove</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        @if($tmp_venue)
            <input type="hidden" name="new_venues[]" value="{{$venue}}" />

            <div class="tab-pane active" id="new_venue{{$venue}}">
                <div class="form-group">
                    <label for="new_venue_name{{$venue}}" class="col-sm-2 control-label">Venue Name:</label>
                    <div class="col-sm-10">
                        <input name="new_venue_name{{$venue}}" id="new_venue_name{{$venue}}" type="text" class="form-control" placeholder="Venue name" value="{{$venue}}">
                    </div>
                </div>

                <div class="form-group ">
                    <label for="address_line_1" class="col-sm-2 control-label">Use venue address:</label>
                    <div class="col-sm-2">
                        <select name="event_address" id="event_address" class="form-control text-centre">
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                    </div>
                </div>                        

                <div class="form-group  event_address">
                    <label for="address_1{{$venue}}" class="col-sm-2 control-label">Address Line 1:</label>
                    <div class="col-sm-10">
                        <input name="address_1{{$venue}}" id="address_1{{$venue}}" type="text" class="form-control" placeholder="First line of the address" value="">
                    </div>
                </div>

                <div class="form-group  event_address">
                    <label for="address_2{{$venue}}" class="col-sm-2 control-label">Address Line 2:</label>
                    <div class="col-sm-10">
                        <input name="address_2{{$venue}}" id="address_2{{$venue}}" type="text" class="form-control" placeholder="Second line of the address" value="">
                    </div>
                </div>

                <div class="form-group  event_address">
                    <label for="address_3{{$venue}}" class="col-sm-2 control-label">Address Line 3:</label>
                    <div class="col-sm-10">
                        <input name="address_3{{$venue}}" id="address_3{{$venue}}" type="text" class="form-control" placeholder="Third line of the address" value="">
                    </div>
                </div>

                <div class="form-group  event_address">
                    <label for="town{{$venue}}" class="col-sm-2 control-label">Town/City:</label>
                    <div class="col-sm-10">
                        <input name="town{{$venue}}" id="town{{$venue}}" type="text" class="form-control" placeholder="E.g Bristol" value="">
                    </div>
                </div>

                <div class="form-group  event_address">
                    <label for="postcode{{$venue}}" class="col-sm-2 control-label">Postcode:</label>
                    <div class="col-sm-10">
                        <input name="postcode{{$venue}}" id="postcode{{$venue}}" type="text" class="form-control" placeholder="E.g NS12 1SW" value="">
                    </div>
                </div>

                <div class="form-group  event_address">
                    <label for="country{{$venue}}" class="col-sm-2 control-label">Country:</label>
                    <div class="col-sm-10">
                        <input name="country{{$venue}}" id="country{{$venue}}" type="text" class="form-control text-uppercase" placeholder="UNITED KINGDOM" value="">
                    </div>
                </div>
            </div>
        @endif

        <div class="tab-pane @if(!$tmp_venue) active @endif" id="contact_venue{{$venue}}">
            <div class="form-group">
                <label for="contact_venue{{$venue}}" class="col-sm-2 control-label">Contact Name:</label>
                <div class="col-sm-10">
                    <input name="contact_venue{{$venue}}" id="contact_venue{{$venue}}" type="text" class="form-control" placeholder="Event contact name" value="">
                </div>
            </div>

            <div class="form-group">
                <label for="email_venue{{$venue}}" class="col-sm-2 control-label">Contact Email:</label>
                <div class="col-sm-10">
                    <input name="email_venue{{$venue}}" id="email_venue{{$venue}}" type="text" class="form-control" placeholder="Event email" value="">
                </div>
            </div>

            <div class="form-group">
                <label for="telephone_venue{{$venue}}" class="col-sm-2 control-label">Contact Telephone:</label>
                <div class="col-sm-10">
                    <input name="telephone_venue{{$venue}}" id="telephone_venue{{$venue}}" type="text" class="form-control" placeholder="Event telephone" value="">
                </div>
            </div>

            <div class="form-group">
                <label for="website_venue{{$venue}}" class="col-sm-2 control-label">Contact Website:</label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon">http://</span>
                        <input name="website_venue{{$venue}}" id="website_venue{{$venue}}" type="text" class="form-control" placeholder="Event website" value="">
                    </div>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="dates_venue{{$venue}}">
            <div class="date-ranges col-sm-12 clearfix">
                <input type="hidden" id="current_dates_venue{{$venue}}" name="current_dates_venue{{$venue}}" value="" />
                <input type="hidden" id="new_dates_venue{{$venue}}" name="new_dates_venue{{$venue}}" value="" />
                <input type="hidden" id="remove_dates_venue{{$venue}}" name="remove_dates_venue{{$venue}}" value="" />

                <div class="form-group regular-events-group">
                    <label for="frequency" class="col-sm-2 control-label"></label>
                    <div class="checkbox col-sm-10">
                        <label>
                            <input name="frequency_venue{{$venue}}" type="hidden" value="0" />
                            <input name="frequency_venue{{$venue}}" type="checkbox" value="1" class="regular-event"> Event is on regular dates
                        </label>
                    </div>
                </div>

                <div class="regular-events hide">
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Repeats:</label>
                        <div class="col-sm-10">
                            <select name="frequency_occurance_venue{{$venue}}" class="form-control frequency_occurance">
                                <option value="daily" selected="selected">Daily/Weekly</option>
                                <option value="custom">Custom</option>
                            </select>
                        </div>
                    </div>

                    <?php $times = DateTimeHelper::get_time_increments($event->time_increments, true);
                    $days = DateTimeHelper::get_days();
                    ?>

                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Days:</label>
                        <div class="col-sm-10">
                            @foreach($days as $key => $day)
                                <label class="checkbox-inline">
                                    <input class="frequency_occurance_bydays" type="checkbox" name="frequency_occurance_bydays_venue{{$venue}}[]" value="{{$key}}" data-name="{{$day['short']}}"> {{$day['short']}}
                                </label>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group regular-custom hide">
                        <label for="description" class="col-sm-2 control-label">Days/Times:</label>
                        <div class="col-sm-10 days">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Day</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @for($i = 1; $i <= 7; $i++)
                                        <tr class="{{$days[$i]['short']}} hide">
                                            <td>{{$days[$i]['short']}}</td>

                                            <td>{{ Form::select('start_time_venue'.$venue.'_'.$i, $times, '', array('class' => 'form-control')) }}</td>
                                            <td>{{ Form::select('finish_time_venue'.$venue.'_'.$i, $times, '', array('class' => 'form-control')) }}</td>
                                            <td><input class="frequency_all_day" type="checkbox" name="frequency_all_day_venue{{$venue}}[]" value="{{$i}}"> All day</td>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-6 nodays">
                            <div class="alert alert-info text-center">Please select some days</div>
                        </div>
                    </div>

                    <div class="form-group regular-daily-weekly">
                        <label for="start_date" class="col-sm-2 control-label">Dates:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                <input name="regular_start_date_venue{{$venue}}" id="regular_start_date_venue{{$venue}}" type="text" readonly="readonly" class="datepicker datepicker-start_date form-control">

                                <span class="input-group-addon" style="border-left:0;border-right:0;">- to - </span>
                                
                                <input name="regular_finish_date_venue{{$venue}}" id="regular_finish_date_venue{{$venue}}" type="text" readonly="readonly" class="datepicker timepicker-finish_date form-control">
                                
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group regular-daily-weekly">
                        <label for="start_date" class="col-sm-2 control-label">Times:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                <input name="regular_start_date_time_venue{{$venue}}" id="regular_start_date_time_venue{{$venue}}" type="text" readonly="readonly" class="timepicker timepicker-start_date form-control">

                                <span class="input-group-addon" style="border-left:0;border-right:0;">- to - </span>
                                
                                <input name="regular_finish_date_time_venue{{$venue}}" id="regular_finish_date_time_venue{{$venue}}" type="text" readonly="readonly" class="timepicker timepicker-finish_date form-control">
                                
                                <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group regular-daily-weekly">
                        <label for="regular_event_duration_venue{{$venue}}" class="col-sm-2 control-label">Event Duration:</label>
                        <div class="col-sm-10">
                            <input name="regular_event_duration_venue{{$venue}}" id="regular_event_duration_venue{{$venue}}" type="text" class="form-control" size="8" maxlength="8" value="0" placeholder="0" />
                            <span class="help-block">Number of days the event spans</span>
                        </div>
                    </div>
                </div>

                <h5 class="event-single">Add Date:</h5>

                <div class="form-group event-single" style="margin-bottom:30px;">
                    <div class="col-sm-9">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            <input name="start_date_venue{{$venue}}" id="start_date_venue{{$venue}}" type="text" readonly="readonly" class="datetimepicker datetimepicker-venue form-control" placeholder="Event tickets" value="">

                            <span class="input-group-addon" style="border-left:0;border-right:0;">- to - </span>
                            
                            <input name="finish_date_venue{{$venue}}" id="finish_date_venue{{$venue}}" type="text" readonly="readonly" class="datetimepicker datetimepicker-venue form-control" placeholder="Event tickets" value="">
                            
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <a href="#" class="event-add-date btn btn-success rounded" data-venue="{{$venue}}" data-event="{{$event->id}}"><i class="fa fa-plus"></i></a>
                    </div>
                </div>

                <div class="event-single">
                    <h5>Selected Dates</h5>

                    <p class="no-dates">No dates</p>

                    <table class="hide date-table table table-striped table-hover">
                        <thead>
                            <th>Start</th>
                            <th>Finish</th>
                            <th>Remove?</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>            
            </div>
        </div>

        @if($event->ticketsLive())
            <div class="tab-pane" id="tickets_venue{{$venue}}">
                <div class="form-group">
                    <div class="checkbox col-sm-10">
                        <label>
                            <input name="registrations" type="hidden" value="0" />
                            <input name="registrations" type="checkbox" value="1" class="registrations"> Enable registrations for this event:
                        </label>
                    </div>
                </div>                  

                <div class="form-group show-tickets hide">
                    <label for="cost" class="col-sm-2 control-label">Costs:</label>
                    
                    <div class="col-sm-10">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Ticket Name</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td>Standard</td>
                                    <td>Standard admission</td>
                                    <td>&pound;1.99</td>
                                    <td>
                                        <a class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="pull-right">
                            <a href="#" class="add-ticket btn btn-xs btn-success"><i class="fa fa-plus"> Add ticket</i></a>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="tab-pane" id="booking_venue{{$venue}}">
            <div class="form-group">
                <label for="booking_venue{{$venue}}" class="col-sm-2 control-label">Booking:</label>
                <div class="col-sm-10">
                    {{ Form::select('booking_venue'.$venue, array('' => 'No', 'external' => 'External'), '0', array('class' => 'show_book_link form-control', 'id' => 'booking_venue$venue')) }}
                </div>
            </div>

            <div class="form-group hide show-booking-external">
                <label for="booking_external_venue{{$venue}}" class="col-sm-2 control-label">Link:</label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon">http://</span>
                        <input name="booking_external_venue{{$venue}}" id="booking_external_venue{{$venue}}" type="text" class="form-control" placeholder="URL" value="">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="booking_info_venue{{$venue}}" class="col-sm-2 control-label">Information:</label>
                <div class="col-sm-10">
                    {{ Form::textarea('booking_info_venue'.$venue, '', array('class' => 'form-control', 'rows' => '3')) }}
                    <span class="help-block">Any event booking information will be displayed above any payment or registrations</span>
                </div>
            </div>
        </div>
    </div>
</div>