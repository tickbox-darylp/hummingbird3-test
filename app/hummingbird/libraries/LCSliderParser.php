<?php 

/**
 * Hummingbird3 Slider Parser
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */
class LCSliderParser
{
    protected $slider;
    protected $slides;
    protected $valid = FALSE;

    /* Templates */
    protected $templates_field_pattern = '#\[(.*?)\]#';
    protected $templates_loaded = array();
    protected $templates = array(
        1 => array(
            'standard' => '/lutonculture/sliders/1.htm',
            'list' => '/lutonculture/sliders/1[regions-list].htm',
        ),
        2 => array(
            'structure' => '/lutonculture/sliders/2.htm',
            'standard' => '/lutonculture/sliders/2[regions].htm',
            'list' => '/lutonculture/sliders/5[regions-list].htm',
            'region-1' => '/lutonculture/sliders/2[region-1].htm'
        ),
        3 => array(
            'structure' => '/lutonculture/sliders/3.htm',
            'standard' => '/lutonculture/sliders/3[regions].htm',
            'region-1' => '/lutonculture/sliders/3[region-1].htm'
        ),
        4 => '/lutonculture/sliders/4.htm',
        5 => array(
            'structure' => '/lutonculture/sliders/5.htm',
            'standard' => '/lutonculture/sliders/5[regions].htm',
            'list' => '/lutonculture/sliders/5[regions-list].htm',
            'region-1' => '/lutonculture/sliders/5[region-1].htm'
        ),
        'full' => '/lutonculture/sliders/full.htm'
    );

    /* Counting */
    public $counter = 0;

    /* Event Details */
    public $event_counter = 0;
    public $events_used = array();
    public $event_url = '/whats-on/whats-on/view/';

    /* Output */
    public $render;
    public $data;
    public $ignoreSlides = array();
    public $num_slides = 0;

    public function __construct($slider, $render = TRUE)
    {
        if(!class_exists('Events')) throw new Exception('Hummingbird requires HB:Events to be installed.');

        /* Initialise */
        $this->render = $render;

        /* The slider object */
        $this->slider = LCSlider::find($slider);

        /* Slider is valid */
        if(null !== $this->slider)
        { 
            $this->valid = TRUE;

            /* Slider data */
            $this->slides = $this->slider->slider_data;

            /* Load page templates */
            $this->preloadTemplates();

            /* Parse content */
            $this->parse();

            /* Loaded slides */
            $this->num_slides = count($this->slides) - count($this->ignoreSlides);
        }
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function isValid()
    {
        return $this->valid;
    }


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function getSlider()
    {
        return $this->slider;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function getSlides()
    {
        return $this->slides;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function getTemplate($key)
    {
        if(isset($this->templates_loaded[$key])) return $this->templates_loaded[$key];

        return;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function getChildTemplates($template)
    {
        if(is_array($template)) return TRUE;

        return FALSE;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function setSlides($render = TRUE)
    {
        $this->render = $render;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function setTemplate($template, $template_id, $region_id, $key = NULL)
    {
        /* No array - return main template */
        if(!is_array($template)) return $template;

        /* See if a preset key is available */
        if(null !== $key AND isset($template[$key])) return $template[$key];

        /* see if region id is set for template */
        if(isset($template[$region_id])) return $template[$region_id];

        /* Return standard structure */
        if(isset($template['standard'])) return $template['standard']; 
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function preloadTemplates()
    {
        if(is_array($this->templates) AND count($this->templates) > 0)
        {
            foreach($this->templates as $template_key => $location)
            {
                if(is_array($location))
                {
                    foreach($location as $item_key => $item)
                    {
                        $this->templates_loaded[$template_key][$item_key] = $this->loadTemplate_ByLocation($item);
                    }
                }
                else
                {
                    $this->templates_loaded[$template_key] = $this->loadTemplate_ByLocation($location);
                }
            }
        }
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function loadTemplate_ByLocation($location)
    {
        $filename = base_path() . General::theme_path() . $location;

        if(File::exists($filename))
        {
            try
            {
                return File::get($filename);
            }
            catch (Illuminate\Filesystem\FileNotFoundException $exception)
            {
                die("The file doesn't exist");
            }
        }
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function addEventToIgnoreList($id)
    {
        if(!isset($id) OR !is_numeric($id) OR $id <= 0) return;

        if(!in_array($id, $this->events_used)) $this->events_used[] = $id;
    }

    

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function parse()
    {
        foreach($this->slides as $key => $slide)
        {
            if(!isset($slide['enabled']) OR $slide['enabled'] == 0)
            {
                $this->ignoreSlides[] = $key;
                continue;
            }

            if(!isset($this->slides[$key]['html'])) $this->slides[$key]['html'] = array();

            $has_regions = ($slide['template'] != 'full') ? TRUE:FALSE; //used for detecting 

            if(isset($this->tmp_data)) unset($this->tmp_data); //remove all temporary data (if set)

            /* Load template - DO NOT OVERWRITE LOADED TEMPLATE */
            $MASTER_TEMPLATE = $this->getTemplate($slide['template']);
            $TEMPLATES = $this->getChildTemplates($MASTER_TEMPLATE);

            /* Iterate through each region */
            foreach($slide['regions'] as $region_id => $region)
            {
                $use_slide = TRUE;
                $template = $this->setTemplate($MASTER_TEMPLATE, $slide['template'], $region_id);

                if($region['type'] == 'custom')
                {
                    /* Standard content */
                    if(isset($region['title']) AND $region['title'] != '') $template = str_replace("[title]", $region['title'], $template);
                    if(isset($region['summary']) AND $region['summary'] != '') $template = str_replace("[summary]", $region['summary'], $template);

                    /* Check link */
                    if(isset($region['link']) AND $region['link'] != '')
                    {
                        $template = str_replace("[link]", $region['link'], $template);
                    }

                    /* Check image(s) */
                    if(isset($region['image']) AND $region['image'] != '' AND File::exists(base_path() . $region['image']))
                    {
                        $template = str_replace("[image]", $region['image'], $template);
                    }

                    // if($has_regions) $this->slides[$key]['html'][$region_id] = $template;
                    $this->slides[$key]['html'][$region_id] = $template;
                }
                else if($region['type'] == 'events')
                {
                    $event_counter = 0;
                    $limit = 1;

                    if(isset($region['regions']) AND count($region['regions']) > 0)
                    {
                        $limit += count($region['regions']); //minus regions with "lists"

                        if(isset($slide['appearance']))
                        {
                            // $limit -= count($slide['appearance']);
                        }
                    }

                    /* Check if the appearance is list mode - if so get some more event */
                    if(isset($slide['appearance']) AND count($slide['appearance']) > 0)
                    {
                        foreach($slide['appearance'] as $item_app)
                        {
                            $limit += (is_numeric($item_app->limit) AND $item_app->limit > 0) ? $item_app->limit:5;
                        }
                    }

                    $events_to_use = $this->queryEvents($region, $limit);

                    if($events_to_use !== NULL AND count($events_to_use) > 0)
                    {   
                        $event = FALSE;
                        $total_regions = (isset($region['regions'])) ? count($region['regions']):0; //number of regions to iterate

                        while($event !== NULL)
                        {
                            /* Go through each slide region */
                            for($i = 1; $i <= $total_regions + 1; $i++)
                            {
                                /* looping through regions - 1 > 2 > n+1 */
                                $template = $MASTER_TEMPLATE; //reset template

                                if($has_regions)
                                {
                                    /* Change template if dealing with children - precautionary */
                                    $region_index = $i;
                                    $region_key = ($region_index == 1) ? $region_id:$region['regions'][$region_index-2];
                                    $template = $this->setTemplate($MASTER_TEMPLATE, $slide['template'], $region_key);
                                }

                                /* Advanced listing events */
                                if(isset($slide['appearance']) AND isset($slide['appearance']->$region_key))
                                {   
                                    $this_list = ''; //reset html
                                    $this_html = $this->setTemplate($MASTER_TEMPLATE, $slide['template'], $region_key, 'list'); //load template
                                    
                                    if(isset($slide['appearance']->$region_key->title) AND $slide['appearance']->$region_key->title != '') $this_list .= '<h3 style="margin-top:0;">'.$slide['appearance']->$region_key->title.'</h3>';
                                    if(isset($slide['appearance']->$region_key->summary) AND $slide['appearance']->$region_key->summary != '') $this_list .= '<p>'.$slide['appearance']->$region_key->summary.'</p>';

                                    /* While we have not reached our level */
                                    $event_counter_new = 0;
                                    while($event_counter_new < $slide['appearance']->$region_key->limit AND $event !== NULL)
                                    {
                                        $event = (isset($events_to_use[$event_counter])) ? $events_to_use[$event_counter]:NULL;

                                        if(null === $event) continue;

                                        /* Build link */
                                        $event_url = $this->event_url . DateTimeHelper::date_format($event->start_date, 'Y/m/d') . '/' . $event->url . '/';
                                        $event_url .= (isset($event->event_id)) ? $event->event_id:$event->id;
                                        $event_url .= '/';

                                        /* Add to html */
                                        $this_list .= '<p><a href="'.$event_url.'">' . $event->title.'</a></p>';

                                        /* Increment counter */
                                        $event_counter++;
                                        $event_counter_new++;
                                    }

                                    /* Replace [list] - if set */
                                    $this_html = (strpos($this_html, "[list]") !== FALSE) ? str_replace("[list]", $this_list, $this_html):$this_list;
                                    
                                    /* Set html */
                                    $this->slides[$key]['html'][$region_key] = $this_html; //use the region ID as a sorter!
                                }
                                else
                                {
                                    /* Standard event - content */
                                    if($limit > 1)
                                    {
                                        $event = (isset($events_to_use[$event_counter])) ? $events_to_use[$event_counter]:NULL;
                                    }
                                    else
                                    {
                                        $event = $events_to_use->first();
                                    }

                                    if(null === $event) continue;

                                    /* Not null - so continue */
                                    if(isset($event->title) AND $event->title != '') $template = str_replace("[title]", $event->title, $template);
                                    if(isset($event->summary) AND $event->summary != '') 
                                    {
                                        $template = str_replace("[summary]", $event->summary, $template);
                                    }
                                    else
                                    {
                                        /* Use main description */
                                        $template = str_replace("[summary]", substr(strip_tags($event->description), 0, 255), $template);
                                    }

                                    /* Build link */
                                    $event_url = $this->event_url . DateTimeHelper::date_format($event->start_date, 'Y/m/d') . '/' . $event->url . '/';
                                    $event_url .= (isset($event->event_id)) ? $event->event_id:$event->id;
                                    $event_url .= '/';
                                    
                                    $template = str_replace("[link]", $event_url, $template);

                                    /* Find Image */
                                    $template = str_replace("[image]", General::findImage($event, array('featured_image', 'search_image', 'description')), $template);

                                    /* If it has these fields */
                                    if(strpos($template, '[category]') !== false)
                                    {
                                        $found_cat = FALSE;
                                        $categories = $event->taxonomy();
                                        $category_name = '';

                                        if(count($categories) > 0)
                                        {
                                            while(!$found_cat)
                                            {
                                                foreach($categories as $category)
                                                {
                                                    $__category = Categories::whereNull('deleted_at')->where('id', $category)->first();

                                                    if(null !== $__category)
                                                    {
                                                        $category_name = $__category->name;
                                                        $found_cat = TRUE;
                                                    }
                                                }

                                                $found_cat = TRUE;
                                            }
                                        }

                                        $template = str_replace("[category]", $category_name, $template);
                                    }

                                    if(strpos($template, '[date]') !== false)
                                    {
                                        $template = str_replace("[date]", DateTimeHelper::date_format($event->start_date, 'l j F'), $template);
                                    }

                                    if(strpos($template, '[price]') !== false)
                                    {
                                        $price = $event->cost;
                                        $price = ($price == '') ? 'FREE':'&pound;'.$price;
                                        $template = str_replace("[price]", $price, $template);
                                    }

                                    /* Add to to ignore list if not to use on "ignore_dupes" */
                                    $this->addEventToIgnoreList($event->id);
                                    
                                    $event_counter++;

                                    if($has_regions)
                                    {
                                        $this->slides[$key]['html'][$region_key] = $template; //use the region ID as a sorter!
                                    }
                                    else
                                    {
                                        $this->slides[$key]['html'][$region_id] = $template; //use the region ID as a sorter!
                                    }
                                }
                            }

                            break;
                        }
                    }
                    else
                    {
                        /* Hide this slide */
                        // $use_slide = FALSE;
                        $this->ignoreSlides[] = $key;
                    }
                }

                if($use_slide)
                {
                    // if(!$has_regions AND !isset($this->slides[$key]['html'])) $this->slides[$key]['html'] = '';
                    // if(!$has_regions) $this->slides[$key]['html'] .= $template;
                    // $this->slides[$key]['html'][$region_id] = $template; //use the region ID as a sorter!
                }
                else
                {
                    // $this->ignoreSlides[] = $key;
                }
            }

            /* render if automatic */
            if($this->render) $this->render($this->slides[$key]);
        }
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function queryEvents($event_info, $event_limit = 1)
    {
        if(!isset($event_info['venue']) OR $event_info['venue'] <= 0)
        {
            /* Set up base events */
            $events = Events::whereNull('deleted_at')->future();
        }
        else
        {
            /* Using a venue separator? */
            $events = Events::whereNull('events.deleted_at')->future();
            $events->leftJoin('events_dates', 'events_dates.event_id', '=', 'events.id')
                ->distinct('events_dates.event_id AS event__id, events_dates.id AS event_date_id')
                ->where('venue_id', '=', $event_info['venue']);
        }

        /* Switch the select */
        switch($event_info['select'])
        {
            case 'custom':
                if(count($event_info['events']) > 0) $events->whereIn('events.id', $event_info['events']);
                break;
            case 'filter':
                if($event_info['filter_by'] == 'important')
                {
                    //important events only
                    $events->where('importance', '=', 1);
                }

                if($event_info['filter_by'] == 'categories')
                {
                    if(count($event_info['categories']) > 0)
                    {
                        $categories = $event_info['categories']; //set categories

                        $events->whereHas('taxonomy_relationships', function($query) use ($categories)
                        {
                            return $query->where(function($query) use ($categories)
                            {
                                foreach($categories as $category)
                                {
                                    if(is_numeric($category))
                                    {
                                        $query->orWhere('taxonomy_relationships.term_id', '=' ,$category); 
                                    }
                                }
                            });
                        });
                    }
                }
                else if($event_info['filter_by'] == 'tags')
                {
                    if(isset($event_info['tags']) AND count($event_info['tags']) > 0)
                    {
                        $tags = $event_info['tags']; //set categories
                        
                        $events->whereHas('taxonomy_relationships', function($query) use ($tags)
                        {
                            return $query->where(function($query) use ($tags)
                            {
                                foreach($tags as $tag)
                                {
                                    if(is_numeric($tag))
                                    {
                                        $query->orWhere('taxonomy_relationships.term_id', '=' ,$tag); 
                                    }
                                }
                            });
                        });
                    }
                }
                else if($event_info['filter_by'] == 'dates')
                {
                    $from_date = (isset($event_info['dates']['dates_from'])) ? strtotime($event_info['dates']['dates_from']):null;
                    $to_date = (isset($event_info['dates']['dates_to'])) ? strtotime($event_info['dates']['dates_to']):null;

                    $events->where(function($query) use ($from_date, $to_date)
                    {  
                        $query->future($from_date, $to_date);
                    });
                }
        }

        /* Ignore events that have already been output */
        if($event_info['ignore_dupes'])
        {
            if(count($this->events_used) > 0) $events->whereNotIn('events.id', $this->events_used);
        }

        /* Order By settings */
        
        switch($event_info['order_by'])
        {
            case 'random':
                $events->orderBy(DB::raw('RAND()'));
                break;
            case 'id_ASC':
            default:
                $events->orderBy('events.id', 'ASC');
                break;
            case 'start_date_ASC':
                $events->orderBy('events.start_date', 'ASC');
                break;
            case 'finish_date_ASC':
                $events->orderBy('events.finish_date', 'ASC');
                break;
            case 'id_DESC':
                $events->orderBy('events.id', 'DESC');
                break;
            case 'start_date_DESC':
                default:
                $events->orderBy('events.start_date', 'DESC');
                break;
            case 'finish_date_DESC':
                $events->orderBy('events.finish_date', 'DESC');
                break;
        }

        /* Get the events */
        if($event_limit > 0)
        {
            $events = $events->take($event_limit);
        }

        return $events->get();
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function slideAvailable($key)
    {
        if(in_array($key, $this->ignoreSlides)) return false;

        return true;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function isLastSlide($key)
    {
        return (end($this->slides[$key]));
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function removeEmptyFields($html)
    {
        if($html == '') return $html;

        $fields = array();

        /* get all fields */
        preg_match_all($this->templates_field_pattern, $html, $fields);

        if(count($fields[0]) > 0)
        {
            $html = str_replace($fields[0], "", $html);
        }

        return $html;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function render($slide)
    {
        if(isset($slide['html'])) 
        {
            if(is_array($slide['html']))
            {
                ksort($slide['html']); //sort by indexing keys

                /* Do we have a structure file? */
                $structure = (is_array($this->templates_loaded[$slide['template']]) AND isset($this->templates_loaded[$slide['template']]['structure'])) ? $this->templates_loaded[$slide['template']]['structure']:NULL;

                /* YES we do */
                if($structure !== NULL)
                {
                    $html = $structure;
                    $to_replace = '';

                    foreach($slide['html'] as $region_key => $content)
                    {
                        // region-1
                        if(strpos($html, "[$region_key]") !== FALSE)
                        {
                            $html = str_replace("[$region_key]", $content, $html);
                            continue;
                        }

                        /* standard output - replace */
                        $to_replace .= $content;
                    }

                    if($to_replace != '') $html = str_replace("[regions]", $to_replace, $html);

                    echo $this->removeEmptyFields($html);
                }
                else
                {
                    /* Standard output - just echo */
                    foreach($slide['html'] as $content)
                    {
                        echo $this->removeEmptyFields($content);
                    }
                }

                return;
            }

            echo $this->removeEmptyFields($slide['html']);
        }   
    }
}