<?php

use Illuminate\Support\ServiceProvider;

/*
 * Class that all plugin service providers should inherit from.
 * Used to organise blocks, widgets, modules etc coming from plugins
 */
abstract class HbServiceProvider extends ServiceProvider
{
    public abstract function get_blocks($cms_type); 
}

?>
