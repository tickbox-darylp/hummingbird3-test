<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta http-equiv="Content-Language" content="en" />

        <title>{{$tag}}</title>
        <link rel="icon" href="/themes/hummingbird/default/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="/themes/hummingbird/default/lib/redactor/redactor.css" />

        <!-- Stylesheets -->
        <link href="http://r209.com/templates/moodstrap/style/bootstrap.css" rel="stylesheet"/>
        <!-- Font awesome icon -->
        <link rel="stylesheet" href="/themes/hummingbird/default/css/font-awesome.css"/> 
        <!-- jQuery UI -->
        <link rel="stylesheet" href="http://r209.com/templates/moodstrap/style/jquery-ui-1.9.2.custom.min.css"/> 
        <!-- Calendar -->
        <link rel="stylesheet" href="http://r209.com/templates/moodstrap/style/fullcalendar.css"/>
        <!-- prettyPhoto -->
        <link rel="stylesheet" href="http://r209.com/templates/moodstrap/style/prettyPhoto.css"/>  
        <!-- Star rating -->
        <link rel="stylesheet" href="http://r209.com/templates/moodstrap/style/rateit.css"/>
        <!-- Date picker -->
        <link rel="stylesheet" href="http://r209.com/templates/moodstrap/style/bootstrap-datetimepicker.min.css"/>
        <!-- CLEditor -->
        <link rel="stylesheet" href="http://r209.com/templates/moodstrap/style/jquery.cleditor.css"/> 
        <!-- Uniform 
        <link rel="stylesheet" href="http://r209.com/templates/moodstrap/style/uniform.default.css"/> -->
        <!-- Uniform -->
        <link rel="stylesheet" href="http://r209.com/templates/moodstrap/style/daterangepicker-bs3.css"/>
        <!-- Bootstrap toggle -->
        <link rel="stylesheet" href="http://r209.com/templates/moodstrap/style/bootstrap-switch.css"/>
        <!-- Main stylesheet -->
        <link href="http://r209.com/templates/moodstrap/style/style.css" rel="stylesheet"/>
        <!-- Widgets stylesheet -->
        <link href="http://r209.com/templates/moodstrap/style/widgets.css" rel="stylesheet"/>   
        <!-- Gritter Notifications stylesheet -->
        <link href="http://r209.com/templates/moodstrap/style/jquery.gritter.css" rel="stylesheet"/>   

        <!-- HTML5 Support for IE -->
        <!--[if lt IE 9]>
        <script src="js/html5shim.js"></script>
        <![endif]-->

    </head>
    <body class="bigscreen">
        <header>
            <div class="navbar navbar-fixed-top bs-docs-nav" role="banner">

                <div class="container">

                    <div class="navbar-header">
                        <button class="navbar-toggle btn-navbar" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse"><span>Menu</span></button>
                        <a href="#" class="pull-left menubutton hidden-xs"><i class="fa fa-bars"></i></a>
                        <!-- Site name for smallar screens -->
                        <a href="index.html" class="navbar-brand">Mood<span class="bold">Strap</span></a>
                    </div>

                    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">         

                        <!-- Links -->
                        <ul class="nav navbar-nav pull-right">
                            <li></li>
                        </ul>

                    </nav>

                </div>
            </div>
        </header>
        <div class="content">

            <div class="sidebar">
                <div class="sidebar-dropdown"><a href="#">Navigation</a></div>
                <!-- Search form -->
                <form class="navbar-form" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                            <button class="btn search-button" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                @if (Auth::check())
                @include('cms.include.nav')
                @endif

            </div>
            <div class="mainbar">
                @yield('content')

            </div>
        </div>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Copyright info -->
                        <p class="copy">Copyright © 2013 | <a href="#">Your Site</a> </p>
                    </div>
                </div>
            </div>
        </footer>
        <script type='text/javascript' src="/themes/hummingbird/default/js/jquery-1.11.1.min.js"></script>


        <script src="/themes/hummingbird/default/lib/redactor/redactor.js"></script>
        <script type='text/javascript' src="/themes/hummingbird/default/js/cms_common.js"></script>
        
        <script type="text/javascript">

            $(document).ready(function() {

                if ($('#ckeditor').length > 0) {
                    CKEDITOR.replace('ckeditor');
                }

                $('.section_nav .subnav-section h1 a').click(function(e) {
                    e.preventDefault();

                    $(this).parent().parent().find('h1 > a, ul').toggle();
                });

                $('.redactor').redactor();
            });
        </script>

        @yield('scripts')

        <!-- JS -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/bootstrap.js"></script> <!-- Bootstrap -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/jquery-ui-1.9.2.custom.min.js"></script> <!-- jQuery UI -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/jquery.rateit.min.js"></script> <!-- RateIt - Star rating -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/jquery.prettyPhoto.js"></script> <!-- prettyPhoto -->

        <!-- Morris JS -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/raphael-min.js"></script>
        <script type='text/javascript' src="/themes/hummingbird/default/js/morris.min.js"></script>

        <!-- jQuery Flot -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/excanvas.min.js"></script>
        <script type='text/javascript' src="/themes/hummingbird/default/js/jquery.flot.js"></script>
        <script type='text/javascript' src="/themes/hummingbird/default/js/jquery.flot.resize.js"></script>
        <script type='text/javascript' src="/themes/hummingbird/default/js/jquery.flot.pie.js"></script>
        <script type='text/javascript' src="/themes/hummingbird/default/js/jquery.flot.stack.js"></script>

        <!-- jQuery Notification - Noty -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/jquery.noty.js"></script> <!-- jQuery Notify -->
        <!--<script type='text/javascript' src="/themes/hummingbird/default/js/themes/default.js"></script>  jQuery Notify -->
        <!--<script type='text/javascript' src="/themes/hummingbird/default/js/layouts/bottom.js"></script>  jQuery Notify -->
        <!-- <script type='text/javascript' src="/themes/hummingbird/default/js/layouts/topRight.js"></script> jQuery Notify -->
        <!--<script type='text/javascript' src="/themes/hummingbird/default/js/layouts/top.js"></script>  jQuery Notify -->
        <!-- jQuery Notification ends -->

        <!-- Daterangepicker -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/moment.min.js"></script>
        <script type='text/javascript' src="/themes/hummingbird/default/js/daterangepicker.js"></script>

        <script type='text/javascript' src="/themes/hummingbird/default/js/sparklines.js"></script> <!-- Sparklines -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/jquery.gritter.min.js"></script> <!-- jQuery Gritter -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/jquery.cleditor.min.js"></script> <!-- CLEditor -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/bootstrap-datetimepicker.min.js"></script> <!-- Date picker -->
        <!-- <script type='text/javascript' src="/themes/hummingbird/default/js/jquery.uniform.min.js"></script> jQuery Uniform -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/jquery.slimscroll.min.js"></script> <!-- jQuery SlimScroll -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/bootstrap-switch.min.js"></script> <!-- Bootstrap Toggle -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/filter.js"></script> <!-- Filter for support page -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/custom.js"></script> <!-- Custom codes -->
        <script type='text/javascript' src="/themes/hummingbird/default/js/charts.js"></script> <!-- Charts & Graphs -->


    </body>
</html>