<?php

/*
 *  @author     Aafrin <info@aafrin.com>
 *  @site       www.aafrin.com
 *  @version    1.0
 */

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DatabaseBackup extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup mySQL database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        dd($this->argument());


        $this->info('Database Backup Started');
        dd($this);
        $host = 'localhost';
        $database = getenv('DB_NAME');
        $username = getenv('DB_USER');
        $password = getenv('DB_PASSWORD');
        // $backupPath = base_path() . '/backups/';
        // $backupFileName = $database . "-" . date("Y-m-d-H-i-s") . '.sql';

        //for linux replace the path with /usr/local/bin/mysqldump (The path might varies).
        // $path = "c:\\xampp\mysql\bin\mysqldump";

        //without password
        //$command = $path . " -u " . $username . " " . $database . " > " . $backupPath . $backupFileName;
        //with password
        
        $command =  "/Applications/MAMP/Library/bin/mysqldump -u" . $username . " -p" . $password . " " . $database . " > " . $backupPath;
        system($command);

        echo $command;

        $command = 'gzip -9 ' . $backupPath;
        system($command);

        $this->info('Backup File Created At: ' . $backupPath);
        $this->info('Database Backup Completed');
    }

}