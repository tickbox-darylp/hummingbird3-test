<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MetaContentTable extends Migration 
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('meta_content', function($table) 
        {
            $table->integer('meta_id');
            $table->integer('item_id');
            $table->string('item_type', 255);
            $table->text('key');
            $table->longText('value');
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('meta_content');
	}
}
