@extends('HummingbirdBase::cms.layout')

@section('styles')
    <style>
        .ace-editor.html-structure {min-height:300px;}
        
        .ace-editor.disabled .ace_content:hover {cursor: not-allowed;}
        .ace-editor.disabled .alert {
            z-index: 10;
            position: absolute;
            bottom: 0;
            margin-bottom: 0;
            border-radius: 0;
            width: 100%;
        }

        .ace-editor.disabled .ace_text-layer,
        .ace-editor.disabled .ace_line *,
        .ace-editor.disabled .ace_gutter,
        .ace-editor.disabled .ace_print_margin,
        .ace-editor.disabled .ace_string,
        .ace-editor.disabled .ace_constant.ace_numeric,
        .ace-editor.disabled .ace_keyword,
        .ace-editor.disabled .ace_keyword.ace_operator,
        .ace-editor.disabled .ace_active_line,
        .ace-editor.disabled .ace_selection,
        .ace-editor.disabled .ace_cursor
        {
            color:#999 !important;  
        }
    </style>
@stop

@if(isset($breadcrumbs))
    @include('HummingbirdBase::cms.include.breadcrumbs', array('breadcrumbs' => $breadcrumbs))
@endif

@section('content')
    <div class="row">
        <div class="col-md-12">
            <section class="panel" style="background-color:white;padding:20px;">
                <h3 class="normal">Editing: {{$moduletemplate->name}}</h3>

                {{ Form::open(array('url' => '/'.General::backend_url().'/module-templates/edit/' . $moduletemplate->id, 'class' => 'form-horizontal', 'id' => 'module-submit')) }}
                    <div class="form-group">
                        {{ Form::label('name', 'Name: ', array('class' => 'col-sm-2')) }}
                        <div class="col-sm-6">
                            {{ Form::text('name', $moduletemplate->name, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('notes', 'Notes: ', array('class' => 'col-sm-2')) }}
                        <div class="col-sm-6">
                            <textarea class="form-control textareas" rows="5" name="notes" id="notes" placeholder="Enter description...">{{$moduletemplate->notes}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('html', 'Structure: ', array('class' => 'col-sm-2')) }}
                        <div class="col-sm-10">
                            <span class="help-block">The structure of your new module. Will appear on page as decided here.</span>
                            <textarea name="html" class="hide" id="html"></textarea>
                            <div class="html-structure ace-editor @if(!$moduletemplate->editable) disabled @endif" data-type="html">{{trim(htmlentities($moduletemplate->html))}}</div>
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('css', 'Custom CSS: ', array('class' => 'col-sm-2')) }}
                        <div class="col-sm-10">
                            <span class="help-block">If provided, custom CSS will appear in <span class="italic">head</span> section of your website.</span>
                            <textarea name="css" class="hide" id="css"></textarea>
                            <div class="ace-editor" data-type="css">{{trim($moduletemplate->css)}}</div>
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('js', 'Custom JavaScript: ', array('class' => 'col-sm-2')) }}
                        <div class="col-sm-10">
                            <span class="help-block">If provided, custom Javascript will appear before the final <span class="italic">body</span> section of your website.</span>
                            <textarea name="js" class="hide" id="js"></textarea>
                            <div class="ace-editor" data-type="javascript">{{trim($moduletemplate->js)}}</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default btn-primary pull-right">Update</button>
                        </div>
                    </div> 
                {{Form::close()}}
            </section>
        </div>
    </div>
@stop

@section('scripts')
    <script src="/themes/hummingbird/default/lib/ace/ace.js"></script>
    <script>
        var editor;
        $('.ace-editor').each(function( index ) 
        {
            editor = ace.edit(this);
            editor.setTheme("ace/theme/twilight");
            editor.getSession().setMode("ace/mode/" + $(this).data('type'));
            editor.setOptions({
                'maxLines': 50,
                'minLines': 10,
                'firstLineNumber': 1
            });

            if($(this).hasClass('disabled'))
            {
                editor.setOptions({
                    readOnly: true,
                    highlightActiveLine: false,
                    highlightGutterLine: false
                });

                $(this).append('<div class="alert alert-info text-center">Template can not be edited as it is read only mode.</div>');
            }
        });

        $(document).ready(function()
        {
            if($('#module-submit').length == 1)
            {
                $("#module-submit").submit(function(e)
                {
                    $('.ace-editor').each(function( index ) 
                    {
                        editor = ace.edit(this);
                    
                        $(this).prev().val(editor.getSession().getValue());
                    });

                    return;
                });
            }
        });
    </script>
@stop
