<?php


	class ShopHelper {
		
			function __construct()
			{
				$this->products_table 		= 'shop_products';
				$this->sales_table 			= 'shop_sales';
				$this->options_table 		= 'shop_optionsets';

			}
			
			public function merge_hb_products()
		    {
		    	ini_set('max_execution_time', 0);

		        //this function should be used to merge the HB1 shop database into HB3

		        //1. import shop_cats, shop_prod_cats, shop_prods table from hummingbird
		        //2. rename these hb_old_shop_cats, hb_old_shop_prod_cats, hb_old_shop_prods

		    	//3. copy across categories into new categories

		    	
		    	//we need to preserve the old category ID, so let's create a column to hold it
		    	
		    	$rand_key 				= mt_rand(10000, 20000);

		    	$old_shop_cat_id_col	= 'old_shop_cat_id_'.$rand_key;

		    	DB::unprepared('ALTER TABLE hb_taxonomy ADD '.$old_shop_cat_id_col.' INT(11)');

		    	//4. copy across categories table
		        foreach(DB::table('old_shop_cats')->get() as $category)
		        {
		        	$category = (array) $category; 

		        	$data['name']		 			= $category['name'];
		        	$data['slug'] 					= $category['url'];
		        	$data[$old_shop_cat_id_col] 	= $category['id'];
		        	$data['description'] 			= '';
		        	$data['type'] 					= 'shop_category';
		        	$data['parent'] 				= $category['parentID'];
		        	$data['created_at'] 			= date("Y-m-d H:i:s"); 

		            DB::table('taxonomy')->insert($data);
		        }

		        //4. copy across products table

		        $old_shop_prod_id_col = 'old_shop_prod_id_'.$rand_key;

		    	DB::unprepared('ALTER TABLE hb_shop_products ADD '.$old_shop_prod_id_col.' INT(11)');



		        foreach(DB::table('old_shop_prods')->get() as $product)
		        {
		        	$newproduct = (array) $product;
		        	//drop this field
		        	unset($newproduct['Ollie_ID']);

		        	//rename this field
		        	$newproduct['out_of_stock'] = $newproduct['Out_Of_Stock'];
		        	unset($newproduct['Out_Of_Stock']);

		        	//rename this field
		        	$newproduct['mini_description'] = $newproduct['mini_desc'];
		        	unset($newproduct['mini_desc']);

		        	$newproduct[$old_shop_prod_id_col] = $newproduct['id'];
		        	unset($newproduct['id']);

		            DB::table('shop_products')->insert($newproduct);

		        }

		        //5. copy across category relations

		        $query = 'INSERT INTO hb_taxonomy_relationships (term_id, tax_type, item_id)  	
						SELECT c.id as term_id, "shop_category" as tax_type, p.id as item_id
  							FROM hb_old_shop_prods_cats pc 
  							JOIN hb_shop_products AS p ON pc.prod_id = p.'.$old_shop_prod_id_col.'
  							JOIN hb_taxonomy AS c ON pc.cat_id = c.'.$old_shop_prod_id_col;

				DB::raw($query);

				//6. import dropdowns, needed

		        $old_shop_opts_id_col = 'old_shop_opts_id_'.$rand_key;

		    	DB::unprepared('ALTER TABLE hb_shop_optionsets ADD '.$old_shop_opts_id_col.' INT(11)');

		        foreach(DB::table('old_shop_dropdowns')->get() as $option)
		        {
		        	$option = (array) $option;
		        	//rename this field
		        	$option['friendly_name'] = $option['friendly'];
		        	unset($option['friendly']);

		        	$option[$old_shop_opts_id_col] = $option['id'];
		        	unset($option['id']);

		            DB::table('shop_optionsets')->insert($option);

		        }

		        //copy across product relations

		        $query = 'INSERT INTO hb_shop_optionsets_relationships (product_id, option_id)
							SELECT p.id as product_id, o.id as option_id
							FROM hb_old_shop_prods_dropdowns pd
							JOIN hb_shop_products AS p ON pd.prod_id = p.'.$old_shop_prod_id_col.'
							JOIN hb_shop_optionsets AS o ON pd.drop_id = o.'.$old_shop_opts_id_col;

				DB::raw($query);

		        //7. drop extraneous temp columns

				DB::unprepared('ALTER TABLE hb_taxonomy DROP COLUMN '.$old_shop_cat_id_col);
				DB::unprepared('ALTER TABLE hb_shop_products DROP COLUMN '.$old_shop_prod_id_col);
				DB::unprepared('ALTER TABLE hb_shop_optionsets DROP COLUMN '.$old_shop_opts_id_col);

		        //7. remove old tables

				DB::unprepared('DROP TABLE hb_old_shop_cats');
				DB::unprepared('DROP TABLE hb_old_shop_dropdowns');
				DB::unprepared('DROP TABLE hb_old_shop_prods');
				DB::unprepared('DROP TABLE hb_old_shop_prods_cats');
				DB::unprepared('DROP TABLE hb_old_shop_prods_dropdowns');
				


				echo 'complete';

		    }
			
			
		}
