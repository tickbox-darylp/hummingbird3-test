@extends('cms.moodstrap-layout')

@section('content')

<h1><a href="{{url(App::make('backend_url').'/pages')}}">All Pages</a> > Edit page: {{$page->title}}</h1>

{{ Form::open(array('url' => '/cms/pages/edit/' . $page->id)) }}

<p>
    <a href='/{{$page->permalink}}' target='_blank'>View page</a> | 
    <a href='{{url(App::make('backend_url').'/pages/versions/'.$page->id)}}' target='_blank'>Previous versions</a>
</p>

<div id="edit-pane">

    {{$html}}    

    <div style="clear:both;"></div>
</div>

<div style="clear:both;"></div>
<p><input type="submit" value="Update Page" class="btn btn-primary btn-lg"></p>
{{ Form::close() }}

<style>
    #edit-pane { width: 95%; border:1px solid black;}
    #content, .image-selector, .module-selector { border: 1px dashed;}
    .image-selector { height: 100px; }
    .image-selector, #main, #sidebar, #content, .module-selector { padding: 10px; }
    .module-selector {min-height: 65px; }
    .module-selector .tools { border: 1px dotted; padding: 2px; float:right;}
    .selectable-item:hover {cursor: pointer;}
    .selectable-item.selected { font-weight: bolder;}
</style>

<link rel='stylesheet' href='/themes/public/lutonculture/css/templates.css' />

<div id="add-module" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add module</h4>
            </div>
            <div class="modal-body">
                <p>An error occurred. Please close and try again.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="button" class="btn btn-primary save">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div id="new-module" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>One fine body…</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div id="edit-module" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>One fine body…</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div id="find-module" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>One fine body…</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">

    var page_id = <?php echo $page->id ?>;

    $(document).ready(function() {

        $('.action-module').click(function() {
            var action = $(this).data('action');

            switch (action) {

                case 'add':

                    var area = $(this).parent().parent().parent().attr('id');

                    $.getJSON("/cms/ajax/modules/modulesForArea/" + area, function(data) {
                        console.log(data);
                        
                        var html = '<input type="hidden" id="area" value="'+area+'" />';
                        
                        $(data).each(function(index, item){
                            html += "<p class='selectable-item' data-id='" + item.id + "'>" + item.name + "</p>";
                        });
                        
                        $('#add-module .modal-body').html(html);
                    });


                    break;
            }

        });
        
        $(document).on('click', '.selectable-item', function(){
            $('.selectable-item').removeClass('selected');
            $(this).toggleClass('selected');
        });
        
        $(document).on('click', '#add-module button.save', function(e){
            e.preventDefault();
            
            // get selection
            var selection = $('#add-module .selectable-item.selected');
            var area = $('#add-module #area').val();
            
            if (selection.length == 1) {
                
                // send selection through to server
                $.getJSON("/cms/ajax/pagemodules/addPagemodule/" + area + "/" + page_id + "/" + selection.data('id'), function(result) {
                    alert(result);
                });
            } else if (selection.length > 1) {
                return false;
            } else {
                // nothing selected
                return false;
            }            
            
            // refresh modules
            populateModules();            
            
            //$('#add-module').alert('close');
        });

        populateModules();

    });

    function populateModules()
    {
        // remove all first
        $('.module-selector > *:not(.tools)').remove();
        
        $('.module-area').each(function() {

            var area = $(this).attr('id');

            // get html for that area
            $.getJSON("/cms/ajax/pagemodules/pagemodulesForArea/" + area + "/" + page_id, function(data) {
                console.log(data);

                $.each(data, function(i, obj) {

                    $('#' + area + ' .module-selector').append(obj.html);
                    $('#' + area + ' .module-selector .tools a[data-action="add"]').hide();
                    $('#' + area + ' .module-selector .tools a[data-action="edit"]').show();

                });

            });
        });
    }
</script>
@stop
