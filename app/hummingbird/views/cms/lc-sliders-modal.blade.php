<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Modal settings &raquo; <span class="span-modal-title">[modal-title]</span></h4>
        </div>
        <div class="modal-body form-horizontal">
            <div class="form-group">
                <label for="content_type" class="col-sm-2 control-label">Type:</label>
                <div class="col-sm-8"> 
                    <select name="content_type" id="content_type" class="form-control">
                        <option value="">---</option>
                        <option value="custom" @if((isset($posted['content_type']) AND $posted['content_type'] == 'custom') OR (isset($posted['region']) AND isset($slide_data) AND isset($slide_data->regions->$posted['region']->type) AND $slide_data->regions->$posted['region']->type == 'custom')) selected @endif>Custom</option>
                        <option value="events" @if((isset($posted['content_type']) AND $posted['content_type'] == 'events') OR (isset($posted['region']) AND isset($slide_data) AND isset($slide_data->regions->$posted['region']->type) AND $slide_data->regions->$posted['region']->type == 'events')) selected @endif>Events</option>
                    </select>
                    <span class="help-block">Select the type of content you want to add</span>
                </div>
            </div>

            <div class="alert alert-box alert-info text-center content-picker @if((isset($posted['content_type']) AND $posted['content_type'] != '') OR (isset($posted['region']) AND isset($slide_data) AND isset($slide_data->regions->$posted['region']->type) AND $slide_data->regions->$posted['region']->type != '')) hide @endif">Please select a content type you would like to add.</div>

            @if($posted['get-content-data'])
                @if((isset($posted['content_type'])) OR (isset($posted['region']) AND isset($slide_data) AND isset($slide_data->regions->$posted['region']->type)))
                    @if((isset($posted['content_type']) AND $posted['content_type'] == 'custom') OR (isset($posted['region']) AND isset($slide_data) AND isset($slide_data->regions->$posted['region']->type)) AND $slide_data->regions->$posted['region']->type == 'custom')
                        <div class="modal-data @if((isset($posted['content_type']) AND $posted['content_type'] != 'custom') OR (isset($posted['region']) AND isset($slide_data) AND isset($slide_data->regions->$posted['region']->type) AND $slide_data->regions->$posted['region']->type != 'custom')) hide @endif" data-content-type="custom">
                            <div class="form-group">
                                <label for="fields" class="col-sm-2 control-label">Position:</label>
                                <div class="col-sm-8">
                                    <select name="fields" id="fields" class="form-control">
                                        <option value="">---</option>
                                    </select>
                                    <span class="help-block">Select the space you want this data to appear in</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="modal_title" class="col-sm-2 control-label">Title:</label>
                                <div class="col-sm-10">
                                    <input id="modal_title" type="text" class="form-control" @if(isset($slide_data) AND isset($slide_data->regions->$posted['region']->title)) value="{{$slide_data->regions->$posted['region']->title}}" @endif>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="modal_link" class="col-sm-2 control-label">Link:</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon">http://</span>
                                        <input id="modal_link" type="text" class="form-control" @if(isset($slide_data) AND isset($slide_data->regions->$posted['region']->link)) value="{{$slide_data->regions->$posted['region']->link}}" @endif>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="modal_summary" class="col-sm-2 control-label">Summary:</label>
                                <div class="col-sm-10">
                                    {{ Form::textarea('', ((isset($slide_data) AND isset($slide_data->regions->$posted['region']->summary)) ? $slide_data->regions->$posted['region']->summary:''), array('id' => 'modal_summary', 'class' => 'form-control textareas')) }}
                                </div>
                            </div>
                            
                            <div class="form-group media-library-holder">
                                {{ Form::label('featured_image', 'Featured image ', array('class' => 'col-sm-2')) }}
                                <div class="col-sm-8 image-link">
                                    <div class="input-group">
                                        <input type="text" name="featured_image" class="form-control media-image" readonly="" @if(isset($slide_data) AND isset($slide_data->regions->$posted['region']->image)) value="{{$slide_data->regions->$posted['region']->image}}" @endif>
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file pull-right">
                                                Browse...<input type="file" multiple="" class="browse-media-library" @if(isset($slide_data) AND isset($slide_data->regions->$posted['region']->image) AND $slide_data->regions->$posted['region']->image != '') data-collection-id="{{ Media::where('location', '=', $slide_data->regions->$posted['region']->image)->first()->collection }} @endif">
                                            </span>
                                        </span>
                                    </div>
                                    <span class="help-block @if(!isset($posted['region']) OR !isset($slide_data->regions->$posted['region']->image)) hide @endif"><a href="#" class="remove-media-library">Remove featured image</a></span>
                                </div>

                                <div class="col-sm-2 @if(!isset($posted['region']) OR !isset($slide_data->regions->$posted['region']->image)) hide @endif image-holder">
                                    <img @if(isset($slide_data->regions->$posted['region']->image)) src="{{$slide_data->regions->$posted['region']->image}}" @endif class="img-responsive">
                                </div>
                            </div>
                        </div>
                    @elseif((isset($posted['content_type']) AND $posted['content_type'] == 'events') OR (isset($posted['region']) AND isset($slide_data) AND isset($slide_data->regions->$posted['region']->type)) AND $slide_data->regions->$posted['region']->type == 'events')
                        <div class="modal-data @if((isset($posted['content_type']) AND $posted['content_type'] != 'events') OR (isset($posted['region']) AND isset($slide_data) AND isset($slide_data->regions->$posted['region']->type)) AND $slide_data->regions->$posted['region']->type != 'events') hide @endif" data-content-type="events">
                            <div class="form-group">
                                <label for="fields" class="col-sm-2 control-label">Position:</label>
                                <div class="col-sm-10 region-checkboxes"><div class="row"></div></div>
                                <div class="col-sm-10 col-sm-offset-2"><span class="help-block">Select the spaces you want this data to appear in</span></div>
                            </div>

                            <div class="form-group">
                                <label for="modal_title" class="col-sm-2 control-label">Select?</label>
                                <div class="col-sm-10">
                                    <select name="events-select-by" id="events-select-by" class="form-control">
                                        <option value="">---</option>
                                        <option value="all" @if(!isset($posted['region']) OR !isset($slide_data->regions->$posted['region']) OR $slide_data->regions->$posted['region']->select == 'all') selected @endif>Show all</option>
                                        <option value="custom" @if(isset($slide_data->regions->$posted['region']) AND $slide_data->regions->$posted['region']->select == 'custom') selected @endif>Select events</option>
                                        <option value="filter" @if(isset($slide_data->regions->$posted['region']) AND $slide_data->regions->$posted['region']->select == 'filter') selected @endif>Filter by...</option>
                                    </select>
                                </div>
                            </div>

                            @if(!class_exists('Venues'))
                                <?php $venues = DB::table('venues')->orderBy('name', 'ASC')->get();?>

                                @if(count($venues) > 0)
                                    <div class="form-group">
                                        <label for="modal_title" class="col-sm-2 control-label">Venue?</label>
                                        <div class="col-sm-10">
                                            <select name="events-venue" id="events-venue" class="form-control">
                                                <option value="-">--- No venue ---</option>
                                                @foreach($venues as $venue)
                                                    <option value="{{$venue->id}}" @if(isset($slide_data->regions->$posted['region']->venue) AND $slide_data->regions->$posted['region']->venue == $venue->id) selected @endif>{{$venue->name}}</option> 
                                                @endforeach
                                            </select>
                                            <span class="help-block">Want to select a particular location?</span>
                                        </div>
                                    </div>
                                @endif
                            @endif

                            <div class="form-group events-select @if(!isset($posted['region']) OR !isset($slide_data->regions->$posted['region']) OR $slide_data->regions->$posted['region']->select != 'custom') hide @endif">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <select name="events-select" id="events-select" class="form-control selectpicker" data-live-search="true" multiple data-selected-text-format="count">
                                        @if(isset($slide_data->regions->$posted['region']->select) AND $slide_data->regions->$posted['region']->select == 'custom')
                                            <?php $events_list = Events::future()->live()->whereNull('deleted_at')->orderBy('start_date', 'ASC')->get();?>
                                            @foreach($events_list as $event)
                                                <option value="{{$event->id}}" @if(isset($slide_data->regions->$posted['region']->events) AND in_array($event->id, $slide_data->regions->$posted['region']->events)) selected @endif>{{$event->title}} ({{str_replace(":00", "", DateTimeHelper::date_format($event->start_date, 'j M Y, H:i:s'))}})</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group events-filter-by @if(!isset($posted['region']) OR !isset($slide_data->regions->$posted['region']) OR $slide_data->regions->$posted['region']->select != 'filter') hide @endif">
                                <label for="modal_title" class="col-sm-2 control-label">Filter by:</label>
                                <div class="col-sm-10">
                                    <select name="event-filter-by" id="event-filter-by" class="form-control">
                                        <option value="">---</option>
                                        <option value="important" @if(isset($slide_data->regions->$posted['region']->filter_by) AND $slide_data->regions->$posted['region']->filter_by == 'important') selected @endif>Important (flagged events)</option>
                                        <option value="categories" @if(isset($slide_data->regions->$posted['region']->filter_by) AND $slide_data->regions->$posted['region']->filter_by == 'categories') selected @endif>Categories</option>
                                        <option value="tags" @if(isset($slide_data->regions->$posted['region']->filter_by) AND $slide_data->regions->$posted['region']->filter_by == 'tags') selected @endif>Tags</option>
                                        <option value="dates" @if(isset($slide_data->regions->$posted['region']->filter_by) AND $slide_data->regions->$posted['region']->filter_by == 'dates') selected @endif>Date Range</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group taxonomy-select @if((!isset($slide_data->regions->$posted['region']->filter_by) OR !isset($posted['region'])) OR (isset($slide_data->regions->$posted['region']->filter_by) AND !in_array($slide_data->regions->$posted['region']->filter_by, array('categories', 'tags')))) hide @endif">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <select name="taxonomy-select" id="taxonomy-select" class="form-control selectpicker" data-live-search="true" multiple data-selected-text-format="count">
                                        @if(isset($slide_data->regions->$posted['region']->filter_by) AND $slide_data->regions->$posted['region']->filter_by == 'categories')
                                            <?php 
                                                $event_cat_id = Categories::where('name', '=', 'Events')->type()->get()->first();
                                                $categories = Categories::whereNull('deleted_at')->type()->where('parent', '=', $event_cat_id->id)->get();
                                            ?>
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}" @if(isset($slide_data->regions->$posted['region']->categories) AND in_array($category->id, $slide_data->regions->$posted['region']->categories)) selected @endif>{{$category->name}}</option>
                                            @endforeach
                                        @elseif(isset($slide_data->regions->$posted['region']->filter_by) AND $slide_data->regions->$posted['region']->filter_by == 'tags')
                                            <?php $tags = Tags::whereNull('deleted_at')->type()->orderBy('name', 'ASC')->get();?>
                                            @foreach($tags as $tag)
                                                <option value="{{$tag->id}}" @if(isset($slide_data->regions->$posted['region']->tags) AND in_array($tag->id, $slide_data->regions->$posted['region']->tags)) selected @endif>{{$tag->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group date-select @if((!isset($slide_data->regions->$posted['region']->filter_by) OR !isset($posted['region'])) OR (isset($slide_data->regions->$posted['region']->filter_by) AND $slide_data->regions->$posted['region']->filter_by != 'dates')) hide @endif">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        <div class="relative"><input name="post_date" id="post_date" type="text" class="datetimepicker from form-control" placeholder="From this date" @if(isset($slide_data->regions->$posted['region']->dates->dates_from)) value="{{$slide_data->regions->$posted['region']->dates->dates_from}}" @endif></div>
                                        <span class="input-group-addon" style="border-left:0;border-right:0;">- to - </span>

                                        <div class="relative"><input name="post_end_date" id="post_end_date" type="text" class="datetimepicker to form-control" placeholder="To this date" @if(isset($slide_data->regions->$posted['region']->dates->dates_to)) value="{{$slide_data->regions->$posted['region']->dates->dates_to}}" @endif></div>
                                        
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    </div>
                                    <span class="help-block">The date to show events from and until (if set).</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="event-order-by" class="col-sm-2 control-label">Order by:</label>
                                <div class="col-sm-10">
                                    <select name="event-order-by" id="event-order-by" class="form-control">
                                        <option value="">---</option>
                                        <optgroup label="General">
                                            <option value="random" @if(isset($slide_data->regions->$posted['region']->order_by) AND $slide_data->regions->$posted['region']->order_by == 'random') selected @endif>Random</option>
                                        </optgroup>
                                        <optgroup label="ASC">
                                            <option value="id_ASC" @if(isset($slide_data->regions->$posted['region']->order_by) AND $slide_data->regions->$posted['region']->order_by == 'id_ASC') selected @endif>ID</option>
                                            <option value="start_date_ASC" @if(isset($slide_data->regions->$posted['region']->order_by) AND $slide_data->regions->$posted['region']->order_by == 'start_date_ASC') selected @endif>Start date</option>
                                            <option value="finish_date_ASC" @if(isset($slide_data->regions->$posted['region']->order_by) AND $slide_data->regions->$posted['region']->order_by == 'finish_date_ASC') selected @endif>Finish date</option>
                                        </optgroup>
                                        <optgroup label="DESC">
                                            <option value="id_DESC" @if(isset($slide_data->regions->$posted['region']->order_by) AND $slide_data->regions->$posted['region']->order_by == 'id_DESC') selected @endif>ID</option>
                                            <option value="start_date_DESC" @if(isset($slide_data->regions->$posted['region']->order_by) AND $slide_data->regions->$posted['region']->order_by == 'start_date_DESC') selected @endif>Start date</option>
                                            <option value="finish_date_DESC" @if(isset($slide_data->regions->$posted['region']->order_by) AND $slide_data->regions->$posted['region']->order_by == 'finish_date_DESC') selected @endif>Finish date</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="modal_title" class="col-sm-2 control-label">Ignore duplicates?</label>
                                <div class="col-sm-10">
                                    <input class="events-ignore-dupes" type="checkbox" name="events-ignore-dupes" value="1" @if(isset($slide_data->regions->$posted['region']->ignore_dupes) AND $slide_data->regions->$posted['region']->ignore_dupes == 1) checked @endif> Yes
                                    <span class="help-block">Duplicated events, in previous slides, will be removed</span>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
            @endif
        </div>
        <div class="modal-footer">
            <button type="button" class="@if($posted['get'] == 'by-content') hide @endif pull-left btn btn-danger delete-region">Remove</button>
            <button type="button" class="btn btn-primary">Save changes</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->