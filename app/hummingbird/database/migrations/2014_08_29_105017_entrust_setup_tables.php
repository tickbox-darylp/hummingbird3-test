<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EntrustSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Creates the permissions table
        Schema::create('permissions', function ($table) {
            $table->integer('permission_id');
            $table->integer('user_id');
        });

        // Creates the permission actions table
        Schema::create('permission_actions', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('action');
        });

        // Creates the permissions roles table
        Schema::create('permission_role', function ($table) {
            $table->integer('permission_id');
            $table->integer('role_id');
        });

        // Creates the permission types table
        Schema::create('permission_types', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('type', 255);
            $table->string('label', 255)->nullable();
            $table->string('group', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->string('display_name', 255);
            $table->smallInteger('special_perms')->default(0);
            $table->smallInteger('live')->default(0);
        });

        // Creates the permission types table
        Schema::create('perm_types_actions_xref', function ($table) {
            $table->increments('perm_id')->unsigned();
            $table->integer('perm_type');
            $table->integer('perm_action');
        });

        // Create the permission overrides table
        Schema::create('permission_overrides', function($table) 
        {
            $table->increments('id');
            $table->string('perm_name');
            $table->integer('user_id');
            $table->tinyInteger('enabled')->default(1);
            $table->timestamps();

        });

        // Creates the roles table
        Schema::create('roles', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('name')->unique();
            $table->integer('parentrole_id')->nullable();
            $table->tinyInteger('hidden')->default(0);
            $table->tinyInteger('locked')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('permissions');
        Schema::drop('permission_actions');
        Schema::drop('permission_role');
        Schema::drop('permission_types');
        Schema::drop('perm_types_actions_xref');
        Schema::drop('permission_overrides');
        Schema::drop('roles');
    }

}
