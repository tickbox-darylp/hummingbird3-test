<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Hummingbird CMS | @if(isset($tag)) {{$tag}} @endif</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="/themes/hummingbird/default/css/reset.css"> <!-- CSS reset -->
        <link href="/themes/hummingbird/default/css/bootstrap.css" rel="stylesheet" />
        <link href="/themes/hummingbird/default/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="/themes/hummingbird/default/css/main.css" rel="stylesheet" />
        <link href="/themes/hummingbird/default/css/responsive.css" rel="stylesheet" />
        <link type="text/css" rel="stylesheet" href="/themes/hummingbird/default/lib/redactor/redactor.css"  />
        <link type="text/css" rel="stylesheet" href="/themes/hummingbird/default/lib/redactor/plugins/clips/clips.css" />
        <link type="text/css" rel="stylesheet" href="/themes/hummingbird/default/lib/offline/offline-theme-dark.css" />
        <link type="text/css" rel="stylesheet" href="/themes/hummingbird/default/lib/offline/offline-language-english.css" />
        @yield('styles')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="/themes/hummingbird/default/js/html5shiv.min.js"></script>
            <script src="/themes/hummingbird/default/js/respond.min.js"></script>
        <![endif]-->

        <style>
            .header-flash 
            {
                text-align:center;
                color:white;
                font-size:1em;
                background-color:#2A313A;
                padding-top:20px;
                padding-bottom:20px;
                -webkit-box-shadow: 0px 0px 4px 2px #000000;
                -moz-box-shadow: 0px 0px 4px 2px #000000;
                box-shadow: 0px 0px 4px 2px #000000;
            }
            .header-flash a, .header-flash a:hover
            {
                color:#00AEEF;
            }
        </style>
    </head>

    <body id="dashboard" class="cms-body <?php if(null !== Session::get('view_user_perms') OR null !== Session::get('view_role_perms')) echo 'preview';?>">
        <?php if(null !== Session::get('view_user_perms'))
        {?>
            <div class="header-flash">
                You are currently viewing &quot;{{ App::make('UserController')->preview_user }}&quot; permissions. To cancel <a href="/{{App::make('backend_url')}}/users/preview/?action=remove">click here</a>.
            </div>
        <?php }?>

        <?php if(null !== Session::get('view_role_perms'))
        {?>
            <div class="header-flash">
                You are currently viewing how the role &quot;{{ App::make('RoleController')->preview_role }}&quot; will look. To cancel <a href="/{{App::make('backend_url')}}/users/preview/?action=remove" style="color:red;">click here</a>.
            </div>
        <?php }?>

        <div class="absolute pop-hum" style="top:0;left:0;height:61px;width:68px;">
            <img src="http://heathersanimations.com/birds/oiseau_40.gif">
        </div>

        <div class="absolute pop-pigeon" style="display:none;top:50%;margin-top:-125px;width:100%;background-color:white;z-index: 10001;height: 250px;">
            <img class="relative" src="http://media.giphy.com/media/TOsVFUIg4DX8s/giphy.gif" style="left:-10%;height: 250px;">
        </div>

        <!-- START WRAPPER -->
        <div class="wrapper relative">
            <!-- START NAV -->
            @if (Auth::check())
                @include('HummingbirdBase::cms.include.nav')
            @endif
            <!-- END NAV -->

            <div class="main-content relative">
                <div class="header-section">
                    <div class="pull-left" style="width:50%;">
                        <a href="#" id="nav-toggle"><i class="fa fa-bars"></i></a>

                        @if(General::strpos_array($_SERVER["REQUEST_URI"], array('pages', 'events', 'blog')))
                            <div class="search-box">
                                <form action="/{{General::backend_url().'/'.Request::segment(2).'/'}}" method="get">
                                    <input name="s" type="text" placeholder="Search..." />
                                </form>
                            </div>
                        @endif
                    </div>

                    <a style="float: right; font-weight:bold; padding-right: 20px; padding-top: 23px;" class="logout" href="/hummingbird/logout">Logout</a>

                    <div class="pull-right hide">
                        <div class="profile">
                            <div class="dropdown navbar-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="">
                                    <img src="{{ Auth::user()->profile_image() }}" class="img-circle">
                                    <a class="hide" href="#">Daryl Phillips <i class="fa fa-chevron-right"></i></a>
                                </a>
                                <ul class="dropdown-menu" style="left:auto;right:0;">
                                    <li class="arrow top"></li>
                                    <li><a href="{{ General::backend_url() }}/profile/"><i class="fa fa-user"></i> Profile</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ General::backend_url() }}/logout/"><i class="fa fa-sign-out"></i> Log Out</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                <!-- </div> -->

                    <div class="pull-right profile hide">
                        <div class="menu-right">
                            <ul class="notification-menu relative">
                                <li>
                                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                        <img width="48" src="{{ Auth::user()->profile_image() }}" class="img-circle" alt="{{ Auth::user()->name }}">
                                        {{ Auth::user()->name }}
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                                        <li><a href="#"><i class="fa fa-user"></i>  Profile</a></li>
                                        <li><a href="#"><i class="fa fa-cog"></i>  Settings</a></li>
                                        <li><a href="#"><i class="fa fa-sign-out"></i> Log Out</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>


                <style>
                    .profile .notification-menu {padding-left:0;padding-right:0;}
                </style>

                <div class="content-section clearfix">
                    <?php /*
                    <div class="row ">
                        <div class="col-md-12">
                            <!--breadcrumbs start -->
                            <ul class="breadcrumb panel">
                                <li><a href="/hummingbird/"><i class="fa fa-home"></i> Dashboard</a></li>
                                <li class="active">{PAGE ACCESSED}</li>
                            </ul>
                            <!--breadcrumbs end -->
                        </div>
                    </div>
                    */?>

<!--                     <?php if(!Session::has('show-welcome'))
                    {
                        Session::put('show-welcome', true);?>
                        <h1 class="normal pull-left">Welcome back, {{ Auth::user()->name }}!</h1>
                    <?php }?>

                    <h6 class="normal pull-right" id="clock-date"><?php echo date("l, j F Y"); ?> <span id="clock"></span></h1>
                        
                    <div class="clearfix">&nbsp;</div> -->

                    @yield('breadcrumbs')
                    @yield('content')

                    <div class="clearfix">&nbsp;</div>
                </div>
                
                <!-- TEMP: REMOVE -->
                <section id="footer">
                    <a href="http://www.tickboxmarketing.co.uk/" target="_blank">&copy; Tickbox Marketing <?= date("Y") ?></a> | <small>Hummingbird v{{Config::get('HummingbirdBase::hummingbird.version')}}</small>
                    <br />
                    @if(null !== Config::get('app.START_EXECUTION'))
                        <p><small>Loaded in {{round((microtime(true) - Config::get('app.START_EXECUTION')), 2);}} seconds</small></p>
                    @endif
                </section>
            </div>
            <!-- END OF CONTENT -->
        </div>
        <a id="return-to-top" href="#" class="opacity"><i class="fa fa-angle-up"></i></a>

        <!-- END OF WRAPPER -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/themes/hummingbird/default/js/jquery.min.js"></script>
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/themes/hummingbird/default/js/bootstrap.min.js"></script>

        <!-- Include Hummingbird - Individual JS Files -->        
        <script src="/themes/hummingbird/default/lib/redactor/redactor.min.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/hummingbird.default.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/clips/clips.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/counter.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/definedlinks.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/hummingbird.filemanager.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/fontcolor.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/fontfamily.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/fontsize.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/hummingbird.formatter.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/fullscreen.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/hummingbird.imagemanager.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/limiter.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/table.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/textdirection.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/textexpander.js"></script>
        <script src="/themes/hummingbird/default/lib/redactor/plugins/video.js"></script>

        <script src="/themes/hummingbird/default/lib/offline/offline.min.js"></script>
        <script src="/themes/hummingbird/default/lib/autogrow/autogrow.js"></script>

        <!-- Include Hummingbird Core JS -->
        <script src="/themes/hummingbird/default/js/hummingbird.js"></script>

        <script>
            // var idleSeconds = 1000 * (60 * 15); // 1/4 hour no activity
            // var idleTimer;

            // function resetIdleTimer()
            // {
            //     clearTimeout(idleTimer);
            //     idleTimer = setTimeout(userIdle, idleSeconds);
            // }

            // function userIdle()
            // {
            //     window.location = '/hummingbird/locked/';
            // }
            // http://stackoverflow.com/questions/7276677/jquery-redirect-to-url-after-specified-time

            function animateImage()
            {
                // setTimeout(function()
                // {   
                //     $(".pop-pigeon").fadeIn();
                //     $(".wrapper").css('opacity', '0.5');
                //     $('.pop-pigeon img').animate({left: '110%'}, 7500, 'linear', function()
                //     {
                //         $(".pop-pigeon").fadeOut();
                //         $(".wrapper").css('opacity', '1');
                //     });
                // }, 3500);
            }

            $(document).ready(function()
            {
                if($('.permission-holder').length > 0)
                {
                    $('.permission-holder h3 input:checkbox').click(function()
                    {
                        var el = $(this).closest('.permission-holder');

                        el.find(".perms").slideToggle();
                    });
                }

                // if($('.cms-body').length > 0)
                // {
                //     $(document.body).bind('mousemove keydown DOMMouseScroll mousewheel mousedown touchstart touchmove', resetIdleTimer);
                //     resetIdleTimer(); // Start the timer when the page loads
                // }

                $(document).on('mouseenter', ".tooltip-table", function () {
                    var $this = $(this);
                    if (this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
                        $this.tooltip({
                            container : 'body',
                            title: $this.text(),
                            placement: "bottom"
                        });
                        $this.tooltip('show');
                    }
                });

                if($(".tooltip_show").length > 0)
                {
                    $(".tooltip_show").tooltip();
                }

                if($(".media-upload").length > 0)
                {
                    $(".media-upload").click(function(e)
                    {
                        e.preventDefault();

                        if($("#dropzone").length > 0)
                        {
                            $("#dropzone").slideToggle();
                        }
                    });
                }

                if($(".thumbnail img").length > 0)
                {
                    $(".thumbnail img").click(function()
                    {
                        // $("#editImage").modal();
                    });
                }

                if($(".Activity").length > 0)
                {
                    $(".Activity").append('<div class="updates updates-alert">10</div>');
                }

                if($("#sidebar .menu").length > 0)
                {
                    $(".menu").each(function()
                    {
                        if($(this).find('ul').children().length <= 0)
                        {
                            $(this).remove();
                        }
                        else
                        {
                            if($(this).find('ul li a.active').length > 0)
                            {
                                var el = $(this).find('ul li a.active');

                                $(this).find('ul:not(.submenu)').css('display', 'block');
                                $(this).find('h3 a i.fa').toggleClass("fa-plus fa-minus");

                                if(el.closest('ul').hasClass('submenu'))
                                {
                                    el.closest('li.option').find('a.hasSubMenu').trigger('click');
                                }
                            }
                        }
                    });


                    $(".menu h3 a.menu-collapse").click(function(e)
                    {
                        e.preventDefault();

                        $(this).toggleClass('active');
                        $(this).find('.fa').toggleClass("fa-plus fa-minus");
                        $(this).closest('.menu').find('ul:first').slideToggle();
                    });


                    animateImage();
                }
            });
        </script>

        @yield('scripts')
    </body>
</html>

<!-- .updates{
    font-size: 1.2rem;
    display: inline-block;
    line-height: 1;
    margin-left: 2px;
    color: white;
    width: 24px;
    height: 24px;
    line-height: 24px;
    -webkit-border-radius: 12px;
    -webkit-background-clip: padding-box;
    -moz-border-radius: 12px;
    -moz-background-clip: padding;
    border-radius: 12px;
    background-clip: padding-box;
    text-align: center;
    -moz-box-shadow: 0 2px 5px rgba(0,0,0,.2);
    -webkit-box-shadow: 0 2px 5px rgba(0,0,0,.2);
    box-shadow: 0 2px 5px rgba(0,0,0,.2);
} -->