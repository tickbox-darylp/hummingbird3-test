<?php 

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Events extends LaravelBook\Ardent\Ardent 
{
    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    
    public $table = 'events';

    public $cms_url = 'events';
    public $cms_type = 'Event';

    public $time_increments = 15; //in minutes for recurring events

    protected static $main_category_taxonomy = 'Events';

    protected static $event_tickets = false; //true = live, false = not live

    // protected $fillable = array('title','finish','venue_id','short_title', 'url', 'summary', 'description', 'event_type'
    //     ,'start', 'finish', 'display_in_calendar', 'booking_info', 'cost', 'tickets_available', 'group_registration',
    //     'contact', 'email', 'telephone', 'website', 'tags', 'showcase', 'live');

    protected $fillable = array('title','short_title', 'url', 'summary', 'description', 'contact', 
        'email', 'telephone', 'website', 'showcase', 'live', 'reference', 'application_code', 'importance', 
        'start_date', 'finish_date', 'cost', 'show_in_nav', 'searchable', 'search_image', 'featured_image',
        'event_address', 'address_1', 'address_2', 'address_3', 'town', 'postcode', 'country',
        'booking', 'booking_link', 'booking_info');

    protected $guarded = array('id', 'days_of_week', 'start_times', 'finish_times', 'categories');


    public $search_fields = array('title','short_title', 'url', 'summary', 'description');
    
    // public function user()
    // {
    //     return $this->belongsTo('User');
    // }

    public function setCostAttribute($value)
    {
        $this->attributes['cost'] = (is_numeric($value) AND $value > 0) ? number_format($value, 2):0;
    }

    public function setBookingLinkAttribute($value)
    {
        if($this->attributes['booking'] == 'external')
        {
            if($value == '') return; //return blank value

            /* In /models/redirections.php - move to URLHELPER */
            $replace = array(
                'http://',
                'https://',
                'ftp://'
            );

            if(strpos($value, '://') !== false)
            {
                $url = parse_url($value);
                $value = $url['host'] . $url['path'];
            }

            $value = str_replace($replace, '', $value);
            $this->attributes['booking_link'] = $value; 
        }
    }

    public function setStartDateAttribute($value)
    {
        // $dates = explode("/", trim($value));

        // $dates = array_reverse($dates);

        // $value = implode("/", $dates);

        // $this->attributes['start_date'] = date("Y-m-d", strtotime($value));

        $this->attributes['start_date'] = date("Y-m-d H:i:s", strtotime(str_replace("/", "-", $value)));
    }

    public function setFinishDateAttribute($value)
    {
        // $dates = explode("/", trim($value));

        // $dates = array_reverse($dates);

        // $value = implode("/", $dates);

        // $this->attributes['finish_date'] = date("Y-m-d", strtotime($value));

        $this->attributes['finish_date'] = date("Y-m-d H:i:s", strtotime(str_replace("/", "-", $value)));
    }

    public function getStartDateAttribute($value)
    {
        if($value == '0000-00-00 00:00:00') return time();
        return strtotime($value);
    }

    public function getFinishDateAttribute($value)
    {
        if($value == '0000-00-00 00:00:00') return strtotime("+1 day");
        return strtotime($value);
    }   

    public function getCostAttribute($value)
    {
        if($value > 0) return str_replace(".00", "", $value);
        return;
    } 



    public static function get_selection($emptyitem_text = 'Select an event')
    {
        $selection = array(0 => $emptyitem_text);
        
        $events = Event::all();
        
        foreach($events as $event) {
            $selection[$event->id] = $event->name;
        }
        
        return $selection;
    }

    public function get_importance_types()
    {
        return array('-1' => 'Low', '0' => 'Normal', '1' => 'High');
    }

    public function getBookingOptions()
    {
        return array(
            'no' => 'No',
            // 'online' => 'Online',
            'external' => 'External'
        );        
    }

    public static function ticketsLive()
    {
        return self::$event_tickets;
    }

    public function dateFormat($format = 'd/m/Y')
    {
        return date($format, $this->start_date);
    }


    public function scopeFuture($query, $from_date = null, $to_date = null)
    {
        if(null !== $from_date OR null !== $to_date)
        {
            if(null !== $from_date AND null !== $to_date)
            {
                // return $query->where('events.start_date', '>=', date("Y-m-d H:i:s", $from_date))->where('events.finish_date', '<=', date("Y-m-d H:i:s", $to_date));
                return $query->where(function($query) use ($from_date, $to_date)
                {
                    $query->where(function($query) use ($from_date, $to_date)
                    {
                        $query->where('events.start_date', '>=', date("Y-m-d H:i:s", $from_date))
                            ->where('events.finish_date', '<=', date("Y-m-d 23:59:59", $to_date));
                    })->orWhere(function($query) use ($from_date, $to_date)
                        {
                            $query->where('events.start_date', '<=', date("Y-m-d H:i:s", $from_date))
                                ->where('events.finish_date', '<=', date("Y-m-d 23:59:59", $to_date))
                                ->where('events.finish_date', '>=', date("Y-m-d H:i:s"));
                        });
                });
            }
            else if(null !== $from_date AND null === $to_date)
            {
                return $query->where(function($query) use ($from_date)
                {
                    $query->where('events.start_date', '>=', date("Y-m-d H:i:s", $from_date))
                        ->orWhere('events.finish_date', '>=', date("Y-m-d H:i:s", $from_date));
                });

                // return $query->where(function($query) use ($from_date)
                // {
                //     $query->where('events.start_date', '>=', date("Y-m-d H:i:s", $from_date))
                //         ->orWhere(function($query) use ($from_date)
                //         {
                //             $query->where('events.start_date', '<=', date("Y-m-d H:i:s", $from_date))
                //                 ->where('events.finish_date', '<=', date("Y-m-d H:i:s", $from_date))
                //                 ->orWhere('events.finish_date', '>=', date("Y-m-d H:i:s", $from_date));
                //         });
                // });
            }
            else if(null === $from_date OR null !== $to_date)
            {
                return $query->where('events.finish_date', '<=', date("Y-m-d 23:59:59", $to_date))
                    ->where('events.start_date', '>=', date("Y-m-d H:i:s"));
            }
        }
        else
        {
            $from_date = date("Y-m-d H:i:s");

            // return $query->where(function($query) use ($from_date)
            // {
            //     return $query->where('start_date', '>=', $from_date)
            //         ->where('finish_date', '<=', $from_date);
            // })
            // ->orWhere(function($query) use ($from_date)
            // {
            //     return $query->where('start_date', '<=', $from_date)
            //         ->where('finish_date', '<=', $from_date);
            // })
            // ->orWhere(function($query) use ($from_date)
            // {
            //     return $query->where('start_date', '>=', $from_date)
            //         ->where('finish_date', '>=', $from_date);
            // })
            // ->orWhere(function($query) use ($from_date)
            // {
            //     return $query->where('start_date', '<=', $from_date)
            //         ->where('finish_date', '>=', $from_date);
            // });

            // return $query->where(function($query) use ($from_date)
            // {
            //     return $query->where('start_date', '<=', $from_date)
            //         ->where('finish_date', '>=', $from_date);
            // })
            // ->orWhere(function($query) use ($from_date)
            // {
            //     return $query->where('start_date', '>=', $from_date)
            //         ->where('finish_date', '>=', $from_date);
            // });

            return $query->where(function($query) use ($from_date)
            {
                $query->where('events.start_date', '>=', $from_date)
                    ->orWhere(function($query) use ($from_date)
                    {
                        $query->where('events.start_date', '<=', $from_date)
                            ->where('events.finish_date', '<=', $from_date)
                            ->where('events.finish_date', '>=', date("Y-m-d H:i:s"));
                    });
            });
        }
    }

    public function scopePast($query)
    {
        return $query->where('finish_date', '<=', date("Y-m-d H:i:s"));
    }

    public function scopeLive($query, $live = 1)
    {
        return $query->where('live', '=', $live);
    }

    public function scopeShowcase($query, $value = '0')
    {
        return $query->where('showcase', '=', $value);
    }

    public function storeTaxonomy($tags, $media, $class)
    {
        DB::table('taxonomy_relationships')->where('tax_type', '=', $class)->where('item_id', '=', $media->id)->delete();

        $input = array();
        foreach($tags as $tag)
        {
            $input[] = array('term_id' => $tag, 'tax_type' => $class, 'item_id' => $media->id);
        }

        DB::table('taxonomy_relationships')->insert($input);
    }

    public function taxonomy_relationships()
    {
        return $this->belongsToMany('Categories', 'taxonomy_relationships', 'item_id', 'term_id');
    }

    public function has_venue()
    {

    }

    public function taxonomy()
    {
        $data = array('category' => array(), 'tag' => array());
        $terms = DB::table('taxonomy_relationships')->leftJoin('taxonomy', 'taxonomy_relationships.term_id', '=', 'taxonomy.id')->where('item_id', '=', $this->id)->where('tax_type', '=', get_class($this))->select('taxonomy.id', 'taxonomy.type')->get();

        foreach($terms as $term)
        {
            $data[$term->type][] = $term->id;
        }

        return $data;
    }

    public function scopeSearch($query, $value)
    {
        // dd($value);
        if(trim($value) != '')
        {
            $value = trim($value);

            $fields_exp = implode(", ", $this->search_fields);
            $fields = $this->search_fields;
            $searchTerms = explode(' ', $value);

            // MATCH (pagecontent_standard.name, pagecontent_standard.body, pageindex.tags) AGAINST ('%s' IN BOOLEAN MODE) AS occurrences

            // $query .= sprintf("(pagecontent_standard.name LIKE \"%%%s%%\" OR pagecontent_standard.body LIKE \"%%%s%%\" OR pageindex.tags = '%s' OR pageindex.tags LIKE \"%%,%s,%%\" OR pageindex.tags LIKE \"%s,%%\" OR pageindex.tags LIKE \"%%,%s\") OR ",
            // mysql_real_escape_string($trim_val),
            // mysql_real_escape_string($trim_val),
            // mysql_real_escape_string($trim_val),
            // mysql_real_escape_string($trim_val),
            // mysql_real_escape_string($trim_val),
            // mysql_real_escape_string($trim_val));


// SELECT 
//     DISTINCT MATCH (events.title, events.summary, events.description) AGAINST ('bro' IN BOOLEAN MODE) AS occurrences, 
//     events.*, 
//     venues.name AS centre_name, 
//     venues.name AS centre_name, 
//     venues.default_url, 
//     (SELECT filename FROM images WHERE images.page_id = events.id AND images.type = 'events' AND images.deleted = 0 ORDER BY images.order_id LIMIT 1) AS filename 
// FROM 
//     events 
// LEFT JOIN 
//     venues ON venues.id = events.venue_id
// WHERE 
//     events.live = 1 AND 
//     events.deleted = 0 AND 
//     ((events.title LIKE "%bromance%" OR events.summary LIKE "%bromance%" OR events.description LIKE "%bromance%")) 
//     GROUP BY id


// select 
//     DISTINCT MATCH(title, short_title, url, summary, description) AGAINST ('Bro' IN BOOLEAN MODE) AS relevance, 
//     `events`.* 
// from 
//     `events` 
// where 
//     (`title` LIKE ? or `short_title` LIKE ? or `url` LIKE ? or `summary` LIKE ? or `description` LIKE ?) and 
//     (`start_date` >= ?) 
// having 
//     `relevance` > ? 
// order by 
//     `relevance` desc, 
//     `start_date` asc


            $query->select(array(
                    DB::raw("DISTINCT MATCH(" . $fields_exp . ") AGAINST ('" . $value . "' IN BOOLEAN MODE) AS relevance"),
                    'events.*'
                )
            );

            foreach($searchTerms as $term)
            {
                $query->where(function($query) use ($fields, $term)
                {
                    $i = 0;
                    foreach($fields as $field)
                    {
                        if($i == 0)
                        {
                            $query->orWhere($field, 'LIKE', "%$term%");
                        }
                        else
                        {
                            $query->orWhere($field, 'LIKE', "%$term%");
                        }
                        $i++; 
                    }
                });
            }

            /* just 1 terms */
            $query->groupBy('id')
                ->orderBy('relevance', 'DESC')
                ->having('relevance', '>', '0');
        }

        return $query;
    }

    public function roundToQuarterHour($timestring, $nearest = 15, $round = 'ceil')
    {
        $rounded_seconds = ceil($timestring / ($nearest * 60)) * ($nearest * 60);

        return date('Y-m-d H:i:s', $rounded_seconds);
    }

    public function hasAddress()
    {
        if(!$this->event_address)
        {
            /* remove address */
            $this->address_1 = '';
            $this->address_2 = '';
            $this->address_3 = '';
            $this->town = '';
            $this->postcode = '';
            $this->country = '';
        }
    }

    public function hasBooking()
    {
        if($this->booking == '')
        {
            $this->booking_link = ''; //unset
        }
    }

    public function isRegular($value = NULL)
    {
        $this->frequency = ($value == 1) ? 1:NULL;
    }


    public function getImage($field, $temp_image = '')
    {
        if($this->$field != '') return $this->$field;
        else if($temp_image != '') return $temp_image;
    }
}?>
