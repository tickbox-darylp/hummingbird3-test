
{{$i = 0;}}
@foreach ($nav as $mainsection => $sections)
    <ul id="nav">
        <li class='has_sub'> 
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <img src="img/user1.png">{{$mainsection}}  <b class="caret"></b>              
            </a>
            <ul class="dropdown-menu">
        @foreach ($sections as $section => $items)
                <li class='has_sub'>
                    <a class="subnav_{{$i}}" href="javascript:void(0);">{{$section}}</a>
                    <ul class="dropdown-menu">
                        @foreach ($items as $item)
                            <li><a href="/cms/{{$item->link}}">{{$item->name}}</a></li>
                        @endforeach
                    </ul>
            </li>
        @endforeach
            </ul>
        </li>
    </ul>

@endforeach

<div class="subnav">            
    <h1><a href="/logout" style="color:#666">Log Out</a></h1>
</div>


<style>
    .subnav-section { display: none; }
</style>