<?php

class LCSlidersController extends CmsbaseController 
{
    /**
     * Inject the models.
     * @param LCSlider $Slider
     * @param User $user
     */
    public function __construct(LCSlider $Slider, User $user)
    {
        parent::__construct();

        $this->slider = $Slider;
        $this->user = Auth::user();

        //breadcrumb manager
        $this->data['breadcrumbs'][] = array(
            'classes' => '',
            'icon' => '',
            'title' => 'Sliders',
            'url' => '/' . General::backend_url() . '/' . $this->slider->cms_url
        );
    }

    /**
     * Get a list of all resources
     *
     * @return Response/View
     */
    public function getIndex()
    {
        if($this->user->hasAccess('read', get_class($this->slider)))
        {
            $this->data['sliders'] = LCSlider::whereNull('deleted_at')->orderBy('id', 'ASC')->paginate($this->pagination);

            return View::make('HummingbirdBase::cms.lc-sliders', $this->data);
        }
        else
        {
            return View::make('HummingbirdBase::cms.errors.unauthorised'); //replace with CMS Error/Unauthorised controller
        }
    }

    /**
     * Edit a resource
     * @param Integer $id
     * @return Response/View
     */
    public function getEdit($id = false)
    {
        if($this->user->hasAccess('update', get_class($this->slider)))
        {
            $this->slider = LCSlider::where('id', '=', $id)->whereNull('deleted_at')->first();

            if(null !== $this->slider)
            {
                $this->data['slider'] = $this->slider;

                //breadcrumb manager
                $this->data['breadcrumbs'][] = array(
                    'title' => 'Edit: ' . General::wrap_text($this->slider->title, '&quot;'),
                    'url' => '',
                    'icon' => ''
                );

                return View::make('HummingbirdBase::cms.lc-sliders-edit', $this->data);
            }
            else
            {
                return \Redirect::to(App::make('backend_url').'/lc-sliders/')->with('error', 'There was a problem processing your request. Please try again.');
            }
        }
        else
        {
            return View::make('HummingbirdBase::cms.errors.unauthorised');
        }
    }

    /**
     * Edit a resource and save
     * @param Integer $id
     * @return Response/View
     */
    public function postEdit($id)
    {
        if($this->user->hasAccess('update', get_class($this->slider)))
        {
            $this->slider = LCSlider::where('id', '=', $id)->whereNull('deleted_at')->first();

            if(null !== $this->slider)
            {
                $rules = array();

                $input  = Input::except('_token');

                $validator = Validator::make(
                    $input,
                    $rules
                );

                /* Does it validate? */
                if(!$validator->fails())
                {
                    $this->slider = $this->slider->fill($input);
                    $slides = array();

                    if(isset($input['slides']))
                    {
                        $counter = 0;

                        // we have some slides
                        foreach($input['slides'] as $key => $slide)
                        {
                            $__slide = ArrayHelper::object_to_array(json_decode($slide)); //decode into normal array form
                            $__slide_title = (isset($input['slide_titles']) AND isset($input['slide_titles'][$key]) AND $input['slide_titles'][$key] != '') ? $input['slide_titles'][$key]:$__slide['title'];
                            $__slide_enabled = (isset($input['slide_enabled']) AND isset($input['slide_enabled'][$key]) AND $input['slide_enabled'][$key] != '') ? TRUE:FALSE;

                            /* Slider information */
                            $__slide_info = (isset($__slide['template'])) ? $this->slider->calculate($__slide['template']):$this->slider->calculate('full');

                            /* Set base information */
                            $slides[$counter] = array(
                                'title' => $__slide_title,
                                'template' => $__slide['template'],
                                'rows' => $__slide_info['rows'],
                                'columns' => $__slide_info['columns'],
                                'total-modules' => $__slide_info['total-modules'],
                                'enabled' => $__slide_enabled,
                                'regions' => array()
                            );

                            if(isset($__slide['appearance']))
                            {
                                $slides[$counter]['appearance'] = $__slide['appearance'];
                            }

                            /* Now set regions */
                            for($start_region = 1; $start_region <= $slides[$counter]['total-modules']; $start_region++)
                            {
                                $item = 'region-' . $start_region;

                                if(isset($__slide['regions']->$item)) 
                                {
                                    $__slide['regions']->$item->type = (!isset($__slide['regions']->$item->type)) ? 'custom':$__slide['regions']->$item->type;

                                    if(isset($__slide['regions']->$item))
                                    {
                                        switch($__slide['regions']->$item->type)
                                        {
                                            case 'events':
                                                /* Store event information */
                                                $slides[$counter]['regions']['region-' . $start_region] = array(
                                                    'type' => 'events',
                                                    'select' => $__slide['regions']->$item->select,
                                                    'order_by' => $__slide['regions']->$item->order_by,
                                                    'ignore_dupes' => $__slide['regions']->$item->ignore_dupes,
                                                    'venue' => ((isset($__slide['regions']->$item->venue)) ? $__slide['regions']->$item->venue:0)
                                                );

                                                /* Do we have items that switch over to over areas */
                                                if(isset($__slide['regions']->$item->regions))
                                                {
                                                    $slides[$counter]['regions']['region-' . $start_region]['regions'] = $__slide['regions']->$item->regions;
                                                }

                                                /* Now store details about the events */
                                                if($__slide['regions']->$item->select != 'all' AND $__slide['regions']->$item->select != '')
                                                {
                                                    if($__slide['regions']->$item->select == 'custom')
                                                    {
                                                        $slides[$counter]['regions']['region-' . $start_region]['events'] = $__slide['regions']->$item->events;
                                                    }

                                                    if($__slide['regions']->$item->select == 'filter')
                                                    {
                                                        /* Set filter */
                                                        $slides[$counter]['regions']['region-' . $start_region]['filter_by'] = $__slide['regions']->$item->filter_by;

                                                        /* Filtering by categories */
                                                        if($__slide['regions']->$item->filter_by == 'categories')
                                                        {
                                                            if(count($__slide['regions']->$item->categories) > 0)
                                                            {
                                                                $slides[$counter]['regions']['region-' . $start_region]['categories'] = $__slide['regions']->$item->categories;
                                                            }
                                                        }

                                                        /* Filtering by tags */
                                                        if($__slide['regions']->$item->filter_by == 'tags')
                                                        {
                                                            if(count($__slide['regions']->$item->tags) > 0)
                                                            {
                                                                $slides[$counter]['regions']['region-' . $start_region]['tags'] = $__slide['regions']->$item->tags;
                                                            }
                                                        }

                                                        /* Filtering by dates */
                                                        if($__slide['regions']->$item->filter_by == 'dates')
                                                        {
                                                            /* Dates from */
                                                            if(isset($__slide['regions']->$item->dates->dates_from) AND $__slide['regions']->$item->dates->dates_from != '')
                                                            {
                                                                $slides[$counter]['regions']['region-' . $start_region]['dates']['dates_from'] = $__slide['regions']->$item->dates->dates_from;
                                                            }

                                                            /* Dates to */
                                                            if(isset($__slide['regions']->$item->dates->dates_to) AND $__slide['regions']->$item->dates->dates_to != '')
                                                            {
                                                                $slides[$counter]['regions']['region-' . $start_region]['dates']['dates_to'] = $__slide['regions']->$item->dates->dates_to;
                                                            }
                                                        }
                                                    }
                                                }
                                                break;
                                            case 'custom':
                                            default:
                                                $slides[$counter]['regions']['region-' . $start_region] = array(
                                                    'type' => 'custom',
                                                    'title' => $__slide['regions']->$item->title,
                                                    'summary' => $__slide['regions']->$item->summary,
                                                    'image' => $__slide['regions']->$item->image,
                                                    'link' => (isset($__slide['regions']->$item->link)) ? $__slide['regions']->$item->link:''
                                                );
                                                break;
                                        }
                                    }
                                }
                            }

                            $counter++;
                        }
                    }
                    
                    $this->slider->slider_data = $slides;

                    General::pp($slides);

                    if($this->slider->save())
                    {
                        Activitylog::log([
                            'action' => 'UPDATED',
                            'type' => get_class($this->slider),
                            'link_id' => $this->slider->id,
                            'description' => 'Updated slider',
                            'notes' => Auth::user()->username . " updated the slider &quot;" . $this->slider->title . "&quot;"
                        ]);

                        return Redirect::to(App::make('backend_url').'/lc-sliders/edit/' . $id)->with('success', '&quot;' . $this->slider->title . '&quot; has been updated.');
                    }
                    else
                    {
                        return Redirect::to(App::make('backend_url').'/lc-sliders/edit/' . $id)->with('error', 'There was a problem updating your slider. Please try again.');
                    }
                }
                else
                {
                    return Redirect::to(App::make('backend_url').'/lc-sliders/edit/' . $id)->withErrors($validator);
                }
            }
            else
            {
                return Redirect::to(App::make('backend_url').'/lc-sliders/')->with('error', 'There was a problem. Please try again.');
            }
        }
        else
        {
            return View::make('HummingbirdBase::cms.errors.unauthorised');
        }
    }

    /**
     * Add a new resource
     * @return Response/View
     */
    public function postAdd()
    {
        if($this->user->hasAccess('create', get_class($this->slider)))
        {
            $rules = array(
                'title' => 'required|min:1|unique:lc_sliders,title,NULL,id,deleted_at,NULL'
            );

            $input  = Input::except('_token');

            $validator = Validator::make(
                $input,
                $rules
            );

            /* Does it validate? */
            if(!$validator->fails())
            {
                $this->slider = (new LCSlider)->fill($input);

                if($this->slider->save())
                {
                    Activitylog::log([
                        'action' => 'CREATED',
                        'type' => get_class($this->slider),
                        'link_id' => $this->slider->id,
                        'description' => 'Created a new slider',
                        'notes' => Auth::user()->username . " created a new slider"
                    ]);

                    return Redirect::to(App::make('backend_url').'/lc-sliders/')->with('success', '&quot;' . $this->slider->title . '&quot; has been created.');
                }
                else
                {
                    return Redirect::to(App::make('backend_url').'/lc-sliders/')->with('error', 'There was a problem creating your slider. Please try again.');
                }
            }
            else
            {
                return Redirect::to(App::make('backend_url').'/lc-sliders/')->withErrors($validator);
            }
        }
        else
        {
            return View::make('HummingbirdBase::cms.errors.unauthorised');
        }
    }

    /**
     * Delete a resource
     * @param Integer $id
     * @return Response/View
     */
    public function getDelete($id)
    {
        if($this->user->hasAccess('delete', get_class($this->slider)))
        {
            $this->slider = LCSlider::whereNull('deleted_at')->where('id', $id)->first();

            if(null !== $this->slider)
            {
                if($this->slider->delete())
                {
                    Activitylog::log([
                        'action' => 'DELETED',
                        'type' => get_class($this->slider),
                        'link_id' => $this->slider->id,
                        'description' => 'Deleted slider',
                        'notes' => Auth::user()->username . " deleted the slider &quot;" . $this->slider->title . "&quot;"
                    ]);

                    /* Redirect */
                    return \Redirect::to(App::make('backend_url').'/lc-sliders/')->with('success', 'Slider has been deleted.');
                }
                else
                {
                    /* Redirect */
                    return \Redirect::to(App::make('backend_url').'/lc-sliders/')->with('error', 'Slider could not be deleted.');
                }
            }
            else
            {
                /* Redirect */
                return \Redirect::to(App::make('backend_url').'/lc-sliders/')->with('error', 'There was a problem processing your request. Please try again.');
            }
        }
        else
        {
            return View::make('HummingbirdBase::cms.errors.unauthorised');
        }
    }

    /**
     * Delete a resource
     * @param Integer $id
     * @return Response/View
     */
    public function ajax_getModal()
    {
        if($this->user->hasAccess('read', get_class($this->slider)))
        {
            $this->data['posted'] = (isset($_GET)) ? $_GET:array();
            $this->data['posted']['content_type'] = (isset($this->data['posted']['content_type'])) ? $this->data['posted']['content_type']:'';
            $this->data['posted']['get-content-data'] = false;

            if(isset($this->data['posted']['json']) AND $this->data['posted']['json'] != '')
            {
                $this->data['slide_data'] = json_decode($this->data['posted']['json']);
            }

            if($this->data['posted']['get'] == 'content-type')
            {
                $this->data['posted']['get-content-data'] = true;

                switch($this->data['posted']['content_type'])
                {
                    case 'events':
                    case 'custom':
                    default:
                        return View::make('HummingbirdBase::cms.lc-sliders-modal', $this->data); 
                        break;
                }
            }
            elseif($this->data['posted']['get'] == 'by-content')
            {
                switch($this->data['posted']['content_type'])
                {
                    case 'events':
                    case 'custom':
                    default:
                        return View::make('HummingbirdBase::cms.lc-sliders-modal', $this->data); 
                        break;
                }
            }
            elseif($this->data['posted']['get'] == 'get-events-data')
            {
                switch($this->data['posted']['data'])
                {
                    case '':
                    case 'custom':
                        $events = Events::future()->live()->whereNull('deleted_at')->orderBy('start_date', 'ASC')->get();

                        return json_encode(array('events' => $events));
                        break;
                }
            }
            elseif($this->data['posted']['get'] == 'taxonomy')
            {
                $taxonomy = ($this->data['posted']['taxonomy'] == 'categories') ? 'Categories':'Tags';

                if($taxonomy == 'Categories')
                {
                    $event_cat_id = Categories::where('name', '=', 'Events')->type()->get()->first();
                    $results_pre = $taxonomy::whereNull('deleted_at')->type()->where('parent', '=', $event_cat_id->id)->get();
                }
                else
                {
                    $results_pre = $taxonomy::whereNull('deleted_at')->type()->orderBy('name', 'ASC')->get();
                }

                return json_encode(array('taxonomy' => $results_pre));
            }
            elseif($this->data['posted']['get'] == 'list-mode')
            {
                return View::make('HummingbirdBase::cms.lc-sliders-modal-settings', $this->data);
            }
        }
        else
        {
            return '*'.json_encode(array('title' => 'Permission denied', 'message' => 'Unfortunately, you do not have access to edit this slider. Please try again.'));
        }
    }
}
