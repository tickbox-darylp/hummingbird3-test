<?php

class FrontendController extends Controller 
{
    public $data = array();
    public $settings = array();
    public $theme;
    public $page = array();
    public $url = array();
    
    public function __construct()
    {
        $this->beforeFilter('check.app.up');

        $this->data['path'] = parse_url(Request::url() . '/');

        if(!isset($this->data['path']['path'])) $this->data['path']['path'] = '/';

        $this->data['clean_path'] = ArrayHelper::cleanExplodedArray(explode('/', $this->parseURL($this->data['path'])));

        $this->settings = $this->getSiteSettings();
        $this->theme = Themes::activeTheme();
        
        View::addLocation(base_path()."/themes/public/$this->theme/");
        View::addNamespace('theme', base_path() . '/themes/public/' . $this->theme . '/');

        $this->data['page'] = $this->loadPage($this->data['path']['path']);
    }
    
    public function loadPage($slug, $page = NULL)
    {
        $page = Page::where('permalink', '=', $slug)->whereNull('deleted_at')->where('status', '=', 'public')->get()->first();

        if(null === $page)
        {
            // Replace with Request::segments() //
            $url = ArrayHelper::cleanExplodedArray(explode('/', $slug));
            array_pop($url);

            if(count($url) > 0)
            {
                return $this->loadPage('/' . implode("/", $url) . '/');
            }

            return NULL;
        }

        return $page;
    }

    public function getSiteSettings()
    {
        return Setting::all();
    }

    public function side_navGenerator($page)
    {
        return (new Page)->list_all_pages($page, array('path' => $this->data['clean_path']), true);
    }


    public function shortcodes($html, $page)
    {
        // dd($page);
        // $page_id = ($page->parentpage_id != 1) ? $page->parentpage_id:$page->id;
        if(strpos($html, "[HB::PAGE-NAVIGATION]") !== FALSE)
        {
            $html = str_replace("[HB::PAGE-NAVIGATION]", $this->side_navGenerator($page), $html);
        }

        if(strpos($html, '[HB::MODULE') !== FALSE)
        {
            /*
            $mods = array(7, 8, 9, 10);

            foreach($mods as $mod)
            {
                $mod = Moduletemplate::find($mod);

                $html = str_replace('[HB::MODULETEMPLATE(' . $mod->id . ')]', $mod->html, $html);
            }*/

            $modules = array();

            /* Looking for [HB::MODULETEMPLATE(x)] */
            
            #\{(loop:[a-zA-Z0-9\_\:\|\=\,\.\ ]+)\}(.+)\{\/loop\}#
            #\[(.*?)\]#

            // $find_modules_pattern = '#\[HB::MODULETEMPLATE+(.*?)\]#';
            $find_modules_pattern = '#\[HB::MODULE\((.*?)\)\]#';
            preg_match_all($find_modules_pattern, $html, $modules);

            foreach($modules[1] as $key => $selected_module)
            {
                $module = Module::find($selected_module);

                if(null !== $module AND $module->template())
                {
                    $template = Moduletemplate::find($module->moduletemplate_id); //get template
                    $ModuleBuilder = new ModuleParser($template, $module, $page); //build the ModuleParser

                    $html = str_replace($modules[0][$key], $ModuleBuilder->render(), $html);
                }
                else
                {
                    /* Standard HTML Modules - no template */
                    $mod_html = (null !== $module) ? $module->module_data['structure']:'';
                    $html = str_replace($modules[0][$key], $mod_html, $html);
                    // $html = str_replace($modules[0][$key], $ModuleBuilder->render(), $mod_html);
                }
            }

            $find_modules_pattern = '#\[HB::MODULETEMPLATE\((.*?)\)\]#';
            preg_match_all($find_modules_pattern, $html, $modules);

            foreach($modules[1] as $key => $selected_module)
            {   
                $selected_module = (int) $selected_module;

                $modules = Module::where('moduletemplate_id', '=', $selected_module)->whereNull('deleted_at')->orderBy('name', 'ASC')->get();

                $html_to_store = (($selected_module == 11 OR $selected_module == 4) AND (strpos($_SERVER["HTTP_HOST"], 'bristolhealthpartners') !== false)) ? array():'';

                foreach($modules as $module)
                {
                    if(null !== $module AND $module->template())
                    {
                        $template = Moduletemplate::find($module->moduletemplate_id); //get template
                        $ModuleBuilder = new ModuleParser($template, $module); //build the ModuleParser

                        if(($selected_module == 11 OR $selected_module == 4) AND (strpos($_SERVER["HTTP_HOST"], 'bristolhealthpartners') !== false))
                        {
                            $html_to_store[] = $ModuleBuilder->render();
                        }
                        else
                        {
                            $html_to_store .= $ModuleBuilder->render();
                        }
                    }
                    else
                    {
                        /* Standard HTML Modules - no template */
                        $mod_html = (null !== $module) ? $module->module_data['structure']:'';

                        if(($selected_module == 11 OR $selected_module == 4) AND (strpos($_SERVER["HTTP_HOST"], 'bristolhealthpartners') !== false))
                        {
                            $html_to_store[] = $ModuleBuilder->render();
                        }
                        else
                        {
                            $html_to_store .= $ModuleBuilder->render();
                        }
                    }
                }

                if(($selected_module == 11 OR $selected_module == 4) AND (strpos($_SERVER["HTTP_HOST"], 'bristolhealthpartners') !== false))
                {
                    try
                    {
                        $contents = File::get(base_path() . "/themes/public/$this->theme/tmp/modules-homepage-hits-roller.htm");
                        $contents = str_replace("[template]", implode(" ", $html_to_store), $contents);

                    }
                    catch (Illuminate\Filesystem\FileNotFoundException $exception)
                    {
                        // $event_html = '';
                        // die($exception);
                    }

                    $html = str_replace("[HB::MODULETEMPLATE($selected_module)]", $contents, $html);
                }
                else
                {
                    $html = str_replace("[HB::MODULETEMPLATE($selected_module)]", $html_to_store, $html); 
                }
            }
        }


        return $html;
    }

    // Show a page by slug
    public function show($slug = '/')
    {
        if((boolean) Config::get('HummingbirdBase::hummingbird.maintenance') === TRUE)
        {
            if(View::exists('maintenance')) return View::make('maintenance');

            return Response::make(HtmlHelper::maintenanceMode(), 503);
        } 

        /* This can be done better */
        $slug = ($slug[0] == '/') ? $slug:'/'.$slug; //add a trailing slash
        $slug = ($slug[strlen($slug)-1] == '/') ? $slug:$slug.'/'; //add a ending slash
        // $this->url = explode("/", $slug); // add this to global 

        /* Check redirects */
        $redirect = $this->checkRedirects($slug);
        
        if($redirect) {
            if($redirect->type != 'meta')
            {
                return Redirect::to($redirect->to, $redirect->type);
            }
            else
            {
                $this->data['visible_redirect'] = array('url' => $redirect->to, 'duration' => '5');
            }
        }

        if(null !== Input::get('version'))
        {
            $page = PageVersion::where('id', '=', Input::get('version'))->get()->first();
        }
        else
        {
            // $page = Page::where('permalink', '=', $slug)->whereNull('deleted_at')->where('status', '=', 'public')->get()->first();
            $page = $this->data['page'];
        }

        if(isset($this->data['visible_redirect']))
        {
            $this->data['page'] = NULL;
            $this->data['html'] = HtmlHelper::message('temp-redirect', array('url' => $redirect->to));
            return View::make('internal', $this->data);
        }

        if($page) {
            if($page->status == 'public' OR null !== Input::get('version'))
            {
                // $this->data['html'] = (new ShortcodeManager)->parse($page->content);
                $this->data['html'] = (isset($this->html)) ? $this->html:$this->shortcodes($page->content, $page);
                // $this->data['page'] = $page;

                // todo: do password-protected pages
                if ($page->protected == 1) {
                    
                    // show popup with login form
                    if(View::exists('protected-login'))
                    {
                        return View::make('protected-login', $this->data);
                    }
                    else
                    {
                        $args = array(
                            'username' => (($page->username != '') ? $page->username:false),
                            'password' => $page->password,
                            'login-to' => $page->id
                        );

                        $protected = (new HtmlHelper)->protectedForm($args);

                        if($protected !== true)
                        {
                            $this->data['html'] = $protected;
                            return View::make('internal', $this->data);
                        }
                    }
                }

                if(strpos($_SERVER["HTTP_HOST"], 'bristolhealthpartners') !== false)
                {
                    /* Bristol Health Partners Website */
                    if(strpos($slug, '/whats-on/events') !== false)
                    {
                        $this->data['slug'] = $slug; 
                        return View::make('events', $this->data);
                    } 

                    if(strpos($slug, 'our-research') !== false)
                    {
                        $this->data['slug'] = $slug; 
                        return View::make('our-research', $this->data);
                    } 

                    //if(strpos($slug, 'search') !== false)
                    if($slug == '/search/')
                    {
                        $this->data['slug'] = $slug; 
                        return View::make('search', $this->data);
                    } 

                    if(strpos($slug, 'latest-news') !== false)
                    {
                        $this->data['slug'] = $slug; 
                        return View::make('news', $this->data);
                    }

                    if(strpos($slug, 'blog') !== false)
                    {
                        $this->data['slug'] = $slug; 
                        return View::make('blog', $this->data);
                    }

                    if(strpos($slug, 'contact-us') !== false)
                    {
                        $this->data['slug'] = $slug; 
                        return View::make('internal-template', $this->data);
                    }

                    if($slug == '/health-integration-teams/') return View::make('hits', $this->data);

                    if($slug == '/') return View::make('homepage', $this->data);
                    if($slug == '/subscribe-to-our-newsletter/') return View::make('subscribe', $this->data);
                    if($this->data['clean_path'][0] == 'archives') return View::make('taxonomy', $this->data);
                    return View::make('internal', $this->data);
                }

                if(strpos($_SERVER["HTTP_HOST"], 'alpha.hummingbirdcms.com') !== false OR $_SERVER["REMOTE_ADDR"] == '127.0.0.1')
                {
                    /* Luton Culture Website */
                    if(strpos($slug, 'our-research') !== false)
                    {
                        $this->data['slug'] = $slug; 
                        return View::make('our-research', $this->data);
                    } 

                    //if(strpos($slug, 'search') !== false)
                    if($slug == '/search/')
                    {
                        $this->data['slug'] = $slug; 
                        return View::make('search', $this->data);
                    } 

                    if(strpos($slug, 'latest-news') !== false)
                    {
                        $this->data['slug'] = $slug; 
                        return View::make('news', $this->data);
                    }

                    if(strpos($slug, 'blog') !== false)
                    {
                        $this->data['slug'] = $slug; 
                        return View::make('blog', $this->data);
                    }

                    if(strpos($slug, 'contact-us') !== false)
                    {
                        $this->data['slug'] = $slug; 
                        return View::make('internal-template', $this->data);
                    }

                    if($slug == '/health-integration-teams/') return View::make('hits', $this->data);
                    if($slug == '/whats-on/' OR $slug == '/whats-on') return View::make('whats-on', $this->data);
                    if($slug == '/luton-libraries/' OR $slug == '/luton-libraries/luton-libraries') return View::make('venue', $this->data);
                    if($slug == '/hat-factory/' OR $slug == '/hat-factory/hat-factory') return View::make('venue', $this->data);
                    if($slug == '/stockwood-discovery-centre/' OR $slug == '/stockwood-discovery-centre/stockwood-discovery-centre') return View::make('venue', $this->data);
                    if($slug == '/wardown-park-museum/' OR $slug == '/wardown-park-museum/wardown-park-museum') return View::make('venue', $this->data);
                    if($slug == '/luton-library-theatre/' OR $slug == '/luton-library-theatre/luton-library-theatre') return View::make('venue', $this->data);
                    if($slug == '/community-centres/' OR $slug == '/community-centres/community-centres') return View::make('centres', $this->data);
                    if($slug == '/join-us/' OR $slug == '/join-us/join-us') return View::make('internal-slider', $this->data);
                    if($slug == '/subscribe-to-our-newsletter/') return View::make('subscribe', $this->data);
                    if($slug == '/support-us/') return View::make('internal-slider', $this->data);
                    if($slug == '/venue-hire/') return View::make('internal-slider', $this->data);
                    if($slug == '/other-services/') return View::make('internal-slider', $this->data);
                    if($slug == '/learning/') return View::make('internal-slider', $this->data);
                    if($slug == '/') return View::make('homepage', $this->data);
                    return View::make('internal', $this->data);
                }


                if($slug == '/') return View::make('homepage', $this->data);
                return View::make('internal', $this->data);
            }
        }


        /**
         *
         * Bristol Health Partners Website
         *
         */ 
        if(strpos($_SERVER["HTTP_HOST"], 'bristolhealthpartners') !== false)
        {
            if(strpos($slug, '/events/') !== false)
            {
                $this->data['slug'] = $slug; 
                return View::make('events', $this->data);
            }
            
            if(strpos($slug, 'latest-news') !== false)
            {
                $this->data['slug'] = $slug; 
                return View::make('news', $this->data);
            } 

            if(strpos($slug, 'blog') !== false)
            {
                $this->data['slug'] = $slug; 
                return View::make('blog', $this->data);
            } 
        }


        /**
         *
         * Luton Culture Website
         *
         */ 
        if(strpos($_SERVER["HTTP_HOST"], 'alpha.hummingbirdcms.com') !== false OR $_SERVER["REMOTE_ADDR"] == '127.0.0.1')
        {
            if(strpos($slug, '/whats-on/view/') !== false)
            {
                $this->data['slug'] = $slug; 
                return View::make('whats-on-item', $this->data);
            }
            
            if(strpos($slug, 'latest-news') !== false)
            {
                $this->data['slug'] = $slug; 
                return View::make('news', $this->data);
            } 

            if(strpos($slug, 'blog') !== false)
            {
                $this->data['slug'] = $slug; 
                return View::make('blog', $this->data);
            } 
        }

        /**
         *
         * ERROR HANDLING
         *
         */
        if(count($this->data['clean_path']) == 0 OR $this->data['clean_path'][0] != 'hummingbird')
        {
            $FileHelper = new FileHelper;
            $URL        = Request::path();

            /* Find error */
            $Error      = Error::where('url', '=', $URL)->first();

            if(null === $Error)
            {
                $Error  = new Error;
                $Error->url = $URL;
                $Error->type = 'URL';

                /* Detect type of error */
                $path_parts = pathinfo($URL);

                if(isset($path_parts['extension']))
                {
                    $path_parts['extension'] = strtolower($path_parts['extension']); //lower the extension
                    $error_type = $FileHelper->getValidKeyByExt($path_parts['extension']);
                    $Error->type = ($error_type != 'other') ? 'Files':ucfirst($error_type); //standard error if file not found in allowed types
                }
            }
            else
            {
                //error exists - update fields
                $Error->accessed++;
            }

            $Error->save();


            /**
             *
             * 404 Error Page
             *
             */ 
            $page = Page::where('permalink', '=', '/404/')->whereNull('deleted_at')->where('status', '=', 'public')->first();

            if(null !== $page)
            {
                $this->data['html'] = $this->shortcodes($page->content, $page);
                $this->data['page'] = $page;
                $this->data['slug'] = $slug; 
            }
            else
            {
                $this->data['html'] = HtmlHelper::message('404');
                $this->data['page'] = null;
                $this->data['slug'] = '404'; 
            }


            /**
             *
             * Check View exists
             *
             */ 
            try
            {
                return Response::view('404', $this->data, 404);
            }
            catch (InvalidArgumentException $error)
            {
                return Response::view('internal', $this->data, 404);
            }
        }

        App::abort(404);
    }

    
    public function checkRedirects($slug)
    {
        # see if redreict exists
        $redirect = Redirections::where('from', '=', $slug)->whereNull('deleted_at')->orderBy('id', 'DESC')->first();
        
        if(!$redirect) return false;

        /* Create stat */
        $redirect->addStat();

        # update count
        $redirect->no_used += 1;
        $redirect->last_used = date('Y-m-d H:i:s');
        $redirect->save();
        
        # redirect to url given
        return $redirect;
    }

    private function addShortcodesIn($content)
    {
        // todo: access registered plugins
        $registered_plugins = array('\Tickbox\Events');
        
        foreach($registered_plugins as $registered_plugin) {
            
            $controller_name = "$registered_plugin\\FrontendController";
            
            $controller = new $controller_name;
            
            foreach($controller->views as $view) {
                
                if (substr_count($content, '{{'.$registered_plugin.':'.$view.'}}') > 0) {
                    $html = $controller->$view()->render();
                    $content = str_replace('{{'.$registered_plugin.':'.$view.'}}', $html, $content);
                }
                // todo: regex
                if (substr_count($content, '{{'.$registered_plugin.':'.$view.'(1)}}') > 0) {
                    $html = $controller->$view(1)->render();
                    $content = str_replace('{{'.$registered_plugin.':'.$view.'(1)}}', $html, $content);
                }
                
            }
        
        }
    }
    
    public function search()
    {
        $search = Search::make(array('which_end' => 'front'));
    }

    public function parseURL(Array $url)
    {
        if(!isset($url['path'])) return;

        return $url['path'];
    }
}