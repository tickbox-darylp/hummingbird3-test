<?php

class SettingsController extends CmsbaseController {

    
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}


    public function index()
    {
        $this->data['tag'] = 'Website Settings';
        return View::make('HummingbirdBase::cms/settings', $this->data);
    }




    
    function get_cms_nav()
    {
        // // todo: need to do this dynamically
        // $distinct_pairs = CmsNav::select(DB::raw('mainsectionname, sectionname'))->groupBy(array('mainsectionname', 'sectionname'))->get();
        
        // $nav = array();
        
        // foreach($distinct_pairs as $distinct_pair) {
        //     $nav[$distinct_pair->mainsectionname][$distinct_pair->sectionname] = CmsNav::where('live','=',1)->where('mainsectionname','=',$distinct_pair->mainsectionname)->where('sectionname','=',$distinct_pair->sectionname)->get();
            
        // }
        
        
        // return $nav;
    }
    
    public function search_model($model_name)
    {
        $search = Search::make(array(
            'model' => $model_name, 
            'searchterm' => Request::get('search'),
            'which_end' => 'back'));
        
        $search->run();       
        
        return $search->results;
    }
    
    public function export_model($modelname)
    {
        $model = new $modelname;
        $this->data['modelname'] = strtolower($modelname);
        $this->data['fields'] = $model->export_fields;
        return View::make('HummingbirdBase::cms.export', $this->data);
    }
    
    public function run_export($modelname, $filename)
    {
        $fields = Input::except('_token');
        
        $results = $modelname::select(DB::raw(implode(",", $fields)))->get();
        
        // headings
        $output = '"' . implode('","', $fields) . '"';
        $output .= "\r\n";
 
        foreach ($results as $row) {
            $output .= '"' . implode('","',$row->toArray()) . '"' . "\r\n";
        }
        
        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="'.$filename.'"',
        );
 
        return Response::make(rtrim($output, "\n"), 200, $headers);        
    }
    
    public function pp($arr)
    {
        echo '<pre>'.print_r($arr, true).'</pre>';die();
    }

    public function getSiteSettings()
    {      
        $data = array();

        $results = Setting::all();
        
        foreach($results as $result)
        {
            $data[] = $result['key'];
            $data[$result['key']] = unserialize($result['value']);
        }

        return $data;
    }

    public function updateSiteSettings($key, $value)
    {
        (new Setting)->updateSettings($key, $value);
    }


    public function import()
    {
        DB::unprepared(file_get_contents(base_path() . '/import/sql/sitesettings.sql'));

        $data = DB::table('sitesettings')->get();

        if(count($data) > 0)
        {
            $import_data = array();

            foreach($data as $setting)
            {
                $new_data = array();
                $new_data['key'] = $setting->name;
                $new_data['value'] = $setting->value;

                $import_data[] = $new_data;
            }

            DB::table('settings')->insert($import_data);
            Schema::dropIfExists('sitesettings');
        } 

        return Redirect::to(General::backend_url()); // redirect to dashboard
    }

}
