@extends('HummingbirdBase::install.layout')

@section('content')
<div class='step'>
    <h2>Step {{$step}}</h2>
    <p>Plugins</p>
    <form method='post' action=''>
        <table>
        
        @foreach (Plugins::all() as $plugin)
        
        <tr>
            <td>{{Form::checkbox('plugins[]', $plugin)}}</td>
            <td>{{Form::label('', ucfirst($plugin))}}</td>        
        </tr>
        @endforeach
        
        </table>
        <input type='submit' value='Continue' />
    </form>
</div>
@stop