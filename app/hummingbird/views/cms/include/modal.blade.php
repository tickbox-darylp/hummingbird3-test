<!-- Modal -->
<div class="modal fade" id="{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">{{$title}}</h4>
            </div>
            <div class="modal-body">
                @foreach($inputs as $input)
                {{Form::label($input['field'])}}
                {{Form::text($input['field'], $input['value']) }}<br/>
                @endforeach
                
                
                {{$message}} 
                
            </div>
            <div class="modal-footer">
                
                <div style='clear:both;'></div>
                
                @foreach($buttons as $button)
                
                <button type="button" class="btn btn-primary" data-dismiss="modal" data-action="{{$button['action']}}">{{$button['text']}}</button>
                
                @endforeach
                
                @if ($cancel)
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                @endif
                
            </div>
        </div>
    </div>
</div>