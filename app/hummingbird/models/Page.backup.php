<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class PageBHP extends LaravelBook\Ardent\Ardent
{
    use SoftDeletingTrait;

    public $table = 'pages';
    public $cms_url = '';
    public $dash_icon = 'fa-edit';
    
    protected $fillable = array('title', 'url', 'parentpage_id', 'content', 'enable_comments', 'hide_from_search', 'status', 'protected', 'username', 'password', 'show_in_nav', 'searchable', 'search_image', 'featured_image', 'custom_menu_name');
    // protected $guarded = array('password');
    
    public $search_fields = array('title', 'url', 'content');
    public $export_fields = array('title', 'url', 'content', 'enable_comments', 'hide_from_search', 'status', 'protected');

    protected $permission_type = 'pages';

    public static $rules = array(
        'title' => 'required'
    );

    public function getSpecialPerms()
    {
        $perms = array();

        $perms[] = array(
            'label' => 'Limit accessible pages',
            'data' => array()
        );
    }
    
    public function parentpage()
    {
        return $this->belongsTo('Page');
    }

    public function storeTaxonomy($tags, $page, $class)
    {
        DB::table('taxonomy_relationships')->where('tax_type', '=', $class)->where('item_id', '=', $page->id)->delete();

        $input = array();
        foreach($tags as $tag)
        {
            $input[] = array('term_id' => $tag, 'tax_type' => $class, 'item_id' => $page->id);
        }

        DB::table('taxonomy_relationships')->insert($input);
    }
    
    public function taxonomy()
    {
        $data = array('category' => array(), 'tag' => array());
        $terms = DB::table('taxonomy_relationships')->leftJoin('taxonomy', 'taxonomy_relationships.term_id', '=', 'taxonomy.id')->where('item_id', '=', $this->id)->where('tax_type', '=', get_class($this))->select('taxonomy.id', 'taxonomy.type')->get();

        foreach($terms as $term)
        {
            $data[$term->type][] = $term->id;
        }

        return $data;
    }

    public function setPasswordAttribute($value)
    {
        if(strlen($value) == 0) return $this->attributes['password'] = '';

        $this->attributes['password'] = Hash::make($value);
    }

    public function setUrlAttribute($value)
    {
        #echo "value: $value,";
        $this->attributes['url'] = ($value == '') ? PagesHelper::get_url_from_title($this->attributes['title']) : $value;
        #echo $this->attributes['permalink'];exit;
    }

    public function setPermalinkAttribute($value)
    {
        // dd($this->attributes['parentpage_id']);exit;
        // dd($this->attributes['parentpage_id']);
        $this->attributes['permalink'] = PagesHelper::build_slug($this->attributes['url'], $this->attributes['parentpage_id']);

        if($this->attributes['permalink'] != '' OR $this->attributes['permalink'] != '/')
        {
            if($this->attributes['permalink'][0] != '/')
            {
                $this->attributes['permalink'] = '/'.$this->attributes['permalink'];
            }

            if(substr($this->attributes['permalink'], -1) != '/')
            {
                $this->attributes['permalink'] = $this->attributes['permalink']."/";
            }
        }

        // echo $this->attributes['permalink'];exit;
    }

    public function getPermalinkAttribute($value)
    {
        return $this->attributes['permalink'];
        // return PagesHelper::build_slug($this->attributes['url'], $this->attributes['parentpage_id']);
    }
    
    public static function get_selection($emptyitem_text = 'Select a page', $excludes = array())
    {
        $selection = array(0 => $emptyitem_text);
        
        $pages = Page::all();
        
        foreach($pages as $page) {
            if(in_array($page->id, $excludes)) continue;
            $selection[$page->id] = $page->title;
        }
        
        return $selection;
    }
    
    public static function get_blog_feed_styles()
    {
        return array();
    }

    public static function get_statuses()
    {
        return array('draft' => 'Draft', 'public' => 'Public');
    }
    
    public function getPermissionType()
    {
        return $this->permission_type;
    }

    public function deletedPages($count = false)
    {
        $pages = Page::whereNotNull('deleted_at');

        if($count) return $pages->count();

        return $pages->get();
    }

    public function cms_list_pages($ignore_id = false)
    {
        // return array_flatten($this->buildFlatTree(Page::whereNull('deleted_at')->orderby('parentpage_id', 'ASC')->orderby('id', 'ASC')->get()));
        // return array_flatten(Page::whereNull('deleted_at')->orderby('parentpage_id', 'ASC')->orderby('id', 'ASC')->get());
        // return Page::whereNull('deleted_at')->orderby('parentpage_id', 'ASC')->orderby('id', 'ASC')->get();
        $pages = (!$ignore_id) ? Page::whereNull('deleted_at')->orderby('parentpage_id', 'ASC')->orderby('id', 'ASC')->get():Page::where('id', '!=', $ignore_id)->whereNull('deleted_at')->orderby('parentpage_id', 'ASC')->orderby('id', 'ASC')->get();

        return $this->buildFlatTree($pages);
    }


    public function list_all_pages($page, $args = array(), $return = false)
    {
        // to be returned
        $pages = array();

        // default args
        if(empty($args))
        {
        }

        $these_pages = Page::whereNull('deleted_at')->where('status', '=', 'public')
            ->where(function($query) use($page)
            {
                $query->orWhere('id', '=', $page->id)
                    ->orWhere('parentpage_id', '=', $page->id)
                    ->orWhere('id', '=', $page->parentpage_id);
            })
            ->orderBy('parentpage_id', 'ASC')
            ->get();

/*
    select * from `pages` where `deleted_at` is null and `status` = 'public' 
    and (`id` = 2 or `parentpage_id` = 2 or `id` = 1) ORDER BY `id` DESC
*/
        $selected_pages = array_flatten($these_pages);

        $pages = $this->newBuildTree($selected_pages, $page->id, $page->parentpage_id);

        // General::pp($pages);
        // die();


        if(count($pages) > 0)
        {
            ob_start();?>

            <ul id="sidenav">
                <?php $this->returnTree($pages, $page->id);?>
            </ul>

            <?php $content = ob_get_clean();

            if($return) return $content;
            echo $content;
        }
    }

    public function buildFlatTree($elements, $parentId = null, $level = 0, &$pages = array()) 
    {
        // $pages = array();

        foreach ($elements as $key => $element) {
            /* Top level parent */
            if ($element->parentpage_id !== $parentId) continue;

            unset($elements[$key]);

            $element['title'] = str_repeat("&nbsp;", $level*2).$element['title']; //append arrow to name
            $pages[$element->id] = $element->title;
            $this->buildFlatTree($elements, $element->id, $level + 1, $pages);
        }

        return $pages;
    }

// select * from `pages` where `deleted_at` is null and `status` = 'public' or (`id` = 3 and `parentpage_id` = 3 and `id` = 2)
// SELECT * FROM pages WHERE (id = 3 or parentpage_id = 3 or id = 2) AND deleted_at IS NULL AND status = 'public'


    public function newBuildTree(&$elements, $current_page, $parentId = null, $level = 0, $max_level = 1, $pages = array()) 
    {
        foreach ($elements as $key => $element)
        {
            /* Top level parent */
            // echo $element;
            // die();
            if(($element->id == $parentId AND $level == 0) OR ($element->parentpage_id == $parentId OR ($element->id == $current_page AND $element->id == $parentId)))
            {
                unset($elements[$key]);

                $pages[$element->id]['item'] = $element;
                $pages[$element->id]['children'] = $this->newBuildTree($elements, $element->id, $element->id, $level + 1, $max_level);
            }
        }

        return $pages;

        // foreach ($elements as $key => $element) 
        // {
        //     unset($elements[$key]);
        //     if($element->id === $parentId) continue;

        //     if(($element->id == $parentId AND $level == 0) OR $element->parentpage_id == $parentId OR $element->id == $current_page)
        //     {
                

        //         $pages[$element->id] = $element;

        //         $pages[$element->id]['children'] = $this->buildTree($elements, $element->id, $level + 1, $max_level, $pages);
        //     }
        // }

        // return $pages;
    


// use wp_list_pages to display parent and all child pages all generations (a tree with parent)
// $parent = 93;
// $args=array(
//   'child_of' => $parent
// );
// // $pages = get_pages($args);  
// if ($pages) {
//   $pageids = array();
//   foreach ($pages as $page) {
//     $pageids[]= $page->ID;
//   }

//   $args=array(
//     'title_li' => 'Tree of Parent Page ' . $parent,
//     'include' =>  $parent . ',' . implode(",", $pageids)
//   );
//   // wp_list_pages($args);
// }

    }


    public function buildTree($elements, $parentId = null, $level = 0, $max_level = 1) 
    {
        /* pages to return */
        $pages = array();

        foreach ($elements as $key => $element) 
        {
            if ($element->id !== $parentId AND ($element->parentpage_id !== $parentId OR $element->parentpage_id === NULL)) continue;

            if ($level == 0 AND $element->id !== $parentId) continue;

            if(($element->id == $parentId AND $level == 0) OR $element->parentpage_id === $parentId)
            {
                $pages[$element->id] = $element;

                if($level + 1 <= $max_level) $pages[$element->id]['children'] = $this->buildTree($elements, $element->id, $level + 1, $max_level);
            }
        }

        return $pages;
    }

    public function returnTree($elements, $current_page = false, $count = 0)
    {
        if(is_array($elements))
        {
            foreach($elements as $element)
            {
                $element_children = (isset($element['children'])) ? $element['children']:false;
                $element = (isset($element['item'])) ? $element['item']:$element;

                $class = ($_SERVER["REQUEST_URI"] == $element->permalink) ? 'active':'';
                $page_title = ($element['show_in_nav'] AND $element['custom_menu_name'] != '') ? $element['custom_menu_name']:$element['title'];

                if($element->show_in_nav OR (!$element->show_in_nav AND ($current_page == $element->id OR strpos($_SERVER["REQUEST_URI"], $element->permalink) !== false)))
                {?>
                    <li><?php echo str_repeat("&nbsp;", $count);?>
                        <a data-count="<?=$count?>" class="<?=$class?>" href="<?php echo $element->permalink;?>"><?php echo $page_title;?></a>
                        <?php if(is_array($element_children) AND count($element_children) > 0)
                        {?>
                            <ul>
                                <?php $this->returnTree($element_children, $current_page, $count++);?>
                            </ul>
                        <?php }?>
                    </li>
                <?php }
                else
                {
                    if(is_array($element_children) AND count($element_children) > 0) $this->returnTree($element_children, $current_page);
                }
            }
        }
    }


/*
function buildTree(array &$elements, $parentId = 0) {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[$element['id']] = $element;
            unset($elements[$element['id']]);
        }
    }
    return $branch;
}

*/



// function wp_list_pages( $args = '' ) {
// 1028            $defaults = array(
// 1029                    'depth' => 0, 'show_date' => '',
// 1030                    'date_format' => get_option( 'date_format' ),
// 1031                    'child_of' => 0, 'exclude' => '',
// 1032                    'title_li' => __( 'Pages' ), 'echo' => 1,
// 1033                    'authors' => '', 'sort_column' => 'menu_order, post_title',
// 1034                    'link_before' => '', 'link_after' => '', 'walker' => '',
// 1035            );
// 1036    
// 1037            $r = wp_parse_args( $args, $defaults );
// 1038    
// 1039            $output = '';
// 1040            $current_page = 0;
// 1041    
// 1042            // sanitize, mostly to keep spaces out
// 1043            $r['exclude'] = preg_replace( '/[^0-9,]/', '', $r['exclude'] );
// 1044    
// 1045            // Allow plugins to filter an array of excluded pages (but don't put a nullstring into the array)
// 1046            $exclude_array = ( $r['exclude'] ) ? explode( ',', $r['exclude'] ) : array();
// 1047    
// 1048            /**
// 1049             * Filter the array of pages to exclude from the pages list.
// 1050             *
// 1051             * @since 2.1.0
// 1052             *
// 1053             * @param array $exclude_array An array of page IDs to exclude.
// 1054             */
// 1055            $r['exclude'] = implode( ',', apply_filters( 'wp_list_pages_excludes', $exclude_array ) );
// 1056    
// 1057            // Query pages.
// 1058            $r['hierarchical'] = 0;
// 1059            $pages = get_pages( $r );
// 1060    
// 1061            if ( ! empty( $pages ) ) {
// 1062                    if ( $r['title_li'] ) {
// 1063                            $output .= '<li class="pagenav">' . $r['title_li'] . '<ul>';
// 1064                    }
// 1065                    global $wp_query;
// 1066                    if ( is_page() || is_attachment() || $wp_query->is_posts_page ) {
// 1067                            $current_page = get_queried_object_id();
// 1068                    } elseif ( is_singular() ) {
// 1069                            $queried_object = get_queried_object();
// 1070                            if ( is_post_type_hierarchical( $queried_object->post_type ) ) {
// 1071                                    $current_page = $queried_object->ID;
// 1072                            }
// 1073                    }
// 1074    
// 1075                    $output .= walk_page_tree( $pages, $r['depth'], $current_page, $r );
// 1076    
// 1077                    if ( $r['title_li'] ) {
// 1078                            $output .= '</ul></li>';
// 1079                    }
// 1080            }
// 1081    
// 1082            /**
// 1083             * Filter the HTML output of the pages to list.
// 1084             *
// 1085             * @since 1.5.1
// 1086             *
// 1087             * @see wp_list_pages()
// 1088             *
// 1089             * @param string $output HTML output of the pages list.
// 1090             * @param array  $r      An array of page-listing arguments.
// 1091             */
// 1092            $html = apply_filters( 'wp_list_pages', $output, $r );
// 1093    
// 1094            if ( $r['echo'] ) {
// 1095                    echo $html;
// 1096            } else {
// 1097                    return $html;
// 1098            }
// 1099    }


    public function scopeSearch2($query, $value)
    {
        if($value != '')
        {

        }
        
        return $query;
    }

    public function scopeSearch($query, $value)
    {
        // dd($value);
        if($value != '')
        {
            $searchTerms = explode(' ', $value);

            return $query->select(DB::raw("pages.*, MATCH (" . implode(',', $this->search_fields) . ") AGAINST ('$value' IN BOOLEAN MODE) AS relevance"))->whereRaw("MATCH (" . implode(',', $this->search_fields) . ") AGAINST (? IN BOOLEAN MODE)", array($searchTerms));
// $search_results=DB::table('stuff')
// ->where(DB::raw('MATCH(`tags`)'),'AGAINST', DB::raw('("+'.implode(' +',$search_terms).'" IN BOOLEAN MODE)'))
// ->get();

            // $newsearch_terms = array();

            // foreach($searc)

            // dd($searchTerms);
            // return $query->select(DB::raw("DISTINCT MATCH(" . implode(',', $this->search_fields) . ") AGAINST (? IN BOOLEAN MODE) AS score, pages.*"))->setBindings($searchTerms)->havingRaw("score > 0")->orderBy('score', '>', '0');
            // return $query->whereRaw("MATCH(" . implode(', ', $this->search_fields) . ") AGAINST(? IN BOOLEAN MODE)", array($searchTerms))
            // return $query->select(DB::raw("MATCH(" . implode(', ', $this->search_fields) . ") AGAINST(? IN BOOLEAN MODE)", array($searchTerms)))->having('score', '>', '0');
        }
    }


// public function search() {
// $searchTerm = Input::get('search');
// $posts = DB::table('posts')->select('id', 'title', 'summary', 'created_at', 'category_id', 'title_slug',
// DB::raw('(match (title,content) against (\''.$searchTerm.'\' in boolean mode)) as score'))
// ->whereRaw('match (title,content) against (\''.$searchTerm.'\' in boolean mode)')
// ->where('publish','=','1')
// ->orderBy('score', 'desc')
// ->orderBy('created_at', 'desc');
// return view('welcome')->with('posts', $posts->paginate(10));
// }


    public static function boot()
    {
        parent::boot();                
        // Page::observe(new PageObserver);
    }
    
}
