<?php

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
    Log::info('Application currently in maintenance mode');

	/* Do we have a mainteance file for the theme */
    $theme = Themes::activeTheme();
    
    View::addLocation(base_path()."/themes/public/$theme/");
    View::addNamespace('theme', base_path() . '/themes/public/' . $theme . '/');
    if(View::exists('maintenance')) return Response::make(View::make('maintenance'), 503);

    /* Do we have a maintenance mode function enabled? */
    if(class_exists('HtmlHelper')) return Response::make(HtmlHelper::maintenanceMode(), 503);
    
    /* Standard return */
	return Response::make("The site is currently under construction. Please check back later. ", 503);
});

Event::listen('500', function()
{
    return View::make('500', array());
});
?>
