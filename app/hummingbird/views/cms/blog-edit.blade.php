@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/datetimepicker/datetimepicker.css" />
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />
@stop

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                {{ Session::get('success') }}
            </div>
        </div>
    </div>  
@endif

@if(Session::has('message'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-info fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                {{ Session::get('message') }}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('errors'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                {{ Session::get('errors') }}
            </div>
        </div>
    </div>
@endif


<div class="row">
    <div class="col-md-12">
        <h3><a href="{{url(General::backend_url().'/blog')}}">All Articles</a> &raquo; {{$post->title}}</h3>
    </div>

    <div class="col-md-12">
        <div class="row">
            @if(isset($versions) AND count($versions) > 0)
                <div class="col-md-12">
                    <section class="panel" style="background-color:white;padding:20px;">
                        <h6>Recent versions</h6>
                        <ul>
                            @foreach($versions as $version)
                                <li>{{$version->title}} - created by {{User::find($version->user_id)->name}} on {{DateTimeHelper::date_format(strtotime($version->updated_at), 'j F Y H:i:s')}} [<a href="/{{App::make('backend_url')}}/blog/{{$post->id}}/?version={{$version->id}}">view</a>]</li>
                            @endforeach
                        </ul>
                    </section>
                </div>
            @endif

            <div class="col-md-12">
                <section class="panel" style="background-color:white;padding:20px;">
                    {{ Form::open(array('action' => array('BlogController@update', $post_id), 'method' => 'put', 'class' => 'form-horizontal')); }}

                        <?php if(isset($post_version))
                        {?>
                            <input type="hidden" name="version" value="{{$post_version}}">
                        <?php }?>

                        <div class="form-group">
                            <div class="col-sm-10">
                                <h4>Article Details</h4>
                            </div>
                        </div>

                        <div class="form-group hidden-xs">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-default btn-primary pull-right">Update</button>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Title:</label>
                            <div class="col-sm-10">
                                <input name="title" id="title" type="text" class="form-control" placeholder="Article Title" value="{{$post->title}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="summary" class="col-sm-2 control-label">Summary:</label>
                            <div class="col-sm-10">
                                {{ Form::textarea('summary', $post->summary, array('class' => 'form-control summary-truncate', 'rows' => '3', 'data-summary-limit' => 255)) }}
                                <span class="help-block">The summary will appear as an excerpt on the listings and search pages (if provided).</span>
                                <span class="help-block character-count"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="content" class="col-sm-2 control-label">Description:</label>
                            <div class="col-sm-10">
                                {{ Form::textarea('content', $post->content, array('class' => 'redactor-content')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="start_date" class="col-sm-2 control-label">Post Date:</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input name="post_date" id="post_date" type="text" class="datetimepicker form-control" placeholder="Post available from" value="{{date("d/m/Y H:i:s", $post->post_date)}}">
                                    <span class="input-group-addon" style="border-left:0;border-right:0;">- to - </span>
                                    
                                    <?php $post_end_date = ($post->post_end_date != '') ? date("d/m/Y H:i:s", $post->post_end_date) : '';?>

                                    <input name="post_end_date" id="post_end_date" type="text" class="datetimepicker form-control" placeholder="Turn post off" value="{{$post_end_date}}">
                                    
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                                <span class="help-block">The date your post will be available. By default, it will be the initial date ({{date("d/m/Y H:i:s", strtotime($post->created_at))}}) the post was created.</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="author" class="col-sm-2 control-label">Author:</label>
                            <div class="col-sm-10">
                                <input name="author" id="author" type="text" class="form-control" placeholder="Author" value="{{$post->author}}">
                                <span class="help-block">Contributors (post editors) will appear as a comma separated list if not provided</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-sm-2 control-label">Active:</label>
                            <div class="col-sm-4">
                                {{ Form::select('status', Post::get_statuses(), $post->status, array('class' => 'form-control', 'id' => 'live')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10">
                                <h4>Article Details</h4>
                            </div>
                        </div>

                        <div class="form-group media-library-holder">
                            {{ Form::label('featured_image', 'Featured image ', array('class' => 'col-sm-2')) }}
                            <div class="col-sm-8 image-link">
                                <div class="input-group">
                                    <input type="text" name="featured_image" class="form-control media-image" readonly="" @if($post->featured_image != '') value="{{$post->featured_image}}" @endif>
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file pull-right">
                                            Browse… <input type="file" multiple="" class="browse-media-library" @if($post->featured_image != '') data-collection-id="{{ Media::where('location', '=', $post->featured_image)->first()->collection }} @endif">
                                        </span>
                                    </span>
                                </div>
                                <span class="help-block @if($post->featured_image == '') hide @endif"><a href="#" class="remove-media-library">Remove featured image</a></span>
                            </div>

                            <div class="col-sm-2 @if($post->featured_image == '') hide @endif image-holder">
                                <img src="{{$post->featured_image}}" class="img-responsive">
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('searchable', 'Show in Search? ', array('class' => 'col-sm-2')) }}
                            <div class="col-sm-10">
                                {{ Form::hidden('searchable', '0') }}
                                
                                {{ Form::checkbox('searchable', 1, $post->searchable, array('class' => 'searchable')) }}
                            </div>
                        </div>

                        <div class="form-group show-search-image @if(!$post->searchable) hide @endif media-library-holder">
                            {{ Form::label('search_image', 'Search image ', array('class' => 'col-sm-2')) }}
                            <div class="col-sm-8 image-link">
                                <div class="input-group">
                                    <input type="text" name="search_image" class="form-control media-image" readonly="" @if($post->search_image != '') value="{{$post->search_image}}" @endif>
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file pull-right">
                                            Browse… <input type="file" multiple="" class="browse-media-library" @if($post->featured_image != '') data-collection-id="{{ Media::where('location', '=', $post->featured_image)->first()->collection }} @endif">
                                        </span>
                                    </span>
                                </div>
                                <span class="help-block @if($post->search_image == '') hide @endif"><a href="#" class="remove-media-library">Remove featured image</a></span>
                            </div>

                            <div class="col-sm-2 @if($post->search_image == '') hide @endif image-holder">
                                <img src="{{$post->search_image}}" class="img-responsive">
                            </div>
                        </div>

        <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }</style>

                        <div class="form-group">
                            <div class="col-sm-10">
                                <h4>Article SEO</h4>
                            </div>
                        </div>

                        <?php 
                            $meta_title = (isset($meta['meta_title'])) ? $meta['meta_title']:'';
                            $meta_keywords = (isset($meta['meta_keywords'])) ? $meta['meta_keywords']:'';
                            $meta_description = (isset($meta['meta_description'])) ? $meta['meta_description']:'';
                        ?>

                        <div class="form-group">
                            <label for="meta_title" class="col-sm-2 control-label">Meta Title:</label>
                            <div class="col-sm-10">
                                <input name="meta_title" id="meta_title" type="text" class="form-control" placeholder="Meta-data title" value="{{$meta_title}}">
                                <span class="help-block">By default, the post title will be used if blank</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="meta_keywords" class="col-sm-2 control-label">Meta Keywords:</label>
                            <div class="col-sm-10">
                                <input name="meta_keywords" id="meta_keywords" type="text" class="form-control" placeholder="Meta-data keywords" value="{{$meta_keywords}}">
                                <span class="help-block">Please provide a comma separated list of keywords</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="meta_description" class="col-sm-2 control-label">Meta Description:</label>
                            <div class="col-sm-10">
                                {{ Form::textarea('meta_description', $meta_description, array('class' => 'form-control', 'rows' => '3')) }}
                                <span class="help-block">By default, the post summary will be used if blank</span>
                            </div>
                        </div>

                        @include('HummingbirdBase::cms.taxonomy-layout', array('taxonomy' => $taxonomy))

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default btn-primary pull-right">Update</button>
                            </div>
                        </div>
                    {{Form::close()}}
                </section>
            </div>
        </div>
    </div>
</div>

<div id="media-lib-modal" class="modal fade" style="z-index:100000;">
    <div class="modal-dialog clearfix">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close redactor-modal-btn redactor-modal-close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Media Library</h4>
            </div>
        
            <div class="modal-body media-images">
                <div class="row"></div>
            </div>
            <div class="modal-footer">
                <footer>
                    <button type="button" class="btn btn-primary redactor-modal-btn redactor-modal-action-btn">Insert</button>
                </footer>
            </div>
        </div>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<style type="text/css">
.redactor-box {overflow: hidden;}

/* toolbar background and other styles*/
.redactor-toolbar {
    background: #2A313A;
    color:white;
    -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
}

.redactor-editor {
    border-top:0;
    border-left:1px solid #E9EDF0;
    border-right:1px solid #E9EDF0;
    border-bottom:1px solid #E9EDF0;
}
 
/* toolbar buttons*/
.redactor-toolbar li a {
    color: white;
}
 
 /*buttons hover state*/
.redactor-toolbar li a:hover {
    background: #00AEEF;
    color: white;
}
 
/*buttons active state*/
.redactor-toolbar li a:active,
.redactor-toolbar li a.redactor-act {
    background: #00AEEF;
    color: white;
}

body .redactor-box-fullscreen {
    -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
}

body .wrapper.addOpacity {opacity:0.1;}

.imagemanager-insert.active {
    border:1px solid blue;
}

/*.redactor-editor * {
    color:white !important;
}*/

.bootstrap-datetimepicker-widget {z-index:10001 !important;}


</style>

@stop

@section('scripts')
    <script src="/themes/hummingbird/default/lib/moment/moment.js"></script>
    <script src="/themes/hummingbird/default/lib/datetimepicker/datetimepicker.js"></script>
    <script src="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>
    <script src="/themes/hummingbird/default/lib/media-library/media-library.js"></script>

    <script>
        function truncate(el)
        {
            var parent = el.parent();
            var item = el.val();
            var item_length = item.length;
            var limit = (parseInt(el.data('summary-limit')) > 0) ? parseInt(el.data('summary-limit')):255;
            var limit_area = parent.find('.character-count');
            var remaining = parseInt(limit-item_length);
            remaining = (remaining < 0) ? 0:remaining;

            var limit_text = (remaining != 1) ? ' characters remaining':' character remaining';

            if(limit_area.length < 1)
            {
                parent.append('<div class="character-count" />');
                limit_area = parent.find('.character-count');
            }

            limit_area.text(remaining + limit_text);

            if (remaining <= 0)
            {
                el.val((item).substring(0, limit - 1));
                return false;
            }
        }

        $(document).ready(function()
        {
            if($(".datetimepicker").length > 0)
            {
                $('#post_date.datetimepicker').datetimepicker(
                {
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    defaultDate: $(this).val(),
                    format: "DD/MM/YYYY HH:mm:ss",
                    showClose: true
                });

                $('#post_end_date.datetimepicker').datetimepicker(
                {
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    defaultDate: $(this).val(),
                    minDate: $("#post_date.datetimepicker").data("DateTimePicker").date(),
                    format: "DD/MM/YYYY HH:mm:ss",
                    showClose: true  
                });


                $("#post_date.datetimepicker").on("dp.change", function (e)
                {
                    $('#post_end_date.datetimepicker').data("DateTimePicker").minDate(e.date);
                });
            }
        
            if($(".select").length > 0)
            {
                $(".select").click(function(e)
                {
                    e.preventDefault();

                    var type = $(this).data('select');

                    switch(type)
                    {
                        case 'all':
                            $($(this).data('select-el')).each(function()
                            {
                                $(this).prop('checked', true);
                            });
                            break;
                        case 'none':
                            $($(this).data('select-el')).each(function()
                            {
                                $(this).prop('checked', false);
                            });
                            break;
                        case 'invert':
                            $($(this).data('select-el')).each(function()
                            {
                                var new_val = ($(this).prop('checked')) ? false:true;
                                $(this).prop('checked', new_val);
                            });
                            break;
                    }
                });
            }

            if($(".searchable").length > 0)
            {
                $(".searchable").click(function()
                {
                    $(".show-search-image").toggleClass('hide');
                });
            }

            if($(".summary-truncate").length > 0)
            {
                $(".summary-truncate").on('keyup change', function()
                {
                    truncate($(this));
                });

                $(".summary-truncate").each(function()
                {
                    truncate($(this));
                });
            }
        });
    </script>
@stop