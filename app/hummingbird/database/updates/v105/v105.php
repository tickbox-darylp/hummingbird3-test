<?php

class v105 extends HbSeeder
{
    public function __construct()
    {
    }

    public function run()
    {
        $this->cleanUpFiles();
    }

    public function add_seed_data(){}

    public function register_blocks(){}

    public function register_modules(){}

    public function register_permissions(){}

    public function register_widgets(){}

    public function add_cmsnav_items(){}

    public function cleanUpFiles()
    {
        if(File::exists(HUMMINGBIRD_PATH . '/database/updates/' . get_class($this) . '/files.php'))
        {
            $updated_files = File::getRequire (HUMMINGBIRD_PATH . '/database/updates/' . get_class($this) . '/files.php' );

            if(count($updated_files) > 0)
            {        
                foreach($updated_files as $item)
                {
                    if($item['is_directory'])
                    {
                        if (file_exists(base_path() . $item['location'])) File::deleteDirectory(base_path() . $item['location']);
                    }
                    else
                    {
                        if (file_exists(base_path() . $item['location'])) unlink(base_path() . $item['location']);
                    }
                }
            }
        }
    }
}
