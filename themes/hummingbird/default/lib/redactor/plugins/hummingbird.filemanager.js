if (!RedactorPlugins) var RedactorPlugins = {};

(function($)
{
	RedactorPlugins.filemanager = function()
	{
		return {
	        getTemplate: function()
	        {
	            return String()
	            + '<section id="redactor-modal-advanced">'
	            + '<label>Enter a text</label>'
	            + '<textarea id="mymodal-textarea" rows="6"></textarea>'
	            + '</section>';
	        },
			init: function()
			{
				var button = this.button.add('filemanager', 'File Manager');
				this.button.addCallback(button, this.filemanager.load);

	            // make your added button as Font Awesome's icon
	            this.button.setAwesome('filemanager', 'fa-files-o');
			},
			load: function()
			{
				console.log('opened');
	            this.modal.addTemplate('filemanager', this.filemanager.getTemplate());
	 
	            this.modal.load('filemanager', 'File Manager', 400);
	 
	            this.modal.createCancelButton();
	 
	            var button = this.modal.createActionButton('Insert');
	            button.on('click', this.filemanager.insert);
	 
	            this.selection.save();
	            this.modal.show();
	 
	            $('#mymodal-textarea').focus();

			},
			insert: function(e)
			{
				e.preventDefault();

				var $target = $(e.target).closest('.redactor-file-manager-link');

				this.file.insert('<a href="' + $target.attr('rel') + '">' + $target.attr('title') + '</a>');
			}
		};
	};
})(jQuery);