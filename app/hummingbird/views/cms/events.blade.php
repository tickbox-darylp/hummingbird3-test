@extends('HummingbirdBase::cms.layout')

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                {{ Session::get('success') }}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('error'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                {{ Session::get('error') }}
            </div>
        </div>
    </div>  
@endif

<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <h1 class="pull-left">All Events</h1>

            <!-- Button trigger modal -->
            <button type="button" class="pull-right btn btn-info" data-toggle="modal" data-target="#add-event"><i class="fa fa-plus-circle"></i> Add new event</button>

            @if(count($events) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($events as $event)
                                <?php $duration = ($event->finish_date - $event->start_date > 0) ? (($event->finish_date - $event->start_date) / 86400):'';

                                $date_string = '';
                                if(is_numeric($duration))
                                {
                                    //duration = 1.00035
                                    if(is_float($duration))
                                    {   
                                        if(strtotime(date("Y-m-d", $event->start_date)) == strtotime(date("Y-m-d", $event->finish_date)))
                                        {
                                            $date_string = DateTimeHelper::date_format($event->start_date, "j F Y") . " (" . DateTimeHelper::date_format($event->start_date, "g:ia") . " - " . DateTimeHelper::date_format($event->finish_date, "g:ia") . ")";
                                        }
                                        else
                                        {
                                            $date_string = DateTimeHelper::date_format($event->start_date, "j F Y (g:ia)") . " - " . DateTimeHelper::date_format($event->finish_date, "j F Y (g:ia)");
                                        }
                                    }
                                    else
                                    {
                                        //duration = 1 day
                                        $days = ($duration == 1) ? 'day':'days';
                                        $date_string = DateTimeHelper::date_format($event->start_date, "j F Y") . " ($duration $days)";
                                    }
                                }
                                else
                                {
                                    $date_string = DateTimeHelper::date_format($event->start_date, "j F Y") . $duration;
                                }?>

                                <tr class="<?=(($event->finish_date <= time() AND $event->start_date <= time()) ? 'past':'' )?>">
                                    <td>{{$event->title}}</td>
                                    <td>{{str_replace(":00", "", $date_string)}}</td>
                                    <td>
                                        @if($event->live)
                                            Published
                                        @else
                                            Draft
                                        @endif                                        
                                    </td>
                                    <td>
                                        @if($event->start_date > time() AND isset($events_active))
                                            <a style="background-color:black;color:white;" class="btn btn-xs" href="#" data-original-title="Preview {{$event->title}} permissions"><i class="fa fa-globe"></i></a>
                                        @endif
                                        <a class="btn btn-xs btn-info" href="/{{App::make('backend_url').'/events/edit/'.$event->id}}" data-original-title="Edit {{$event->title}} "><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-xs btn-danger" href="/{{App::make('backend_url').'/events/delete/'.$event->id}}" data-original-title="Delete {{$event->title}} "><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="text-center"> 
                    @if(Input::get('s') == '')
                        {{ $events->links(); }}
                    @endif
                </div>
            @else
                <h3>There are no events planned.</h3>
            @endif
        </div>
    </section>
</div>




<!-- {{ Form::open(array('url' => '/'.General::backend_url().'/events/search')) }}
    {{ Form::label('search', 'Search: ') }}
    {{ Form::text('search', Request::get('search')) }}
    {{ Form::submit('Go') }}
{{ Form::close() }} -->


<div class="modal fade" id="add-event">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add new Event</h4>
            </div>

            <div class="modal-body">
                <section class="panel">
                    <header class="panel-heading tab-bg-dark-navy-blue ">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#single">Single event</a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#import">Mass import</a>
                            </li>
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="single" class="tab-pane active">
                                <?php echo Form::open(array('url' => General::backend_url().'/events/add', 'method' => 'post')) ?>
                                    <h3>Add a new event</h3>

                                    <fieldset>
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Title:</label>
                                            <input type="text" class="form-control" name="title" id="title">
                                            <p class="help-block">Please enter a title for your new event</p>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <div class="form-group pull-right">
                                            <button type="submit" class="btn btn-primary">Add Event</button>
                                        </div>
                                    </fieldset>
                                <?php echo Form::close()?>
                            </div>
                            <div id="import" class="tab-pane">
                                <h3>Import multiple events</h3>

                                <p>If you want to import multiple events, please download <a href="/{{General::backend_url().'/events/skeleton'}}"> this skeleton</a> file and upload as a CSV.</p>

                                <?php echo Form::open(array('url' => General::backend_url().'/events/import', 'method' => 'post', 'files' => true)) ?>
                                    <fieldset>
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Select file:</label>
                                            <input type="file" class="form-control" name="file" id="file">
                                            <p class="help-block">File uploaded must be in <strong><i>.csv</i></strong> format</p>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <div class="form-group pull-right">
                                            <button type="submit" class="btn btn-primary">Import</button>
                                        </div>
                                    </fieldset>
                                <?php echo Form::close()?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<style>
    table .past
    {
        color:#CCC;
    }
</style>

@section('scripts')
    <script>
        $(document).ready(function()
        {
            if($("#add-event").length > 0)
            {
                $("#add-event .modal-footer button[type=submit]").click(function(e)
                {
                    e.preventDefault();

                    var modal = $(this).closest('.modal')

                    modal.find('form').trigger('submit');
                    modal.close();
                });
            }
        });
    </script>
@stop

@stop
