@extends('HummingbirdBase::install.layout')

@section('content')
<div class='step'>
    <h2>Step {{$step}}</h2>
    <p>System administator details</p>
    <form method='post' action='' id='form'>
        <fieldset>
            <label>Name:</label>
            <input type="text" placeholder="John Doe" name="name" value="{{Input::old('name')}}"/>
            {{$errors->first('name')}}
        </fieldset>
        <fieldset>
            <label>Email Address:</label>
            <input type="text" placeholder="john.doe@yourdomain.com" name="email" value="{{Input::old('email')}}"/>
            {{$errors->first('email')}}
        </fieldset>
        <fieldset>
            <label>Username:</label>
            <input type="text" placeholder="Administrator" name="username" value="{{Input::old('username')}}"/>
            {{$errors->first('username')}}
        </fieldset>
        <fieldset>
            <label>Password:</label>
            <input type="password" autocomplete="off" name="password" id="password" class='password-strength' size="40" class="left" data-display="myDisplayElement1" /> 
            <div class="left" id="myDisplayElement1"></div>
            {{$errors->first('password')}}
        </fieldset>
        <fieldset>
            <label>Confirm Password:</label>
            <input type="password" autocomplete="off" name="password_confirmation"/>
            {{$errors->first('password_confirmation')}}
        </fieldset>

        <input type='submit' value='Continue' />
    </form>


</div>


<script type='text/javascript' src='http://code.jquery.com/jquery-2.1.0.min.js'></script>
<script type='text/javascript' src='/assets/cms/js/pStrength.jquery.js'></script>

@include('HummingbirdBase::cms.include.commonjs')

@stop