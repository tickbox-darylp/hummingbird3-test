<?php

class PermalinkTest extends TestCase {

    
	
	public function testSlugCorrect()
	{
		// add page to db
        $page = new Page();
        $page->title = 'First page';
        $page->url = 'first-page';
        $page->save();
        
        // add second with first as parent
        $childPage = new Page();
        $childPage->title = 'Child page';
        $childPage->url = 'child-page';
        $childPage->parentpage_id = $page->id;
        $childPage->save();
        

		$this->assertEquals('first-page/child-page', $childPage->permalink);
	}
    
    	
	public function testUrlCorrect()
	{
		// add page to db
        $page = new Page();
        $page->title = 'First page';
        $page->url = '';
        $page->save();
        

		$this->assertEquals('first-page', $page->url);
	}

}
