<?php

/*
|--------------------------------------------------------------------------
| Site Error Controller
|--------------------------------------------------------------------------
|
| Here is where you can monitor the errors that appear on the site. Manage
| and delete wherever necessary to make sure your site maintains a healthy
| platform.
|
| Needs to INSTALL
| Needs to REGISTER NAVIGATION MENU
| Needs to REGISTER WIDGETS (CMS and FRONTEND)
| Needs to EXPORT DATA
| <iframe width="608" height="358" src="https://www.youtube.com/embed/r1lVPrYoBkA" frameborder="0" allowfullscreen></iframe>
|
*/

class RedirectionsController extends CmsbaseController
{
	public $code_location = 'dashboard';

    public function __construct(Redirections $Redirections)
    {
        parent::__construct();

        $this->model = $Redirections;

        //breadcrumb manager
        $this->data['breadcrumbs'][] = array(
            'classes' => '',
            'icon' => '',
            'title' => 'Website Redirects',
            'url' => '/' . General::backend_url() . '/redirections/'
        );    
    }
    

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        if($this->user->hasAccess('read', get_class($this->model)))
        {
			$this->data['tag'] = 'Manage Redirects';
			$this->data['redirects'] = Redirections::whereNull('deleted_at')->orderBy('created_at', 'DESC')->paginate($this->pagination);
			$this->data['categories'] = $this->getUniqueCategories($this->data['redirects']);

			return View::make('HummingbirdBase::cms/redirections', $this->data);
        }
        else
        {
            return parent::forbidden();
        }
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        if($this->user->hasAccess('create', get_class($this->model)))
        {
			$rules = array(
			    // 'from' => 'required|unique:redirections,from,NULLdeleted_at,NULL|different:to',
			    'from' => 'required|unique:redirections,from,NULL,id,deleted_at,NULL|different:to',
			    // unique:users,email_address,NULL,id,account_id,1
			    'to' => 'required',
			    'type' => 'required'
			);

        	$all_input	= Input::except('_token');
			$input 		= Input::except('category', 'custom_category', '_token');

			$validator = Validator::make(
			    $input,
			    $rules
			);

	        /* Inserting a new category */
	        $redirect = (new Redirections)->fill($input);
	        
	        if(trim($all_input['category']) != '' AND trim($all_input['category']) == 'NEW')
	        {
	        	$redirect->category = trim($all_input['custom_category']);
	        }
	        else
	        {
	        	$redirect->category = trim($all_input['category']);
	        }

	        /* Does it validate? */
	        if($validator->fails())
	        {
	            return Redirect::to(App::make('backend_url').'/redirections/')->withErrors($validator);
	        }

	        $redirect->save();

	        Activitylog::log([
	            'action' => 'CREATED',
	            'type' => get_class($redirect),
	            'link_id' => $redirect->id,
	            'description' => 'Created new redirect',
	            'notes' => Auth::user()->username . " created a new redirect"
	        ]);

	        return Redirect::to(App::make('backend_url').'/redirections/');
        }
        else
        {
            return parent::forbidden();
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        if($this->user->hasAccess('update', get_class($this->model)))
        {
        	$this->data['redirects'] = Redirections::whereNull('deleted_at')->orderBy('created_at', 'DESC')->get();
        	$this->data['redirect'] = Redirections::whereNull('deleted_at')->where('id', '=', $id)->first();

        	if(null !== $this->data['redirect'])
        	{
		        //breadcrumb manager
				$this->data['breadcrumbs'][] = array(
					'classes' => '',
		            'icon' => '',
		            'title' => 'Edit redirect', 
		            'url' => ''
				);

				$this->data['categories'] = $this->getUniqueCategories($this->data['redirects']);

	        	return View::make('HummingbirdBase::cms/redirections-edit', $this->data);
        	}
        	else
        	{
        		die("nothing");
        	}
		}
        else
        {
            return parent::forbidden();
        }
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        if($this->user->hasAccess('update', get_class($this->model)))
        {
        	/* Validation rules */
			$rules = array(
			    'from' => 'required|unique:redirections,id,'.$id.'|different:to',
			    'to' => 'required',
			    'type' => 'required'
			);

			/* Inputs */
        	$all_input	= Input::except('_token');
			$input 		= Input::except('category', 'custom_category', '_token');

			$validator = Validator::make(
			    $input,
			    $rules
			);

	        /* Get redirect */
	        $redirect = Redirections::find($id)->fill($input);

	        /* Update category details */
	        if(trim($all_input['category']) != '' AND trim($all_input['category']) == 'NEW')
	        {
	        	$redirect->category = trim($all_input['custom_category']);
	        }
	        else
	        {
	        	$redirect->category = trim($all_input['category']);
	        }

	        /* Does it validate? */
	        if($validator->fails())
	        {
	        	return Redirect::route(App::make('backend_url').'.redirections.edit', array('redirections' => $id))->withErrors($validator);
	        }

	        /* save? */
	        $redirect->save();

	        /* Store activity log */
	        Activitylog::log([
	            'action' => 'UPDATED',
	            'type' => get_class($redirect),
	            'link_id' => $redirect->id,
	            'description' => 'Updated an existing redirect',
	            'notes' => Auth::user()->username . " updated details on a current redirect"
	        ]);

	        Session::flash('success', 'Redirect successfully updated.');
	        
	        return Redirect::route(App::make('backend_url').'.redirections.edit', array('redirections' => $id));
		}
        else
        {
            return parent::forbidden();
        }
	}


	/**
	 * Remove the specified redirect.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        if($this->user->hasAccess('delete', get_class($this->model)))
        {
			if($id == 'remove-all')
			{
				$redirects = Redirections::whereNull('deleted_at')->get(); //count redirects and check we actually have some!

				if(count($redirects) > 0)
				{
					foreach($redirects as $redirect)
					{
						$redirect->delete();
					}

			        Activitylog::log([
			            'action' => 'DELETED',
			            'type' => get_class(new Redirections),
			            'link_id' => null,
			            'description' => 'Removed all redirects',
			            'notes' => Auth::user()->username . " has removed ".count($redirects)." redirects"
			        ]);

					Session::flash('message', 'Successfully removed all redirections.');
				}
				else
				{
					/* Nothing to truncate */
					Session::flash('error', 'No redirects to remove');
				}
			}
			else
			{
				$redirect = Redirections::find($id);
		        
		        Activitylog::log([
		            'action' => 'DELETED',
		            'type' => get_class($redirect),
		            'link_id' => $redirect->id,
		            'description' => 'Deleted redirect',
		            'notes' => Auth::user()->username . " has removed the website redirect from &quot;$redirect->from&quot; to &quot;$redirect->to&quot;"
		        ]);

				$redirect->delete();
				Session::flash('message', 'Successfully deleted &quot;'.$redirect->from.'&quot; as a '.$redirect->type.' redirect.');
			}

			return Redirect::to(App::make('backend_url').'/redirections');
		}
        else
        {
            return parent::forbidden();
        }
	}


	/**
	 * Collect all unique categories
	 *
	 * @param  array  $arr - All items in database
	 * @return array Array of unique categories
	 */
	public function getUniqueCategories($arr)
	{
		$data = array();

		if(count($arr) > 0)
		{
			//we have some items
			foreach($arr as $item)
			{
				$item->category = trim($item->category); //remove white space

				if(!in_array($item->category, $data))
				{
					if($item->category != '' AND $item->category != 'NEW')
					{
						$data[] = $item->category; //only categories that do not exist or are not empty can be added
					}
				}
			}
			
			asort($data); //sort in descending value all of the categories
		}

		return $data;
	}

	public function import()
	{
        DB::unprepared(file_get_contents(base_path() . '/import/sql/redirects.sql'));

        $data = DB::table('redirects_import')->get();

        if(count($data) > 0)
        {
            $import_data = array();

            foreach($data as $redirect)
            {
                $new_data = array();
                $new_data['from'] = $redirect->from;
                $new_data['to'] = $redirect->to;
                $new_data['type'] = $redirect->type;
                $new_data['comments'] = $redirect->comment;
                $new_data['created_at'] = date("Y-m-d H:i:s");
                $new_data['updated_at'] = date("Y-m-d H:i:s");
                

                $import_data[] = $new_data;
            }

            DB::table('redirections')->insert($import_data);
            Schema::dropIfExists('redirects_import');
        } 

        return Redirect::to(General::backend_url() . '/redirections/'); // redirect to dashboard
    }
}