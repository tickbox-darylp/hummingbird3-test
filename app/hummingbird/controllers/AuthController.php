<?php

// use Hummingbird\Controllers\CmsbaseController;

class AuthController extends CmsbaseController {
    
    public $code_location = 'users';

    public function login()
    {
        if(Auth::check()) return Redirect::to(App::make('backend_url'));

        if(Input::has('username') && Input::has('password'))
        {
            $remember_me = (null === Input::get('remember_me')) ? false:true;

            if (Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')), $remember_me))
            {   
                if(is_null(Auth::user()->deleted_at) AND Auth::user()->status == 'active')
                {   
                    Auth::user()->last_login = new DateTime();
                    Auth::user()->save();

                    return Redirect::intended(App::make('backend_url'));
                }
            } 
            else if (Auth::attempt(array('email' => Input::get('username'), 'password' => Input::get('password')), $remember_me)) 
            {
                if(is_null(Auth::user()->deleted_at) AND Auth::user()->status == 'active')
                {   
                    Auth::user()->last_login = new DateTime();
                    Auth::user()->save();

                    return Redirect::intended(App::make('backend_url'));
                }
            }
        }

        $this->data['tag'] = 'Login';
        return View::make('HummingbirdBase::cms.login', $this->data);
    }
    
    public function logout()
    {   
        Auth::logout(); //logout 
        Session::flush(); //flush the session

        return Redirect::to('/');
    }

}
