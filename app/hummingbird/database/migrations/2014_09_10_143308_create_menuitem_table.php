<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuitemTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('menuitems', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id');
            $table->string('menu_item_name', 255);
            $table->integer('parent')->nullable();
            $table->integer('order')->default(100);
            $table->string('data', 255);

            // $table->integer('page_id');
            // $table->integer('order');
            $table->string('url');
            //$table->tinyInteger('live');
            $table->softDeletes();
            $table->timestamps();   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('menuitems');
    }

}
