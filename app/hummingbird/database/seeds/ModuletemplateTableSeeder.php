<?php

class ModuletemplateTableSeeder extends HbSeeder
{

    public function add_seed_data()
    {
        // $template = new Moduletemplate;
        // $template->name = 'Hello World Module Template';
        // $template->editable = 1;
        // $template->areas = 'AREA_ONE, AREA_TWO';
        // $template->live = 1;
        // $template->html = '<h1>Hello World!</h1><p>This is content coming from the hello world module template</p>';
        // $template->css = 'h1 {color:red;}';
        // $template->notes = 'This is an example of a module template';
        // $template->save();
    }

    public function register_blocks()
    {
        
    }

    public function register_modules()
    {
        
    }

    public function register_permissions()
    {
        
    }

    public function register_widgets()
    {
        
    }

    public function add_cmsnav_items()
    {
        $cmsnav = new CmsNav;
        $cmsnav->name = 'Templates';
        $cmsnav->link = 'module-templates';
        $cmsnav->section = 'Content';
        $cmsnav->parent = CmsNav::where('link', '=', 'modules')->first()->id;
        $cmsnav->live = 1;
        $cmsnav->save();
        
    }

}
