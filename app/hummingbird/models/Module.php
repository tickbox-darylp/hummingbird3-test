<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Module extends LaravelBook\Ardent\Ardent
{
    use SoftDeletingTrait;

    public $table = 'modules';
    public $cms_url = 'modules';
    protected $fillable = array('name', 'moduletemplate_id', 'notes');

    protected $shortcode = '[HB::MODULE(id)]';
    
    protected $guarded = array();
    
    public function shortcode()
    {
        return str_replace("id", $this->attributes['id'], $this->shortcode);
    }

    public function moduletemplate()
    {
        return $this->belongsTo('Moduletemplate');
    }
    
    public static function get_selection($emptyitem_text = 'Select a module')
    {
        $selection = array(0 => $emptyitem_text);
        
        $modules = Module::all();
        
        foreach($modules as $module) {
            $selection[$module->id] = $module->name;
        }
        
        return $selection;
    }

    public function setModuleTemplateIdAttribute($value)
    {
        $this->attributes['moduletemplate_id'] = ($value == '' OR !is_numeric($value) OR $value <= 0) ? NULL:$value;
    }

    // public function setModuleDataAttribute($value)
    // {

    //     if(is_array($value))
    //     {
    //         // die("YES");
    //         return serialize($value);
    //     }
    //     else
    //     {
    //         die("NO");
    //     }

    //     if(is_array($value)) return serialize($value);

    //     return;
    // }

    public function getModuleDataAttribute($value)
    {
        if(is_array($value)) return;
        
        if($value != '') return unserialize($value);

        return;
    }

    public function getTemplateName()
    {
        if($this->template())
        {
            return $this->moduletemplate->name;
        }

        return 'No template';
    }

    public function storeTaxonomy($tags)
    {
        $class = get_class($this);

        DB::table('taxonomy_relationships')->where('tax_type', '=', $class)->where('item_id', '=', $this->id)->delete();

        $input = array();
        foreach($tags as $tag)
        {
            $input[] = array('term_id' => $tag, 'tax_type' => $class, 'item_id' => $this->id);
        }

        DB::table('taxonomy_relationships')->insert($input);
    }
    
    public function taxonomy()
    {
        $data = array('category' => array(), 'tag' => array());
        $terms = DB::table('taxonomy_relationships')->leftJoin('taxonomy', 'taxonomy_relationships.term_id', '=', 'taxonomy.id')->where('item_id', '=', $this->id)->where('tax_type', '=', get_class($this))->select('taxonomy.id', 'taxonomy.type')->get();

        foreach($terms as $term)
        {
            $data[$term->type][] = $term->id;
        }

        return $data;
    }

    public function template()
    {
        if(null !== $this->moduletemplate_id) return true;
        return;
    }
    
    public function render()
    {
        #var_dump($this);exit;
        // get any module-specific variables & apply
        
        // str_replace
        return $this->moduletemplate->html;
    }
}
