if (!RedactorPlugins) var RedactorPlugins = {};

(function($)
{
	RedactorPlugins.formatter = function()
	{
		return {
			init: function()
			{
				var button = this.button.add('formatter', 'Remove Formatting');
	            
	            // make your added button as Font Awesome's icon
	            this.button.setAwesome('formatter', 'fa-magic');

				this.button.addCallback(button, this.formatter.format);
			},
	        format: function()
	        {
	        	var html_txt = $(this.code.get()).text();
	        	// var html_txt = $.parseHTML(this.code.get());
	        	// var formatted_html;

	        	// $(html_txt).each(function()
	        	// {
	        	// 	$(this).removeAttr('style');
	        	// 	console.log($(this).html());
	        	// 	formatted_html += $(this).wrap('<div>').parent().html();
	        	// });

	        	this.code.set(html_txt);
	        }
		};
	};
})(jQuery);