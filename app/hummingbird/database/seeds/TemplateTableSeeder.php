<?php

class TemplateTableSeeder extends HbSeeder
{

    public function add_seed_data()
    {
        // $template = new Template;
        // $template->name = "Home";
        // $template->html = 'Homepage template! <b/> {{$content}}';
        // $template->layout = "homepage";
        // $template->structure = json_encode(array());
        // $template->live = 1;
        // $template->save();

        // /*$template = new Template;
        // $template->name = "Standard";
        // $template->html =
        //         '<div id="wrapper">
        //             <div id="main" class="two_thirds">
        //                 {{BANNER}}
        //                 {{CONTENT}}
        //                 <div class="module-area" id="AREA_ONE">
        //                     {{MODULES:AREA_ONE}}
        //                 </div>
        //             </div>
        //             <div id="sidebar" class="one_third">
        //                 <div class="module-area" id="AREA_TWO">
        //                     {{MODULES:AREA_TWO}}
        //                 </div>
        //             </div>
        //         </div>';
        // $template->layout = "internal";
        // $template->live = 1;
        // $template->save();*/
        
        // $template = new Template;
        // $template->name = "Full width content (100%)";
        // $template->html = '<div class="row"><div class="col-sm-12"></div></div>';
        // $template->layout = "internal";
        // $template->structure = '[{"col":1,"row":1,"size_x":12,"size_y":1}]';
        // $template->live = 1;
        // $template->save();
        
        // $template = new Template;
        // $template->name = "Left sidebar and content pane (25:75 ratio %)";
        // $template->html = '<div class="row"><div class="col-sm-3">25%</div><div class="col-sm-9">75%</div></div>';
        // $template->layout = "internal";
        // $template->structure = '[{"col":1,"row":1,"size_x":3,"size_y":1},{"col":4,"row":1,"size_x":9,"size_y":1}]';
        // $template->live = 1;
        // $template->save();
        
        // $template = new Template;
        // $template->name = "Right sidebar and content pane (75:25 ratio %)";
        // $template->html = '<div class="row"><div class="col-sm-9">75%</div><div class="col-sm-3">25%</div></div>';
        // $template->layout = "internal";
        // $template->structure = '[{"col":1,"row":1,"size_x":9,"size_y":1},{"col":10,"row":1,"size_x":3,"size_y":1}]';
        // $template->live = 1;
        // $template->save();
        
        // $template = new Template;
        // $template->name = "Left/Right sidebar and content pane (25:50:25 ratio %)";
        // $template->html = '<div class="row"><div class="col-sm-3">25%</div><div class="col-sm-6">50%</div><div class="col-sm-3">25%</div></div>';
        // $template->layout = "internal";
        // $template->structure = '[{"col":1,"row":1,"size_x":3,"size_y":1},{"col":4,"row":1,"size_x":6,"size_y":1},{"col":10,"row":1,"size_x":3,"size_y":1}]';
        // $template->live = 1;
        // $template->save();
    }

    public function register_blocks()
    {
        
    }

    public function register_modules()
    {
        
    }

    public function register_permissions()
    {
        $permissions = new Hummingbird\Libraries\PermissionManager;

        /* array of permissions required for user to access/use. */
        $actions_required = array('lock');
        $actions_required = array_merge($permissions->get_standard_actions(), $actions_required);

        $permissions->install_actions($actions_required);

        /* Now install permissions for CMS area */
        $data = array(
            'type' => get_class(new Template),
            'label' => 'Website Templates',
            'group' => 'Content Management',
            'description' => 'Manage website templates',
            'live' => 1,
            'perms' => $actions_required
        );

        $permissions->install_permissions($data);
    }

    public function register_widgets()
    {
        
    }

    public function add_cmsnav_items()
    {
        $cmsnav = new CmsNav;
        $cmsnav->name = 'Website Templates';
        $cmsnav->link = 'templates';
        $cmsnav->section = 'Content';
        $cmsnav->live = 1;
        $cmsnav->save();
        
    }

}
