<?php


class Plugins
{
    private $plugins;

    public function __construct()
    {
        $this->plugins = self::all();
    }
    
    public static function all()
    {
        return FileHelper::directories(base_path() . '/workbench/tickbox');
    }
    
    public static function isInstalled($plugin)
    {
        return Plugin::where('foldername', '=', $plugin)->get()->count();
    }

    public function registerAll()
    {
        // $PluginBase = base_path() . '/workbench/tickbox/';
        // var_dump($PluginBase);
        // echo '<br /><br />';
        // foreach ($this->plugins as $plugin) 
        // {
            
        //     var_dump($plugin);
        //     echo '<br />';
        //     App::->package('Hummingbird'.$plugin, $plugin, $PluginBase . $plugin);
        // }

        // die();

        // $HummingbirdBase = base_path() . '/app/hummingbird';

        // $this->package('app/hummingbird', 'HummingbirdBase', $HummingbirdBase);

        /*
         * Check files exist, that we need for routing, events and filtering
         */
        // $files = array('/routes.php', '/events.php', '/filters.php');

        // // Main routes, events and filters for Hummingbird
        // foreach($files as $file)
        // {
        //     if(\File::exists($HummingbirdBase . $file))
        //     {
        //          require $HummingbirdBase . $file;
        //     }
        // }

        $PluginBase = base_path() . '/workbench/tickbox/';

        foreach ($this->plugins as $plugin) 
        {
            $PluginPath = $PluginBase . $plugin . '/src/';

            $files = array('/routes.php', '/events.php', '/filters.php');

            // Main routes, events and filters for Hummingbird
            foreach($files as $file)
            {
                if(\File::exists($PluginPath . $file))
                {
                     require $PluginPath . $file;
                }
            }
        }


    }
    
    public static function install($plugins = false)
    {
        $plugin_obj = new Plugins;
        $plugin_obj->plugins = $plugins ?: self::all();
        
        $plugin_obj->migrate();          
        $plugin_obj->seed();
    }
    
    public static function uninstall($plugins = false)
    {
        $plugin_obj = new Plugins;
        $plugin_obj->plugins = $plugins ?: self::all();
        
        $plugin_obj->reset();
        $plugin_obj->unseed();
    }
    
    private function migrate()
    {        
        foreach($this->plugins as $plugin) {
            
            Artisan::call('migrate', [
                '--bench'=>"tickbox/$plugin"
            ]);
        }
    }
    
    private function seed()
    {        
        foreach($this->plugins as $plugin) {
            
            $classname = ucfirst($plugin).'Seeder';
            
            $seederClass = new $classname;
            $seederClass->run();        
        }
    }
    
    private function unseed()
    {        
        foreach($this->plugins as $plugin) {
            
            $classname = ucfirst($plugin).'Seeder';
            
            $seederClass = new $classname;
            $seederClass->unseed();        
        }
    }
    
    // migrate:reset doesn't support --bench
    public function reset()
    {     
        $migrate_obj = new \Tickbox\Events\CreateEventsTable;
        $migrate_obj->down();

        // foreach($this->plugins as $plugin) {
            
        //     $migrations = FileHelper::filenames(base_path() . '/workbench/tickbox/' . $plugin . '/src/migrations');

        //     if(count($migrations) > 0)
        //     {
        //         foreach($migrations as $migration)
        //         {
        //             // $classname = ucfirst($plugin).'Seeder';
        //             $migrate_obj = new $migration;
        //             $migrate_obj->down();   
        //         }
        //     }
        // }

        // foreach($this->plugins as $plugin) {
        //     $classname = ucfirst($plugin);

        //     $migrate_obj = new Tickbox\$classname;
        //     $migrate_obj->down();
 
            // $migrations = FileHelper::filenames(base_path() . '/workbench/tickbox/' . $plugin);
                
            // // dd($migrations);

            // foreach($migrations as $migration) {


            //     #todo: find class name trhough reflection
                
            //     // $classname = ucfirst($plugin).'Seeder';
            //     $migrate_obj = new CreateEventsTable;
            //     $migrate_obj->down();
            // }
            
            // Artisan::call('migrate:reset', [
            //     '--bench'=>"tickbox/$plugin"
            // ]);
        // }       
    }
    
    public static function get_blocks_for_area($area)
    {
        $plugins = self::all();
        
        foreach($plugins as $plugin) {
            #Illuminate\Workbench\Package::all();
        }
    }
}

?>
