<?php


class CmsObserver { 
    
    public function creating($model)
    {
        if(Installer::isInstalled() && in_array('create', $this->permissions)) {
            // return Auth::user()->hasAccess('create', $this->object_type);
        }
    }

    public function created($model)
    {
        $this->store($model, 'CREATED');
    }
    
    public function updating($model)
    {
        if(in_array('edit', $this->permissions)) {
            // return Auth::user()->hasAccess('edit', $this->object_type);
        }
    }

    public function updated($model)
    {
        $this->store($model, 'UPDATED');
    }
    
    public function deleting($model)
    {
        if(in_array('delete', $this->permissions)) {
            // return Auth::user()->hasAccess('delete', $this->object_type);
        }
    }

    public function deleted($model)
    {
        $this->store($model, 'DELETED');
    }

    public function restored($model)
    {
        $this->store($model, 'RESTORED');
    }
    
    private function store($model, $action = '')
    {
        $log = new Activitylog();
        $log->user_id = (Auth::check()) ? Auth::user()->id : null;
        $log->type = $model->table;
        $log->action = $action;
        $log->link_id = $model->id;        
        $log->url = '';
        $log->save();
        
    }
}
