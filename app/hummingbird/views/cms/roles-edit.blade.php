@extends('HummingbirdBase::cms.layout')

@section('styles')
    <style type="text/css">
        .permission-holder .row 
        {
            margin-bottom:10px;
            padding-bottom:10px;
            border-bottom:1px solid #CCC;
            margin-left:0;
            margin-right:0;
        }
    </style>
@stop

@if(isset($breadcrumbs))
    @include('HummingbirdBase::cms.include.breadcrumbs', array('breadcrumbs' => $breadcrumbs))
@endif

@section('content')


@foreach ($errors->all('<li>:message</li>') as $error)
    {{$error}}    
@endforeach

{{ Form::open(array('url' => App::make('backend_url') .'/roles/edit/' . $role->id, 'class' => 'form-horizontal')) }}
    <div class="row">
        <div class="col-md-12">  
            <div class="row">      
                <div class="col-md-8">
                    <section class="panel" style="background-color:white;padding:20px;">
                        <h3 class="normal">Role Details</h3>

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name: </label>
                            <div class="col-sm-10">
                                <input id="name" name="name" type="text" class="form-control" placeholder="Users name" value="{{$role->name}}">
                            </div>
                        </div>

                        @if(count($roles) > 0)
                            <div class="form-group">
                                <label for="role" class="col-sm-2 control-label">Parent role:</label>
                                <div class="col-sm-10">
                                    <select name="parentrole_id" id="parentrole_id" class="form-control">
                                        <option value="{{ Role::getUserRoleID() }}" <?php if($role->parentrole_id == Role::getUserRoleID()) echo 'selected';?>>{{ Role::getUserRoleName(Role::getUserRoleID()) }}</option>

                                        @foreach($roles as $list_role)
                                            @if(!in_array($list_role->parentrole_id, $role_parents) OR $role->id === $list_role->id)
                                                <option value="{{ $list_role->id }}" <?php if($role->parentrole_id == $list_role->id) echo 'selected';?> <?php if($role->id == $list_role->id) echo 'disabled';?>><?=str_repeat('&nbsp;&nbsp;&nbsp;', $list_role->level+1)?>{{ $list_role->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @else
                            <input type="hidden" name="parentrole_id" value="{{ Role::getUserRoleID() }}" />        
                        @endif

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="pull-right btn btn-default btn-primary">Update</button>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="col-md-4">
                    <section class="panel" style="background-color:white;padding:20px;">
                        <h3 class="normal">Users</h3>
                        <p class="small">Users associated with {{$role->name}}</p>

                        @if(count($users) > 0)
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Last logged in</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->last_login ?: '-'}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <p>No users found with this role</p>
                        @endif
                    </section>
                </div>
            </div>
        </div>

        <div class="col-md-12">  
            <section class="panel" style="background-color:white;padding:20px;">
                <h3 class="normal">Permissions</h3>

                @if($role->id > 1)
                    <div id="permissions">
                        @foreach($permissions as $type => $perm_details)
                            <?php $has_perms = false;
                            $selected_perms = array();

                            if(count($perm_details['actions']) > 0)
                            {?>
                                <?php 

                                    if(isset($role_perms[$type]) AND count($role_perms[$type] > 0))
                                    {
                                        $has_perms = true;
                                    }
                                ?>

                                <div class="permission-holder col-md-3" data-type="<?=$type?>">
                                    <div class="row">
                                        <h5 class="permission-holder-title">{{$perm_details['data']['label']}} <input class="show-hide-perms" type="checkbox" name="activate[]" value="<?=$type?>" <?php if($has_perms) echo 'checked="checked"';?>/></h5>

                                        @if($perm_details['data']['description'] != '')
                                            <p class="small">{{$perm_details['data']['description']}}</p>
                                        @endif

                                        @foreach($perm_details['actions'] as $key => $action)
                                            <?php $selected = false; ?>

                                            <?php 
                                                $disabled = (strtolower($action) == 'read') ? true:false;
                                            ?>

                                            @if((isset($role_perms[$type]) AND in_array($key, $role_perms[$type])) OR strtolower($action) == 'read')
                                                <?php $selected = true;?>
                                            @endif

                                            @if($disabled)
                                                <input type="hidden" name="perms[<?=$type?>][]" value="{{$key}}" />
                                            @else
                                                <div class="perms" <?php if(!$has_perms) echo 'style="display:none;"';?>><input type="checkbox" name="perms[<?=$type?>][]" value="{{$key}}" <?php if($selected) echo 'checked="checked"';?>>&nbsp;{{$action}}<br /></div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            <?php }?>
                        @endforeach
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button name="submit" value="update-all" type="submit" class="pull-right btn btn-default btn-primary margin-left-15">Update Role and All Users</button>
                            <button name="submit" value="update" type="submit" class="pull-right btn btn-default btn-primary">Update</button>
                        </div>
                    </div>
                @else
                    <div class="alert alert-info text-center">Permissions for this role are not available.</p>
                @endif
            </section>
        </div>
    </div>
{{ Form::close() }}

@stop

@section('scripts')
    <script src='/themes/hummingbird/default/lib/masonry/masonry.js'></script>

    <script>
        var $msnry;

        function initMasonry()
        {
            // initialize
            $msnry = $('#permissions');

            $msnry.masonry(
            {
                columnWidth: '.permission-holder',
                itemSelector: '.permission-holder'
            });
        }

        $(document).ready(function()
        {
            if($('#permissions .permission-holder').length > 0)
            {
                initMasonry();
            }

            if($(".show-hide-perms").length > 0)
            {
                $(".show-hide-perms").click(function()
                {  
                    $(this).closest('.permission-holder').find('.perms').slideToggle('fast', function()
                    {
                        $msnry.masonry('layout');       
                    });
                });
            }
        });
    </script>
@stop