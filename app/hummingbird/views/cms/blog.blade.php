@extends('HummingbirdBase::cms.layout')

@section('content')

<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
                
                @if(Request::segment(3) == 'trash')
                <h1 class="pull-left">Articles &raquo; Trash</h1>
                @else
                <h1>Articles</h1>
                @endif

                @if(Auth::user()->hasAccess('create', get_class(new Post)))
                    <button type="button" class="pull-right btn btn-info" data-toggle="modal" data-target="#add-article"><i class="fa fa-plus-circle"></i> Add Article</button>
                @endif
                <a href="/{{App::make('backend_url')}}/blog/trash/" class="pull-right btn btn-info" style="margin-right:20px;">View Trash</a>

              @if(Input::get('s') AND Input::get('s') != '')
                <div class="clearfix">
                    <h3 class="normal">Searching for: {{Input::get('s')}}</h3>
                </div>
            @endif    

            @if(count($posts) > 0)
                <table class="table table-striped">
                    <thead>
                        <th scope="row">Title</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        @foreach($posts as $post)
                        
                            <tr>
                                <?php 
                                    $link = ($post->permalink[0] != '/') ? '/'.$post->permalink:$post->permalink;
                                    $link = str_replace("//", "/", $link);
                                ?>
                                
                                <td><a href='/{{App::make('backend_url')}}/blog/{{$post->id}}'>{{$post->title}}</a> <i>({{trim(str_replace("00:00:00", "", date("d/m/Y H:i:s", $post->post_date)))}})</i></td>
                                <!-- <td>{{$post->url}}</td> -->
                                <!-- <td>{{($post->parentpage_id > 0) ? $post->parentpage->title : '-'}}</td> -->
                                <td>{{$post->status}}</td>
                                <td>
                                    @if($post->status == 'public')
                                        <!-- <a target="_blank" href="{{$link}}" class="btn btn-xs" style="background-color:black;color:white;"><i class="fa fa-globe"></i> Preview</a> -->
                                    @endif

                                    @if(Request::segment(3) == 'trash')
                                        <a href='/{{App::make('backend_url')}}/blog/restore/{{$post->id}}' class="btn btn-xs btn-info">Remove from Trash</a>
                                    @else
                                        <a href='/{{App::make('backend_url')}}/blog/{{$post->id}}' class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>
                                        <a href='/{{App::make('backend_url')}}/blog/delete/{{$post->id}}'  class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                    @endif

                                </td>
                            </tr>
                        
                        @endforeach
                    </tbody>
                </table>

                <div class="text-center"> 
                    @if(null === Input::get('s') AND Input::get('s') == '')
                        {{ $posts->appends(Request::except('page'))->links(); }}
                    @endif
                </div>
            @else
                <div class="alert alert-info alert-info fade in text-center">
                    No blog posts added
                </div>
            @endif
        </section>
    </div>
</div>

@if(Auth::user()->hasAccess('create', get_class(new Post)))
    <div class="modal fade" id="add-article">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add new article</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12"> 
                            <?php echo Form::open(array('action' => 'BlogController@store', 'method' => 'post', 'class' => 'form-horizontal')) ?>
                                <div class="form-group">
                                    {{ Form::label('title', 'Title: ', array('class' => 'col-sm-3')) }}
                                    <div class="col-sm-9">
                                        {{ Form::text('title', '', array('class' => 'form-control')) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <button type="submit" class="btn btn-default btn-primary">Add Article</button>
                                    </div>
                                </div> 
                            <?php echo Form::close()?>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endif

@stop
