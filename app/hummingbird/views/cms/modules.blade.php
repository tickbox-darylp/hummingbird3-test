@extends('HummingbirdBase::cms.layout')

@section('styles')
    <style>
        .light {color:#CCC;}
        section.modules ul {padding-left:0;}
        section.modules ul li {list-style-type: none;padding-top:10px;padding-bottom:5px;}


        .panel {padding:20px;}
        .panel.btn-danger {color:#d43f3a;background-color:#d43f3a;color:white;}
        .panel.btn-danger a, .panel.btn-danger a:visited, .panel.btn-danger a:active {color:white;}
    </style>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="pull-left normal">All Modules</h1>

            @if(Auth::user()->hasAccess('create', get_class(new Module)))
                <button type="button" class="pull-right btn btn-info" data-toggle="modal" data-target="#add-module"><i class="fa fa-plus-circle"></i> Add Module</button>
            @endif
        </div>

        <div class="clearfix">&nbsp;</div>

        <div class="col-md-12">
            <div class="row">

                @if(isset($errors) AND count($errors) > 0)
                    @foreach ($errors->all('<li>:message</li>') as $error)
                        {{$error}}    
                    @endforeach
                @endif

                @if(count($modules) == 0)
                    <div class="col-md-12">
                        <section class="panel" style="padding:20px;">
                            <div class="alert alert-warning text-center">There are no modules.</div>
                        </section>
                    </div>
                @else
                    @foreach($modules as $group => $module_arr)
                        <?php 

                        $template = ($group != '') ? Moduletemplate::where('id', '=', $group)->first():NULL;
                        $name = (null !== $template) ? $template->name:'No template';?>

                        <div class="col-md-4">
                            <div class="panel @if($template AND null !== $template->deleted_at) btn-dangser @endif">
                                <header class="module-title ">
                                    <h4 class="normal">{{$name}}</h4>
                                
                                    @if(null !== $template AND ($template->notes != '' OR null !== $template->deleted_at))
                                        @if(null !== $template->deleted_at)
                                            <span class="small white"><span class="badge"><i class="fa fa-info"></i></span> Template has been deleted</span>
                                        @else
                                            <span class="small light">{{strip_tags($template->notes)}}</span>
                                        @endif
                                    @endif

                                    <hr />
                                </header>

                                <section class="modules">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <td>Title</td>
                                                <td>Actions</td>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach($module_arr as $module)
                                                <tr>
                                                    <td><a href="/{{App::make('backend_url').'/modules/edit/'.$module->id}}">{{$module->name}}</a></td>
                                                    <td>
                                                        <a href="/{{App::make('backend_url').'/modules/replicate/'.$module->id}}" class="btn btn-xs btn-default" title="Replicate &quot;{{$module->name}}&quot;"><i class="fa fa-copy"></i></a>
                                                        <a href="/{{App::make('backend_url').'/modules/edit/'.$module->id}}" class="btn btn-xs btn-info" title="Edit &quot;{{$module->name}}&quot;"><i class="fa fa-edit"></i></a>
                                                        <a href="/{{App::make('backend_url').'/modules/delete/'.$module->id}}" class="btn btn-xs btn-danger" title="Delete &quot;{{$module->name}}&quot;"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach               
                                        </tbody>
                                    </table>

                                </section>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    
    @if(Auth::user()->hasAccess('create', get_class(new Module)))
        <div class="modal fade" id="add-module">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add new module</h4>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12"> 
                                {{ Form::open(array('url' => '/'.General::backend_url().'/modules/add/', 'class' => 'form-horizontal')) }}
                                    <div class="form-group">
                                        {{ Form::label('name', 'Name: ', array('class' => 'col-sm-3')) }}
                                        <div class="col-sm-9">
                                            {{ Form::text('name', '', array('class' => 'form-control')) }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {{ Form::label('moduletemplate_id', 'Template: ', array('class' => 'col-sm-3')) }}
                                        <div class="col-sm-9">
                                            <select class="form-control" name="moduletemplate_id" id="moduletemplate_id">
                                                <optgroup label="General">
                                                    <option value="">No template (plain HTML)</option>
                                                    <option disabled></option>
                                                </optgroup>

                                                <?php $templates = (new Moduletemplate)->get_selection();?>

                                                @if(count($templates) > 0)
                                                    <optgroup label="Templates">
                                                        @foreach($templates as $template_key => $template)
                                                            <option value="{{$template_key}}">{{$template}}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-10">
                                            <button type="submit" class="btn btn-default btn-primary">Add Module</button>
                                        </div>
                                    </div> 
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif
@stop


@section('scripts')
    <script>
        $(document).ready(function()
        {
            if($(".help-expand").length > 0)
            {
                $(".help-expand").click(function(e)
                {
                    e.preventDefault();

                    $(this).parent().next().slideToggle(function()
                    {
                        $(this).toggleClass('hide');
                    });

                });
            }

            $('[data-toggle="popover"]').popover(); 
        });
    </script>
@stop
