<?php

// namespace Hummingbird\Controllers;
// use Hummingbird\Controllers\CmsbaseController;

class RoleController extends CmsbaseController
{
    public $preview_role = false;
    protected $all_roles = array();
    protected $roles = array();
    protected $user_role_id = 0;

    public function __construct(Role $role)
    {
        parent::__construct();

        $this->model = $role;

        $this->checkPreview();

        /* User role id */
        $this->user_role_id = Role::getUserRoleID();

        /* Get all the roles - ordered by parentrole_id (Null first, n+1 last) */
        $this->all_roles = Role::orderby('parentrole_id', 'ASC')->orderby('id', 'ASC')->get();

        /* Store roles in that we need */
        $this->buildTree($this->all_roles, $this->user_role_id);

        $this->data['breadcrumbs'][] = array(
            'classes' => '',
            'icon' => '',
            'title' => 'Roles',
            'url' => '/' . General::backend_url() . '/roles'
        ); //breadcrumb manager
    }

    public function getIndex()
    {
        if($this->user->hasAccess('read', get_class($this->model)))
        {
            $this->data['tag'] = 'Manage Roles';

            $this->data['roles'] = array_flatten($this->roles);

            return View::make('HummingbirdBase::cms/roles', $this->data);
        }
        else
        {
            return parent::forbidden();
        }
    }

    public function getIndex_call()
    {
        return array_flatten($this->roles);
    }

    function buildTree($elements, $parentId = 1, $level = 0, $stop_at_parent = false) 
    {
        foreach ($elements as $key => $element) {
            /* Top level parent */
            if ($element->parentrole_id !== $parentId OR $element->parentrole_id === NULL) continue;

            /* If hidden don't show */
            if(!$element->hidden)
            {
                /* Do not show in edit mode - stops moving child into child */
                if($element->id === $stop_at_parent OR $element->parentrole_id === $stop_at_parent) $this->data['role_parents'][] = $element->id;

                // $element['name'] = ($level > 0) ? '&#8627;'.$element['name']:$element['name']; //append arrow to name
                $this->roles[$element->id] = $element;
                $this->roles[$element->id]['level'] = $level;
                $this->buildTree($elements, $element->id, $level + 1, $stop_at_parent);
            }
        }
    }

    public function getEdit($id)
    {
        if($this->user->hasAccess('update', get_class($this->model)))
        {
            $role = Role::find($id);
            $this->data['role'] = $role;
            $this->data['tag'] = 'Edit: '.$this->data['role']->name;
            $this->data['roles'] = array_flatten($this->roles);
            $this->data['users'] = $this->data['role']->users()->get();

            unset($this->roles);

            // /* Store roles in that we need */
            $this->buildTree($this->all_roles, $this->user_role_id, 0, $role->id);
            $this->data['roles'] = array();

            if(isset($this->roles) AND null !== $this->roles)
            {
                $this->data['roles'] = array_flatten($this->roles);
            }

            $this->data['breadcrumbs'][] = array(
                'classes' => '',
                'icon' => '',
                'title' => $this->data['tag'],
                'url' => Request::url()
            ); //breadcrumb manager        

            $this->data['permissions'] = Permission::getAllPermissions($role->parentrole_id);
            $this->data['role_perms'] = Permission::getRolePermissions($id);

            return View::make('HummingbirdBase::cms/roles-edit', $this->data);
        }
        else
        {
            return parent::forbidden();
        }
    }
    
    public function postEdit($id)
    {
        if($this->user->hasAccess('update', get_class($this->model)))
        {
            $role = Role::find($id)->fill(Input::all());

            if (!$role->save() AND count($role->errors()) > 0)
            {
                return Redirect::to(App::make('backend_url').'/roles/edit/'.$id)->withErrors($role->errors());
            }
            
            $post_perms = Input::get('perms');
            $activate_perms = Input::get('activate');

            /* Now update new permissions */
            Permission::storeRolePermissions($id, $activate_perms, $post_perms);

            /* If applying to all, update all users who are active */
            if(Input::get('submit') == 'update-all')
            {
                $users = User::where('role_id', '=', $id)
                    ->whereNull('deleted_at')
                    ->get();

                if(count($users) > 0)
                {
                    $role_perms = Permission::getRolePermissions($id);

                    foreach($users as $user)
                    {
                        Permission::updateUserPermissions($user->id, $role_perms);
                    }
                }
            }

            Activitylog::log([
                'action' => 'UPDATED',
                'type' => get_class($role),
                'link_id' => $role->id,
                'description' => 'Updated role',
                'notes' => Auth::user()->username . " updated the role - &quot;$role->name&quot;"
            ]);

            return Redirect::to(App::make('backend_url').'/roles/edit/'.$id);
        }
        else
        {
            return parent::forbidden();
        }
    }
    
    public function postAdd()
    {
        if($this->user->hasAccess('create', get_class($this->model)))
        {
            $input = Input::except('_token', 'add');
            $role = (new Role)->fill($input);

            if (!$role->save(Role::$rules['create'])) 
            {
                return Redirect::to(App::make('backend_url').'/roles/')->withErrors($role->errors());
            }

            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class($role),
                'link_id' => $role->id,
                'description' => 'Created role',
                'notes' => Auth::user()->username . " created a new role - &quot;$role->name&quot;"
            ]);
            return Redirect::to(App::make('backend_url').'/roles/edit/'.$role->id);
        }
        else
        {
            return parent::forbidden();
        }
    }
    
    public function getDelete($id)
    {
        if($this->user->hasAccess('delete', get_class($this->model)))
        {
    		$role = Role::find($id);

            Activitylog::log([
                'action' => 'DELETED',
                'type' => get_class($role),
                'link_id' => null,
                'description' => 'Deleted role',
                'notes' => Auth::user()->username . " deleted the &quot;$role->name&quot; role"
            ]);

            $role->delete();
            
            return Redirect::to(App::make('backend_url').'/roles');
        }
        else
        {
            return parent::forbidden();
        }
    }

    public function checkPreview()
    {
        if(null !== Session::get('view_role_perms'))
        {
            $this->preview_role = $this->getPreviewRole(Session::get('view_role_perms'));
        }
        else
        {
            $this->removePreviewPerms();
            return parent::forbidden();
        }
    }

    public function preview($role_id = null)
    {
        if(null !== Session::get('view_role_perms'))
        {
            switch(Input::get('action'))
            {
                case 'remove':
                    $this->removePreviewPerms();
                    break;
                default:
                    if(is_numeric($role_id))
                    {
                        Session::put('view_role_perms', $role_id);
                    }
                    break;
            }

            return Redirect::to(General::backend_url());
        }
        else
        {
            $this->removePreviewPerms();
            return parent::forbidden();
        }
    }

    public function removePreviewPerms()
    {
        Session::forget('view_role_perms');

        return Redirect::to(General::backend_url());
    }

    public function getPreviewRole($role_id)
    {
        $role = Role::find($role_id);

        return $role->name;
    }
}