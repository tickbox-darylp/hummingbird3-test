<?php 

/**
 * Array Helper
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */
class ArrayHelper
{
    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function cleanExplodedArray(Array $exploded_items, $remove_items = false)
    {
    	$new_data = array();

    	if(count($exploded_items) > 0)
    	{
    		foreach($exploded_items as $item)
    		{
    			if($remove_items !== false)
    			{
    				$item = str_replace($remove_items, "", $item);
    			}

    			if($item != '') $new_data[] = trim($item);
    		}
    	}

    	return $new_data;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function object_to_array($object) 
    {
        return (array) $object;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function array_to_object($array)
    {
        return (object) $array;
    }

}
?>