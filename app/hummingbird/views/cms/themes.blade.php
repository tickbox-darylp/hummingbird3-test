@extends('HummingbirdBase::cms.layout')

@section('content')

<?php $even = false; ?>

<h1>All Themes</h1>
<hr/>

<table class='results' cellpadding='5' cellspacing='0'>
    <thead>
        <th>Name</th>
        <th>Description</th>
        <th>Version</th>
        <th>Screenshot</th>
        <th>Actions</th>
    </thead>
    <tbody>
        @foreach($themes as $theme)
            <?php $theme_desc = Themes::themeDescription($theme); ?>
        
        <tr <?php echo ($even) ? 'class="even"': '';$even = !$even;?>>
            <td>{{$theme}}</td>
            <td>{{$theme_desc->description}}</td>
            <td>{{$theme_desc->version}}</td>
            <td><img src="{{General::placeHoldItImage(50, 50)}}" alt="{{$theme}} screenshot" /></td>
            <td><a href="{{url(App::make('backend_url').'/themes/activate', array('theme'=>$theme))}}">{{$theme!=$activeTheme? 'Activate': ''}}</a></td>
        </tr>
        
        @endforeach
    </tbody>
</table>

@stop
