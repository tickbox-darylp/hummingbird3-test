<?php

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
	public $table = 'permissions';
	public $cms_url = 'permissions';

	protected $fillable = array();
	protected $guarded = array();

	static $tables = array(
		'main' => 'permissions',
		'actions' => 'permission_actions',
		'overrides' => 'permission_overrides',
		'perms_role' => 'permission_role',
		'types' => 'permission_types',
		'types_actions_xref' => 'perm_types_actions_xref'
	);

	public static function getActions()
	{
		$actions_arr = array();

		$actions = DB::table(self::$tables['actions'])->get();

		foreach ($actions as $action)
		{
			$actions_arr[] = $action->action;
		}

		return $actions_arr;
	}

	public static function getAllPermissions($role_parent)
	{
		$has_permissions = false;

		// get all perm types and the children
		$permissions = array();

		if($role_parent == 1)
		{
			// super duper admin
			$perm_types = DB::table(self::$tables['types'])->select('id', 'type', 'label', 'description')->orderBy('type', 'ASC')->get();

			if(count($perm_types) > 0)
			{
				// we have permissions
				foreach($perm_types as $perm_type)
				{
					$permissions[$perm_type->type] = array();
					$permissions[$perm_type->type]['data'] = array('label' => $perm_type->label, 'id' => $perm_type->id, 'description' => $perm_type->description);
					$permissions[$perm_type->type]['actions'] = array();

					$perm_actions = DB::table(self::$tables['types_actions_xref'])
					->join(self::$tables['actions'], self::$tables['types_actions_xref'].'.perm_action', '=', self::$tables['actions'].'.id')
					->select(self::$tables['actions'].'.*', self::$tables['types_actions_xref'].'.perm_id AS perm_id')
					->where(self::$tables['types_actions_xref'].'.perm_type', '=', $perm_type->id)
					->get();

					foreach($perm_actions as $perm_action)
					{
						$has_permissions = true;
						$permissions[$perm_type->type]['actions'][$perm_action->perm_id] = $perm_action->action;
					}
				} 
			}
		}
		else
		{
			// Lower down the tree
			$perm_types = DB::table(self::$tables['types'])->select('id', 'type', 'label', 'description')->get();

			if(count($perm_types) > 0)
			{
				// we have permissions
				foreach($perm_types as $perm_type)
				{
					$permissions[$perm_type->type] = array();
					$permissions[$perm_type->type]['data'] = array('label' => $perm_type->label, 'id' => $perm_type->id, 'description' => $perm_type->description);
					$permissions[$perm_type->type]['actions'] = array();

					$perm_actions = DB::table(self::$tables['types_actions_xref'])
					->join(self::$tables['actions'], self::$tables['types_actions_xref'].'.perm_action', '=', self::$tables['actions'].'.id')
					->join(self::$tables['perms_role'], self::$tables['types_actions_xref'].'.perm_id', '=', self::$tables['perms_role'].'.permission_id')
					->select(self::$tables['actions'].'.*', self::$tables['types_actions_xref'].'.perm_id AS perm_id')
					->where(self::$tables['types_actions_xref'].'.perm_type', '=', $perm_type->id)
					->where(self::$tables['perms_role'].'.role_id', '=', $role_parent)	
					->get();

					foreach($perm_actions as $perm_action)
					{
						$has_permissions = true;
						$permissions[$perm_type->type]['actions'][$perm_action->perm_id] = $perm_action->action;
					}
				} 
			}
		}

		if(!$has_permissions) $permissions = array();

		return $permissions;
	}

	public static function getRolePermissions($role_id)
	{
		$permissions = array();

		/* Get all permissions for this role */
		$perm_actions = DB::table(self::$tables['perms_role'])
		->join(self::$tables['types_actions_xref'], self::$tables['perms_role'].'.permission_id', '=', self::$tables['types_actions_xref'].'.perm_id')
		->join(self::$tables['actions'], self::$tables['types_actions_xref'].'.perm_action', '=', self::$tables['actions'].'.id')
		->join(self::$tables['types'], self::$tables['types_actions_xref'].'.perm_type', '=', self::$tables['types'].'.id')
		->select('perm_types_actions_xref.perm_id AS perm_action')
		->addSelect('permission_types.type AS perm_type')
		->where(self::$tables['perms_role'].'.role_id', '=', $role_id)
		->get();


        // $queries = DB::getQueryLog();
        // $last_query = end($queries);

		// dd($last_query);
		// dd(count($perm_actions));

		if(count($perm_actions) > 0)
		{
			foreach($perm_actions as $action)
			{
				$permissions[$action->perm_type][] = $action->perm_action;
			}
		}

		return $permissions;
	}

	public static function getUserPermissions($user_id)
	{
		$permissions = array();

		/* Get all permissions for this role */
		$perm_actions = DB::table(self::$tables['main'])
		->join(self::$tables['types_actions_xref'], self::$tables['main'].'.permission_id', '=', self::$tables['types_actions_xref'].'.perm_id')
		->join(self::$tables['actions'], self::$tables['types_actions_xref'].'.perm_action', '=', self::$tables['actions'].'.id')
		->join(self::$tables['types'], self::$tables['types_actions_xref'].'.perm_type', '=', self::$tables['types'].'.id')
		->select('perm_types_actions_xref.perm_id AS perm_action')
		->addSelect('permission_types.type AS perm_type')
		->where(self::$tables['main'].'.user_id', '=', $user_id)
		->get();

		if(count($perm_actions) > 0)
		{
			foreach($perm_actions as $action)
			{
				$permissions[$action->perm_type][] = $action->perm_action;
			}
		}

		return $permissions;
	}

	public static function userHasAccess($action, $type, $user_id)
	{
		$perm_actions = DB::table(self::$tables['main'])
		->join(self::$tables['types_actions_xref'], self::$tables['main'].'.permission_id', '=', self::$tables['types_actions_xref'].'.perm_id')
		->join(self::$tables['actions'], self::$tables['types_actions_xref'].'.perm_action', '=', self::$tables['actions'].'.id')
		->join(self::$tables['types'], self::$tables['types_actions_xref'].'.perm_type', '=', self::$tables['types'].'.id')
		->select('*')
		->where(self::$tables['main'].'.user_id', '=', $user_id)
		->where(self::$tables['actions'].'.action', '=', $action)
		->where(self::$tables['types'].'.type', '=', $type)
		->get();


		// dd($last_query);
		// dd(count($perm_actions));

		// if($action == 'read')
		// {
  //       $queries = DB::getQueryLog();
  //       $last_query = end($queries);
		// echo '<pre>'. print_r($last_query, true) .'</pre>';
		// 	dd(count($perm_actions) > 0);
		// }

		return count($perm_actions) > 0;
	}

	public static function storeRolePermissions($role_id, $active_perms, $new_perms)
	{
		// We are not a super admin
		if($role_id > 1)
		{
			/* Remove old permissions */
			DB::table(self::$tables['perms_role'])->where('role_id', '=', $role_id)->delete();

			/* Insert new permissions for role */
			$data_insert = array();

			$new_perms = (count($active_perms) > 0) ? $new_perms:false;

			if(is_array($new_perms) AND count($new_perms) > 0)
			{
				foreach($new_perms as $perm_type => $perms)
				{
					if(!in_array($perm_type, $active_perms))
					{
						unset($new_perms[$perm_type]);
						continue;
					}

					foreach($perms as $perm)
					{
						$data_insert[] = array('permission_id' => $perm, 'role_id' => $role_id);
					}

					DB::table(self::$tables['perms_role'])->insert($data_insert);
				}
			}
		}
	}	

	public static function updateUserPermissions($user_id, $new_perms)
	{
		/* Remove old permissions */
		DB::table(self::$tables['main'])->where('user_id', '=', $user_id)->delete();

		$data_insert = array();

		if(false !== $new_perms)
		{
			foreach($new_perms as $perm)
			{
				foreach($perm as $type)
				{
					$data_insert[] = array('permission_id' => $type, 'user_id' => $user_id);
				}
			}

			DB::table(self::$tables['main'])->insert($data_insert);
		}
	}

	public static function overrideUserPermissions($user_id, $active_perms, $new_perms)
	{
		// We are not a super admin
		$user = User::find($user_id);

		if($user->role_id > 1)
		{
			//not a super user
			/* Remove old permissions */
			DB::table(self::$tables['main'])->where('user_id', '=', $user_id)->delete();

			/* Insert new permissions for role */
			$data_insert = array();

			$new_perms = (count($active_perms) > 0) ? $new_perms:false;

			if(is_array($new_perms) AND count($new_perms) > 0)
			{
				foreach($new_perms as $perm_type => $perms)
				{
					if(!in_array($perm_type, $active_perms))
					{
						unset($new_perms[$perm_type]);
						continue;
					}

					foreach($perms as $perm)
					{
						$data_insert[] = array('permission_id' => $perm, 'user_id' => $user_id);
					}

					DB::table(self::$tables['main'])->insert($data_insert);
				}
			}
		}
	}	
}