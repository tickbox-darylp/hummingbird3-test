<?php

class CmsNavController extends CmsbaseController {
    
    public $code_location = 'dashboard';
    
    public function getIndex()
    {
        $this->data['tag'] = 'Manage CmsNavs';
        $this->data['cmsnavs'] = CmsNav::all();
        return View::make('HummingbirdBase::cms/cmsnavs', $this->data);
    }
    
    public function getEdit($id)
    {
        $this->data['cmsnav'] = CmsNav::find($id);
        $this->data['tag'] = 'Edit '.$this->data['cmsnav']->title;
        $this->getCommonData();
        return View::make('HummingbirdBase::cms/cmsnavs-edit', $this->data);
    }
    
    public function getCommonData()
    {
        $this->data['cmsnavs'] = CmsNav::get_selection();
    }
    
    public function postAdd()
    {
        $input = Input::except('_token', 'add');        
        $cmsnav = (new CmsNav)->fill($input);
        if(!$cmsnav->save()){
            return Redirect::to(App::make('backend_url').'/cmsnavs')->withErrors($cmsnav->errors());
        }
        return Redirect::to(App::make('backend_url').'/cmsnavs/edit/'.$cmsnav->id);
    }

    // Show a cmsnav by slug
    public function show($slug = 'home')
    {
        $redirect = $this->checkRedirects($slug);
        
        if($redirect) {
            return Redirect::to($redirect);
        }
        
        $cmsnav = CmsNav::where('permalink', '=', $slug)->get()->first();
        
        if($cmsnav) {
        
            // todo: get cmsnav, and use that as view

            return View::make('cmsnavs.test')->with('cmsnav', $cmsnav);
        }
        
        App::abort(404);
    }

    
    public function checkRedirects($slug)
    {
        # see if redreict exists
        $redirect = Redirection::where('from', '=', $slug)->first();
        
        if(!$redirect) return false;
        
        # update count
        $redirect->no_used += 1;
        $redirect->last_used = date('Y-m-d H:i:s');
        $redirect->save();
        
        # redirect to url given
        return $redirect->to;
    }
}
