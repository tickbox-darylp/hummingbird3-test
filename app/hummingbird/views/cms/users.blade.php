@extends('HummingbirdBase::cms.layout')

@if(isset($breadcrumbs))
    @include('HummingbirdBase::cms.include.breadcrumbs', array('breadcrumbs' => $breadcrumbs))
@endif

@section('content')

<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <h1>Manage Users</h1>

            @if(count($users) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Role</th>
                                <th>Last logged in</th>
                                <th>Latest activity</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <th>{{User::returnActiveLabel($user)}} {{$user->name}}</th>
                                    <th>{{$user->email}}</th>
                                    <td>{{$user->getRole()->name}}</td>
                                    <td>{{$user->last_login ?: '-'}}</td>
                                    <td>{{$user->formatActivity($user->activity()->first())}}</td>
                                    <td>
                                        <a style="background-color:black;color:white;" class="btn btn-xs" href="/{{App::make('backend_url').'/users/preview/'.$user->id}}" data-original-title="Preview {{$user->name}} permissions"><i class="fa fa-globe "></i></a>
                                        <a class="btn btn-xs btn-info" href="/{{App::make('backend_url').'/users/edit/'.$user->id}}" data-original-title="Edit {{$user->name}} "><i class="fa fa-edit "></i></a>
                                        <a class="btn btn-xs btn-danger" href="/{{App::make('backend_url').'/users/delete/'.$user->id}}" data-original-title="Delete {{$user->name}} "><i class="fa fa-trash "></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <h3>There are no users. Please try adding some.</h3>
            @endif
        </div>
    </section>
</div>

<div class="row hide">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <h3>Run import </h3>

            <div class="table-responsive">
                <?php echo Form::open(array('url' => App::make('backend_url').'/users/import', 'method' => 'post')) ?>
                    <table class="table table-striped table-hover">
                        <tbody>
                            <tr>
                                <td colspan="2" align="center">
                                    <input type="submit" value="Import" name="import">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php echo Form::close()?>
            </div>
        </section>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <h1>Add new user</h1>

            @foreach ($errors->all('<li>:message</li>') as $error)
                {{$error}}    
            @endforeach

            <div class="table-responsive">
                <?php echo Form::open(array('url' => App::make('backend_url').'/users/add', 'method' => 'post')) ?>
                    <table class="table table-striped table-hover">
                        <tbody>
                            <tr>
                                <td class="row_name">{{ Form::label('name', 'Name: ') }}</td>
                                <td>{{Form::text('name', Input::old('name'))}}</td>
                            </tr>
                            <tr>
                                <td class="row_name">{{ Form::label('username', 'Username: ') }}</td>
                                <td>{{Form::text('username', Input::old('username'))}}</td>
                            </tr>
                            <tr>
                                <td class="row_name">{{ Form::label('email', 'Email: ') }}</td>
                                <td>{{Form::text('email', Input::old('email'))}}</td>
                            </tr>
                            <tr>
                                <td class="row_name">{{ Form::label('password', 'Password: ') }}</td>
                                <td>
                                    <input type="password" autocomplete="off" name="password" id="password" class='password-strength' size="40" class="left" data-display="myDisplayElement1" /> 
                                    <div class="left" id="myDisplayElement1"></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="row_name">{{ Form::label('password_confirmation', 'Confirm password: ') }}</td>
                                <td>{{Form::password('password_confirmation')}}</td>
                            </tr>
                            <?php /*
                            <tr>
                                <td class="row_name">{{ Form::label('role_id', 'Role: ') }}</td>
                                <td>{{Form::select('role_id', Role::get_selection())}}</td>
                            </tr>

                            */?>

                            @if(count($roles) > 0)
                                <tr>
                                    <td class="row_name">Role: <strong>*</strong></td>
                                    <td>
                                        <select name="role_id" id="role_id">
                                            <option value="{{ Role::getUserRoleID() }}">{{ Role::getUserRoleName(Role::getUserRoleID()) }}</option>

                                            @foreach($roles as $role)
                                                <option value="{{ $role->id }}"><?=str_repeat('&nbsp;&nbsp;&nbsp;', $role->level+1)?>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            @else
                                <input type="hidden" name="role_id" value="{{ Role::getUserRoleID() }}" />        
                            @endif

                            <tr>
                                <td colspan="2" align="center">
                                    <input type="submit" value="Add User" name="add">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php echo Form::close()?>
            </div>
        </section>
    </div>
</div>

@stop

    
    <script type='text/javascript' src='/assets/cms/js/pStrength.jquery.js'></script>

    <script>
        $(".test").click(function(e)
        {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: 0
            },10000);
        });
    </script>
@stop
