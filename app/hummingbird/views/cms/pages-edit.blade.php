@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/templates/builder.css" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />
@stop

@section('content')

<div class="row">
    <div class="col-md-12">
        <h1><a href="{{url(General::backend_url().'/pages')}}">All Pages</a> > Edit page: {{$page->title}}</h1>
    </div>
</div>

@if(isset($versions) AND count($versions) > 0)
    <div class="row">
        <div class="col-md-12">
            <section class="panel" style="background-color:white;padding:20px;">
                <h6>Recent versions</h6>
                <ul>
                    @foreach($versions as $version)
                        <li>{{$version->title}} - created by @if($version->user_id > 0) {{User::find($version->user_id)->name}} @else "SYSTEM" @endif on {{DateTimeHelper::date_format(strtotime($version->updated_at), 'j F Y H:i:s')}} [<a href="/{{App::make('backend_url')}}/pages/edit/{{$page_id}}/?version={{$version->id}}">view</a>]</li>
                    @endforeach
                </ul>
            </section>
        </div>
    </div>
@endif  

@if(Session::get('preview'))
    <div id="preview">
        <a href="{{$page->permalink}}{{$page_version}}" target="_blank">&nbsp;</a>
    </div>
    <?php Session::forget('preview');?>
@endif

{{ Form::open(array('url' => '/'.General::backend_url().'/pages/edit/' . $page_id . $page_version, 'class' => 'form-horizontal')) }}
    <input id="action" type="hidden" name="action" value="" />
    <!-- <input type="hidden" name="parentpage_id" value="{{$page->parentpage_id}}" > -->

    <div class="row">
        <div class="col-md-12">
            <section class="panel" style="background-color:white;padding:20px;">
                <div class="form-group hidden-xs">
                    <div class="col-sm-12">
                        @if(Auth::user()->hasAccess('publish', get_class(new Page)))<a href="#" class="update btn btn-default btn-primary pull-right" style="margin-left:5px;" data-action="publish">Publish</a>@endif
                        <a href="#" class="update btn btn-default btn-default pull-right" style="margin-left:5px;" data-action="save"><i class="fa fa-save"></i> Save</a>
                        <a href="#" class="update btn btn-default btn-default pull-right" style="margin-left:5px;" data-action="preview"><i class="fa fa-globe"></i> Preview</a>
                    </div>
                </div>
                
                <div class="form-group">
                    {{ Form::label('title', 'Title: ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-6">
                        {{ Form::text('title', $page->title, array('class' => 'form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('URL', 'URL: ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-6">
                        <?php $disabled = ($page->locked OR $_SERVER["REMOTE_ADDR"] == '213.104.183.234') ? 'disabled':'disabled';?>
                        <input class="form-control" {{$disabled}} name="url" type="text" value="{{$page->url}}">
                        @if($page->status == 'public')
                            <span class="help-block"><a href="{{$page->permalink}}" title="View: {{$page->title}}" target="_blank"><i class="glyphicon glyphicon-new-window btn-xs"></i> {{$page->permalink}}</a> - The resource will open in a new glyphicon-new-window</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('parentpage_id', 'Parent Page: ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-6">
                        <select name="parentpage_id" id="parentpage_id" class="form-control">
                            <option value=""> --- Root ---</option>
                            @foreach($page_list as $key => $title)
                                <option value="{{$key}}" @if($key == $page->parentpage_id) selected="selected" @endif>{{$title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>                

                <div class="form-group">
                    {{ Form::label('template', 'Template: ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-6">
                        {{ Form::select('template', $templates, '', array('class' => 'form-control')) }}
                    </div>
                    <div class="col-sm-4">
                        <button id="add-template" class="btn btn-success btn-sm disabled" disabled="disabled"><i class="fa fa-plus"></i> Apply Template</button>
                        <a id="save-template" class=" hide btn btn-info btn-sm" href="#"><i class="fa fa-save"></i> Save </a>
                        <a class="clean btn btn-danger btn-sm" href="#"><i class="fa fa-trash"> Reset</i></a>
                    </div>
                </div>

                <div class="form-group template-content">
                    <textarea class="hide" id="generated_html" name="generated_html">{{$page->content}}</textarea>

                    <div class="flexi_outer">
                        <div class="relative @if($page->content == '') empty @endif" id="flexicontainer">
                            @if($page->content != '')
                                {{$page->content}}
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group hide">
                    {{ Form::label('content', 'Content: ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-12">
                        {{ Form::textarea('content', $page->content, array('class' => 'redactor-content')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('protected', 'Password protected? ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-10">
                        {{ Form::checkbox('protected', 1, $page->protected, array('class' => 'password-protected')) }}
                    </div>
                </div>

                <div class="form-group password-protection @if(!$page->protected) hide @endif">
                    {{ Form::label('username', 'Username ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-6">
                        {{ Form::text('username', $page->username, array('class' => 'form-control', 'id' => 'username')) }}
                    </div>
                </div>

                <div class="form-group password-protection @if(!$page->protected) hide @endif">
                    {{ Form::label('password', 'Password ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-6">
                        {{ Form::password('password', array('class' => 'form-control', 'id' => 'password')) }}
                        <span class="help-block">Leave empty to keep the same password</span>
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-default toggle-password">Show/Hide password</button>
                        <button type="button" class="btn btn-primary generate-password">Create password</button>
                    </div>
                </div>      

                <div class="form-group">
                    <div class="col-sm-10">
                        <h4>Article Details</h4>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('show_in_nav', 'Show in Nav? ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-10">
                        {{ Form::hidden('show_in_nav', '0') }}

                        {{ Form::checkbox('show_in_nav', 1, $page->show_in_nav, array('id' => 'show-in-nav')) }}
                    </div>
                </div>

                <div class="form-group show-in-nav-holder @if(!$page->show_in_nav) hide @endif">
                    {{ Form::label('custom_menu_name', 'Custom Menu Name ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-6">
                        {{ Form::text('custom_menu_name', $page->custom_menu_name, array('class' => 'form-control', 'id' => 'custom_menu_name')) }}
                    </div>
                </div>

                <div class="form-group media-library-holder">
                    {{ Form::label('featured_image', 'Featured image ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-8 image-link">
                        <div class="input-group">
                            <input type="text" name="featured_image" class="form-control media-image" readonly="" @if($page->featured_image != '') value="{{$page->featured_image}}" @endif>
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file pull-right">
                                    Browse… <input type="file" multiple="" class="browse-media-library" @if($page->featured_image != '') data-collection-id="{{ ''; /* Media::where('location', '=', $page->featured_image)->whereNull('deleted_at')->first()->collection */ }}" @endif>
                                </span>
                            </span>
                        </div>
                        <span class="help-block @if($page->featured_image == '') hide @endif"><a href="#" class="remove-media-library">Remove featured image</a></span>
                    </div>

                    <div class="col-sm-2 @if($page->featured_image == '') hide @endif image-holder">
                        <img src="{{$page->featured_image}}" class="img-responsive">
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('searchable', 'Show in Search? ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-10">
                        {{ Form::hidden('searchable', '0') }}

                        {{ Form::checkbox('searchable', 1, $page->searchable, array('class' => 'searchable')) }}
                    </div>
                </div>

                <div class="form-group show-search-image @if(!$page->searchable) hide @endif media-library-holder">
                    {{ Form::label('search_image', 'Search image ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-8 image-link">
                        <div class="input-group">
                            <input type="text" name="search_image" class="form-control media-image" readonly="" @if($page->search_image != '') value="{{$page->search_image}}" @endif>
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file pull-right">
                                    Browse… <input type="file" multiple="" class="browse-media-library" @if($page->featured_image != '') data-collection-id="{{ ''; /* Media::where('location', '=', $page->featured_image)->whereNull('deleted_at')->first()->collection */ }} @endif">
                                </span>
                            </span>
                        </div>
                        <span class="help-block @if($page->search_image == '') hide @endif"><a href="#" class="remove-media-library">Remove featured image</a></span>
                    </div>

                    <div class="col-sm-2 @if($page->search_image == '') hide @endif image-holder">
                        <img src="{{$page->search_image}}" class="img-responsive">
                    </div>
                </div>

<style>
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}</style>


                <div class="form-group">
                    <div class="col-sm-10">
                        <h4>Article SEO</h4>
                    </div>
                </div>

                <?php 
                    $meta_title = (isset($meta['meta_title'])) ? $meta['meta_title']:'';
                    $meta_keywords = (isset($meta['meta_keywords'])) ? $meta['meta_keywords']:'';
                    $meta_description = (isset($meta['meta_description'])) ? $meta['meta_description']:'';
                ?>

                <div class="form-group">
                    <label for="meta_title" class="col-sm-2 control-label">Meta Title:</label>
                    <div class="col-sm-10">
                        <input name="meta_title" id="meta_title" type="text" class="form-control" placeholder="Meta-data title" value="{{$meta_title}}">
                        <span class="help-block">By default, the post title will be used if blank</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="meta_keywords" class="col-sm-2 control-label">Meta Keywords:</label>
                    <div class="col-sm-10">
                        <input name="meta_keywords" id="meta_keywords" type="text" class="form-control" placeholder="Meta-data keywords" value="{{$meta_keywords}}">
                        <span class="help-block">Please provide a comma separated list of keywords</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="meta_description" class="col-sm-2 control-label">Meta Description:</label>
                    <div class="col-sm-10">
                        {{ Form::textarea('meta_description', $meta_description, array('class' => 'form-control', 'rows' => '3')) }}
                        <span class="help-block">By default, the post summary will be used if blank</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a href="#" class="update btn btn-default btn-default" style="margin-left:5px;" data-action="preview"><i class="fa fa-globe"></i> Preview</a>
                        <a href="#" class="update btn btn-default btn-default" style="margin-left:5px;" data-action="save"><i class="fa fa-save"></i> Save</a>

                        @if(Auth::user()->hasAccess('publish', get_class(new Page)))<a href="#" class="update btn btn-default btn-primary pull-right" style="margin-left:5px;" data-action="publish">Publish</a>@endif
                    </div>
                </div>    

                @include('HummingbirdBase::cms.taxonomy-layout', array('taxonomy' => $taxonomy))

            </section>
        </div>
    </div>
{{Form::close()}}


<div id="media-lib-modal" class="modal fade" style="z-index:100000;">
    <div class="modal-dialog clearfix">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close redactor-modal-btn redactor-modal-close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Media Library</h4>
            </div>
        
            <div class="modal-body media-images">
                <div class="row"></div>
            </div>
            <div class="modal-footer">
                <footer>
                    <button type="button" class="btn btn-primary redactor-modal-btn redactor-modal-action-btn">Insert</button>
                </footer>
            </div>
        </div>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@include('HummingbirdBase::cms.templates-new-modal')

<!-- @include('HummingbirdBase::cms.include.blocks') -->


<style type="text/css">
#flexicontainer.empty {
    border:1px dashed #EDEDED;
    min-height:150px;
}

#flexicontainer.empty .empty-text
{
    text-align: center;
    margin:0 auto;
    top:50%;
    left:45%;
    margin-top:50px;   
}

#flexicontainer.empty .empty-text h6 {font-size:30px;font-size:3rem;text-shadow: 2px 2px 3px rgba(0,0,0,0.2);}
#flexicontainer.empty .empty-text p {font-size:15px;font-size:1.5rem;}
.redactor-box {overflow: hidden;}

/* toolbar background and other styles*/
.redactor-toolbar {
    background: #2A313A;
    color:white;
    -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
}

.redactor-editor {
    border-top:0;
    border-left:1px solid #E9EDF0;
    border-right:1px solid #E9EDF0;
    border-bottom:1px solid #E9EDF0;
}
 
/* toolbar buttons*/
.redactor-toolbar li a {
    color: white;
}
 
 /*buttons hover state*/
.redactor-toolbar li a:hover {
    background: #00AEEF;
    color: white;
}
 
/*buttons active state*/
.redactor-toolbar li a:active,
.redactor-toolbar li a.redactor-act {
    background: #00AEEF;
    color: white;
}

body .redactor-box-fullscreen {
    -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
}

body .wrapper.addOpacity {opacity:0.1;}

.imagemanager-insert.active {
    border:1px solid blue;
}

.redactor-toolbar, .redactor-dropdown {z-index:1 !important;}

button.disabled {background-color: #C1C6C7 !important;
border-color: #8C8C8C !important;}

</style>

@stop

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="/themes/hummingbird/default/lib/templates/builder.templator.js"></script>
    <script src="/themes/hummingbird/default/lib/templates/builder.js"></script>
    <script src="/themes/hummingbird/default/lib/media-library/media-library.js"></script>
    <script src="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>

    <script>
    $(document).ready(function()
    {
        if($(".empty").length > 0)
        {
            $(".clean").trigger('click');
             //  str =       '<div class="container hb-item column f_pad hb_container">';
             //  str +=     '<div class="tools">';
             //  str +=             '<div class="tool"><a class="remove_tool" href="#"><i class="fa fa-times"></i></a></div>';
             //  str +=            '<div class="tool draghandle"><i class="fa fa-arrows"></i></div>';
             //  str +=        '</div>';

             //  str += '<div class="row f_pad hb-item clearfix row-wrapper hb_row">';
             //  str +=     '<div class="tools">';
             //  str +=             '<div class="tool"><a class="remove_tool" href="#"><i class="fa fa-times"></i></a></div>';
             //  str +=            '<div class="tool draghandle"><i class="fa fa-arrows"></i></div>';
             //  str +=            '<div class="tool"><a class="settings_tool" href="#"><i class="fa fa-gears"></i></a></div>'; 
             //  str +=        '</div>';

             //    str += '<div class="col-md-12 f_pad column hb_column hb-item">';
             //  str +=     '<div class="tools">';
             //  // str += '<div class="tool"><a href="#" class="add_tool"><i class="fa fa-edit"></i></a></div>';
             //  str +=             '<div class="tool"><a class="remove_tool" href="#"><i class="fa fa-times"></i></a></div>';
             //  str +=            '<div class="tool draghandle"><i class="fa fa-arrows"></i></div>';
             //  str +=            '<div class="tool"><a class="settings_tool" href="#"><i class="fa fa-gears"></i></a></div>';
             //  str +=            '<div class="tool"><a class="replicate-column" href="#"><i class="fa fa-copy"></i></a></div>'; 
             //  str += '</div>';
             //    str += '</div>';

             // str+= '</div></div>';

             // $('#flexicontainer').append(str);
        }

        if($("#template").length > 0)
        {
            $("#template").change(function()
            {
                if($(this).val() > 0)
                {
                    $("#add-template").prop('disabled', false);
                    $("#add-template").removeClass('disabled');
                }
                else
                {
                    $("#add-template").prop('disabled', true);
                    $("#add-template").addClass('disabled');
                }
            });

            $("#add-template").click(function(e)
            {
                e.preventDefault();

                $('.form-group.error').remove();

                if($("#template").val() > 0)
                {
                    $.ajax({
                        dataType: "json",
                        cache: false,
                        url: '/hummingbird/templates-new/get-html/'+$("#template").val(),
                        success: $.proxy(function(data)
                        {
                            if(data.state)
                            {
                                $("#flexicontainer").append(data.message);
                                activate_init();
                                $("#template").val(0).trigger('change');
                            }
                            else
                            {
                                $(this).closest('.form-group').after('<div class="form-group error"><div class="col-md-12 alert alert-danger text-center">'+data.message+'</div></div>').delay(5000).queue(function(next)
                                {
                                    $('.form-group.error').remove();
                                });
                            }
                        }, this)
                    });
                }
                else
                {
                    $(this).closest('.form-group').after('<div class="form-group error"><div class="clearfix col-md-12 alert alert-danger text-center">Please select a template</div></div>').delay(5000).queue(function(next)
                    {
                        $('.form-group.error').remove(); 
                    });
                }
            });
        }

        if($("#preview").length > 0)
        {
            $("#preview a")[0].click();
        }

        if($(".select").length > 0)
        {
            $(".select").click(function(e)
            {
                e.preventDefault();

                var type = $(this).data('select');

                switch(type)
                {
                    case 'all':
                        $($(this).data('select-el')).each(function()
                        {
                            $(this).prop('checked', true);
                        });
                        break;
                    case 'none':
                        $($(this).data('select-el')).each(function()
                        {
                            $(this).prop('checked', false);
                        });
                        break;
                    case 'invert':
                        $($(this).data('select-el')).each(function()
                        {
                            var new_val = ($(this).prop('checked')) ? false:true;
                            $(this).prop('checked', new_val);
                        });
                        break;
                }
            });
        }
        
        if($(".password-protected").length > 0)
        {
            $(".password-protected").click(function()
            {
                $(".password-protection").toggleClass("hide");

                return;
            });
        }

        if($(".generate-password").length > 0)
        {
            $(".generate-password").click(function(e)
            {
                e.preventDefault();

                var text = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                for( var i=0; i < 10; i++ )
                    text += possible.charAt(Math.floor(Math.random() * possible.length));

                $("#password").val(text);
            });
        }

        if($(".toggle-password").length > 0)
        {
            $(".toggle-password").click(function(e)
            {
                var password_field = $("#password");

                if(password_field.attr('type') == 'password')
                {
                    password_field.attr('type', 'text');
                }
                else
                {
                    password_field.attr('type', 'password');
                }
            });
        }


        if($(".searchable").length > 0)
        {
            $(".searchable").click(function()
            {
                $(".show-search-image").toggleClass('hide');
            });
        }

        if($("#show-in-nav").length > 0)
        {
            $("#show-in-nav").click(function()
            {
                $(".show-in-nav-holder").toggleClass('hide');
            });
        }
    });
    </script>
@stop