<?php

class DashboardController extends CmsbaseController {
    
    public $code_location = 'dashboard';


	public function index()
	{
//         $insert_data = array();
//         $used = array('tags' => array(), 'slugs' => array());

//         $events = DB::table('events')->get();

//         if(count($events) > 0)
//         {
//             foreach($events as $event)
//             {
//                 if($event->tags != '')
//                 {
//                     $tags = explode(",", $event->tags);
//                     $added = array();

//                     if(count($tags) > 0)
//                     {
//                         foreach($tags as $tag_name)
//                         {
//                             $tag_name = trim($tag_name); //remove white space
//                             $tag_slug = Str::slug($tag_name);

//                             if($tag_name != '')
//                             {
//                                 if(!in_array($tag_name, $used['tags']))
//                                 {
//                                     $used['tags'][] = $tag_name;

//                                     $insert_data[] = array(
//                                         'name' => $tag_name,
//                                         'slug' => $tag_slug,
//                                         'parent' => NULL,
//                                         'type' => 'tag',
//                                         'created_at' => date("Y-m-d H:i:s"),
//                                         'updated_at' => date("Y-m-d H:i:s")
//                                     );
//                                 }
//                             }
//                         }
//                     }
//                 }
//             }   
//         }

//         $pages = DB::table('pageindex')->get();

//         if(count($pages) > 0)
//         {
//             foreach($pages as $page)
//             {
//                 if($page->tags != '')
//                 {
//                     $tags = explode(",", $page->tags);
//                     $added = array();

//                     if(count($tags) > 0)
//                     {
//                         foreach($tags as $tag_name)
//                         {
//                             $tag_name = trim($tag_name); //remove white space
//                             $tag_slug = Str::slug($tag_name);

//                             if($tag_name != '')
//                             {
//                                 if(!in_array($tag_name, $used['tags']))
//                                 {
//                                     $used['tags'][] = $tag_name;

//                                     $insert_data[] = array(
//                                         'name' => $tag_name,
//                                         'slug' => $tag_slug,
//                                         'parent' => NULL,
//                                         'type' => 'tag',
//                                         'created_at' => date("Y-m-d H:i:s"),
//                                         'updated_at' => date("Y-m-d H:i:s")
//                                     );
//                                 }
//                             }
//                         }
//                     }
//                 }
//             }   
//         }
//         DB::table('taxonomy')->insert($insert_data);

//         // $queries = DB::getQueryLog();
//         // $last_query = end($queries);

//         // dd($last_query);
//         // dd(count($perm_actions));


// // select * from `taxonomy` where (`name` = '-' or `slug` = '') and `deleted_at` is null



//         $this->pp($insert_data);
//         die();

        // $table = Cpmreport::all();
        // $file = fopen('file.csv', 'w');
        // foreach ($table as $row) {
        //     fputcsv($file, $row->to_array());
        // }
        // fclose($file);
        // return Redirect::to('consolidated');





        // $columns = Schema::getColumnListing('taxonomy');

        // $filename = "taxonomy-tags.csv";
        // $handle = fopen($filename, 'w+');
        
        // fputcsv($handle, $columns);

        // $tags = Tags::all();

        // foreach($tags as $tag)
        // {
        //     fputcsv($handle, $tag->toArray());
        // }

        // fclose($handle);

        // $headers = array(
        //     'Content-Type' => 'text/csv',
        // );

        // Activitylog::log([
        //     'action' => 'EXPORTS',
        //     'type' => get_class(new Events),
        //     'description' => 'Exported event import skeleton file',
        //     'notes' => Auth::user()->username . " requested the download of the event import skeleton file"
        // ]);

        // return Response::download($filename, 'taxonomy-tags.csv', $headers);


        $this->data['tag'] = 'Dashboard';        
        
        $this->getCommonData();
        return View::make('HummingbirdBase::cms.dashboard', $this->data);

	}
    
    private function getCommonData()
    {
        $this->data['pages'] = Page::get_selection('Select a parent page');
        $this->data['dashboard_widgets'] = DashboardWidgets::all();
        #$this->data['venues'] = Tickbox\Venues\Venue::get_selection();
    }
        

}
