@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/datetimepicker/datetimepicker.css" />
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />


    <style type="text/css">
        .tab-content .tab-pane {padding-top:10px;}
        .tab-content .table {margin-bottom:20px;}

        .nav-tabs {padding:0;}
        .nav-tabs > li > a {
            margin-right:0;
        }

        .nav > li.pull-right {margin-top:13px;}
        .nav > li > a.remove-venue {padding: 5px 10px 5px 5px;}
        .nav > li > a.remove-venue:hover, .nav > li > a.remove-venue:focus
        {
            color: #fff;
            background-color: #c9302c;
            border-color: #ac2925;
        }
    </style>
@stop

@section('content')

<?php  
    $distinct_venues = array();
    if(count($event_dates) > 0)
    {
        foreach($event_dates as $key => $items)
        {
            $distinct_venues[] = $key;
        }
    }
?>

<div class="row">
    <div class="col-md-12">
        <h3><a href="{{url(General::backend_url().'/events')}}">All Events</a> &raquo; {{$event->title}}</h3>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <section class="panel" style="background-color:white;padding:20px;">
                    {{ Form::open(array('url' => App::make('backend_url').'/events/edit/' . $event->id, 'class' => 'form-horizontal')) }}
                        <input type="hidden" id="event-type" name="type" value="{{$event->type}}" />

                        <div class="form-group hidden-xs">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-default btn-primary pull-right">Update</button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10">
                                <h4>Event Details</h4>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Title:</label>
                            <div class="col-sm-10">
                                <input name="title" id="title" type="text" class="form-control" placeholder="Event Title" value="{{$event->title}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="short-title" class="col-sm-2 control-label">Short Title:</label>
                            <div class="col-sm-10">
                                <input name="short_title" id="short-title" type="text" class="form-control" placeholder="Event Short Title" value="{{$event->short_title}}">
                                <span class="help-block">The short title can be used for listings and search pages (if provided and set up to do so).</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="summary" class="col-sm-2 control-label">Summary:</label>
                            <div class="col-sm-10">
                                {{ Form::textarea('summary', $event->summary, array('class' => 'form-control textareas', 'rows' => '3', 'data-summary-limit' => 255)) }}
                                <span class="help-block">The event summary will appear as an excerpt on the listings and search pages (if provided).</span>
                                <span class="help-block character-count2"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Description:</label>
                            <div class="col-sm-10">
                                {{ Form::textarea('description', $event->description, array('class' => 'redactor-content')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="start_date" class="col-sm-2 control-label">Event Date:</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input name="start_date" id="start_date" type="text" class="datetimepicker-start_date form-control" placeholder="Event tickets" value="{{date("d/m/Y H:i:s", $event->start_date)}}">

                                    <span class="input-group-addon" style="border-left:0;border-right:0;">- to - </span>
                                    
                                    <input name="finish_date" id="finish_date" type="text" class="datetimepicker-finish_date form-control" placeholder="Event tickets" value="{{date("d/m/Y H:i:s", $event->finish_date)}}">
                                    
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                                <span class="help-block">For events that occur on multiple dates/venues select a date range and add specific event details in the additional venues and dates fields</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10">
                                <h4>Additional Venues and Dates</h4>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="event_type" class="col-sm-2 control-label">Venues:</label>
                            <div class="col-sm-6">
                                <select id="venues" name="venue" class="selectpicker form-control" data-live-search="true">
                                    <option value="">--- Select venue ---</option>
                                    <option value="new">New Venue</option>
                                    <option disabled class="text-center"> --- </option>
                                    @foreach($venues as $venue)
                                        <option value="{{ $venue->id }}" data-title="{{$venue->name}}" <?php if(in_array($venue->id, $distinct_venues)) echo 'disabled';?>> {{ $venue->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <a href="#" class="event-add-venue btn btn-success rounded"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>

                        <div class="venues-content-wrapper <?php if(count($event_dates) == 0) echo 'hide'; ?> col-md-12" style="padding-bottom:20px;margin-bottom:20px;border-bottom:1px dashed #CECECE;">                            
                            <input type="hidden" id="venues" name="venues" value="{{implode(",", $distinct_venues)}}" />
                            <input type="hidden" id="selected_venues" name="selected_venues" value="" />
                            <input type="hidden" id="remove_venues" name="remove_venues" value="" />

                            <ul class="nav nav-pills nav-stacked col-md-2" id="tabs">
                                @if(count($event_dates) > 0)
                                    @foreach($event_dates as $key => $items)
                                        <li><a id="tab_pane_venue{{$key}}" href="#pane_venue{{$key}}" data-toggle="tab">{{$items['data']['name']}}</a></li>
                                    @endforeach
                                @endif
                            </ul>

                            <div class="tab-content col-md-10">
                                @if(count($event_dates) > 0)
                                    @foreach($event_dates as $key => $items)
                                        <?php $item_0 = $items['events'][0];?>
                                        <div id="pane_venue{{$key}}" class="tab-pane">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" id="venue{{$key}}-tabs">
                                                <li role="presentation" class="active"><a href="#contact_venue{{$key}}" data-toggle="tab">Contact</a></li>
                                                <li role="presentation"><a href="#dates_venue{{$key}}" data-toggle="tab">Dates</a></li>
                                                @if($event->ticketsLive())<li role="presentation"><a href="#tickets_venue{{$key}}" data-toggle="tab">Tickets</a></li>@endif
                                                <li role="presentation"><a href="#booking_venue{{$key}}" data-toggle="tab">Booking &amp; Information</a></li>
                                                <li role="presentation" class="pull-right"><a href="#remove" data-id="{{$key}}" class="remove-venue btn btn-danger btn-xs"><i class="fa fa-trash"></i> Remove</a></li>
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="contact_venue{{$key}}">
                                                    <div class="form-group">
                                                        <label for="contact_venue{{$key}}" class="col-sm-2 control-label">Contact Name:</label>
                                                        <div class="col-sm-10">
                                                            <input name="contact_venue{{$key}}" id="contact_venue{{$key}}" type="text" class="form-control" placeholder="Event contact name" value="{{$item_0->contact}}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="email_venue{{$key}}" class="col-sm-2 control-label">Contact Email:</label>
                                                        <div class="col-sm-10">
                                                            <input name="email_venue{{$key}}" id="email_venue{{$key}}" type="text" class="form-control" placeholder="Event email" value="{{$item_0->email}}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="telephone_venue{{$key}}" class="col-sm-2 control-label">Contact Telephone:</label>
                                                        <div class="col-sm-10">
                                                            <input name="telephone_venue{{$key}}" id="telephone_venue{{$key}}" type="text" class="form-control" placeholder="Event telephone" value="{{$item_0->telephone}}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="website_venue{{$key}}" class="col-sm-2 control-label">Contact Website:</label>
                                                        <div class="col-sm-10">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">http://</span>
                                                                <input name="website_venue{{$key}}" id="website_venue{{$key}}" type="text" class="form-control" placeholder="Event website" value="{{$item_0->website}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div role="tabpanel" class="tab-pane" id="dates_venue{{$key}}">
                                                    <div class="date-ranges col-sm-12 clearfix">
                                                        <?php 
                                                            $dates = array();
                                                            foreach($items['events'] as $item)
                                                            {
                                                                $dates[] = array('start_date' => strtotime($item->start_date), 'finish_date' => strtotime($item->finish_date));
                                                            }
                                                        ?>

                                                        <input type="hidden" id="current_dates_venue{{$key}}" name="current_dates_venue{{$key}}" value="{{json_encode($dates)}}" />
                                                        <input type="hidden" id="new_dates_venue{{$key}}" name="new_dates_venue{{$key}}" value="" />
                                                        <input type="hidden" id="remove_dates_venue{{$key}}" name="remove_dates_venue{{$key}}" value="" />

                                                        <h5>Add Date:</h5>

                                                        <div class="form-group event-single" style="margin-bottom:30px;">
                                                            <div class="col-sm-9">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                                    <input name="start_date_venue{{$key}}" id="start_date_venue{{$key}}" type="text" class="datetimepicker-venue form-control" placeholder="Event tickets" value="">

                                                                    <span class="input-group-addon" style="border-left:0;border-right:0;">- to - </span>
                                                                    
                                                                    <input name="finish_date_venue{{$key}}" id="finish_date_venue{{$key}}" type="text" class="datetimepicker-venue form-control" placeholder="Event tickets" value="">
                                                                    
                                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <a href="#" class="event-add-date btn btn-success rounded" data-venue="{{$key}}" data-event="{{$event->id}}"><i class="fa fa-plus"></i></a>
                                                            </div>
                                                        </div>

                                                        <h5>Selected Dates</h5>

                                                        <p class="no-dates <?php if(count($items) > 0) echo 'hide';?>">No dates</p>

                                                        <table class="<?php if(count($items) == 0) echo 'hide';?> date-table table table-striped table-hover">
                                                            <thead>
                                                                <th>Start</th>
                                                                <th>Finish</th>
                                                                <th>Remove?</th>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($items['events'] as $item)
                                                                    <tr>
                                                                        <td>{{date("F jS Y, H:i:s A", strtotime($item->start_date))}}</td>
                                                                        <td>{{date("F jS Y, H:i:s A", strtotime($item->finish_date))}}</td>
                                                                        <td><a href="#" class="remove-date btn btn-xs btn-danger" data-start="{{strtotime($item->start_date)}}" data-finish="{{strtotime($item->finish_date)}}" data-venue="{{$key}}"><i class="fa fa-trash"></i> Remove</td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>                
                                                    </div>
                                                </div>

                                                @if($event->ticketsLive())
                                                    <div class="tab-pane" id="tickets_venue{{$key}}">
                                                        <div class="form-group">
                                                            <label for="tickets_venue{{$key}}" class="col-sm-2 control-label">Tickets:</label>
                                                            <div class="col-sm-4">
                                                                <input id="tickets_venue{{$key}}" type="text" class="form-control" placeholder="Event tickets" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                <div class="tab-pane" id="booking_venue{{$key}}">
                                                    <div class="form-group">
                                                        <label for="booking_venue{{$key}}" class="col-sm-2 control-label">Booking:</label>
                                                        <div class="col-sm-10">
                                                            {{ Form::select('booking_venue$key', array('' => 'No', 'external' => 'External'), $item_0->booking, array('class' => 'show_book_link form-control', 'id' => 'booking_venue$key')) }}
                                                        </div>
                                                    </div>

                                                    <div class="form-group hide show-booking-external">
                                                        <label for="booking_external_venue{{$key}}" class="col-sm-2 control-label">Link:</label>
                                                        <div class="col-sm-10">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">http://</span>
                                                                <input name="booking_external_venue{{$key}}" id="booking_external_venue{{$key}}" type="text" class="form-control" placeholder="URL" value="{{$item_0->booking_external}}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="booking_info_venue{{$key}}" class="col-sm-2 control-label">Information:</label>
                                                        <div class="col-sm-10">
                                                            {{ Form::textarea('booking_info_venue$key', $item_0->booking_info, array('class' => 'form-control', 'rows' => '3')) }}
                                                            <span class="help-block">Any event booking information will be displayed above any payment or registrations</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10">
                                <h4>Event Options</h4>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="cost" class="col-sm-2 control-label">Cost:</label>
                            <div class="col-xs-4 col-sm-4 col-md-2">
                                <div class="input-group">
                                    <span class="input-group-addon">&pound;</span>
                                    <input name="cost" id="cost" type="text" class="form-control" placeholder="Event cost" value="{{$event->cost}}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="reference" class="col-sm-2 control-label">Reference:</label>
                            <div class="col-sm-10">
                                <input name="reference" id="reference" type="text" class="form-control" placeholder="EVENT::REF:1" value="{{$event->reference}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="application_code" class="col-sm-2 control-label">Application Code:</label>
                            <div class="col-sm-10">
                                <input name="application_code" id="application_code" type="text" class="form-control" placeholder="Event code" value="{{$event->application_code}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="importance" class="col-sm-2 control-label">Importance:</label>
                            <div class="col-sm-4">
                                {{ Form::select('importance', $event->get_importance_types(), $event->importance, array('class' => 'form-control', 'id' => 'importance')) }}
                                <span class="help-block">The higher the priority, the higher it will appear on listings and result pages.</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10">
                                <h4>Event Address</h4>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="address_line_1" class="col-sm-2 control-label">Add event address:</label>
                            <div class="col-sm-2">
                                <select name="event_address" id="event_address" class="form-control text-centre">
                                    <option value="0">No</option>
                                    <option value="1" @if($event->event_address) selected @endif>Yes</option>
                                </select>
                            </div>
                        </div>                        

                        <div class="form-group @if(!$event->event_address) hide @endif event_address">
                            <label for="address_1" class="col-sm-2 control-label">Address Line 1:</label>
                            <div class="col-sm-10">
                                <input name="address_1" id="address_1" type="text" class="form-control" placeholder="First line of the address" value="{{$event->address_1}}">
                            </div>
                        </div>

                        <div class="form-group @if(!$event->event_address) hide @endif event_address">
                            <label for="address_2" class="col-sm-2 control-label">Address Line 2:</label>
                            <div class="col-sm-10">
                                <input name="address_2" id="address_2" type="text" class="form-control" placeholder="Second line of the address" value="{{$event->address_2}}">
                            </div>
                        </div>

                        <div class="form-group @if(!$event->event_address) hide @endif event_address">
                            <label for="address_3" class="col-sm-2 control-label">Address Line 3:</label>
                            <div class="col-sm-10">
                                <input name="address_3" id="address_3" type="text" class="form-control" placeholder="Third line of the address" value="{{$event->address_3}}">
                            </div>
                        </div>

                        <div class="form-group @if(!$event->event_address) hide @endif event_address">
                            <label for="town" class="col-sm-2 control-label">Town/City:</label>
                            <div class="col-sm-10">
                                <input name="town" id="town" type="text" class="form-control" placeholder="E.g Bristol" value="{{$event->town}}">
                            </div>
                        </div>

                        <div class="form-group @if(!$event->event_address) hide @endif event_address">
                            <label for="postcode" class="col-sm-2 control-label">Postcode:</label>
                            <div class="col-sm-10">
                                <input name="postcode" id="postcode" type="text" class="form-control" placeholder="E.g NS12 1SW" value="{{$event->postcode}}">
                            </div>
                        </div>

                        <div class="form-group @if(!$event->event_address) hide @endif event_address">
                            <label for="country" class="col-sm-2 control-label">Country:</label>
                            <div class="col-sm-10">
                                <input name="country" id="country" type="text" class="form-control text-uppercase" placeholder="UNITED KINGDOM" value="{{$event->country}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10">
                                <h4>Event Booking Information</h4>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="booking" class="col-sm-2 control-label">Booking:</label>
                            <div class="col-sm-10">
                                {{ Form::select('booking', array('' => 'No', 'external' => 'External'), $event->booking, array('class' => 'show_book_link form-control', 'id' => 'booking')) }}
                            </div>
                        </div>

                        <div class="form-group @if($event->booking != 'external') hide @endif show-booking-external">
                            <label for="booking_link" class="col-sm-2 control-label">Link:</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon">http://</span>
                                    <input name="booking_link" id="booking_link" type="text" class="form-control" placeholder="URL" value="{{$event->booking_link}}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="booking_info" class="col-sm-2 control-label">Information:</label>
                            <div class="col-sm-10">
                                {{ Form::textarea('booking_info', $event->booking_info, array('class' => 'form-control textareas', 'rows' => '3')) }}
                                <span class="help-block">Any event booking information will be displayed above any payment or registrations</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-10">
                                <h4>Event Contact Information</h4>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="contact" class="col-sm-2 control-label">Contact Name:</label>
                            <div class="col-sm-10">
                                <input name="contact" id="contact" type="text" class="form-control" placeholder="Event contact name" value="{{$event->contact}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Contact Email:</label>
                            <div class="col-sm-10">
                                <input name="email" id="email" type="text" class="form-control" placeholder="Event email" value="{{$event->email}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="telephone" class="col-sm-2 control-label">Contact Telephone:</label>
                            <div class="col-sm-10">
                                <input name="telephone" id="telephone" type="text" class="form-control" placeholder="Event telephone" value="{{$event->telephone}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="website" class="col-sm-2 control-label">Contact Website:</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon">http://</span>
                                    <input name="website" id="website" type="text" class="form-control" placeholder="Event website" value="{{$event->website}}">
                                </div>
                            </div>
                        </div>


                        <div class="form-group hide">
                            <div class="col-sm-10">
                                <h4>Event Questions</h4>
                            </div>
                        </div>


                <div class="form-group">
                    <div class="col-sm-10">
                        <h4>Event Details</h4>
                    </div>
                </div>


                <div class="form-group">
                    {{ Form::label('show_in_nav', 'Show in Nav? ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-10">
                        {{ Form::hidden('show_in_nav', '0') }}

                        {{ Form::checkbox('show_in_nav', 1, $event->show_in_nav) }}
                    </div>
                </div>

                <div class="form-group media-library-holder">
                    {{ Form::label('featured_image', 'Featured image ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-8 image-link">
                        <div class="input-group">
                            <input type="text" name="featured_image" class="form-control media-image" readonly="" @if($event->featured_image != '') value="{{$event->featured_image}}" @endif>
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file pull-right">
                                    Browse… <input type="file" multiple="" class="browse-media-library" @if($event->featured_image != '') data-collection-id="{{ ''; /* Media::where('location', '=', $event->featured_image)->whereNull('deleted_at')->first()->collection */ }} @endif">
                                </span>
                            </span>
                        </div>
                        <span class="help-block @if($event->featured_image == '') hide @endif"><a href="#" class="remove-media-library">Remove featured image</a></span>
                    </div>

                    <div class="col-sm-2 @if($event->featured_image == '') hide @endif image-holder">
                        <img src="{{$event->featured_image}}" class="img-responsive">
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('searchable', 'Show in Search? ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-10">
                        {{ Form::hidden('searchable', '0') }}
                        
                        {{ Form::checkbox('searchable', 1, $event->searchable, array('class' => 'searchable')) }}
                    </div>
                </div>

                <div class="form-group show-search-image @if(!$event->searchable) hide @endif media-library-holder">
                    {{ Form::label('search_image', 'Search image ', array('class' => 'col-sm-2')) }}
                    <div class="col-sm-8 image-link">
                        <div class="input-group">
                            <input type="text" name="search_image" class="form-control media-image" readonly="" @if($event->search_image != '') value="{{$event->search_image}}" @endif>
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file pull-right">
                                    Browse… <input type="file" multiple="" class="browse-media-library" @if($event->featured_image != '') data-collection-id="{{ ''; /* Media::where('location', '=', $event->featured_image)->whereNull('deleted_at')->first()->collection */ }} @endif">
                                </span>
                            </span>
                        </div>
                        <span class="help-block @if($event->search_image == '') hide @endif"><a href="#" class="remove-media-library">Remove featured image</a></span>
                    </div>

                    <div class="col-sm-2 @if($event->search_image == '') hide @endif image-holder">
                        <img src="{{$event->search_image}}" class="img-responsive">
                    </div>
                </div>

<style>
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
</style>


                        <div class="form-group">
                            <div class="col-sm-10">
                                <h4>Event Status</h4>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="showcase" class="col-sm-2 control-label">Featured:</label>
                            <div class="col-sm-4">
                                {{ Form::select('showcase', array('0' => 'No', '1' => 'Feature'), $event->showcase, array('class' => 'form-control', 'id' => 'showcase')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="live" class="col-sm-2 control-label">Active:</label>
                            <div class="col-sm-4">
                                {{ Form::select('live', array('0' => 'Offline', '1' => 'Publish'), $event->live, array('class' => 'form-control', 'id' => 'live')) }}
                            </div>
                        </div>

                        @include('HummingbirdBase::cms.taxonomy-layout', array('taxonomy' => $taxonomy))

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default btn-primary">Update</button>
                            </div>
                        </div>
                    {{Form::close()}}
                </section>
            </div>
        </div>
    </div>
</div>

<div id="media-lib-modal" class="modal fade" style="z-index:100000;">
    <div class="modal-dialog clearfix">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close redactor-modal-btn redactor-modal-close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Media Library</h4>
            </div>
        
            <div class="modal-body media-images">
                <div class="row"></div>
            </div>
            <div class="modal-footer">
                <footer>
                    <button type="button" class="btn btn-primary redactor-modal-btn redactor-modal-action-btn">Insert</button>
                </footer>
            </div>
        </div>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<style type="text/css">
.redactor-box {overflow: hidden;}

/* toolbar background and other styles*/
.redactor-toolbar {
    background: #2A313A;
    color:white;
    -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
}

.redactor-editor {
    border-top:0;
    border-left:1px solid #E9EDF0;
    border-right:1px solid #E9EDF0;
    border-bottom:1px solid #E9EDF0;
}
 
/* toolbar buttons*/
.redactor-toolbar li a {
    color: white;
}
 
 /*buttons hover state*/
.redactor-toolbar li a:hover {
    background: #00AEEF;
    color: white;
}
 
/*buttons active state*/
.redactor-toolbar li a:active,
.redactor-toolbar li a.redactor-act {
    background: #00AEEF;
    color: white;
}

body .redactor-box-fullscreen {
    -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
}

body .wrapper.addOpacity {opacity:0.1;}

.imagemanager-insert.active {
    border:1px solid blue;
}

/*.redactor-editor * {
    color:white !important;
}*/


</style>

@stop

@section('scripts')
    <script src="/themes/hummingbird/default/lib/moment/moment.js"></script>
    <script src="/themes/hummingbird/default/lib/datetimepicker/datetimepicker.js"></script>
    <script src="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>
    <script src="/themes/hummingbird/default/lib/media-library/media-library.js"></script>

    <script>
        function truncate(el)
        {
            var parent = el.parent();
            var item = el.val();
            var item_length = item.length;
            var limit = (parseInt(el.data('summary-limit')) > 0) ? parseInt(el.data('summary-limit')):255;
            var limit_area = parent.find('.character-count');
            var remaining = parseInt(limit-item_length);
            remaining = (remaining < 0) ? 0:remaining;

            var limit_text = (remaining != 1) ? ' characters remaining':' character remaining';

            if(limit_area.length < 1)
            {
                parent.append('<div class="character-count" />');
                limit_area = parent.find('.character-count');
            }

            limit_area.text(remaining + limit_text);

            if (remaining <= 0)
            {
                el.val((item).substring(0, limit - 1));
                return false;
            }
        }

        function initVenueDatepicker()
        {
            $('.datetimepicker-venue').datetimepicker(
            {
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: "DD/MM/YYYY HH:mm:ss"
            });

            var start = $(".datetimepicker-start_date").data("DateTimePicker").date();
            var finish = $(".datetimepicker-finish_date").data("DateTimePicker").date();

            $('.datetimepicker-venue').each(function()
            {
                start = (start > $(this).data("DateTimePicker").date()) ? start:$(this).data("DateTimePicker").date();
                finish = (finish > $(this).data("DateTimePicker").date()) ? finish:$(this).data("DateTimePicker").date();

                $(this).data("DateTimePicker").minDate(start);
                $(this).data("DateTimePicker").maxDate(finish);
            });
        }

        $(document).ready(function()
        {
            if($(".datetimepicker-start_date").length > 0)
            {
                $('.datetimepicker-start_date').datetimepicker(
                {
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    defaultDate: $(this).val(),
                    format: "DD/MM/YYYY HH:mm:ss"  
               });

                $(".datetimepicker-start_date").on("dp.change", function (e) {
                    $('.datetimepicker-finish_date').data("DateTimePicker").minDate(e.date);

                    // if($('.datetimepicker-venue').length > 0)
                    // {
                    //     $('.datetimepicker-venue').data("DateTimePicker").minDate(e.date);
                    // }
                    initVenueDatepicker();
                });
            }

            if($(".datetimepicker-finish_date").length > 0)
            {
                $('.datetimepicker-finish_date').datetimepicker(
                {
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    defaultDate: $(this).val(),
                    format: "DD/MM/YYYY HH:mm:ss"  
                });

                $(".datetimepicker-finish_date").on("dp.change", function (e) 
                {
                    $('.datetimepicker-start_date').data("DateTimePicker").maxDate(e.date);

                    // if($('.datetimepicker-venue').length > 0)
                    // {
                    //     $('.datetimepicker-venue').data("DateTimePicker").maxDate(e.date);
                    // }

                    initVenueDatepicker();
                });
            }


            if($(".datetimepicker").length > 0)
            {
                $('.datetimepicker').datetimepicker(
                {
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    defaultDate: $(this).val(),
                    format: "DD/MM/YYYY HH:mm:ss"  
               });
            }

            $('.datetimepicker-venue').datetimepicker(
            {
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                format: "DD/MM/YYYY HH:mm:ss"
            });

            if($(".date-option").length > 0)
            {
                $(".date-option").change(function()
                {
                    var panel = $(this).closest('.tab-pane');

                    switch($(this).val())
                    {
                        default:
                        case 'single':
                            panel.find('.event-regular, .event-irregular').hide();
                            panel.find('.event-single').show();
                            break;
                        case 'regular':
                            panel.find('.event-single, .event-irregular').hide();
                            panel.find('.event-regular').show();
                            break;
                        case 'irregular':
                            panel.find('.event-regular, .event-single').hide();
                            panel.find('.event-irregular').show();
                            break;
                    }
                });
            }

            if($(".event-add-venue").length > 0)
            {
                $(".event-add-venue").click(function(e)
                {
                    e.preventDefault();

                    var selected_item = $("#venues").find(':selected');

                    if(!selected_item.is(':disabled'))
                    {
                        var venue = selected_item.val();
                        var venue_title = selected_item.data('title');
                        venue_title = (venue == 'new') ? 'New Venue':venue_title;

                        if(venue != 'new') selected_item.prop('disabled', true).addClass('disabled');
                        else {
                            venue = 'tmp_' + $.now();
                            venue_title = venue;
                        }

                        var data = {};

                        $.post('/hummingbird/events/venueHTML/{{$event->id}}/'+venue+'/', data, function(response)
                        {
                            // if(response.state == true)
                            // {

                                $(".venues-content-wrapper .nav-pills").append('<li><a id="tab_pane_venue'+venue+'" href="#pane_venue'+venue+'" data-toggle="tab">'+venue_title+'</a></li>');
                                $(".venues-content-wrapper > .tab-content").append(response);

                                /* Add venue to the venue list */
                                var current_venues = $(".venues-content-wrapper #selected_venues").val();
                                if(current_venues == '') current_venues += venue;
                                else current_venues += ','+venue;
                                $(".venues-content-wrapper #selected_venues").val(current_venues);

                                if($(".venues-content-wrapper").hasClass('hide'))
                                {
                                    $(".venues-content-wrapper .nav-pills li:first-child a").trigger('click');
                                    $(".venues-content-wrapper").removeClass("hide");
                                }

                                $(".nav-pills a:last").tab('show');
                                $('#venues').selectpicker('render');
                                initVenueDatepicker();
                            // }
                        });
                    }
                });
            }

            if($(".add-irregular-date").length > 0)
            {
                $(".add-irregular-date").click(function(e)
                {
                    e.preventDefault();

                    var parent_el = $(this).closest('.form-group');
                    console.log(parent_el);

                    var start_date = parent.find('.event-irregular input[name="start_date"]').val();
                    console.log(start_date);
                    
                    var finish_date = parent.find('.event-irregular input[name="finish_date"]').val();
                    console.log(finish_date);
                });
            }

            if($(".add-regular-date").length > 0)
            {
                $(".add-regular-date").click(function(e)
                {
                    e.preventDefault();
                });
            }

            $(document.body).on('change', '.show_book_link' ,function()
            {
                var parent = $(this).closest('.form-group');

                if($(this).val() == 'external')
                {
                    parent.parent().find('.show-booking-external').removeClass('hide');
                }
                else
                {
                    parent.parent().find('.show-booking-external').addClass('hide');
                }
            });

            $(document.body).on('click', '.event-add-date' ,function(e)
            {
                e.preventDefault();
                var in_array = false;

                var venue = $(this).data('venue');
                var start_date = $("#start_date_venue"+venue).data("DateTimePicker").date();
                var finish_date = $("#finish_date_venue"+venue).data("DateTimePicker").date();
                var parent_el = $(this).closest('.date-ranges');

                parent_el.find('.alert-block').remove();

                if(start_date <= finish_date)
                {
                    var new_date = {};
                    new_date.start_date = start_date.unix();
                    new_date.finish_date = finish_date.unix();

                    var current_dates = $("#current_dates_venue"+venue).val();
                    var new_dates = $("#new_dates_venue"+venue).val();

                    if(new_dates == '')
                    {
                        var new_dates = [];
                        new_dates.push(new_date);

                        $("#new_dates_venue"+venue).val(JSON.stringify(new_dates));
                    }
                    else
                    {
                        new_dates = $.parseJSON(new_dates);

                        $.each(new_dates, function( key, value )
                        {
                            if(value.start_date == new_date.start_date && value.finish_date == new_date.finish_date)
                            {
                                in_array = true;
                                return false;
                            }
                        });

                        if(!in_array)
                        {
                            new_dates.push(new_date);
                            $("#new_dates_venue"+venue).val(JSON.stringify(new_dates));
                        }
                    }

                    if(!in_array)
                    {
                        /* Find parent */
                        var table = parent_el.find('table.date-table');

                        /* Build html */
                        var html = '<tr>';
                        html += '<td>'+start_date.format('MMMM Do YYYY, h:mm:ss a')+'</td>';
                        html += '<td>'+finish_date.format('MMMM Do YYYY, h:mm:ss a')+'</td>';
                        html += '<td><a href="#" class="remove-date btn btn-xs btn-danger" data-start="'+new_date.start_date+'" data-finish="'+new_date.finish_date+'" data-venue="'+venue+'"><i class="fa fa-trash"></i> Remove</td>';
                        html += '</tr>';

                        /* add to table */
                        table.find('tbody').append(html);
                        table.removeClass('hide');

                        /* remove no dates text */
                        parent_el.find('.no-dates').addClass('hide');
                    }
                    else
                    {
                        var error = '<div class="alert alert-block alert-info fade in"><strong>Ooops!</strong> Looks like you\'ve already added that date.</div>';
                        parent_el.find('.event-single').prepend(error).delay(3000).queue(function()
                        {
                            $(this).find('.alert').remove();
                        }); 
                    }
                }
                else
                {
                    var error = '<div class="alert alert-block alert-danger fade in"><strong>Oh snap!</strong> The start date should be in the future.</div>';
                    parent_el.find('.event-single').prepend(error).delay(3000).queue(function()
                    {
                        $(this).find('.alert').remove();
                    });
                }
            });

            $(document.body).on('click', '.remove-date' ,function(e)
            {
                e.preventDefault();

                var parent_el = $(this).closest('.tab-pane');

                /* get dates and venue */
                var venue = $(this).data('venue');
                var start_date = $(this).data('start');
                var finish_date = $(this).data('finish');

                /* Lists of dates per venue */
                var current_dates = $("#current_dates_venue"+venue).val();
                var new_dates = $("#new_dates_venue"+venue).val();
                var removed_dates = $("#remove_dates_venue"+venue).val();

                if(current_dates != '')
                {
                    current_dates = $.parseJSON(current_dates);

                    $.each(current_dates, function( key, value )
                    {
                        if(value.start_date == start_date && value.finish_date == finish_date)
                        {
                            current_dates.splice(key, 1);
                            return false;
                        }
                    }); 

                    /* If blank store empty value */
                    if(current_dates.length > 0)
                    {
                        $("#current_dates_venue"+venue).val(JSON.stringify(current_dates));    
                    }
                    else
                    {
                        $("#current_dates_venue"+venue).val('');
                    }

                    /* add to remove array */
                    var new_date = {};
                    new_date.start_date = start_date;
                    new_date.finish_date = finish_date;

                    if(removed_dates != '')
                    {
                        removed_dates = $.parseJSON(removed_dates);
                    }
                    else
                    {
                        removed_dates = [];
                    }

                    removed_dates.push(new_date);
                    $("#remove_dates_venue"+venue).val(JSON.stringify(removed_dates));
                }
                else
                {
                    if(new_dates != '')
                    {
                        new_dates = $.parseJSON(new_dates);

                        $.each(new_dates, function( key, value )
                        {
                            if(value.start_date == start_date && value.finish_date == finish_date)
                            {
                                new_dates.splice(key, 1);
                                return false;
                            }
                        }); 

                        /* If blank store empty value */
                        if(new_dates.length > 0)
                        {
                            $("#new_dates_venue"+venue).val(JSON.stringify(new_dates));    
                        }
                        else
                        {
                            $("#new_dates_venue"+venue).val('');
                        }
                    }
                }

                
                $(this).closest('tr').fadeOut().remove();

                if(parent_el.find('table tbody').children().length == 0)
                {
                    parent_el.find('table').addClass('hide');
                    parent_el.find('.no-dates').removeClass('hide');
                }
            });
            

            $(document.body).on('click', '.remove-venue' ,function(e)
            {
                e.preventDefault();

                var children = $(this).closest('.tab-content').children('.tab-pane').length;

                var venue = String($(this).data('id'));
                // console.log(venue);

                var venues = $(".venues-content-wrapper #venues").val();
                // console.log(venues);

                var current_venues = $(".venues-content-wrapper #selected_venues").val();
                // console.log(current_venues);

                var remove_venues = $(".venues-content-wrapper #remove_venues").val();
                // console.log(remove_venues);

                $(this).closest('.tab-pane').remove();
                $("#tab_pane_venue"+venue).remove();

                /* Remove venue to the venue list */
                if(current_venues != '') 
                {
                    var array = current_venues.split(",");
                    array.splice($.inArray(venue, array),1);
                    $(".venues-content-wrapper #selected_venues").val(array.join());
                }

                /* Add venue to the remove venue list */
                if(venues != '') 
                {
                    var array = venues.split(",");
                    // console.log(array);
                    // console.log($.inArray(venue, array));
                    if($.inArray(venue, array) > -1)
                    {
                        // console.log('in array');

                        if(remove_venues == '')
                        {
                            remove_venues += venue;
                        }
                        else
                        {
                            remove_venues += ','+venue;
                        }

                        // console.log(remove_venues);

                        $(".venues-content-wrapper #remove_venues").val(remove_venues).removeClass('disabled');
                    }
                }

                if(children > 1)
                {
                    $(".nav-pills a:first").tab('show');
                }

                $("#venues").find('option[value='+venue+']').prop('disabled',false);
            });

            if($(".datetimepicker-venue").length > 0)
            {
                initVenueDatepicker();
            }

            if($(".searchable").length > 0)
            {
                $(".searchable").click(function()
                {
                    $(".show-search-image").toggleClass('hide');
                });
            }

            if($("#event_address").length > 0)
            {
                $("#event_address").change(function()
                {
                    $(".event_address").toggleClass('hide');
                });
            }

            if($('.form-control.textareas').length > 0)
            {
                $(".form-control.textareas").autogrow();
            }

            if($(".summary-truncate").length > 0)
            {
                $(".summary-truncate").on('keyup change', function()
                {
                    truncate($(this));
                });

                $(".summary-truncate").each(function()
                {
                    truncate($(this));
                });
            }
        });
    </script>
@stop