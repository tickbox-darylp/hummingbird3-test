<?php


class PageObserver extends CmsObserver {     
    
    protected $permissions = array('create', 'read', 'edit', 'delete', 'preview');
    protected $object_type = 'pages';

    public function created($model)
    {
        $input = Input::except('_token');
        $page_version_one = new PageVersion;
        $page_version_one->fill($input);
        $page_version_one->page_id = $model->id;
        $page_version_one->save();
        
        parent::created($model);
    }

    public function updated($model)
    {
        $input = Input::except('_token');
        $page_version = new PageVersion;
        $page_version->fill($input);
        $page_version->page_id = $model->id;
        $page_version->save();
        
        parent::updated($model);
    }
    
    public function saving($page)
    {
        $page->permalink = PagesHelper::build_slug($page->url, $page->parentpage_id);
    }
}
