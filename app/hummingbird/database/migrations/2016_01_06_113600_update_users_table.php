<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration 
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(Schema::hasTable('users'))
		{
			Schema::table('users', function($table)
			{
			    $table->tinyInteger('is_admin')->nullable()->after('role_id');
			});

			$affected = DB::table('users')->whereNotNull('role_id')->update(array('is_admin' => 1));
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('users'))
		{
			Schema::table('users', function($table)
			{
			    $table->dropColumn('is_admin');
			});
		}
	}
}
