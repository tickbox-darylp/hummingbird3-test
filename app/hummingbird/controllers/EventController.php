<?php

class EventController extends CmsbaseController 
{
    public $imported = true;

    public $pagination = 15;

    /**
     * Inject the models.
     * @param Post $post
     * @param User $user
     */
    public function __construct(Events $event)
    {
        parent::__construct();

        $this->user = Auth::user();
        $this->event = $event;
    }

    public function getIndex()
    {
        if($this->user->hasAccess('read', get_class($this->event)))
        {
            Paginator::setCurrentPage(Session::get(get_class())); //set the current 

            $this->data['tag'] = 'Manage Events';

            if(Input::get('s') AND Input::get('s') != '')
            {
                $this->data['events'] = $this->cms_search();
            }
            else
            {
                $this->data['events'] = Events::orderBy('start_date', 'DESC')->whereNull('deleted_at')->paginate($this->pagination);
                Session::put(get_class(), Input::get('page'));
            }

            if(!$this->imported) $this->import();

            return View::make('HummingbirdBase::cms/events', $this->data);
        }
        else
        {
            return View::make('HummingbirdBase::cms.errors.unauthorised'); //replace with CMS Error/Unauthorised controller
        }
    }
    
    public function getEdit($id)
    {
        $this->data['event'] = Events::find($id);
        $this->data['event_dates'] = $this->getDateInformation($id);

        // $this->data['venues'] = array();
        $this->data['venues'] = DB::table('venues')->get();
        // $this->data['categories'] = Categories::ofType('category')->get();
        // $this->data['tags'] = Tags::ofType('tag')->get();
        // $this->getCommonData();

        $this->data['taxonomy'] = $this->data['event']->taxonomy();

        return \View::make('HummingbirdBase::cms/events-edit', $this->data);
    }
    
    public function postEdit($id)
    {
        /* Clean input */
        $input = \Input::except('_token');

        /* Find and fill event data */
        $event = Events::find($id);
        $event->fill($input);
        $event->hasAddress();
        // $event->isRegular($input['frequency']);
        $event->hasBooking();
        $event->save();

        /* Insert/Update/Remove venues from Event */
        $this->updateEventVenues($input, $event);

        /* Store cats and tags */
        $input['taxonomy'] = (isset($input['taxonomy'])) ? $input['taxonomy']:array();
        $event->storeTaxonomy($input['taxonomy'], $event, get_class($event));

        /* record activity */
        Activitylog::log([
            'action' => 'UPDATED',
            'type' => get_class(new Events),
            'link_id' => $event->id,
            'description' => 'Updated event',
            'notes' => Auth::user()->username . " updated details for the event &quot;$event->title&quot;"
        ]); 



        /* Redirect */
        return \Redirect::to(\General::backend_url().'/events/edit/'.$id)->with('success', "$event->title has been updated");
    }

    public function updateEventVenues($input, $event)
    {
        $venues = $this->cleanCommaArray($input['venues']); //already added - update
        $selected_venues = $this->cleanCommaArray($input['selected_venues']); // new venues - insert
        $remove_venues = $this->cleanCommaArray($input['remove_venues']); //remove venues - delete

        /* Remove venues */
        if(count($remove_venues) > 0)
        {
            foreach($remove_venues as $venue)
            {
                /* We are not saving just removing them */
                if(in_array($venue, $selected_venues))
                {
                    $key = array_search($venue, $selected_venues);
                    unset($selected_venues[$key]);
                }

                /* We are not saving just removing them */
                if(in_array($venue, $venues))
                {
                    $key = array_search($venue, $venues);
                    unset($venues[$key]);
                }

                $this->deleteEventVenue($event->id, $venue);
            }
        }

        $new_venues = array_merge($venues, $selected_venues);

        // echo '<pre>STARTED VENUES: '.print_r($venues, true).'</pre>';
        // echo '<pre>NEW VENUES: '.print_r($selected_venues, true).'</pre>';
        // echo '<pre>REMOVE VENUES: '.print_r($remove_venues, true).'</pre>';
        // echo '<pre>MERGED VENUES: '.print_r($new_venues, true).'</pre>';

        // die();

        /* Selected and Pre-selected Venues */
        foreach($new_venues as $venue)
        {
            $old_venue_id = $venue;

            if(strpos($venue, 'tmp_') !== false)
            {
                $venue = $this->cleanVenueTempID($venue, $input);
            }

            $booking_data = DB::table('events_booking_info')
            ->where('event_id', '=', $event->id)
            ->where('venue_id', '=', $venue)->take(1)->get();

            $this->insertEventVenue($event, $venue, $old_venue_id, $input, $booking_data);
        }

        // echo '<pre>STARTED VENUES: '.print_r($venues, true).'</pre>';
        // echo '<pre>NEW VENUES: '.print_r($selected_venues, true).'</pre>';
        // echo '<pre>REMOVE VENUES: '.print_r($remove_venues, true).'</pre>';
        // echo '<pre>MERGED VENUES: '.print_r($new_venues, true).'</pre>';
    }

    public function cleanCommaArray($array_to_clean)
    {
        $array = array(); /* to return */

        if($array_to_clean != '')
        {
            $array_exploded = explode(",", $array_to_clean); //comma sepearated

            if(count($array_exploded) > 0)
            {
                foreach($array_exploded as $item)
                {
                    $array[] = trim($item);
                }
            }
        }

        return $array;
    }

    public function cleanVenueTempID($venue_tmp_id, $input)
    {
        return DB::table('venues')->insertGetId(
            array(
                'name' => $input['new_venue_name' . $venue_tmp_id], 
                'address_1' => $input['address_1' . $venue_tmp_id],
                'address_2' => $input['address_2' . $venue_tmp_id],
                'address_3' => $input['address_3' . $venue_tmp_id], 
                'town' => $input['town' . $venue_tmp_id],
                'postcode' => $input['postcode' . $venue_tmp_id],
                'country' => $input['country' . $venue_tmp_id]
            )
        );
    }


    public function insertEventVenue($event, $venue, $venue_id = null, $input, $booking_info = array())
    {
        $booking_info_record_created = false;
        $regular = false;
        $regular_data = array();

        $insert_dates = array(); //new dates
        $events_booking_info = array(); //new venue added
        $updates_dates = array(); //updated dates
        $updates_events_booking_info = array(); //already inserted - updated

        /* make sure we don't update/insert twice */
        $events_booking_info_arr = array();

        /* Clean the venue id */
        $venue_id = (strpos($venue_id, 'tmp_') !== false) ? $venue_id:$venue; //tmp venue id or not

        /* Check if this event is regular */
        if(isset($input['frequency_venue' . $venue_id]) AND $input['frequency_venue' . $venue_id])
        {
            $new_dates = array();
            $regular = true;

            $regular_data = array(
                'frequency_occurance' => $input['frequency_occurance_venue' . $venue_id],
                'all' => array(
                    'days' => $input['frequency_occurance_bydays_venue' . $venue_id]
                )
            );

            switch($input['frequency_occurance_venue' . $venue_id])
            {
                case 'daily':
                case 'weekly':
                    $regular_data['regular'] = array(
                        'start_date' => $input['regular_start_date_venue' . $venue_id],
                        'finish_date' => $input['regular_finish_date_venue' . $venue_id],
                        'start_time' => $input['regular_start_date_time_venue' . $venue_id],
                        'finish_time' => $input['regular_finish_date_time_venue' . $venue_id],
                        'duration' => $input['regular_event_duration_venue' . $venue_id]
                    );

                    /* Reverse the timings for strtotime */
                    $input['regular_start_date_venue' . $venue_id] = implode("/", array_reverse(explode("/", $regular_data['regular']['start_date'])));
                    $input['regular_finish_date_venue' . $venue_id] = implode("/", array_reverse(explode("/", $regular_data['regular']['finish_date'])));

                    /* Append times to the end */
                    $start_date = strtotime($input['regular_start_date_venue' . $venue_id] . " " . $input['regular_start_date_time_venue' . $venue_id]);
                    $finish_date = strtotime($input['regular_finish_date_venue' . $venue_id] . " " . $input['regular_finish_date_time_venue' . $venue_id]);                    
                    
                    /* Get the correct minimum start time and the maximum end date */
                    $event_start = strtotime(str_replace("/", "-", $input['start_date']));
                    $event_finish = strtotime(str_replace("/", "-", $input['finish_date']));
                    $event_start_loop = ($start_date > $event_start) ? $start_date:$event_start;
                    $event_finish_loop = ($finish_date < $event_finish) ? $finish_date:$event_finish;

                    /* Set the timestamp */
                    $current_time = strtotime(date("Y-m-d", $event_start_loop)); //reset to midnight

                    /* loop until the maximum date has been exceeded */
                    while($current_time < $event_finish_loop)
                    {
                        if(in_array(date("N", $current_time), $regular_data['all']['days']))
                        {
                            $new_start  = strtotime(date("Y-m-d", $current_time) . " " . $input['regular_start_date_time_venue' . $venue_id]);
                            $new_finish = strtotime(date("Y-m-d", $current_time) . " " . $input['regular_finish_date_time_venue' . $venue_id]) + (86400 * $regular_data['regular']['duration']);

                            if(($new_start >= $event_start) AND ($new_finish <= $event_finish))
                            {
                                $new_dates[] = array('start_date' => $new_start, 'finish_date' => $new_finish);
                            }
                        }

                        $current_time += 86400;
                    }

                    break;
                case 'custom':
                    $input['frequency_all_day_venue' . $venue_id] = (isset($input['frequency_all_day_venue' . $venue_id])) ? $input['frequency_all_day_venue' . $venue_id]:array();

                    $regular_data['custom'] = array(
                        'all_day_events' => $input['frequency_all_day_venue' . $venue_id],
                        'day_starts' => array(),
                        'day_finishes' => array()   
                    );

                    /* Get the correct minimum start time and the maximum end date */
                    $event_start_loop = strtotime(str_replace("/", "-", $input['start_date']));
                    $event_finish_loop = strtotime(str_replace("/", "-", $input['finish_date']));

                    /* Set the timestamp */
                    $current_time = strtotime(date("Y-m-d", $event_start_loop)); //reset to midnight

                    /* loop until the maximum date has been exceeded */
                    while($current_time < $event_finish_loop)
                    {
                        $today = date("N", $current_time);

                        if(in_array($today, $regular_data['all']['days']))
                        {
                            $regular_data['custom']['day_starts'][$today]   = $input['start_time_venue'.$venue_id.'_'.$today];
                            $regular_data['custom']['day_finishes'][$today] = $input['finish_time_venue'.$venue_id.'_'.$today];

                            $start_time     = (in_array($today, $regular_data['custom']['all_day_events'])) ? '00:00:00':$input['start_time_venue'.$venue_id.'_'.$today];
                            $finish_time    = (in_array($today, $regular_data['custom']['all_day_events'])) ? '23:59:59':$input['finish_time_venue'.$venue_id.'_'.$today];

                            $new_start  = strtotime(date("Y-m-d", $current_time) . " " . $start_time);
                            $new_finish = strtotime(date("Y-m-d", $current_time) . " " . $finish_time);

                            if(($new_start >= $event_start_loop) AND ($new_finish <= $event_finish_loop))
                            {
                                $new_dates[] = array('start_date' => $new_start, 'finish_date' => $new_finish);
                            }
                        }

                        $current_time += 86400;
                    }
                    break;
            }

            $input['new_dates_venue'.$venue_id] = json_encode($new_dates);
        }


        if(isset($input['new_dates_venue'.$venue_id]))
        {
            $dates = json_decode($input['new_dates_venue'.$venue_id]);

            if(count($dates) > 0)
            {
                foreach($dates as $date)
                {
                    if(($date->start_date != '' AND $date->finish_date != '') OR ($date->start_date <= $date->finish_date))
                    {
                        /* check this date */
                        $dates = DB::table('events_dates')
                        ->where('event_id', '=', $event->id)
                        ->where('venue_id', '=', $venue)
                        ->where('start_date', '=', date("Y-m-d H:i:s", $date->start_date))
                        ->where('finish_date', '=', date("Y-m-d H:i:s", $date->finish_date))
                        ->whereNull('deleted_at')
                        ->get();

                        if(count($dates) > 0)
                        {
                            // update this date
                            foreach($dates as $db_date)
                            {
                                $updates_dates[$db_date->id] = array(
                                    'event_id' => $event->id,
                                    'start_date' => date("Y-m-d H:i:s", $date->start_date),
                                    'finish_date' => date("Y-m-d H:i:s", $date->finish_date),
                                    'venue_id' => $venue,
                                    'updated_at' => date("Y-m-d H:i:s"),
                                    'deleted_at' => NULL
                                );
                            }
                        }
                        else
                        {
                            /* Create new date for event */
                            $insert_dates[] = array(
                                'event_id' => $event->id,
                                'start_date' => date("Y-m-d H:i:s", $date->start_date),
                                'finish_date' => date("Y-m-d H:i:s", $date->finish_date),
                                'venue_id' => $venue,
                                'created_at' => date("Y-m-d H:i:s"),
                                'updated_at' => date("Y-m-d H:i:s"),
                                'deleted_at' => NULL
                            );
                        }

                        if(count($booking_info) > 0 OR $booking_info_record_created)
                        {
                            foreach($booking_info as $booking_info_item)
                            {
                                /* update booking info reference - only once */
                                $updates_events_booking_info[$booking_info_item->id] = array(
                                    'event_id' => $event->id,
                                    'venue_id' => $venue,
                                    'contact' => (isset($input['contact_venue'.$venue_id])) ? $input['contact_venue'.$venue_id]:'',
                                    'email' => (isset($input['email_venue'.$venue_id])) ? $input['email_venue'.$venue_id]:'',
                                    'telephone' => (isset($input['telephone_venue'.$venue_id])) ? $input['telephone_venue'.$venue_id]:'',
                                    'website' => (isset($input['website_venue'.$venue_id])) ? $input['website_venue'.$venue_id]:'',
                                    'booking' => (isset($input['booking_venue'.$venue_id])) ? $input['booking_venue'.$venue_id]:'',
                                    'booking_info' => (isset($input['booking_info_venue'.$venue_id])) ? $input['booking_info_venue'.$venue_id]:'',
                                    'booking_external' => (isset($input['booking_venue'.$venue_id]) AND $input['booking_venue'.$venue_id] == 'external') ? $input['booking_external_venue'.$venue_id]:'',
                                    'frequency' => $regular,
                                    'frequency_info' => serialize($regular_data)
                                );
                            }

                            $events_booking_info_arr[] = $venue;
                        }
                        else
                        {
                            /* create booking info reference - only once */
                            $events_booking_info[] = array(
                                'event_id' => $event->id,
                                'venue_id' => $venue,
                                'contact' => (isset($input['contact_venue'.$venue_id])) ? $input['contact_venue'.$venue_id]:'',
                                'email' => (isset($input['email_venue'.$venue_id])) ? $input['email_venue'.$venue_id]:'',
                                'telephone' => (isset($input['telephone_venue'.$venue_id])) ? $input['telephone_venue'.$venue_id]:'',
                                'website' => (isset($input['website_venue'.$venue_id])) ? $input['website_venue'.$venue_id]:'',
                                'booking' => (isset($input['booking_venue'.$venue_id])) ? $input['booking_venue'.$venue_id]:'',
                                'booking_info' => (isset($input['booking_info_venue'.$venue_id])) ? $input['booking_info_venue'.$venue_id]:'',
                                'booking_external' => (isset($input['booking_venue'.$venue_id]) AND $input['booking_venue'.$venue_id] == 'external') ? $input['booking_external_venue'.$venue_id]:'',
                                'frequency' => $regular,
                                'frequency_info' => serialize($regular_data)
                            );

                            $events_booking_info_arr[] = $venue;
                            $booking_info_record_created = true;
                        }
                    }
                }
            }

            /* bulk insert */
            if(count($insert_dates) > 0) DB::table('events_dates')->insert($insert_dates);
            if(count($events_booking_info) > 0) DB::table('events_booking_info')->insert($events_booking_info);

            /* Updates */
            if(count($updates_dates) > 0)
            {
                foreach($updates_dates as $key => $update)
                {
                    DB::table('events_dates')->where('id', '=', $key)->update($update);
                }
            }

            if(count($updates_events_booking_info) > 0)
            {
                foreach($updates_events_booking_info as $key => $update)
                {
                    DB::table('events_booking_info')->where('id', '=', $key)->update($update);
                }
            }
        }
    }

    public function deleteEventVenue($event_id, $venue_id)
    {
        // $venue_id = ($venue_id == 0) ? NULL:$venue_id;

        DB::table('events_dates')
            ->where('event_id', '=', $event_id)
            ->where('venue_id', '=', $venue_id)
            ->update(array('deleted_at' => date("Y-m-d H:i:s")));
    }
    
    public function getCommonData()
    {
        // $this->data['venues'] = \Tickbox\Venues\Venue::get_selection();
    }

    public function postAdd()
    {
        $input = Input::except('_token', 'add');   

    // protected $fillable = array('title','finish','venue_id','short_name', 'url', 'summary', 'description', 'event_type'
    //     ,'start', 'finish', 'display_in_calendar', 'booking_info', 'cost', 'tickets_available', 'group_registration',
    //     'person_contact', 'email', 'telephone', 'website', 'tags', 'feature');

        $input['short_title'] = $input['title'];
        $input['url'] = Str::slug($input['title']);

        $event = (new Events)->fill($input);
        $event->start_date = $event->roundToQuarterHour(time()); //create a nice start date
        $event->finish_date = $event->roundToQuarterHour(time() + 86400); //create a nice finish date
        $event->save();

        Activitylog::log([
            'action' => 'CREATED',
            'type' => get_class($event),
            'link_id' => $event->id,
            'description' => 'Created event',
            'notes' => Auth::user()->username . " created an event &quot;$event->title&quot;"
        ]);

        return Redirect::to(General::backend_url().'/events/edit/'.$event->id);
    }
    
    public function getDelete($id)
    {
        $event = Events::find($id);
        $event_name = (isset($event->title)) ? '&quot;'.$event->title.'&quot;':'Event';
        $event->delete();

        Activitylog::log([
            'action' => 'DELETED',
            'type' => get_class($event),
            'description' => 'Deleted event',
            'notes' => Auth::user()->username . " deleted the event &quot;$event->title&quot;"
        ]);

        $page = (null !== Session::get(get_class())) ? Session::get(get_class()):1;

        return \Redirect::to(General::backend_url().'/events/?page='.$page)->with('success', $event_name.' has been deleted.');
    }
    
    public function search_backend()
    {
        $this->data['events'] = parent::search_model('Tickbox\Events\Event');
        return \View::make('HummingbirdBase::cms/events', $this->data);
    }

    public function generateSkeleton()
    {
        // $columns = Schema::getColumnListing(Events::$table);

        $filename = "event-skeleton.csv";
        $handle = fopen($filename, 'w+');
        
        fputcsv($handle, array('title', 'short_title', 'summary', 'description'));

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );

        Activitylog::log([
            'action' => 'EXPORTS',
            'type' => get_class(new Events),
            'description' => 'Exported event import skeleton file',
            'notes' => Auth::user()->username . " requested the download of the event import skeleton file"
        ]);

        return Response::download($filename, 'event-skeleton.csv', $headers);
    }

    public function importEvents()
    {

        $failed = false;
        $file = Input::file('file');

        // Within the ruleset, make sure we let the validator know that this
        // file should be an image
        if (File::extension($file) == 'csv')
        {
            $failed = true;
        }

        // Check to see if validation fails or passes
        if ($failed)
        {
            // Redirect with a helpful message to inform the user that 
            // the provided file was not an adequate type
            return Redirect::to(General::backend_url().'/events/')->with('error', 'The provided file was not a valid CSV file.');
        } 
        else
        {
            // fix mac csv issue
            ini_set("auto_detect_line_endings", true);

            //start importing
            $header = NULL;
            $data = array();
            if (($handle = fopen($file, 'r')) !== FALSE)
            {
                $counter = 0;
                while (($row = fgetcsv($handle, 1000, ',')) !== FALSE)
                {
                    if(!$header)
                    {
                        $header = $row;
                    }
                    else
                    {
                        $data[] = array_combine($header, $row);
                    }

                    $counter++;
                }

                fclose($handle);

                if(count($data) > 0)
                {
                    $uploaded_text = (count($data) > 1) ? count($data).' events':'&quot;'.$data[0]['title'].'&quot;';
                    DB::table('events')->insert($data);

                    foreach($data as $event)
                    {
                        /* needs a little work as it will only return the first one it finds */
                        $event_obj = Events::where('title', '=', $event['title'])->first();

                        Activitylog::log([
                            'action' => 'CREATED',
                            'type' => get_class($event_obj),
                            'link_id' => $event_obj->id,
                            'description' => 'Created a new event',
                            'notes' => Auth::user()->username . " imported &quot;$event_obj->title&quot;"
                        ]);
                    }

                    return Redirect::to(General::backend_url().'/events/')->with('success', "We have uploaded $uploaded_text.");   
                }
                else
                {
                    return Redirect::to(General::backend_url().'/events/')->with('error', 'Nothing to import.');
                }
            }
            else
            {
                return Redirect::to(General::backend_url().'/events/')->with('error', 'There was a problem reading your CSV.');
            }
        }
    }

    public function importDatabase()
    {
        ini_set('memory_limit','4096M');
        set_time_limit(0); //300 = 5

        $inserted_images = array(); // key = value

        $festivals = array();
        $insert_data = array();

        /* set up categories */
        $cats = DB::table('old_event_cats')->get();

        foreach($cats as $cat)
        {
            $cat_tax = new Categories;
            $cat_tax->name = $cat->name;
            $cat_tax->slug = Str::slug($cat->name);
            $cat_tax->type = 'category';
            $cat_tax->parent = 1;
            $cat_tax->save();
        }

        /* Festival category */
        $cat_fest = new Categories;
        $cat_fest->name = "Festivals";
        $cat_fest->slug = Str::slug($cat->name);
        $cat_fest->type = 'category';
        $cat_fest->parent = 1;
        $cat_fest->save();

        /* set up categories */
        $cats = DB::table('old_event_festivals')->get();

        foreach($cats as $cat)
        {
            $cat_tax = new Categories;
            $cat_tax->name = $cat->name;
            $cat_tax->slug = Str::slug($cat->name);
            $cat_tax->type = 'category';
            $cat_tax->parent = $cat_fest->id;
            $cat_tax->save();
            $festivals[$cat->id] = $cat_tax->id;
        }

        /* get all events */
        // $events = Events::all();
        $events = DB::table('old_events')->orderBy('id', 'ASC')->get();
        // $events = DB::table('old_events')->where("event_type", '=', 'once')->take(1)->get();

        foreach($events as $event)
        {
            General::pp($event->id);
            // echo '<pre>'.print_r($event, true).'</pre>';

            $categories = ($event->categories != '') ? unserialize($event->categories):false;

            /* Process categories */
            if(is_array($categories))
            {
                foreach($categories as $category)
                {
                    $insert_data[] = array('term_id' => $category+1, 'tax_type' => get_class(new Events), 'item_id' => $event->id);
                }
            }

            $tags = explode(",", $event->tags); //get tags

            foreach($tags as $tag)
            {
                $tag = strtolower(trim($tag));
                // var_dump($tag);

                if($tag != '')
                {
                    $stored_tag = Tags::where('slug', '=', Str::slug($tag))->where('type', '=', 'tag')->first();

                    if(count($stored_tag) == 0)
                    {
                        $new_tag = new Tags;
                        $new_tag->name = $tag;
                        $new_tag->slug = Str::slug($tag);
                        $new_tag->type = 'tag';
                        $new_tag->parent = null;
                        $new_tag->save();
                    }
                    else
                    {
                        $id = $stored_tag->id;  
                    }

                    if(count($stored_tag) == 0) $id = $new_tag->id;

                    $insert_data[] = array('term_id' => $id, 'tax_type' => get_class(new Events), 'item_id' => $event->id);
                }
            }

            $festivals_event = unserialize($event->festivals);
            if($festivals_event != '' AND count($festivals_event > 0))
            {
                foreach($festivals_event as $item_festival)
                {
                    $insert_data[] = array('term_id' => $festivals[trim($item_festival)], 'tax_type' => get_class(new Events), 'item_id' => $event->id);
                }
            }


            /* Now insert events */
            $new_event = (new Events)->fill((array) $event);
            $new_event->start_date = $event->start . " " . $event->start_time;
            $new_event->finish_date = $event->finish . " " . $event->finish_time;

            if($event->deleted)
            {
                $new_event->deleted_at = date("Y-m-d H:i:s");
            }

            /* Create new media collection inside migration */
            $collection = array();
            $collection['name'] = $new_event->title . time();
            $collection['parent_collection'] = 4; //Events collection
            $collection['permalink'] = Str::slug($collection['name']);

            // /* insert and get id for upload */
            $collection_id = DB::table('media-collections')->insertGetId($collection);

            General::pp($collection_id);

            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class(new Media),
                'link_id' => $collection_id,
                'url' => General::backend_url() . "/media/view/$collection_id",
                'description' => 'Created collection',
                'notes' => "System created a new collection &quot;".$collection['name']."&quot;"
            ]);
            
            /* 
                Do something with the documents attached 
                
                NOT DOING THIS NOW AS IT HAS EMBEDS - NOT SURE WHAT TO DO WITH THESE! Possibly can just be uploaded to the page.

            */
            $event_documents = DB::table('old_downloads_xref')
                ->join('old_downloads', 'old_downloads_xref.dl_id', '=', 'old_downloads.id')
                ->where('old_downloads_xref.page_id', '=', $event->id)
                ->where('old_downloads_xref.type', '=', 'events')
                ->select('old_downloads_xref.order_id as old_d_x_order_id', 'old_downloads_xref.deleted as old_d_x_deleted', 'old_downloads.*')
                ->orderBy('old_d_x_order_id', 'ASC')
                ->get();

            if(count($event_documents) > 0)
            {
                $insert_event_docs = array();

                foreach($event_documents as $event_document)
                {
                    if($event_document->filename != '')
                    {
                        //filename = 1328109287.jpg - need to be absolute
                        $orig_file = '/uploads/documents/' . $event_document->filename;

                        /* find in database */
                        $resource = DB::table('media_xref')->where("source", '=', trim($orig_file))->first();

                        if(null !== $resource)
                        {
                            $source = $resource->destination;
                        }
                        else
                        {
                            /* process images */
                            $base_file = base_path() . '/import/documents/' . $event_document->filename;
                            
                            $source = App::make('MediaController')->uploadFile_byFunction($base_file, $collection_id);

                            if($source !== false)
                            {
                                /* Add to media_xref */
                                DB::table('media_xref')->insert(array
                                    (
                                        'source' => $orig_file,
                                        'destination' => $source
                                    )
                                );
                            }
                        }

                        if($source !== false)
                        {
                            if($event_document->deleted == 0 AND $event_document->old_d_x_deleted == 0)
                            {
                                $insert_event_docs[] = array(
                                    'event_id' => $event->id,
                                    'document' => $source
                                );
                            }
                        }
                    }
                    else
                    {
                        if($event_document->deleted == 0 AND $event_document->old_d_x_deleted == 0)
                        {
                            $new_event->description .= $event_document->embed;
                        }
                    }
                }

                if(count($insert_event_docs) > 0)
                {
                    DB::table('events_docs')->insert($insert_event_docs);
                }
            }


            /* Save events */
            $new_event->save();

            switch($event->event_type)
            {
                case 'irregular':
                    // dates in database
                    $event_dates = DB::table('old_event_dates')->where('event_id', '=', $event->id)->get();

                    foreach($event_dates as $event_date)
                    {
                        $insert_dates[] = array(
                            'event_id' => $new_event->id,
                            'start_date' => $event_date->start . " " . $event_date->start_time . ":00",
                            'finish_date' => $event_date->finish . " " . $event_date->finish_time . ":00",
                            'venue_id' => $event->venue_id,
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s"),
                            'deleted_at' => (($event_date->deleted) ? date("Y-m-d H:i:s"):NULL)
                        );
                    }

                    break;
                case 'regular':
                    $start_date = $event->start . " " . $event->start_time;
                    $start_date_time = strtotime($start_date);

                    $finish_date = $event->finish . " " . $event->finish_time;
                    $finish_date_time = strtotime($finish_date);

                    $adding_date_time = $start_date_time; //using this to compare

                    while($adding_date_time <= $finish_date_time)
                    {
                        /* get current day */
                        $current_day = date("w", $adding_date_time) + 1;
                        $this_day = "days_of_week_" . $current_day;
                        $start_day = "start_time_".$current_day;
                        $finish_day = "finish_time_".$current_day;

                        // monday = 1 and sunday = 7
                        if($event->$this_day == 1)
                        {
                            $adding_date_time_start = date("Y-m-d", $adding_date_time) . " " . $event->$start_day . ":00";
                            $adding_date_time_end = date("Y-m-d", $adding_date_time) . " " . $event->$finish_day;

                            if(strtotime($adding_date_time_start) <= $finish_date_time)
                            {
                                /* Create new date for event */
                                $insert_dates[] = array(
                                    'event_id' => $new_event->id,
                                    'start_date' => $adding_date_time_start,
                                    'finish_date' => $adding_date_time_end,
                                    'venue_id' => $event->venue_id,
                                    'created_at' => date("Y-m-d H:i:s"),
                                    'updated_at' => date("Y-m-d H:i:s"),
                                    'deleted_at' => NULL
                                );     
                            }                       
                        }

                        $adding_date_time += 86400; //add 24 hours for next day
                    }

                    break;
                case 'once':
                default:

                    /* Create new date for event */
                    $insert_dates[] = array(
                        'event_id' => $new_event->id,
                        'start_date' => date("Y-m-d H:i:s", $new_event->start_date),
                        'finish_date' => date("Y-m-d H:i:s", $new_event->finish_date),
                        'venue_id' => $event->venue_id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s"),
                        'deleted_at' => NULL
                    );
                    break;
            }


            $insert_booking[] = array(
                'event_id' => $new_event->id,
                'venue_id' => $event->venue_id,
                'booking' => (($event->tickets != '') ? "external":"no" ),
                'booking_info' => $event->cost,
                'booking_external' => $event->tickets
            );

            $insert_questions[] = array(
                'event_id' => $new_event->id,
                'key' => 'Culture Card',
                'value' => (($event->culture_card == 1) ? "Yes":"No")
            );

            $insert_questions[] = array(
                'event_id' => $new_event->id,
                'key' => 'Venue Information',
                'value' => $event->venue
            );

            $insert_questions[] = array(
                'event_id' => $new_event->id,
                'key' => 'Concessions',
                'value' => (($event->concessions == 1) ? "Yes":"No")
            );
        }

        if(count($insert_data) > 0)
        {
            DB::table('taxonomy_relationships')->insert($insert_data);
        }

        DB::table('events_dates')->insert($insert_dates);
        DB::table('events_booking_info')->insert($insert_booking);
        DB::table('events_questions')->insert($insert_questions);

        die("completed");   
    }

    public function getVenueAjaxView($event_id, $venue_id)
    {
        $this->data['event'] = Events::find($event_id);
        $this->data['venue'] = $venue_id;
        $this->data['tmp_venue'] = (strpos($venue_id, 'tmp_') !== false) ? true:false;

        $html = \View::make('HummingbirdBase::cms.events-edit-venue-ajax', $this->data);
        // $html = str_replace("{x}", $venue_id, $html);
        return $html;
    }

    public function getDateInformation($event_id)
    {
        $events = DB::table('events_dates')
                    ->leftJoin('events_booking_info', function($join)
                        {   
                            $join->on('events_dates.event_id', '=', 'events_booking_info.event_id');
                            $join->on('events_dates.venue_id', '=', 'events_booking_info.venue_id');
                        })
                    ->leftJoin('venues', 'events_dates.venue_id', '=', 'venues.id')
                    ->where('events_dates.event_id', '=', $event_id)
                    ->whereNull('events_dates.deleted_at')->get();

        $data = array();
        if(count($events) > 0)
        {
            $base_data = false;

            foreach($events as $event)
            {
                $venue_id = ($event->venue_id === NULL) ? 'none':$event->venue_id;

                if(!isset($data[$venue_id])) $base_data = false; //reset this

                $data[$venue_id]['events'][] = $event;

                if(!$base_data)
                {
                    $data[$venue_id]['data']['name'] = $event->name;
                    $base_data = true;
                }
            }
        }

        return $data;
    }

    /**
     * Searching for events
     *
     * @param  array  $args
     * @return Array of results
     */
    public function search($args = array())
    {
        $venues = array(
            1 => "All of Luton",
            2 => "Town Centre",
            3 => "Bushmead",
            4 => "Bury Park",
            5 => "Leagrave",
            6 => "Marsh Farm",
            7 => "Lewsey",
            8 => "Stopsley",
            9 => "Stockwood",
            10 => "Farley",
            11 => "Wardown",
            12 => "Saints",
            13 => "Limbury"
        );

        $venues_map = array(
            1 => '',
            2 => array(14, 4, 1, 25),
            3 => array(23),
            4 => array(38, 9),
            5 => array(26, 8, 29),
            6 => array(12, 31),
            7 => array(10, 18),
            8 => array(33, 7),
            9 => array(2),
            10 => array(28),
            11 => array(3),
            12 => array(27),
            13 => array(32)
        );


        if(is_array($args) OR is_empty($args))
        {
            $args = array(
                'categories' => '',
                'venue' => '',
                'from' => '',
                'to' => '',
                'search' => ''
            );
        }

        // 27 + 10 + 16 + 0 = 53


        if(null !== Input::all())
        {
            $categories = (count(Input::get('categories')) > 0) ? Input::get('categories'):array();
            $venue = Input::get('venue');
            $search = Input::get('s');

            switch(Input::get('date'))
            {
                case 'today':
                    $from_date = time();
                    $to_date = strtotime(date("Y-m-d") . " 23:59:59");
                    break;
                case 'tomorrow':
                    $from_date = strtotime(date("Y-m-d", strtotime("+1 day")) . " 00:00:00");
                    $to_date = strtotime(date("Y-m-d", strtotime($from_date)) . " 23:59:59");
                    break;
                case 'weekend':
                    $from_date = strtotime(date("Y-m-d", strtotime("next Saturday")) . " 00:00:00");
                    $to_date = strtotime(date("Y-m-d", $from_date + 86400) . " 23:59:59");
                    break;
                case '7days':
                    $from_date = time();
                    $to_date = strtotime("+1 week");
                    break;
                case 'month':
                    $from_date = time();
                    $to_date = strtotime(date("Y-m-").date('t',time()) . " 23:59:59");
                    break;
                case 'range':
                    $from_date = Input::get('from_date'); //timestamp only
                    $to_date = Input::get('to_date');
                    // dd(Input::get());
                    break;
                case 'to':
                    $from_date = null;
                    $to_date = Input::get('to_date');
                    // dd('to');
                    break;
                case 'from':
                default:
                    $from_date = (Input::get('from_date') == '') ? time():Input::get('from_date');
                    $to_date = null;
                    break;
            }

// 1425
// 1465
// 1490
// 1491
// 1492
// 1493
// 1494

            /* Single From */
            // $from_date = time();
            // $to_date = null; 

            /* Single To */
            // $from_date = null;
            // $to_date = time(); 

            /* From and To */
            // $from_date = time();
            // $to_date = strtotime("+1 week");

            /* Next weekend */
            // $from_date = strtotime(date("Y-m-d", strtotime("next Saturday")) . " 00:00:00");
            // $to_date = strtotime(date("Y-m-d", $from_date + 86400) . " 23:59:59");

            /* Next 7 days */
            // $from_date = time();
            // $to_date = strtotime("+1 week");

            /* This month */
            // $from_date = time();
            // $to_date = strtotime(date("Y-m-").date('t',time()) . " 23:59:59");

            /* Other */
            // $from_date = (null !== Input::get('from_date')) ? strtotime(Input::get('from_date')):time();
            // $to_date = (null !== Input::get('to_date')) ? strtotime(Input::get('to_date')):time();

            // $events = Events::whereHas('taxonomy_relationships', function($query) use ($categories)
            // {
            //     $query->whereIn('item_id', $categories);
            // })->get();
            $events = new Events;

            // $events = $events->select('.*');
            $events = $events->search($search);

            if(count($categories) > 0)
            {
                $events = $events->whereHas('taxonomy_relationships', function($query) use ($categories)
                {
                    foreach($categories as $category)
                    {
                        if(is_numeric($category))
                        {
                            $query->where('taxonomy_relationships.term_id', '=' ,$category); 
                        }
                    }
                });
            }

            $events = $events->where(function($query) use ($from_date, $to_date)
            {  
                $query->future($from_date, $to_date);
            });

            if($venue != '' AND $venue > 1)
            {
                if(isset($venues_map[$venue]))
                {
                    if(count($venues_map[$venue]) > 0)
                    {
                        $events = $events->leftJoin('events_dates', 'events_dates.event_id', '=', 'events.id')
                                ->where(function($query) use ($venues_map, $venue)
                                {
                                    foreach($venues_map[$venue] as $key)
                                    {
                                        $query->orWhere('venue_id', '=', $key);
                                    }
                                });
                    }
                }
            }

            $events = $events->whereNull('events.deleted_at')->where('events.start_date', '!=', '0000-00-00 00:00:00')->orderBy('events.start_date', 'ASC')->get();
        }
        else
        {
            $events = Events::future()->orderBy('start_date', 'ASC')->get();
        }

        // DatabaseHelper::getDatabaseQueries();die();


// select * from `events` where (select count(*) from `taxonomy` inner join `taxonomy_relationships` on `taxonomy`.`id` = `taxonomy_relationships`.`term_id` where `taxonomy_relationships`.`item_id` = `events`.`id`) >= 1 and (((`start_date` <= '2015-06-22 16:52:48' and `finish_date` >= '2015-06-22 16:52:48') or (`start_date` >= '2015-06-22 16:52:48' and `finish_date` >= '2  '))) and `events`.`deleted_at` is null and `events`.`start_date` != '0000-00-00 00:00:00' order by `events`.`start_date` asc

        $return_events = array(
            'pagetitle' => 'Displaying events: ' . count($events) . ' results'
        );

        foreach($events as $event)
        {
            $event->venues = $this->getDateInformation($event->id);
            $event->taxonomy = $event->taxonomy();
            $event->image = General::findImage($event, array('featured_image', 'search_image', 'description'));

            switch($event->showcase)
            {
                case true:
                    $return_events['showcase'][] = $event;
                    break;
                case false:
                default:
                    $return_events['general'][] = $event;
                    break;
            }
        }

        // echo '<h1>'.date("d-M-Y", $from_date).' - '.date("d-M-Y", $to_date).'</h1>';
        // foreach($events as $event)
        // {
        //     echo '<pre>'.print_r($event->title, true).'</pre>';
        // }

        // die();

        return json_encode($return_events);
    }

    /**
     *
     *
     * @param  
     * @return 
     */
    public function cms_search($ajax = false)
    {
        if(Input::get('ajax')) $ajax = true;

        $words = trim(Input::get('s'));
        $words = explode(" ", $words);

        $to_search = array();

        foreach($words as $word)
        {
            // $word = trim($word);
            // $to_search[] = Porter::Stem($word);
            $to_search[] = trim($word);
        }

        $to_search_words = trim(implode(" ", $to_search));

        return $this->event->select(DB::raw("events.*, 
            (
                (
                    1 * (
                            MATCH(title) AGAINST(?)
                        )
                )
                +
                (
                    1 * (
                            MATCH(url) AGAINST(?)
                        )
                )
                +
                (
                    1 * (
                            MATCH(summary) AGAINST(?)
                        )
                )
                +
                (
                    1 * (
                            MATCH(description) AGAINST(?)
                        )
                )
            ) AS `score`"))
        // $search will NOT be bind here
        // it will be bind when calling setBindings
        ->whereRaw("MATCH(title, url, summary, description) AGAINST(?)")
        // I want to keep only published quotes
        // ->where('approved', '=', 1)
        // Order by the rank column we got with our FULLTEXT index
        ->orWhere(function($query) use ($words)
        {
            foreach($words as $word)
            {
                $query->orWhereRaw("(title LIKE '%%$word%%' OR url LIKE '%%$word%%' OR summary LIKE '%%$word%%' OR description LIKE '%%$word%%')");
            }
        })
        ->havingRaw('score > 0')
        ->orderBy('score', 'DESC')
        // Bind variables here
        // We really need to bind ALL variables here
        // question marks will be replaced in the query
        ->setBindings([$to_search_words, $to_search_words, $to_search_words, $to_search_words, $to_search_words])
        ->get();

        DatabaseHelper::getDatabaseQueries();
        die();
    }


    function run_event_images()
    {
        ini_set('memory_limit','4096M');
        set_time_limit(0); //300 = 5

        $inserted_images = array(); // key = value

        $festivals = array();
        $insert_data = array();

        $events = Events::orderBy('id', 'ASC')->take(100)->get();
        
        $collection_id = 265; // 265 + 97 = 362

        foreach($events as $event)
        {
            $collection_id++; //start at 266

            /* Load the html and get images */
            // $doc = new DOMDocument();
            // @$doc->loadHTML($event->description);

            // $tags = $doc->getElementsByTagName('img');

            // foreach ($tags as $tag)
            // {
            //     $src = $tag->getAttribute('src');

            //     /* find in database */
            //     $resource = DB::table('media_xref')->where("source", '=', trim($src))->first();

            //     if(null !== $resource)
            //     {
            //         $event->description = str_replace($src, $resource->destination, $event->description);
            //     }
            //     else
            //     {
            //         /* process images */
            //         $base_image = base_path() . '/import' . $src;
                    
            //         $resource = App::make('MediaController')->uploadFile_byFunction($base_image, $collection_id);

            //         if($resource !== false)
            //         {
            //             $event->description = str_replace($src, $resource, $event->description);

            //             /* Add to media_xref */
            //             DB::table('media_xref')->insert(array
            //                 (
            //                     'source' => $src,
            //                     'destination' => $resource
            //                 )
            //             );
            //         }
            //     }
            // }

            // die();

            
            // /* Do something with the images */
            $event_images = DB::table('old_images')->where('page_id', '=', $event->id)->where('type', '=', 'events')->orderBY('order_id', 'ASC')->orderBy('deleted', 'ASC')->get();

            if(count($event_images) > 0)
            {
                $insert_event_images = array();
                $counter = 0;

                foreach($event_images as $event_image)
                {   
                    //filename = 1328109287.jpg - need to be absolute
                    $orig_file = '/uploads/images/raw/' . $event_image->filename;

                    /* find in database */
                    $resource = DB::table('media_xref')->where("source", '=', trim($orig_file))->first();

                    if(null !== $resource)
                    {
                        $image_source = $resource->destination;
                    }
                    else
                    {
                        /* process images */
                        $base_image = base_path() . '/import/images/raw/' . $event_image->filename;
                        
                        $image_source = App::make('MediaController')->uploadFile_byFunction($base_image, $collection_id);

                        if($image_source !== false)
                        {
                            /* Add to media_xref */
                            DB::table('media_xref')->insert(array
                                (
                                    'source' => $orig_file,
                                    'destination' => $image_source
                                )
                            );
                        }
                    }

                    if($image_source !== false)
                    {
                        if($event_image->deleted == 0)
                        {
                            if($counter == 0)
                            {
                                $event->featured_image = $event->search_image = $image_source;
                            }
                            else
                            {
                                $insert_event_images[] = array(
                                    'event_id' => $event->id,
                                    'image' => $image_source
                                );
                            }

                            $counter++;
                        }
                    }
                }

                if(count($insert_event_images) > 0)
                {
                    DB::table('events_images')->insert($insert_event_images);
                    General::pp($insert_event_images);
                }
            }
            
            $event->save();
        }
    }
}
