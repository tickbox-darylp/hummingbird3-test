<?php


class UserObserver extends CmsObserver { 
    
    protected $permissions = array('create', 'read', 'edit', 'delete', 'preview');
    protected $object_type = 'users';
}
