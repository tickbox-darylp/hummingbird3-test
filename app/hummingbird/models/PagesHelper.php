<?php


class PagesHelper
{
    public static function build_slug($url, $parent_id = 0) 
    {
        # recursivley get parent url
        // dd($parent_id);

        $permalink = PagesHelper::get_parent_url($url, $parent_id);

        return $permalink;
    }

    public static function get_parent_url($slug = '/', $parent_id = 0)
    {
        // echo "parentid: $parent_id, slug: $slug \n";exit;
        if(null === $parent_id) return $slug;
        
        // echo "parentid: $parent_id, slug: $slug \n";exit;
        $parent = Page::find($parent_id);

        $slug = ($parent->url == '/' OR $parent->url == '') ? "/$slug":"$parent->url/$slug";

        // - ///support-us//support-us///help//
        // - ///support-us//support-us//help/
        // - support-us/support-us/help


        return PagesHelper::get_parent_url($slug, $parent->parentpage_id);
        //return $slug;
    }
    
    public static function get_url_from_title($title) {
        
        $url = str_replace(" ", "-", $title);
        $url = preg_replace('/[^A-Za-z0-9\-]/', '', $url);
        return strtolower($url);
    }
    
    public static function parseTemplateForEditPane($template_html)
    {
        
    }
}

?>
