var active_el = '';
var remove_classes = ["hb-item", "column", "f_pad", "pending-settings", "ui-sortable", "ui-draggable", "ui-sortable-handler", "clearfix", "ui-sortable-handle"];
var container_classes = ["hb_container"];
var row_classes = ["row-wrapper", "hb_row"];
var column_classes = ["hb_column"];
var item_classes = [];

function activate_init()
{
    if($('#flexicontainer').children().length > 0)
    {   
        $("#flexicontainer").removeClass('empty').find('.empty-text').remove();

        $("#flexicontainer .add-btn").remove();
        $("#flexicontainer .hb-item").each(function()
        {   
            if($(this).hasClass('hb_container'))
            {
                $(this).data('type', 'container');
                $(this).prepend(getTools('container'));
                $(this).append(addBtn('container'));
            }
            else if($(this).hasClass("hb_row"))
            {
                $(this).data('type', 'hb_row');
                $(this).prepend(getTools('row'));
                $(this).append(addBtn('row'));
            }
            else if($(this).hasClass("column"))
            {
                $(this).data('type', 'column');
                $(this).prepend(getTools('column'));
                $(this).append(addBtn('column'));
            }
            
            $(this).addClass('f_pad');
        });

        $("#flexicontainer .item").each(function()
        {
            var tools = '<div class="tools hide"><div class="tool handle hide"><i class="fa fa-arrows"></i></div><div class="tool remove"><a class="remove_item" href="#"><i class="fa fa-trash"></i></a></div></div>';

            if($(this).hasClass('image-wrapper'))
            {
                $(this).prepend(tools);
            }
            else if($(this).hasClass("redactor-wrapper"))
            {
                var id = randString();
                var html = $(this).html();
                $(this).html('');
                $(this).append(tools+'<textarea id="'+id+'" class="redactor-content" name="redactor[]">'+html+'</textarea>');
            }
        });

        init_redactor();

        init();
    }
}

function getTools(type)
{
    if(typeof locked === 'undefined' || !locked)
    {
        str = '<div class="tools">';

        str +=  '<div class="tool"><a class="remove_tool" href="#"><i class="fa fa-times"></i></a></div>'; //all
        str +=  '<div class="tool draghandle"><i class="fa fa-arrows"></i></div>'; //all
        str +=  '<div class="tool"><a class="settings_tool" href="#"><i class="fa fa-gears"></i></a></div>'; //all

        if(type == 'container')
        {
            str +=  '<div class="tool"><a class="replicate-container" href="#"><i class="fa fa-copy"></i></a></div>'; 
        }
        else if(type == 'row')
        {
            str +=  '<div class="tool"><a class="replicate-row" href="#"><i class="fa fa-copy"></i></a></div>';
        }
        else if(type == 'column')
        {
            // str +=  '<div class="tool"><a class="cascade" href="#"><i class="fa fa-chevron-down"></i></a></div>';
            str +=  '<div class="tool"><a class="replicate-column" href="#"><i class="fa fa-copy"></i></a></div>';
        }

        str +=  '</div>';

        return str;
    }
}

function setUpAddBtns()
{
    if(typeof locked === 'undefined' || !locked)
    {
        $(".hb-item .add-btn").remove();

        $(".hb-item").each(function()
        {
            var type;
            if($(this).hasClass('container'))
            {
                type = 'container';
            }
            else if($(this).hasClass('row'))
            {
                type = 'row';
            }
            else if($(this).hasClass('column'))
            {
                type = 'column';
            }
            $(this).append(addBtn(type))
        });
    }
}

function addBtn(type)
{
    if(typeof locked === 'undefined' || !locked)
    {
        return '<div class="add-btn text-center" data-type="' + type + '"><i class="fa fa-plus fa-2x"></i></div>';
    }
}

function generateHTML(button_obj, wrapper)
{
    if (wrapper === undefined) {
        wrapper = false;
    }

    // var original_ = $("#flexicontainer");
    var flex_el = $("#flexicontainer").clone();

    if(wrapper) flex_el.find('#hb-template-wrapper').remove();

    flex_el.find('.tools').remove();
    flex_el.find('.add-btn').remove();
    flex_el.find('*').removeClass('f_pad ui-draggable ui-sortable');
    flex_el.find('*').removeAttr('style');

    flex_el.find('.redactor-wrapper').each(function()
    {
        var el = $(this).find('.redactor-content').attr('id');
        var new_html = $("#" + el).redactor('code.get');
        $(this).html(new_html);
    });

    var html = $.trim(flex_el.html());

    if(wrapper && html != '')
    {
        html = '<div id="hb-template-wrapper">'+html+'</div>';
    }

    $("#generated_html").val(html);

    button_obj.closest('form').trigger('submit');
}

/**
 * Function generates a random string for use in unique IDs, etc
 *
 * @param <int> n - The length of the string
 */
function randString(n)
{
    if(!n)
    {
        n = 10;
    }

    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for(var i=0; i < n; i++)
    {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

function init_items()
{

}

function init()
{
    /* drag and drop rows */
    $( "#flexicontainer .row-wrapper" ).draggable({
        connectToSortable: "#flexicontainer",
        // helper: "clone",
        revert: "invalid",
        handle: ".draghandle",
        drag: function(event, ui) {
            ui.helper.width('68%');
        },
        stop: function( event, ui ) {
            $('#flexicontainer .column').sortable({ 
                opacity: 0.35,
                connectWith: '.column'
            });
            $(this).removeAttr('style');
        }

        // $(ui.draggable).remove(); //remove from list
    });


    $( "#flexicontainer .hb_column" ).draggable({
        connectToSortable: "#flexicontainer .row-wrapper",
        // helper: "clone",
        revert: "invalid",
        handle: ".draghandle",
        drag: function(event, ui) {
            ui.helper.width('10%');
        },
        stop: function( event, ui ) {
            $('#flexicontainer .row-wrapper').sortable({ 
                opacity: 0.35,
                connectWith: '.row-wrapper'
            });
            $(this).removeAttr('style');
        }

        // $(ui.draggable).remove(); //remove from list
    });


    $( "#flexicontainer .item" ).draggable({
        connectToSortable: "#flexicontainer .row-wrapper",
        // helper: "clone",
        revert: "invalid",
        handle: ".draghandle",
        drag: function(event, ui) {
            ui.helper.width('10%');
        },
        stop: function( event, ui ) {
            $('#flexicontainer .row-wrapper').sortable({ 
                opacity: 0.35,
                connectWith: '.row-wrapper'
            });
            $(this).removeAttr('style');
        }

        // $(ui.draggable).remove(); //remove from list
    });
}


$(function() {
    /* sortables */
    $( "#flexicontainer" ).sortable(
    {
        revert: true,
        connectWith: '.column, .item, .row-wrapper',
        opacity: 0.35,
        handle: ".draghandle",
        zIndex:1
    });

    // $( "#flexicontainer .row-wrapper" ).sortable(
    // {
    //     revert: true,
    //     connectWith: '.column',
    //     opacity: 0.35,
    //     handle: ".draghandle"
    // }).disableSelection();

$('.modal-option').click(function(e)
{
    e.preventDefault();

    var tools = '<div class="tools hide"><div class="tool handle hide"><i class="fa fa-arrows"></i></div><div class="tool remove"><a class="remove_item" href="#"><i class="fa fa-trash"></i></a></div></div>';

    if($(this).data('type') == 'row')
    {
        active_el.append('<div class="row f_pad hb-item clearfix row-wrapper hb_row ui-draggable">'+tools+'</div>');

        setUpAddBtns();
        
        // var add_btn = active_el.find('.add-btn').clone();
        // active_el.find('.add-btn').remove();
        // active_el.append(add_btn);

        $('.modal-footer .btn').trigger('click');
    }
    if($(this).data('type') == 'column')
    {
        active_el.append('<div class="col-md-12 hb-item f_pad column hb_column">'+tools+ '</div>');

        setUpAddBtns();

        // var add_btn = active_el.find('.add-btn').clone();
        // active_el.find('.add-btn').remove();
        // active_el.append(add_btn);

        $('.modal-footer .btn').trigger('click');
    }
    else if($(this).data('type') == 'wysiwyg')
    {
        var id = randString();

        destroy_redactor(); // destroy all redactors

        active_el.append('<div class="item redactor-wrapper relative">'+tools+'<textarea id="'+id+'" class="redactor-content" name="redactor[]"></textarea></div>');

        init_redactor();

        var add_btn = active_el.find('.add-btn').clone();
        active_el.find('.add-btn').remove();
        active_el.append(add_btn);

        $('.modal-footer .btn').trigger('click');

    }
    else if($(this).data('type') == 'image')
    {
        $.ajax({
            dataType: "json",
            cache: false,
            url: '/hummingbird/media/globalMediaLibrary',
            success: $.proxy(function(data)
            {  
                $("body").append(data.html);
                $(".modal").modal('hide');
                $("#media-lib").addClass("global").modal('show');

                // $("#content-settings").find('#image-library.modal-body .row').html(data);
                // $("#content-settings .modal-body").toggleClass("hide");
            }, this)
        });
    }
    else if($(this).data('type') == 'module')
    {
        
    }
});

$('.clean').on('click', function(e)
{
    e.preventDefault();
    $('#flexicontainer').html('');
    $("#flexicontainer").addClass('empty').html('<div class="empty-text"><h6>No content</h6><p>Get started by selecting a template</p></div>');
});

$('.structure_input a, #structure_button').on('click', function(e)
{
    e.preventDefault();

    if($(this).attr('id') != 'structure_button')
    {
        var structure = $(this).data('size');
    }
    else
    {
        var structure = $('input[name=structure]').val();
    }


    if(structure == 'container')
    {
        str = '<div class="container hb-item column f_pad hb_container">';
        str += '<div class="tools">';
        str += '<div class="tool"><a class="remove_tool" href="#"><i class="fa fa-times"></i></a></div>';
        str += '<div class="tool draghandle"><i class="fa fa-arrows"></i></div>';
        str +=            '<div class="tool"><a class="settings_tool" href="#"><i class="fa fa-gears"></i></a></div>';
        str +=            '<div class="tool"><a class="replicate-container" href="#"><i class="fa fa-copy"></i></a></div>'; 
        str += '</div>';
        str += '</div>';
    }
    else if(structure == 'row')
    {
        str = '<div class="row hb-item f_pad clearfix row-wrapper hb_row">';
        str +=     '<div class="tools">';
        str +=             '<div class="tool"><a class="remove_tool" href="#"><i class="fa fa-times"></i></a></div>';
        str +=            '<div class="tool draghandle"><i class="fa fa-arrows"></i></div>';
        str +=            '<div class="tool"><a class="settings_tool" href="#"><i class="fa fa-gears"></i></a></div>'; 
        str +=        '</div>';
        str += '</div>';
    }
    else if(structure == 'single-column')
    {
        str = '<div class="col-md-12 hb-item f_pad column hb_column">';
        str +=     '<div class="tools">';
        str +=             '<div class="tool"><a class="remove_tool" href="#"><i class="fa fa-times"></i></a></div>';
        str +=            '<div class="tool draghandle"><i class="fa fa-arrows"></i></div>';
        str +=            '<div class="tool"><a class="settings_tool" href="#"><i class="fa fa-gears"></i></a></div>'; 
        str +=        '</div>';
        str += '</div>';
    }
    else
    {
        if(structure != '12')
        {
            var spl = structure.split(',')
            var myTotal = 0;
              for(var i=0, len=spl.length; i<len; i++){
                    myTotal += parseInt(spl[i]);
            }
        }
        else
        {
            spl = new Array();
            spl[0] = 12;
            myTotal = 12;
        }

        // if(myTotal !== 12)
        // {
        //     alert('You should ensure that column widths add up to 12');
        // }
        // else
        // {
              str =       '<div class="container hb-item column f_pad hb_container">';
              str +=     '<div class="tools">';
              str +=             '<div class="tool"><a class="remove_tool" href="#"><i class="fa fa-times"></i></a></div>';
              str +=            '<div class="tool draghandle"><i class="fa fa-arrows"></i></div>';
              str +=        '</div>';

              str += '<div class="row f_pad hb-item clearfix row-wrapper hb_row">';
              str +=     '<div class="tools">';
              str +=             '<div class="tool"><a class="remove_tool" href="#"><i class="fa fa-times"></i></a></div>';
              str +=            '<div class="tool draghandle"><i class="fa fa-arrows"></i></div>';
              str +=            '<div class="tool"><a class="settings_tool" href="#"><i class="fa fa-gears"></i></a></div>'; 
              str +=        '</div>';

                 for(var i=0, len=spl.length; i<len; i++){
                
                str += '<div class="col-md-'+parseInt(spl[i])+' f_pad column hb_column hb-item">';
              str +=     '<div class="tools">';
              // str += '<div class="tool"><a href="#" class="add_tool"><i class="fa fa-edit"></i></a></div>';
              str +=             '<div class="tool"><a class="remove_tool" href="#"><i class="fa fa-times"></i></a></div>';
              str +=            '<div class="tool draghandle"><i class="fa fa-arrows"></i></div>';
              str +=            '<div class="tool"><a class="settings_tool" href="#"><i class="fa fa-gears"></i></a></div>';
              str +=            '<div class="tool"><a class="replicate-column" href="#"><i class="fa fa-copy"></i></a></div>'; 
              str += '</div>';
                str += '</div>';

                }

             str+= '</div></div>';
        // }
    }
    
    $('#flexicontainer').append(str);
    $('#save_button').show();

    init();

    setUpAddBtns();
});



$("#settings-modal .modal-footer").click(function(e)
{
    e.preventDefault();

    var active_el = $(".pending-settings");
    active_el.attr('id', $("#settings-modal #id").val());

    var classes = $("#settings-modal #class").val().split(/\s+/);
    var old_classes = $("#settings-modal #hidden-classes").val().split(/\s+/);
    var newClasses = $.merge(classes, old_classes);

    active_el.attr('class', newClasses.join(" "));
    active_el.removeClass("pending-settings");

    return true; 

});


$('#save_button').on('click', function()
{   
    generateHTML($(this));
});


$(document).on({
    mouseenter: function ()
    {
        $(this).find('.tools').removeClass('hide');
    },
    mouseleave: function () 
    {
        $(this).find('.tools').addClass('hide');
    }
}, ".item");

    $(document).on("click",'.tools .remove_item',function(e)
    {
        e.preventDefault();

        $(this).closest('.item').remove();
    });

    $(document).on("click",'.tools .remove_tool',function(e)
    {
        e.preventDefault();

        var el = $(this);
        var classes = ["hb_column", "hb_row", "hb_container", "hb_container-fluid"];
        var classList = $(this).closest('.hb-item').attr('class').split(/\s+/);

        $.each(classes, function(key, value)
        {
            if($.inArray(value, classList) > -1)
            {
                switch(value)
                {
                    case 'hb_column':
                    case 'hb_row':
                    case 'hb_container':
                    case 'hb_container-fluid':
                        el.closest('.hb-item').remove();
                        return false;
                        break;
                }
            }            
        });
    });

    $(document).on("click",'.add-btn',function(e)
    {
        active_el = $(this).closest('.hb-item');

        $("#content-settings .modal-body").addClass('hide');
        $("#content-settings #options").removeClass('hide');

        $("#content-settings").modal('show');
    });

    $(document).on("click",'#content-settings #image-library .thumbnail',function(e)
    {
        e.preventDefault();

        var tools = '<div class="tools hide"><div class="tool handle hide"><i class="fa fa-arrows"></i></div><div class="tool remove"><a class="remove_item" href="#"><i class="fa fa-trash"></i></a></div></div>';
        var img = $(this).find('img');
        var html = '<div class="item image-wrapper">'+tools+'<img src="'+img.attr('src')+'" alt="'+img.attr('alt')+'" title="'+img.attr('title')+'" /></div>';

        active_el.append(html);

        var add_btn = active_el.find('.add-btn').clone();
        active_el.find('.add-btn').remove();
        active_el.append(add_btn);

        $('.modal-footer .btn').trigger('click');

        $("#content-settings .modal-body").addClass('hide');
        $("#content-settings #options").removeClass('hide');

        $("#content-settings").modal('hide');
    });

    $(document).on("click",'.tools .settings_tool',function(e)
    {
        e.preventDefault();

        // $("#settings-modal .form-group.column-size").addClass('hide');

        active_el = $(this).closest('.hb-item');
        active_el.addClass("pending-settings");

        var id = active_el.attr('id');
        var classes = active_el.attr('class').split(/\s+/);
        var search_array = remove_classes;
        var newClasses = [];
        var removedClasses = [];

        if(active_el.hasClass('hb_container'))
        {
            search_array = $.merge(search_array, container_classes);
        }
        else if(active_el.hasClass('hb_row'))
        {
            search_array = $.merge(search_array, row_classes);
        }
        else if(active_el.hasClass('hb_column'))
        {
            // $("#settings-modal .form-group.column-size").removeClass('hide');

            search_array = $.merge(search_array, column_classes);
        }
        else
        {
            search_array = $.merge(search_array, item_classes);
        }

        $.each(classes, function(key, value)
        {
            value = $.trim(value)
            if($.inArray(value, remove_classes) < 0)
            {
                newClasses.push(value);
            }    
            else
            {
                removedClasses.push(value);
            }        
        });

        $("#settings-modal #id").val(id);
        $("#settings-modal #class").val(newClasses.join(" "));
        $("#settings-modal #hidden-classes").val(removedClasses.join(" "));

        $("#settings-modal").modal({backdrop: 'static'});
    });

    $(document).on("click",'.tools .replicate-container',function(e)
    {
        e.preventDefault();

        var this_column = $(this).closest('.hb_container').clone();
        var this_row = $(this).closest('.hb-item');

        this_row.after(this_column);

        init_items(this_row);
    });

    $(document).on("click",'.tools .replicate-row',function(e)
    {
        e.preventDefault();

        var this_column = $(this).closest('.hb_row').clone();
        var this_row = $(this).closest('.hb-item');

        this_row.after(this_column);
    
        init_items(this_row);
    });

    $(document).on("click",'.tools .replicate-column',function(e)
    {
        e.preventDefault();

        var this_column = $(this).closest('.hb_column').clone();
        var this_row = $(this).closest('.hb-item');

        this_row.after(this_column);

        init();
    });

    $(document).on("click",'.select-area',function(e)
    {
        e.preventDefault();
        var hasClass = $(this).hasClass('active');
        $(".select-area").removeClass('active');
        
        if(!hasClass) $(this).addClass('active');
        
    });


    $(document).on("click",'.redactor-editor',function(e)
    {
        e.preventDefault();
    });

    $(document).on('click', '.column-width', function(e)
    {
        e.preventDefault();

        $(".column-width").removeClass('btn-success');
        $(this).removeClass('btn-primary').addClass('btn-success');
    });


    $(document).on('click', '#media-lib.global #library .view-collection', function(e)
    {
        e.preventDefault();

        $("#library").html('<h3>Loading...</h3>');

        var data = {};
        data.action = 'library';
        data.collection = $(this).data('id');

        $.ajax({
            dataType: "json",
            data: data,
            cache: false,
            url: '/hummingbird/media/globalMediaLibrary',
            success: $.proxy(function(data)
            {
                data = $(data.html);
                html = data.find('#library').html();

                $("#media-lib.global #library").html(html);
            }, this)
        });
    });

    $(document).on('click', '#media-lib.global .gallery .thumb', function(e)
    {
        e.preventDefault();

        $("#library").html('<h3>Loading...</h3>');
        $("#media-lib.global .modal-footer").remove();

        var data = {};
        data.action = 'item';
        data.item = $(this).data('item-id');

        $.ajax({
            dataType: "json",
            data: data,
            cache: false,
            url: '/hummingbird/media/globalMediaLibrary',
            success: $.proxy(function(data)
            {
                data = $(data.html);
                html = data.html();

                $("#media-lib.global #library").html(html);

                $("#media-lib.global #library").closest('.modal-content').append('<div class="modal-footer"><footer><button type="submit" class="create-image btn btn-primary">Insert Image</button></footer></div>');
            }, this)
        });
    });

    $(document).on('click', '#media-lib.global .create-image', function(e)
    {
        e.preventDefault();

        var tools = '<div class="tools hide"><div class="tool handle hide"><i class="fa fa-arrows"></i></div><div class="tool remove"><a class="remove_item" href="#"><i class="fa fa-trash"></i></a></div></div>';

        var html = '<div class="item image-wrapper relative">'+tools;
        
        if($("#media-lib.global #url").val() == 'url')
        {
            console.log($("#target_window").val());
            html += '<a href="'+$("#media-lib.global #link").val()+'" target="'+$("#media-lib.global #target_window").val()+'">';
        }

        html += '<img src="'+$("#media-lib.global #library #src").val()+'" class="image-content" alt="'+$("#media-lib.global #library #alt").val()+'" title="'+$("#media-lib.global #library #title").val()+'" />';

        if($("#media-lib.global #url").val() == 'url')
        {
            html += '</a>';
        }

        html += '</div>';

        active_el.append(html);
        $("#media-lib.global").modal('hide');

        var add_btn = active_el.find('.add-btn').clone();
        active_el.find('.add-btn').remove();
        active_el.append(add_btn);
    });

    $(document).on('change', '#media-lib.global #url', function(e)
    {
        e.preventDefault();

        $(".url-show").toggleClass('hide');
    });

    if($("#template").length > 0)
    {
        $("#template").change(function()
        {
            var template = $(this).val();
        });
    }

    function count_spaces(html)
    {
       return $(html).find('.column').length;
    }
});