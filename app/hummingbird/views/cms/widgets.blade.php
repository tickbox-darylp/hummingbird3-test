@extends('HummingbirdBase::cms.layout')

@section('content')

<?php $even = false; ?>

<h1>All Widgets</h1>

<table class='results' cellpadding='5' cellspacing='0'>
    <thead>
        <th>Name</th>
        <th>View</th>
        <th>Live</th>
        <th>Actions</th>
    </thead>
    <tbody>
        @foreach($widgets as $widget)
        
        <tr <?php echo ($even) ? 'class="even"': '';$even = !$even;?>>
            <td>{{$widget->name}}</td>
            <td>{{$widget->view}}</td>
            <td>{{($widget->live)?'Active':'Not live'}}</td>
            <td>
                <a href='/{{App::make('backend_url')}}/widgets/edit/{{$widget->id}}'>Edit</a> | 
                <a href='/{{App::make('backend_url')}}/widgets/delete/{{$widget->id}}'>Delete</a>
            </td>
        </tr>
        
        @endforeach
    </tbody>
</table>

<h1>Add new widget</h1>
<?php echo Form::open(array('url' => App::make('backend_url').'/widgets/add', 'method' => 'post')) ?>
    <table cellpadding="5" cellspacing="0" id="add-mini">
        <tbody>
            <tr>
                <td class="row_name">Name: <strong>*</strong></td>
                <td><input class="required input_box" id="name" type="text" name="name"></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="submit" value="Add Widget" name="add">
                </td>
            </tr>
        </tbody>
    </table>
<?php echo Form::close()?>

@stop
