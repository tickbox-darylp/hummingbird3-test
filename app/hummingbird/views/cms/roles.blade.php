@extends('HummingbirdBase::cms.layout')

@if(isset($breadcrumbs))
    @include('HummingbirdBase::cms.include.breadcrumbs', array('breadcrumbs' => $breadcrumbs))
@endif

@section('content')

<?php $even = false; ?>

<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <h1>Manage User Roles</h1>

            @if(count($roles) > 0)
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($roles as $role)
                                @if(!$role->hidden OR $role->hidden)

                                    <tr <?php echo ($even) ? 'class="even"': '';$even = !$even;?>>
                                        <td>{{str_repeat('&nbsp;&nbsp;&nbsp;', $role->level)}} {{ $role->name}}</td>
                                        <td>
                                            @if($role->showPreviewLink($role->id))
                                                <a style="background-color:black;color:white;" class="btn btn-xs" href="/{{App::make('backend_url').'/roles/preview/'.$role->id}}" data-original-title="Preview {{$role->name}}"><i class="fa fa-globe "></i></a>
                                            @endif

                                            @if(!$role->locked)
                                                <a href="/{{App::make('backend_url').'/roles/edit/'.$role->id}}" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>
                                                
                                                <a href="/{{App::make('backend_url').'/roles/delete/'.$role->id}}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                            @else
                                                <img src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQYmxCNd0RIQJytdZl4TVPYFOKw5h5E147WSv3zge8ENkp_DLggZnh7pJI" width="15" />
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <h3>No roles have been created.</h3>
            @endif
        </div>
    </section>
</div>


<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <h1>Add new role</h1>

            @foreach ($errors->all('<li>:message</li>') as $error)
                {{$error}}    
            @endforeach

            <div class="table-responsive">
                <?php echo Form::open(array('url' => App::make('backend_url').'/roles/add', 'method' => 'post')) ?>
                    <table class="table table-striped table-hover">
                        <tbody>
                            <tr>
                                <td class="row_name">Name: <strong>*</strong></td>
                                <td><input class="required input_box" id="name" type="text" name="name"></td>
                            </tr>
                            @if(count($roles) > 0)
                                <tr>
                                    <td class="row_name">Parent role: <strong>*</strong></td>
                                    <td>
                                        <select name="parentrole_id" id="parentrole_id">
                                            <option value="{{ Role::getUserRoleID() }}">{{ Role::getUserRoleName(Role::getUserRoleID()) }}</option>

                                            @foreach($roles as $role)
                                                <option value="{{ $role->id }}"><?=str_repeat('&nbsp;&nbsp;&nbsp;', $role->level+1)?>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            @else
                                <input type="hidden" name="parentrole_id" value="{{ Role::getUserRoleID() }}" />        
                            @endif
                            
                            <tr>
                                <td colspan="2" align="center">
                                    {{Form::submit('Add Role')}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php echo Form::close()?>
            </div>
        </section>
    </div>
</div>

@stop