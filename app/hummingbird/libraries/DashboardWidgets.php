<?php

class DashboardWidgets
{
    public static function all()
    {
        $widgets = array();
        
        return $widgets;
        
        //todo: check for installed plugins
        
        // loop all plugins looking for widgets
        $plugins = CmsAreas::all();
        
        foreach($plugins as $plugin)
        {
            View::addLocation(app_path() . "/hummingbird/$plugin/views/cms");
            
            $path = app_path() . "/hummingbird/$plugin/views/cms/dashboard-widgets";
            
            if (!is_dir($path)) continue;
            
            $files = scandir($path);
            
            foreach($files as $file)
            {
                if ($file == '.' || $file == '..') continue;
                
                $widgets[] = 'dashboard-widgets/' . substr($file, 0, -10);
            }
        }
        
        return $widgets;
    }
}

?>
