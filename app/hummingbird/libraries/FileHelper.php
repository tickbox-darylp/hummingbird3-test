<?php

/**
 * Defines some file-system helpers for the CMS system.
 *
 */
class FileHelper
{
    /**
    * @var string Specifies the theme directory name.
    */
    protected $uploadDirectory;

    // valid file extensions
    protected $valid = array();
    protected $valid_all = array();
    protected $max_upload_size = array();
    protected $error;


    /**
    * CHANGE *
    * @param string $fileName Specifies a path to validate
    */    
    public function __construct()
    {
        $this->uploadDirectory = Config::get('HummingbirdBase::hummingbird.uploadsDir');

        $this->valid['images'] = array('jpg', 'jpeg', 'png', 'gif');
        $this->valid['documents'] = array('doc', 'docx', 'ppsx', 'pps', 'ppt', 'pptx', 'pptm', 'odt', 'pdf', 'zip', 'xlsm', 'xlsx', 'xls');
        $this->valid['audio'] = array('mp3', 'm4a', 'ogg', 'wav');
        // $this->valid['video'] = array('mp4', 'm4v', 'mov', 'wmv', 'avi', 'mpg', 'ogv', 'webm', 'flv', '3gp', '3g2', 'flv');
        $this->valid['video'] = array('mp4', 'webm', 'ogg', 'flv'); //html5 and flash supported videos
        $this->valid['other'] = array();

        $this->mergeValidExtensions();

        // 1000 = 1mb
        $this->max_upload_size = array(
            'images' => 320000,
            'documents' => 320000,
            'audio' => 320000,
            'video' => 320000,
            'other' => 10000
        );
    }

    public function getValidKeys()
    {
        return array_keys($this->valid);
    }

    public function mergeValidExtensions()
    {
        foreach($this->valid as $type => $items)
        {
            $this->valid_all = array_merge($this->valid_all, $items);
        }
    }

    public function isValidExtension($file_ext)
    {
        if(in_array($file_ext, $this->getAllExtensions())) return true;
    }

    public function checkMediaFileSize($filesize, $file_ext)
    {
        return $this->greaterThanMaxFileSize($filesize, $this->getValidKeyByExt($file_ext));
    }

    public function greaterThanMaxFileSize($file, $type = 'images')
    {
        /*
        * Validate
        */
        $rules = array(
            'file' => 'max:'.$this->max_upload_size[$type]
        );
         
        $inputs = array(
            'file' => $file
        );
         
        $validation = Validator::make($inputs, $rules);

        if(! $validation->passes() )
        {
            $this->error['filesize'] = 'The maximum file size for '.$type.' is &quot;'.$this->formatBytes($this->max_upload_size[$type] * 1000, 0).'&quot.';
            return true;
        }
    }

    public function getValidKeyByExt($file_ext)
    {
        foreach($this->valid as $type => $items)
        {
            if(in_array($file_ext, $items)) return $type;
        }

        return 'other'; //if not found use this directory for storing uploads
    }

    /**
    * CHANGE *
    * @param string $fileName Specifies a path to validate
    */
    public static function formatFileName($fileName)
    {
        return Str::slug($fileName);
    }

    /**
    * CHANGE *
    */
    public static function validateExtension($fileName, $allowedExtensions, $allowEmpty = true)
    {
        $extension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
        if (!strlen($extension))
            return $allowEmpty;

        return in_array($extension, $allowedExtensions);
    }

    public static function replaceContents($old_contents, $new_contents, $file)
    {        
        $file_contents = File::get($file);
        $new_file_contents = str_replace($old_contents, $new_contents, $file_contents);
        File::put($file, $new_file_contents);
    }
    
    public static function filenames($path)
    {
        $array = array();
        
        $files = scandir($path);
        
        foreach($files as $file)
        {
            if ($file == '.' || $file == '..' || is_dir($file)) continue;
            
            $array[] = $file;
        }
        
        return $array;
    }
    
    public static function directories($path)
    {
        $array = array();
        
        $dirs = scandir($path);
        
        foreach($dirs as $dir)
        {
            if ($dir == '.' || $dir == '..' || is_file($dir)) continue;
            
            $array[] = $dir;
        }
        
        return $array;
    }
    
    // function taken from codeigniter. this might be a bit too sensitive: it fails on my folders, but don't have problems with writing 
    // to storae for example
    public static function is_really_writable($file)
	{
		// If we're on a Unix server with safe_mode off we call is_writable
		if (DIRECTORY_SEPARATOR == '/' AND @ini_get("safe_mode") == FALSE)
		{
			return is_writable($file);
		}

		// For windows servers and safe_mode "on" installations we'll actually
		// write a file then read it.  Bah...
		if (is_dir($file))
		{
			$file = rtrim($file, '/').'/'.md5(mt_rand(1,100).mt_rand(1,100));

			if (($fp = @fopen($file, FOPEN_WRITE_CREATE)) === FALSE)
			{
				return FALSE;
			}

			fclose($fp);
			@chmod($file, DIR_WRITE_MODE);
			@unlink($file);
			return TRUE;
		}
		elseif ( ! is_file($file) OR ($fp = @fopen($file, FOPEN_WRITE_CREATE)) === FALSE)
		{
			return FALSE;
		}

		fclose($fp);
		return TRUE;
	}


    public function getExtensions($key = false)
    {
        if(!$key) return $this->valid_all;

        return $this->valid[$key];
    }

    public function getAllExtensions()
    {
        return $this->valid_all;
    }

    public function getErrorByKey($key)
    {
        if(isset($this->error[$key])) return $this->error[$key];
    }


    public function formatBytes($bytes, $precision = 2) { 
        $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

        $bytes = max($bytes, 0); 
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
        $pow = min($pow, count($units) - 1); 

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . $units[$pow]; 
    }

    public function uploadFile(Symfony\Component\HttpFoundation\File\UploadedFile $file, $filename = false, $destination = false)
    {
        if(!$filename) $filename = str_random(25).'.'.$file->getClientOriginalExtension();
        if(!$destination) $destination = $this->uploadDirectory; //make sure the destination is set

        try
        {
            $file->move($destination, $filename);
            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

    public function uploadFile_byFunction($file, $filename = false, $destination = false)
    {
        $file = new Symfony\Component\HttpFoundation\File\File($file);

        try
        {
            $file->move($destination, $filename);
            return true;
        }
        catch(Exception $e)
        {
            // try {
            //     File::move($file, $destination . $filename);
            // } 
            // catch (Exception $e) 
            // {
                // dd($e);   
            // }
        }

        return false;
    }

    public function isImage($extension)
    {
        if(in_array($extension, $this->valid['images'])) return true;

        return;
    }

    public function getFileExtension($filename)
    {
        try
        {
            return File::extension(base_path() . $filename);
        }
        catch (Illuminate\Filesystem\FileNotFoundException $exception)
        {
            die("The file doesn't exist - " . $filename);
        }
    }

    public function create_image($filename, $save_path, $width = null, $height = null, $media)
    {
        $new__image = Image::make(base_path() . $save_path . $filename); //make image

        if((isset($width) AND $width > 0) AND (isset($height) AND $height > 0))
        {
            $new__image->resize($width, $height, function ($constraint)
            {
                $constraint->upsize();
            });
        }
        else
        {
            $new__image->resize($width, $height, function ($constraint)
            {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        }

        $filename = explode(".", $filename);
        $filename[0] = $filename[0] . "[" . $new__image->width() . "x" . $new__image->height() ."]";
        $filename = implode(".", $filename);

        $media->location = $save_path . $filename;

        if($media->save())
        {
            $new__image->save(base_path() . $save_path . $filename, 100);
        }
    }

    /**
     *
     * 
     *
     *
     */  
    public function deleteFiles( $files , $versions = array() )
    {
        $files = (is_array($files)) ? $files:array($files);
        $files = array_merge($files, $versions);


        File::delete($files);
    }
}

?>
