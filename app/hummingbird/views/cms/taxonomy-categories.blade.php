@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    @if (count($breadcrumbs) > 0)
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <?php $i = 0;?>
                    @foreach ($breadcrumbs as $breadcrumb)
                        <?php $i++;?>
                        <li>
                        @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                            <a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                        @else
                            @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                        @endif
                        </li>
                    @endforeach
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
    @endif
@stop

@section('content')

@if (Session::has('error'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                {{ Session::get('error') }}
            </div>
        </div>
    </div>  
@endif

<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <h1 class="pull-left normal">Categories</h1>

            <!-- Button trigger modal -->
            @if(isset($deleted_cats) AND $deleted_cats > 0)
                <a href="{{action('CategoriesController@showDeleted')}}" class="pull-right btn btn-danger"><i class="fa fa-archive"></i> Show Deleted ({{$deleted_cats}})</a>                
            @endif

            <button type="button" class="pull-right btn btn-default" data-toggle="modal" data-target="#add-cats"><i class="fa fa-plus"></i> Create Category</button>

            <div class="clearfix">&nbsp;</div>

            @if(count($cats) > 0)
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Category name</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        @foreach($list_cats as $cat)
                            <tr @if(null !== $cat->parent) class="child hide" data-parent="{{$cat->parent}}" @endif>
                                <td>{{$cat->name}} ({{$cat->used()}}) @if($cat->hasChildren()) <a href="#" class="expand" data-id="{{$cat->id}}"><i class="fa fa-plus font-size-10"></i></a> @endif</td>
                                <td>{{$cat->description}}</td>
                                <td>
                                    <a href='/{{App::make('backend_url')}}/categories/{{$cat->id}}' class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>

                                    {{ Form::open(array('route' => array('hummingbird.categories.destroy', $cat->id), 'method' => 'delete', 'class' => 'inline-block')) }}
                                        <button type="submit" data-id="{{$cat->id}}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>

                <div class="clearfix"></div>
            @else
                <h3>There are no categories. Please try adding some.</h3>
            @endif
        </div>
    </section>
</div>

<div class="modal fade" id="add-cats">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add new Category</h4>
            </div>

            <div class="modal-body">
                <section class="panel">
                    {{ Form::open(array('action' => array('CategoriesController@index'), 'method' => 'post')); }}
                        
                        <div class="form-group">
                            <label for="tags" class="control-label">Name:</label>
                            <input name="name" id="name" type="text" class="tax-name form-control" placeholder="Category name">
                        </div>

                        @if(count($list_cats) > 0)
                            <div class="form-group">
                                <label for="tags" class="control-label">Parent Category:</label>
                                <select class="form-control" id="parent" name="parent">
                                    <option value=""> -- </option>
                                    @foreach($list_cats as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        @else
                            <input type="hidden" name="parent" />
                        @endif


                        <fieldset>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </fieldset>
                    {{ Form::close() }}
                </section>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('scripts')
    <script>
        $(document).ready(function()
        {
            if($(".expand").length > 0)
            {
                $(".expand").click(function(e)
                {
                    e.preventDefault();

                    $(this).toggleClass("active");

                    if($(this).hasClass('active'))
                    {
                        $(this).find('.fa').removeClass('fa-plus').addClass('fa-minus');
                        $(this).closest('table').find(".child[data-parent='" + $(this).data('id') + "']").removeClass('hide');

                        var stop = false;
                        var hidden = false;
                        var tr = $(this).closest('tr');
                        var current_parent = $(this).find('a.expand').data('id');

                        while(!stop)
                        {
                            tr = tr.next(); //next tr
                            current_parent = tr.find('a.expand').data('id');

                            if(typeof tr.data('parent') === 'undefined')
                            {
                                stop = true;
                                continue;
                            }

                            if(tr.find('a.expand').hasClass('active') && !hidden)
                            {
                                $(this).closest('table').find(".child[data-parent='" + current_parent + "']").removeClass('hide');
                            }
                            else
                            {
                                stop = true;
                                continue;
                            }
                        }
                    }
                    else
                    {
                        var stop = false;
                        var tr = $(this).closest('tr');

                        $(this).find('.fa').removeClass('fa-minus').addClass('fa-plus');

                        while(!stop)
                        {
                            tr = tr.next(); //next tr

                            if(typeof tr.data('parent') === 'undefined')
                            {
                                stop = true;
                                continue;
                            }

                            tr.addClass('hide');
                        }
                    }
                });
            }
        });
    </script>
@stop
