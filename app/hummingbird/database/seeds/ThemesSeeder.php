<?php namespace Hummingbird\Themes\Seeds;

class ThemesSeeder extends \Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Eloquent::unguard();
        
        // ordering can be important here
        $this->call('ThemeInfoSeeder');
    }

}
