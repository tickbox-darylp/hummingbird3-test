<div class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal settings &raquo; <span class="span-modal-title">[modal-title]</span></h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="form-group">
                    <label for="content_type" class="col-sm-2 control-label">Type:</label>
                    <div class="col-sm-8">
                        <select name="content_type" id="content_type" class="form-control">
                            <option value="">---</option>
                            <option value="custom">Custom</option>
                        </select>
                        <span class="help-block">Select the type of content you want to add</span>
                    </div>
                </div>

                <div class="modal-data hide" data-content="custom">
                    <div class="form-group">
                        <label for="fields" class="col-sm-2 control-label">Position:</label>
                        <div class="col-sm-8">
                            <select name="fields" id="fields" class="form-control">
                                <option value="">---</option>
                            </select>
                            <span class="help-block">Select the space you want this data to appear in</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="modal_title" class="col-sm-2 control-label">Title:</label>
                        <div class="col-sm-10">
                            <input id="modal_title" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="modal_summary" class="col-sm-2 control-label">Summary:</label>
                        <div class="col-sm-10">
                            {{ Form::textarea('', '', array('id' => 'modal_summary', 'class' => 'form-control textareas')) }}
                        </div>
                    </div>
                    <div class="form-group media-library-holder">
                        <label for="modal_image" class="col-sm-2 control-label">Image:</label>
                        <div class="col-sm-10">
                            <input id="modal_image" type="text" class="form-control" value="http://croud.com/wp-content/uploads/2013/11/google-paint.jpg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->