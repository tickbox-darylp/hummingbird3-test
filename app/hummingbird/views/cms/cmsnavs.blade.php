@extends('HummingbirdBase::cms.layout')

@section('content')

<?php $even = false; ?>

<h1>All CMS Navigation items</h1>

<table class='results' cellpadding='5' cellspacing='0'>
    <thead>
        <th>Name</th>
        <th>Link</th>
        <th>Section name</th>
        <th>Main section name</th>
        <th>Live</th>
        <th>Actions</th>
    </thead>
    <tbody>
        @foreach($cmsnavs as $cmsnav)
        
        <tr <?php echo ($even) ? 'class="even"': '';$even = !$even;?>>
            <td>{{$cmsnav->name}}</td>
            <td>{{$cmsnav->link}}</td>
            <td>{{$cmsnav->sectionname}}</td>
            <td>{{$cmsnav->mainsectionname}}</td>
            <td>{{($cmsnav->live)?'Active':'Not live'}}</td>
            <td>
                <a href='/{{App::make('backend_url')}}/cmsnavs/edit/{{$cmsnav->id}}'>Edit</a> | 
                <a href='/{{App::make('backend_url')}}/cmsnavs/delete/{{$cmsnav->id}}'>Delete</a>
            </td>
        </tr>
        
        @endforeach
    </tbody>
</table>

<h1>Add new cmsnav</h1>
<?php echo Form::open(array('url' => App::make('backend_url').'/cmsnavs/add', 'method' => 'post')) ?>
    <table cellpadding="5" cellspacing="0" id="add-mini">
        <tbody>
            <tr>
                <td class="row_name">Name: <strong>*</strong></td>
                <td><input class="required input_box" id="name" type="text" name="name"></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="submit" value="Add CmsNav" name="add">
                </td>
            </tr>
        </tbody>
    </table>
<?php echo Form::close()?>

@stop
