<?php

class MenuController extends CmsbaseController {
    
    public $code_location = 'pages';
    
    public function getIndex()
    {
        $this->data['tag'] = 'Manage Menus';
        $this->data['menus'] = Menu::whereNull('deleted_at')->get();
        $this->data['menuitems'] = Menuitem::all();
        return View::make('HummingbirdBase::cms/menus', $this->data);
    }
    
    public function getEdit($id)
    {
        $this->data['menu'] = Menu::find($id);
        $this->data['menuitems'] = Menuitem::where('menu_id', '=', $id)->whereNull('deleted_at')->orderBy('order')->get();
        //dd($this->data['menuitems']);
        /*
		if(null !== $this->data['menu']->deleted_at)
        {
        	return View::make('HummbingbirdBase::cms/menus', $this->data);
		}
		*/
        $this->data['tag'] = 'Edit '.$this->data['menu']->title;
		/* $this->getCommonData(); */
        return View::make('HummingbirdBase::cms/menus-edit', $this->data);
    }
    
    public function getItem($id)
    {
    	$this->data['menuitems'] = Menuitem::find($id);
    	return View::make('HummingbirdBase::cms/menus-edit', $this->data);
    }
    
    public function postEdit($id)
    {
    	$input = Input::except('_token', 'add');
        $menus = Menu::find($id);

        $menus = $menus->fill($input);

        $menuitems = Input::get('menu_items');
        $menuitem_names_arr = Input::get('item_name');
        $menuitem_urls_arr = Input::get('item_url');

        if(count($menuitems) > 0)
        {
            foreach($menuitems as $item)
            {
                $new_name = $menuitem_names_arr[$item];
                $new_url = $menuitem_urls_arr[$item];

                $menuitem = Menuitem::find($item);
                $menuitem->menu_item_name = ($new_name != $menuitem->name AND $new_name != '') ? $new_name:$menuitem->menu_item_name;
                $menuitem->url = ($new_url != $menuitem->url AND $new_url != '') ? $new_url:$menuitem->url;
                $menuitem->save();
            }
        }
        
        if(Input::get('menu_item_name') != '' AND Input::get('url') != '')
        {
            $menuitem = new Menuitem;
            $menuitem = $menuitem->fill($input);
            $menuitem->menu_id = $id;
            $menuitem->save();
        }

        if (!$menus->save()) {
        	return Redirect::to(App::make('backend_url').'/menus/edit/'.$id)->withErrors($page->errors());
        }
        return Redirect::to(App::make('backend_url').'/menus/edit/'.$id);
        
    }
    
    public function getDelete($id)
    {
        $menus = Menu::find($id);
    	$menus->delete();
    	
    	//$menuitem = Menuitem::find($id);
    	//dd($menuitem);
    	//$menuitem->delete();
    	
    	
    	return Redirect::to(App::make('backend_url').'/menus')->with('success','Menu has been deleted');
    }
    
    public function deleteItem($id)
    {
    	$menuitem = Menuitem::find($id);
        $menu_id = $menuitem->menu_id;
    	$menuitem->delete();
    	
    	return Redirect::to(App::make('backend_url').'/menus/edit/'.$menu_id)->with('success','Menu item has been deleted');
    }
    
    public function getCommonData()
    {
        $this->data['menus'] = Menu::get_selection();
        $this->data['menuitems'] = Menuitem::get_selection();
    }
    
    public function postAdd()
    {
        $input = Input::except('_token', 'add');        
        $menu = (new Menu)->fill(Input::all());
        $menu['name'] = Input::get('row_name');
       
        //dd($input);
       // $menuitem = (new Menuitem)->fill($input);
       	
        if(!$menu->save()) {
            return Redirect::to(App::make('backend_url').'/menus')->withErrors($menu->errors());
        }
        //if(!$menuitem->save()) {
        //    return Redirect::to(App::make('backend_url').'/menus/edit/'.$id)->withErrors($menuitems->errors());
        //}
        //if($menuitem->save()) {
        //    return Redirect::to(App::make('backend_url').'/menus/edit/')->withErrors($menuitems->errors());
        //}
/*         return Redirect::to(App::make('backend_url').'/menus/edit/'.$menu->id); */
        return Redirect::to(App::make('backend_url').'/menus/');
    }

    // Show a menu by slug
    public function show($slug = 'home')
    {
        $redirect = $this->checkRedirects($slug);
        
        if($redirect) {
            return Redirect::to($redirect);
        }
        
        $menu = Menu::where('permalink', '=', $slug)->get()->first();
        
        if($menu) {
        
            // todo: get menu, and use that as view

            return View::make('menus.test')->with('menu', $menu);
        }
        
        App::abort(404);
    }

    
    public function checkRedirects($slug)
    {
        # see if redreict exists
        $redirect = Redirection::where('from', '=', $slug)->first();
        
        if(!$redirect) return false;
        
        # update count
        $redirect->no_used += 1;
        $redirect->last_used = date('Y-m-d H:i:s');
        $redirect->save();
        
        # redirect to url given
        return $redirect->to;
    }
}
