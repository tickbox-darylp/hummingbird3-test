@extends('HummingbirdBase::cms.layout')

@if(isset($breadcrumbs))
    @include('HummingbirdBase::cms.include.breadcrumbs', array('breadcrumbs' => $breadcrumbs))
@endif

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h1 class="normal">All Module Templates</h1>
        </div>

        <div class="col-md-12">
            <section class="panel" style="background-color:white;padding:20px;">
                @if(count($moduletemplates) > 0)
                    <div class="table">
                        <table class="table table-striped">
                            <thead>
                                <th scope="row">Name</th>
                                <th scope="row">Description</th>
                                <th>Actions</th>
                            </thead>

                            <tbody>
                                @foreach($moduletemplates as $moduletemplate)
                                    <tr>
                                        <td>{{$moduletemplate->name}}</td>
                                        <td>{{$moduletemplate->getNotes()}}
                                        <td>
                                            @if(Auth::user()->hasAccess('update', get_class($moduletemplate)))<a href='/{{App::make('backend_url')}}/module-templates/edit/{{$moduletemplate->id}}' class="btn btn-xs btn-info" title="Edit {{$moduletemplate->name}}"><i class="fa fa-edit"></i></a>@endif
                                            @if(Auth::user()->hasAccess('lock', get_class($moduletemplate)))<a href='/{{App::make('backend_url')}}/module-templates/lock/{{$moduletemplate->id}}' class="btn btn-xs btn-warning" title="Delete {{$moduletemplate->name}}"><i class="fa fa-unlock"></i></a>@endif
                                            @if(Auth::user()->hasAccess('delete', get_class($moduletemplate)) AND $moduletemplate->editable)<a href='/{{App::make('backend_url')}}/module-templates/delete/{{$moduletemplate->id}}' class="btn btn-xs btn-danger" title="Delete {{$moduletemplate->name}}"><i class="fa fa-trash"></i></a>@endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="text-center"> 
                        {{ $moduletemplates->appends(Request::except('page'))->links(); }}
                    </div>
                @else
                    <div class="alert alert-info text-center">
                        No templates have been added.
                    </div>
                @endif
            </section>
        </div>
    </div>

    @if(Auth::user()->hasAccess('create', get_class(new Moduletemplate)))
        <div class="row">
            <div class="col-md-12">
                <section class="panel" style="background-color:white;padding:20px;">
                    <h3 class="normal">Add new template</h3>

                    @foreach ($errors->all('<li>:message</li>') as $error)
                        {{$error}}    
                    @endforeach

                    {{ Form::open(array('url' => '/'.General::backend_url().'/module-templates/add/', 'class' => 'form-horizontal')) }}
                        <div class="form-group">
                            {{ Form::label('name', 'Name: ', array('class' => 'col-sm-3')) }}
                            <div class="col-sm-9">
                                {{ Form::text('name', '', array('class' => 'form-control')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-default btn-primary">Add Template</button>
                            </div>
                        </div> 
                    {{Form::close()}}
                </section>
            </div>
        </div>
    @endif
@stop
