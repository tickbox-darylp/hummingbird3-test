<?php

class PagesTableSeeder extends HbSeeder 
{

    /*
     * requires templates to have been created first
     */
    public function add_seed_data()
    {   
        $pages_to_install = Config::get('HummingbirdBase::hummingbird.installPages');

        if(count($pages_to_install) > 0)
        {
            foreach($pages_to_install as $page)
            {
                /* Create version */
                $new_page = new Page;
                $new_page->parentpage_id = NULL;
                $new_page->title = $page['Name'];
                $new_page->url = $page['URL'];
                $new_page->permalink = $page['URL'];
                $new_page->content = '';
                $new_page->status = 'public';
                $new_page->locked = 1;
                $new_page->save();

                /* Create a new version */
                $PageVersion = new PageVersion;
                $PageVersion->fill($new_page->toArray());
                $PageVersion->hash = time();
                $PageVersion->page_id = $new_page->id;
                $PageVersion->save();

                /* Store activity */
                Activitylog::log([
                    'action' => 'CREATED',
                    'type' => get_class($new_page),
                    'link_id' => $new_page->id,
                    'description' => 'Created page',
                    'notes' => "System created a new page - &quot;$new_page->title&quot;"
                ]);
            }
        }
    }

    public function register_blocks()
    {
        
    }

    public function register_modules()
    {
        
    }

    public function register_permissions()
    {
        $permissions = new Hummingbird\Libraries\PermissionManager;

        /* array of permissions required for user to access/use. */
        $actions_required = array('lock', 'publish', 'export');
        $actions_required = array_merge($permissions->get_standard_actions(), $actions_required);

        $permissions->install_actions($actions_required);

        /* Now install permissions for CMS area */
        $data = array(
            'type' => get_class(new Page),
            'label' => 'Pages',
            'group' => 'Content',
            'description' => 'Manage page content with CRUD permissions',
            'live' => 1,
            'perms' => $actions_required
        );

        $permissions->install_permissions($data);
    }

    public function register_widgets()
    {
        
    }

    public function add_cmsnav_items()
    {
        $cmsnav = new CmsNav;
        $cmsnav->name = 'Pages';
        $cmsnav->link = 'pages';
        $cmsnav->section = 'Content';
        $cmsnav->live = 1;
        $cmsnav->save();
        
    }

}
