<?php 
    // FRONTEND PAGES
    Route::any('{slug}', array('uses' => 'FrontendController@show'))->where('slug', '(.*)?');

    App::missing(function($exception)
    {
        // 404
        return App::make('CmsbaseController')->error($exception);
        // Route::any(General::backend_url().'/forbidden', 'CmsbaseController@forbidden');
        // return View::make('HummingbirdBase::cms.errors.404', array('exception' => $exception)); 
    });

?>