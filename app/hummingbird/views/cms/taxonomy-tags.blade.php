@extends('HummingbirdBase::cms.layout')

@section('styles')
    <style>
        .btn-group-wrap {
            text-align: center;
        }

        div.btn-toolbar {
            margin: 0 auto; 
            text-align: center;
            width: inherit;
            display: inline-block;
        }

        .btn-hummingbird:hover, .btn-hummingbird:focus, .btn-hummingbird:active, .btn-hummingbird.active, .open > .dropdown-toggle.btn-hummingbird
        {
            background-color: #00AEEF;
            color: white;
            border-color: #00AEEF;
        }

        #taxonomies .tax 
        {
            margin-bottom:15px;
        }

        #taxonomies .tax h6 
        {
            padding:10px;
            text-align:center;
            background-color: #00AEEF;
            color:white;
            -webkit-box-shadow: 0px 0px 2px -1px #000000;
            -moz-box-shadow: 0px 0px 2px -1px #000000;
            box-shadow: 0px 0px 2px -1px #000000;
        }

        .taxonomy {line-height: 200%;}

        .taxonomy .remove-taxonomy {
            font-size: 1.2rem;
            display: inline-block;
            line-height: 1;
            margin-left: 0;
            color: white;
            width: 24px;
            height: 24px;
            line-height: 20px;
            -webkit-border-radius: 12px;
            -webkit-background-clip: padding-box;
            -moz-border-radius: 12px;
            -moz-background-clip: padding;
            border-radius: 12px;
            background-clip: padding-box;
            text-align: center;
            -moz-box-shadow: 0 2px 5px rgba(0,0,0,.2);
            -webkit-box-shadow: 0 2px 5px rgba(0,0,0,.2);
            box-shadow: 0 2px 5px rgba(0,0,0,.2);
            right:0px;
            top:0px;
        }
    </style>
@stop

@section('breadcrumbs')
    @if (count($breadcrumbs) > 0)
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <?php $i = 0;?>
                    @foreach ($breadcrumbs as $breadcrumb)
                        <?php $i++;?>
                        <li>
                        @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                            <a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                        @else
                            @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                        @endif
                        </li>
                    @endforeach
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
    @endif
@stop

@section('content')


<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <h1 class="normal pull-left">Tags</h1>

            <!-- Button trigger modal -->
            @if(isset($deleted_tags) AND $deleted_tags > 0)
                <a href="{{action('TagsController@showDeleted')}}" class="pull-right btn btn-danger"><i class="fa fa-archive"></i> Show Deleted ({{$deleted_tags}})</a>                
            @endif

            <button type="button" class="pull-right btn btn-default" data-toggle="modal" data-target="#add-tags"><i class="fa fa-plus"></i> Create Tags</button>

            <div class="clearfix">&nbsp;</div>

            @if(count($alphabetical) > 0)
                <div class="btn-group-wrap">
                    <div class="btn-toolbar">
                        <div class="btn-group btn-group-sm">
                            @foreach($alphabetical as $item_key => $items)
                                <?php $target_key = ($item_key == '0-9') ? 'numeric':$item_key;?>
                                <button class="btn btn-default btn-hummingbird target" data-target="{{$target_key}}">{{$item_key}}</button>
                            @endforeach 
                        </div>
                    </div>
                </div>

                <div id="taxonomies">
                    @foreach($alphabetical as $item_key => $items)
                        <?php $target_key = ($item_key == '0-9') ? 'numeric':$item_key;?>

                        <div class="col-sm-2 tax" id="{{$target_key}}">
                            <h6>{{$item_key}}</h6>

                            @foreach($items as $item)
                                <div class="taxonomy relative">
                                    {{ Form::open(array('route' => array('hummingbird.tags.destroy', $item->id), 'method' => 'delete')) }}
                                        <button type="submit" data-id="{{$item->id}}" class="hide absolute remove-taxonomy btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                    {{ Form::close() }}

                                    <a href="/hummingbird/tags/{{$item->id}}">{{$item->name}} ({{$item->used()}})</a>
                                </div>
                            @endforeach
                        </div>
                    @endforeach 
                    <div class="clearfix">&nbsp;</div>
                </div>
            @else
                <h3>There are no tags. Please try adding some.</h3>
            @endif
        </div>
    </section>
</div>

<div class="modal fade" id="add-tags">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add new Tag</h4>
            </div>

            <div class="modal-body">
                <section class="panel">
                    <header class="panel-heading tab-bg-dark-navy-blue ">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#single">Add new tags</a>
                            </li>
                            <li class="hide">
                                <a data-toggle="tab" href="#import">Mass import</a>
                            </li>
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="single" class="tab-pane active">
                                {{ Form::open(array('action' => array('TagsController@index'), 'method' => 'post', 'class' => 'form-horizontal')); }}
                                    <fieldset>
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label for="tags" class="control-label">Tags:</label>
                                            {{ Form::textarea('tags', '', array('class' => 'form-control textareas', 'size' => '2x2', 'id' => 'tags')) }}
                                            <p class="help-block">To add more than one tag, simply add a comma delimited list.<br />
                                            <strong>For example: </strong> <i class="italic">New tags, tag 1</i></p>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <div class="form-group pull-right">
                                            <button type="submit" class="btn btn-primary">Insert</button>
                                        </div>
                                    </fieldset>
                                <?php echo Form::close()?>
                            </div>
                            <div id="import" class=" hide tab-pane">
                                <h3>Import multiple events</h3>

                                <p>If you want to import multiple events, please download <a href="/{{General::backend_url().'/events/skeleton'}}"> this skeleton</a> file and upload as a CSV.</p>

                                <?php echo Form::open(array('url' => General::backend_url().'/events/import', 'method' => 'post', 'files' => true)) ?>
                                    <fieldset>
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Select file:</label>
                                            <input type="file" class="form-control" name="file" id="file">
                                            <p class="help-block">File uploaded must be in <strong><i>.csv</i></strong> format</p>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <div class="form-group pull-right">
                                            <button type="submit" class="btn btn-primary">Import</button>
                                        </div>
                                    </fieldset>
                                <?php echo Form::close()?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('scripts')
    <script type='text/javascript' src='/themes/hummingbird/default/lib/masonry/masonry.js'></script>

    <script>
        var masonry_cont;

        function initMasonry()
        {
            var $masonry_cont = $('#taxonomies');
            // initialize
            $masonry_cont.masonry(
            {
                columnWidth: '#taxonomies .tax',
                itemSelector: '#taxonomies .tax'
            });
        }

        $(document).ready(function()
        {
            if($(".tax .taxonomy").length > 0)
            {
                $(document).on({
                    mouseenter: function ()
                    {
                        $(this).find('.remove-taxonomy').removeClass('hide');
                    },
                    mouseleave: function () 
                    {
                        $(this).find('.remove-taxonomy').addClass('hide');
                    }
                }, ".tax .taxonomy");
            }

            if($('.target').length > 0)
            {
                $(".target").click(function(e)
                {
                    e.preventDefault();

                    /* reset to normal */
                    $(".target").removeClass('active');
                    $("#taxonomies .tax.active").removeClass('col-sm-12, active').addClass('col-sm-2');
                    $("#taxonomies .tax").removeClass('hide');

                    /* Now add new styles */
                    $(this).addClass("active");
                    $("#taxonomies .tax:not(#" + $(this).data('target')).addClass("hide");
                    $("#taxonomies #" + $(this).data('target')).removeClass('col-sm-2').addClass("col-sm-12 active").find('.taxonomy').addClass("col-sm-2");

                    /* Re-init masonry */
                    initMasonry();
                });

                $(".target-new").click(function(e)
                {
                    e.preventDefault();

                    $('html, body').animate({
                        scrollTop: $("#" + $(this).data('target')).offset().top
                    }, 1000);
                });
            }
        });
    </script>
@stop
