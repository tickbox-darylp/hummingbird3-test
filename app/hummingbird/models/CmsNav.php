<?php

class CmsNav extends LaravelBook\Ardent\Ardent {

    public $table = 'cmsnavs';
    public $cms_url = 'cms-navigation';
    protected $fillable = array('name');
    
    protected $guarded = array();
    
    public static function get_selection()
    {
        $selection = array();
        
        $cmsnavs = CmsNav::all();
        
        foreach($cmsnavs as $cmsnav) {
            $selection[$cmsnav->id] = $cmsnav->title;
        }
        
        return $selection;
    }
    
}
