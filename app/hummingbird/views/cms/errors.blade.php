@extends('HummingbirdBase::cms.layout')

@section('content')

<div class="row">
    <div class="col-md-10">
        <h1>Website Errors</h1>
    </div>

    @if(count($errors) > 0)
        <div class="col-md-2">
            {{ Form::open(array('route' => array(App::make('backend_url').'.errors.destroy', 'remove-all'), 'method' => 'delete')) }}
                <button type="submit" class="btn btn-danger pull-right"><i class="fa fa-trash"></i>&nbsp; Remove all</button>
            {{ Form::close() }}
        </div>
    @endif
</div>

@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<div class="clearfix">&nbsp;</div> 

@if(count($errors) > 0)
    
<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <div class="table">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th># Visited</th>
                            <th>URL</th>
                            <th>Type</th>
                            <th>Last activated</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($errors as $error)
                        
                        <tr>
                            <td>{{$error->accessed}}</td>
                            <td><div class="tooltip-table">{{$error->url}}</div></td>
                            <td>{{$error->type}}</td>      
                            <td>{{$error->updated_at}}</td>
                            <td>
                                {{ Form::open(array('route' => array(App::make('backend_url').'.errors.destroy', $error->id), 'method' => 'delete')) }}
                                    <button type="submit" class="btn btn-xs btn-danger" data-original-title="Delete {{$error->url}} "><i class="fa fa-trash"></i></a>
                                {{ Form::close() }}
                            </td>
                        </tr>
                        
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="text-center"> 
                {{ $errors->links(); }}
            </div>
        </section>
    </div>
</div>

<style>
    .table {
      table-layout:fixed;
    }

    .table td {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    .tooltip-table {width:100%;word-wrap:break-word;}
</style>

@else
    <div class="alert alert-success">No errors found.</div>
@endif

@stop
