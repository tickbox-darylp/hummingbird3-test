<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Modal settings &raquo; <span class="span-modal-title">[modal-title]</span></h4>
        </div>
        <div class="modal-body form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Display?</label>
                <div class="col-sm-8">
                    <label class="checkbox-inline">
                        <input type="checkbox" id="display_by" class="display_by" name="display_by" value="list" @if(isset($slide_data->appearance->$posted['region'])) checked="checked" @endif> List events
                    </label>
                    <span class="help-block">Select how you want events to display</span>
                </div>
            </div>

            <div class="form-group display_by_list" @if(!isset($slide_data->appearance->$posted['region'])) style="display:none;" @endif>
                <label for="display_limit" class="col-sm-2 control-label"># of events:</label>
                <div class="col-sm-8">
                    <input id="display_limit" type="number" min="0" max="20" class="form-control" @if(isset($slide_data->appearance->$posted['region']->limit)) value="{{$slide_data->appearance->$posted['region']->limit}}" @endif>
                    <span class="help-block">Leave blank for showing the default number of events (5)</span>
                </div>
            </div>

            <div class="form-group display_by_list" @if(!isset($slide_data->appearance->$posted['region'])) style="display:none;" @endif>
                <label for="display_title" class="col-sm-2 control-label">Title:</label>
                <div class="col-sm-8">
                    <input id="display_title" type="text" class="form-control" @if(isset($slide_data->appearance->$posted['region']->title)) value="{{$slide_data->appearance->$posted['region']->title}}" @endif>
                    <span class="help-block">Add a heading to your list to make it stand out</span>
                </div>
            </div>

            <div class="form-group display_by_list" @if(!isset($slide_data->appearance->$posted['region'])) style="display:none;" @endif>
                <label for="display_summary" class="col-sm-2 control-label">Summary:</label>
                <div class="col-sm-8">
                    {{ Form::textarea('display_summary', (isset($slide_data->appearance->$posted['region']->summary) ? $slide_data->appearance->$posted['region']->summary:''), array('id' => 'display_summary', 'class' => 'form-control textareas', 'rows' => '3')) }}
                    <span class="help-block">Add a brief summary describing the events you want to output</span>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary save-region-settings">Save changes</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->