@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />

    <style type="text/css">
        .img-edit-link .edit-image
        {
            top:0;
            right:0;
            padding:3px 5px;
            color:white;
            background-color:#2A313A;
            font-size:1rem;
            font-size:10px;
            transition: all 0.5s ease;
        }

        .img-edit-link .edit-image a {color:white;}
        .img-edit-link .edit-image a:hover {text-decoration: none;}
        .img-edit-link:hover img {
            opacity:0.5;
            transition: all 0.5s ease;
        }
    </style>
@stop

@section('breadcrumbs')
    @if (count($breadcrumbs) > 0)
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <?php $i = 0;?>
                    @foreach ($breadcrumbs as $breadcrumb)
                        <?php $i++;?>
                        <li>
                        @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                            <a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                        @else
                            @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                        @endif
                        </li>
                    @endforeach
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
    @endif
@stop

@section('content') 

<?php 
    $FileHelper = new FileHelper;
    $file_extension = $FileHelper->getFileExtension($media->location);
    $isImage = $FileHelper->isImage($file_extension);

    if($isImage)
    {
        $image = Image::make(base_path() . $media->location); //make image
    }
?>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8">
                <section class="panel" style="background-color:white;padding:20px;">
                    {{ Form::open(array('action' => array('MediaController@postEditMediaItem', $media->id), 'method' => 'post', 'class' => 'form-horizontal')); }}
                        <input id="action" type="hidden" name="action" value="update" />

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Title</label>
                            <div class="col-sm-10">
                                <input name="title" id="title" type="text" class="form-control" placeholder="Media Title" value="{{$media->title}}">
                            </div>
                        </div>

                        @if(count($collections) > 0)
                            <div class="form-group">
                                <label for="collection" class="col-sm-2 control-label">Collection</label>
                                <div class="col-sm-10">
                                    <select name="collection" id="collection" class="form-control">
                                        <option value="">-- Root --</option>
                                        <option disabled></option>
                                        @foreach($collections as $collection)
                                            <option value="{{$collection->id}}" @if($collection->id === $media->collection) selected @endif>{{$collection->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @else
                            <input type="hidden" name="collection" />
                        @endif

                        <div class="form-group">
                            <label for="caption" class="col-sm-2 control-label">Location</label>
                            <div class="col-sm-10">
                                <?php $link = explode("/", $media->location);?>

                                <span class="help-block"><a href="{{$media->location}}" title="View: {{$media->filename}}" target="_blank" download="{{$link[0]}}"><i class="glyphicon glyphicon-new-window btn-xs"></i> {{$media->location}}</a></span>
                                <span class="help-block">The resource will open in a new window</span>
                            </div>
                        </div>

                        @if($is_image)
                            <div class="form-group">
                                <label for="alt" class="col-sm-2 control-label">Alt Text:</label>
                                <div class="col-sm-10">
                                    <input name="alt" type="text" class="form-control" id="alt" placeholder="Caption" value="{{$media->alt}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="caption" class="col-sm-2 control-label">Caption</label>
                                <div class="col-sm-10">
                                    <input name="caption" type="text" class="form-control" id="caption" placeholder="Caption" value="{{$media->caption}}">
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                <textarea style="height:auto;" class="form-control textareas" rows="5" name="description" id="description" placeholder="Enter description...">{{$media->description}}</textarea>
                            </div>
                        </div>

                        @include('HummingbirdBase::cms.taxonomy-layout', array('taxonomy' => $taxonomy))
                        
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <a href="#" class="update btn btn-default btn-primary" style="margin-left:5px;" data-action="update">Update</a>
                                
                                @if($is_image)
                                    <a href="#" class="update btn btn-default btn-primary" style="margin-left:5px;" data-action="update-all"><i class="fa fa-save"></i> Save + Apply changes to all versions</a>
                                @endif
                            </div>
                        </div>
                    {{ Form::close() }}
                </section>
            </div>

            <div class="col-md-4">
                <section class="panel" style="background-color:white;padding:20px;">
                    @if($isImage)
                        <div class="img-edit-link relative">
                            <a href="/hummingbird/media/edit-image/{{$media->id}}/">
                                <img class="img-responsive" src="{{$media->location}}" alt="{{strip_tags($media->media_description)}} " title="{{$media->filename}}" />
                                <span class="hide edit-image absolute">Edit Image</span>
                            </a>
                        </div>
                    @else
                        <div class="text-center"><i class="fa fa-file-{{Str::slug($file_extension)}} fa-5x text-center"></i></div>
                    @endif

                    <div class="clearfix">&nbsp;</div> 
                    @if($is_image)
                        @if($media->parent !== NULL)
                            <p><span><strong>Original:</strong> <a href="/{{General::backend_url()}}/media/view-media/{{$media->parent}}"><i class="glyphicon glyphicon-new-window btn-xs"></i> Open original </a></span></p>
                        @else
                            @if($media->children > 0)
                                <p><span><strong>Versions:</strong> <a href="#" class="versions"><i class="fa fa-edit"></i> {{$media->children}}</a></span></p>
                            @endif
                        @endif
                        <p><span><strong>Dimensions:</strong> {{$image->width()}} x {{$image->height()}}</span></p>
                    @endif
                    <p><span><strong>Date Uploaded:</strong> {{date("d-m-Y", strtotime($media->created_at))}}</span></p>
                    <p><span><strong>Last Updated:</strong> {{ (strtotime($media->created_at) == strtotime($media->updated_at)) ? 'n/a':$media->updated_at->diffForHumans() }}</span></p>
                    <p><span><strong>File Size:</strong> {{$FileHelper->formatBytes(File::size(base_path().$media->location))}}</span></p>
                    <p><span><strong>File Type:</strong> {{strtoupper($file_extension)}}</span></p>
                </section>
            </div>
        </div>

        @if($isImage AND $media->children > 0)
            <div class="row versions-holder">
                <div class="col-md-12">
                    <section class="panel" style="background-color:white;padding:20px;">
                        <h4>Versions</h4>

                        <div class="row">
                            <?php $all_media = Media::where('parent', '=', $media->id)->whereNull('deleted_at')->get(); ?>

                            @if(count($all_media) > 0)
                                <div id="media-lib" class="gallery">
                                    @foreach($all_media as $listed_media)
                                        <div class="col-sm-6 col-md-2 thumb">
                                            <div class="images link-click">
                                                <a href="/{{General::backend_url()}}/media/view-media/{{$listed_media->id}}"><img src="{{$listed_media->location}}" alt="{{$listed_media->filename}}" /></a>
                                            </div>

                                            <div class="overlay-holder relative">
                                                <div class="absolute" style="width:100%;padding: 7px 15px;height: 30px;background:black;color:white;bottom: 0px;font-size: 12px;text-align: center;"> 
                                                    <?php 
                                                        $_child = Image::make(base_path() . $listed_media->location);
                                                    ?>
                                                    {{$_child->width()}}x{{$_child->height()}}
                                                </div>
                                            </div>

                                            <div class="options"> 
                                                <span class="option view btn-default"><a href="/{{General::backend_url()}}/media/view-media/{{$listed_media->id}}"><i class="fa fa-pencil"></i></a></span>
                                                
                                                <span class="option edit btn-info">
                                                    <a href="/{{General::backend_url()}}/media/edit-image/{{$listed_media->id}}"><i class="fa fa-crop"></i></a>
                                                </span>
                                                
                                                @if(!$listed_media->locked)
                                                    <span class="option delete btn-danger"><a href="/{{General::backend_url()}}/media/delete-item/{{$listed_media->id}}" class="media-delete" data-id="{{$media->id}}"><i class="fa fa-trash"></i></a></span>
                                                @endif
                                            </div>
                                        </div>   
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </section>
                </div>
            </div>
        @endif
    </div>
</div>

    <style>
        .overlay-holder {display:none;}
        .gallery .thumb {
            margin-top:15px;
            margin-bottom:15px;
        }

        .gallery .thumb .images .filename {word-wrap:break-word;font-size:1.3rem;font-size:13px;}

        .gallery .thumb .images img 
        {
            position: relative;
            display:block;
            max-width:100%;
            height:auto;
            width:100%;
            transition: all 0.5s ease;
            opacity:1.0;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .gallery .thumb .options
        {
            position:absolute;
            top:-6px;
            right:6px;
            display:none;
        }

        .gallery .thumb .options .option
        {
            font-size:1.2rem;
            display: inline-block;
            line-height: 1;
            margin-left: 2px;
            color: white;
            width: 24px;
            height: 24px;
            line-height: 24px;
            -webkit-border-radius: 12px;
            -webkit-background-clip: padding-box;
            -moz-border-radius: 12px;
            -moz-background-clip: padding;
            border-radius: 12px;
            background-clip: padding-box;
            text-align: center;
            -moz-box-shadow: 0 2px 5px rgba(0,0,0,.2);
            -webkit-box-shadow: 0 2px 5px rgba(0,0,0,.2);
            box-shadow: 0 2px 5px rgba(0,0,0,.2);
        }

        .gallery .thumb .options .option a {
            color:white;
            display:block;
        }

        .gallery .thumb .options .option.btn-default a {color:white;}
        .gallery .thumb .options .option.btn-default {background-color: #2A313A}
        .gallery .thumb .options .option.btn-default:hover {background-color:black;} 

        .link-click {display:block;}
        .link-click:hover {text-decoration: underline;cursor:pointer;}
    </style>


@stop

@section('scripts')
    <script type='text/javascript' src='/themes/hummingbird/default/lib/dropzone/js/dropzone.js'></script>
    <script type='text/javascript' src='/themes/hummingbird/default/lib/masonry/masonry.js'></script>
    <script src="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>

    <script>
        var masonry_cont;

        function initMasonry()
        {
            var $masonry_cont = $('#media-lib');
            // initialize
            $masonry_cont.masonry(
            {
                columnWidth: '#media-lib .thumb',
                itemSelector: '#media-lib .thumb'
            });
        }

        $(document).ready(function()
        {
            if($('#media-lib').length == 1)
            {
                $("#media-lib .thumb").hover(function() { // Mouse over
                    $(this).find('.options, .overlay-holder').fadeIn();
                    $(this).siblings().stop().fadeTo(300, 0.5);
                    $(this).parent().siblings().stop().fadeTo(300, 0.3); 
                }, function() { // Mouse out
                    $(this).find('.options, .overlay-holder').fadeOut();
                    $(this).siblings().stop().fadeTo(300, 1);
                    $(this).parent().siblings().stop().fadeTo(300, 1);
                });

                initMasonry();
            }

            if($('.form-control.textareas').length > 0)
            {
                $(".form-control.textareas").autogrow();
            }

            if($(".img-edit-link").length > 0)
            {
                $(".img-edit-link").hover(function()
                {
                    $(".img-edit-link").find('.edit-image').toggleClass('hide');
                }, function() 
                {  
                    $(".img-edit-link .edit-image").toggleClass('hide');
                });
            }

            if($(".versions").length > 0)
            {
                $(".versions").click(function(e)
                {
                    e.preventDefault();

                    $('html, body').animate({
                        scrollTop: $(".versions-holder").offset().top
                    }, 1000);
                });
            }

            if($(".update").length > 0)
            {
                $(".update").click(function(e)
                {
                    e.preventDefault();

                    $("#action").val($(this).data('action'));

                    $(this).closest('form').submit();
                });
            }
        });
    </script>   
@stop