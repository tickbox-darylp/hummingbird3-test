<?php namespace Hummingbird\Pages\Seeds;

class PagesSeeder extends \Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Eloquent::unguard();
        
        // ordering is important here
        $this->call('ModuletemplateTableSeeder');
        $this->call('ModuleTableSeeder');
        $this->call('TemplateareaTableSeeder');
        $this->call('TemplateTableSeeder');
        $this->call('PagesTableSeeder');
        $this->call('PagemoduleTableSeeder');
        $this->call('MenusTableSeeder');
    }

}
