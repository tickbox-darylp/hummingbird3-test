<?php

namespace Hummingbird\Traits;

trait Activity
{
    /**
     *
     *
     * @return 
     */
    protected static function boot()
    {
        parent::boot();

        foreach(static::getModelEvents() as $event)
        {
            static::$event(function($model) use ($event)
            {
                $model->addActivity($event);
            });
        }
    }

    /**
     *
     *
     * @return 
     */
    protected function addActivity($event)
    {
        Activitylog::create([
            'user_id' => (is_numeric(Auth::user()->id)) ? Auth::user()->id:NULL,
            'type' => get_class($this),
            'action' => $event,
            'link_id' => $this->id
        ]);
    }

    /**
     *
     *
     * @return 
     */
    protected function getModelEvents()
    {
        if(isset(static::$recordableEvents))
        {
            return static::$recordableEvents;
        }

        return [
            'created', 
            'updated', 
            'deleted'
        ];
    }
}