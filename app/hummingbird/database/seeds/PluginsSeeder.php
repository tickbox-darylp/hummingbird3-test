<?php namespace Hummingbird\Plugins\Seeds;

class PluginsSeeder extends \Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Eloquent::unguard();
        
        // ordering can be importnat here
        $this->call('PluginsTableSeeder');
    }

}
