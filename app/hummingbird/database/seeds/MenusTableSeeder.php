<?php

class MenusTableSeeder extends HbSeeder
{


    public function add_seed_data()
    {
        
    }

    public function register_blocks()
    {
        
    }

    public function register_modules()
    {
        
    }

    public function register_permissions()
    {
        $permissions = new Hummingbird\Libraries\PermissionManager;

        /* array of permissions required for user to access/use. */
        $actions_required = array('lock');
        $actions_required = array_merge($permissions->get_standard_actions(), $actions_required);

        $permissions->install_actions($actions_required);

        /* Now install permissions for CMS area */
        $data = array(
            'type' => get_class(new Menu),
            'label' => 'Navigation Manager',
            'group' => 'Content Management',
            'description' => 'Manage navigation menus',
            'live' => 1,
            'perms' => $actions_required
        );

        $permissions->install_permissions($data);
    }

    public function register_widgets()
    {
        
    }

    public function add_cmsnav_items()
    {
        $cmsnav = new CmsNav;
        $cmsnav->name = 'Menus';
        $cmsnav->link = 'menus';
        $cmsnav->section = 'Content';
        $cmsnav->live = 1;
        $cmsnav->save();
        
    }

}
