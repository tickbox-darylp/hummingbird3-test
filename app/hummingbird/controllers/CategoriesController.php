<?php

class CategoriesController extends CmsbaseController
{
	protected $list_cats = array();

    public function __construct()
    {
        parent::__construct();

		/* Build breadcrumb */
		$this->breadcrumbs = array(
			0 => array(
	            'classes' => '',
	            'icon' => '',
	            'title' => 'Categories',
	            'url' => ''
			)
		);

        $this->data['list_cats'] = array();
		$this->data['cats'] = Categories::whereNull('deleted_at')->type()->orderBy('name', 'ASC')->orderby('id', 'ASC')->get();

		$this->data['deleted_cats'] = Categories::deletedCategories(true);

        $this->buildTree($this->data['cats']);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if($this->user->hasAccess('read', get_class(new Categories)))
		{
			$this->renderBreadcrumb();

	        if(null !== Input::get('s') OR Input::get('s') != '')
	        {
		        $this->data['cats'] = Categories::whereNull('deleted_at')->type()->search(Input::get('s'))->orderBy('name', 'ASC')->orderby('id', 'ASC')->get();	
	        }

	        return View::make('HummingbirdBase::cms/taxonomy-categories', $this->data);
        }
        else
        {
        	return $this->forbidden(true);
        }
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex_call()
	{
		return $this->data['list_cats'];
	}


    public function buildTree($elements, $parentId = NULL, $level = 0) 
    {
        foreach ($elements as $key => $element)
        {
        	/* Top level parent */
            if ($element->parent !== $parentId) continue;

            // $element['name'] = ($level > 0) ? '&#8627;'.$element['name']:$element['name']; //append arrow to name
            $new_name = ($level > 0) ? str_repeat('&nbsp;', ($level * 3) * 2) . '<i class="fa fa-long-arrow-right"></i> ' . $element['name']: $element['name'];
            $element['name'] = $new_name;

            // $element['name'] = ($level > 0) ? '&nbsp;'.$element['name']:$element['name']; //append arrow to name
            $this->data['list_cats'][$element->id] = $element;
            $this->data['list_cats'][$element->id]['level'] = $level;
            $this->buildTree($elements, $element->id, $level + 1);
        }
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		dd("create");
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$messages = array();

		$input = Input::except('_token');

		if($input['name'] != '')
		{
			$category = new Categories;

			if($category->exists($input['name']))
			{
		        if(!isset($input['ajax']))
		        {
					return Redirect::to(App::make('backend_url').'/categories/')->with('error', $input['name'] . " could not be created as it already exists.");
				}
				else
				{
					//returning ajax
		        	$message = (object) array('state' => false, 'message' => $input['name'] . " could not be created as it already exists.");
		        	return Response::json($message, 200);
				}
			}

			$category->fill($input);
			$category->slug = $category->name;
			$category->parent = (!isset($input['ajax'])) ? $input['parent']:null;
			$category->type = $category->getType();
			
			if($category->save())
			{
	        	/* record activity */
		        Activitylog::log([
		            'action' => 'UPDATED',
		            'type' => get_class($category),
		            'link_id' => $category->id,
		            'description' => 'Created a new category',
		            'notes' => Auth::user()->username . " created a new category - &quot;$category->name&quot;"
		        ]);

		        if(!isset($input['ajax']))
		        {
		        	return Redirect::to(App::make('backend_url').'/categories/')->with('success', "Category &quot;$category->name&quot; created");
				}
				else
				{
					//returning ajax
		        	$message = (object) array('state' => true, 'data' => $category, 'message' => "$category->name has been created");
		        	return Response::json($message, 200);
				}
			}
			else
			{
		        if(!isset($input['ajax']))
		        {
					return Redirect::to(App::make('backend_url').'/categories/')->with('error', 'There was a problem saving this category.');
				}
				else
				{
					//returning ajax
		        	$message = (object) array('state' => false, 'message' => "$category->name could not be created");
		        	return Response::json($message, 200);
				}
			}
		}
		else
		{
	        if(!isset($input['ajax']))
	        {
				return View::make('HummingbirdBase::cms/taxonomy-categories', $this->data)->with('error', 'Please enter a name for a category.');
			}
			else
			{
				//returning ajax
	        	$message = (object) array('state' => false, 'message' => "Please enter a name for a category");
	        	return Response::json($message, 200);
			}	
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if($this->user->hasAccess('update', get_class(new Categories)))
		{
			if(is_numeric($id) AND $id > 0)
			{
				$category = Categories::whereNull('deleted_at')->where('id', '=', $id)->first();

				if(null !== $category)
				{
					$this->data['category'] = $category;
					$this->data['children'] = Categories::whereNull('deleted_at')->ChildOf($category->id)->get();
					$this->data['references'] = DB::table('taxonomy_relationships')->where('term_id', '=', $id)->get();

					$this->breadcrumbs[0]['url'] = 'categories';

					/* Breadcrumb build for all children */
					$breadcrumbs = array();
					$parent = $this->data['category']->parent;

					while(null !== $parent)
					{
						$category = Categories::whereNull('deleted_at')->where('id', '=', $parent)->first();

						/* Build breadcrumb */
						array_unshift($breadcrumbs, array(
				            'classes' => '',
				            'icon' => '',
				            'title' => $category->name,
				            'url' => '/' . General::backend_url() . '/categories/' . $category->id
						));

						$parent = $category->parent;
					}

					if(count($breadcrumbs) > 0)
					{
						$this->breadcrumbs = array_merge($this->breadcrumbs, $breadcrumbs);
					}

					/* Build breadcrumb */
					$this->breadcrumbs[] = array(
			            'classes' => '',
			            'icon' => '',
			            'title' => $this->data['category']->name,
			            'url' => ''
					);

					$this->renderBreadcrumb();

					return View::make('HummingbirdBase::cms/taxonomy-categories-edit', $this->data);
				}
				else
				{
					return Redirect::to(App::make('backend_url').'/categories/')->with('errors', 'This category no longer exists.');
				}
			}

			return Redirect::to(App::make('backend_url').'/categories/')->with('errors', 'Please select a category to view.');
		}
        else
        {
        	if($this->user->hasAccess('read', get_class(new Categories)))
        	{
				return Redirect::to(App::make('backend_url').'/categories/')->with('errors', 'You do not have permission to delete this category.');
        	}

        	return $this->forbidden(true);
        }
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		dd("edit");
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(is_numeric($id) AND $id > 0)
		{
			$input = Input::except('_token');

			$cat = Categories::find($id);

			if(null !== $cat)
			{
				$cat->fill($input);

				if($cat->save())
				{
		        	/* record activity */
			        Activitylog::log([
			            'action' => 'UPDATED',
			            'type' => get_class($cat),
			            'link_id' => $cat->id,
			            'description' => 'Updated category',
			            'notes' => Auth::user()->username . " updated the cateogry - &quot;$cat->name&quot;"
			        ]);
					return Redirect::to(App::make('backend_url').'/categories/'.$cat->id);
				}
				else
				{
					return Redirect::to(App::make('backend_url').'/categories/')->with('errors', 'There was a problem updating this category.');
				}
			}
			else
			{
				return Redirect::to(App::make('backend_url').'/categories/')->with('errors', 'This category could not be found.');
			}
		}

		return Redirect::to(App::make('backend_url').'/categories/')->with('errors', 'There was a problem finding this category. Please try again.');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(is_numeric($id) AND $id > 0)
		{
			$cat = Categories::find($id);
			$cat_name = $cat->name;

			if(null !== $cat)
			{
				if($cat->delete())
				{
					// successfully deleted tag - add activity
		        	/* record activity */
			        Activitylog::log([
			            'action' => 'DELETED',
			            'type' => get_class(new Categories),
			            'link_id' => $cat->id,
			            'description' => "Category has been deleted",
			            'notes' => Auth::user()->username . " deleted the tag &quot;$cat_name&quot;"
			        ]);

			        return Redirect::to(App::make('backend_url').'/categories/')->with('success', 'Category has been deleted.');
				}
				else
				{
			        return Redirect::to(App::make('backend_url').'/categories/')->with('error', 'Category could not be deleted.');
				}
			}
			else
			{
				return Redirect::to(App::make('backend_url').'/categories/')->with('error', 'Category could not be found. Please try again.');
			}
		}

		return Redirect::to(App::make('backend_url').'/categories/')->with('error', 'Please select a tag to delete.');
	}

	/**
	 * Show the soft deleted items
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showDeleted()
	{
		$this->data['categories'] = Categories::deletedCategories();

		/* Build breadcrumb */
		$breadcrumbs = array(
			0 => array(
	            'classes' => '',
	            'icon' => '',
	            'title' => 'Categories',
	            'url' => '/' . General::backend_url() . '/categories/'
			),
			1 => array(
	            'classes' => '',
	            'icon' => '',
	            'title' => 'Trash',
	            'url' => ''
			)
		);

		foreach($breadcrumbs as $crumb)
		{
			$this->addToBreadcrumb($crumb);
		}

        return View::make('HummingbirdBase::cms/taxonomy-categories-deleted', $this->data);
	}

	/**
	 * Physically remove the item
	 * POST will send "all" OR "single"
	 *
	 * @param  int  $id (default = null)
	 * @return Response
	 */
	public function purge($id = null)
	{	
		if($id == 'all' OR (is_numeric($id) AND $id > 0))
		{
			$deleted = array();
			$delete_array = array(); //delete from taxonomy table and all references

			switch($id)
			{
				case 'all':
					$cats = Categories::whereNotNull('deleted_at')->get();

					if(count($cats) > 0)
					{
						foreach($cats as $cat)
						{
							$deleted[] = $cat;
							$delete_array[] = $cat->id;
						}
					}
					else
					{
						return Redirect::to(App::make('backend_url').'/categories/deleted/')->with('error', 'No categories could be removed. Please try again.');
					}
					break;
				default:
					$cat = Categories::whereNotNull('deleted_at')->where('id', '=', $id)->first();

					if(null !== $cat)
					{
						$deleted[] = $cat;
						$delete_array[] = $cat->id;
					}
					else
					{
						return Redirect::to(App::make('backend_url').'/categories/deleted/')->with('error', 'Category could not be removed. Please try again.');
					}
					break;
			}

			if(count($delete_array) > 0)
			{
				DB::table('taxonomy')->whereIn('id', $delete_array)->delete(); //remove taxonomy
				DB::table('taxonomy_relationships')->whereIn('term_id', $delete_array)->delete(); //remove relationships

				foreach($deleted as $cat)
				{
		        	/* record activity */
			        Activitylog::log([
			            'action' => 'PURGED',
			            'type' => get_class(new Categories),
			            'link_id' => $cat->id,
			            'description' => 'Permanently removed category',
			            'notes' => Auth::user()->username . " permanently deleted &quot;$cat->name&quot;"
			        ]);

			  		/* Update the children categories */
			  		Categories::where('parent', '=', $cat->id)->update(array('parent' => $cat->parent));
			        
			        /* Force remove the category */
			        $cat->forceDelete();
				}
			}
			else
			{
				return Redirect::to(App::make('backend_url').'/categories/deleted/')->with('error', 'No categories could be removed. Please try again.');
			}
		}
		else
		{
			return Redirect::to(App::make('backend_url').'/categories/deleted/')->with('error', 'Please select a valid category to permanently delete.');
		}

		return Redirect::to(App::make('backend_url').'/categories/deleted/');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function reinstate($id)
	{
		if($id == 'all' OR (is_numeric($id) AND $id > 0))
		{
			switch($id)
			{
				case 'all':
					$cats = Categories::whereNotNull('deleted_at')->get();

					if(count($cats) > 0)
					{
						foreach($cats as $cat)
						{
							$cat->deleted_at = NULL;
							$cat->save();

				        	/* record activity */
					        Activitylog::log([
					            'action' => 'UPDATED',
					            'type' => get_class(new Categories),
					            'link_id' => $cat->id,
					            'description' => 'Restored category',
					            'notes' => Auth::user()->username . " restored the category &quot;$cat->name&quot;"
					        ]);
						}
					}
					else
					{
						dd("NO TAGS FOUND");
					}
					break;
				default:
					$cat = Categories::whereNotNull('deleted_at')->where('id', '=', $id)->first();

					if(null !== $cat)
					{
						$cat->deleted_at = NULL;
						$cat->save();

			        	/* record activity */
				        Activitylog::log([
				            'action' => 'UPDATED',
				            'type' => get_class(new Categories),
				            'link_id' => $cat->id,
				            'description' => 'Restored category',
				            'notes' => Auth::user()->username . " restored the category &quot;$cat->name&quot;"
				        ]);
					}
					else
					{
						dd("NO CATEGORY $id FOUND");
					}
					break;
			}
		}
		else
		{
			dd("NO ID GIVEN");
		}

		return Redirect::to(App::make('backend_url').'/categories/deleted/');
	}





	public function import()
	{
		$import_files = array('download_categories', 'event_cats', 'gallerycategories', 'shop_cats');

		foreach($import_files as $import_file)
		{
	        DB::unprepared(file_get_contents(base_path() . '/import/sql/' . $import_file . '.sql'));

	        $data = DB::table($import_file)->get();

	        /* data to insert */
	        $insert_data = array();
		}
	}


	public function getChildren()
	{
		$data = array();

		if(null !== Input::get('category'))
		{
			$category = Categories::find(Input::get("category"));

			if(null !== $category)
			{
				$children = Categories::whereNull('deleted_at')->type()->childOf($category->id)->orderBy('name', 'ASC')->get();

				if(count($children) > 0)
				{
					$data['state'] = true;
					foreach($children as $cat)
					{
						$data['categories'][$cat->id] = $cat->name;
					} 
				}
				else
				{
					$data['state'] = false;
					$data['message'] = 'no category found';					
				}
			}
			else
			{
				$data['state'] = false;
				$data['message'] = 'no category found';
			}
		}
		else
		{
			$data['message'] = 'no input';
			$data['state'] = false;
		}

		return json_encode($data);
	}
}
