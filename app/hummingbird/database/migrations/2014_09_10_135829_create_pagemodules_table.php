<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagemodulesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('pagemodules', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id');
            $table->integer('module_id');
            $table->integer('position');
            $table->string('area');
            $table->tinyInteger('live');
            $table->softDeletes();
            $table->timestamps();
            
            $table->unique(array('page_id', 'module_id', 'area', 'position'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('pagemodules');
    }

}
