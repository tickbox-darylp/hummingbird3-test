<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

Route::filter('check.app.up', function()
{
    if (file_exists(base_path().'/.maintenance'))
    {
	    Log::info('Application currently in maintenance mode');

		/* Do we have a mainteance file for the theme */
	    $theme = Themes::activeTheme();
	    
	    View::addLocation(base_path()."/themes/public/$theme/");
	    View::addNamespace('theme', base_path() . '/themes/public/' . $theme . '/');
	    if(View::exists('maintenance')) return Response::make(View::make('maintenance'), 503);

	    /* Do we have a maintenance mode function enabled? */
	    if(class_exists('HtmlHelper')) return Response::make(HtmlHelper::maintenanceMode(), 503);
	    
	    /* Standard return */
		return Response::make("The site is currently under construction. Please check back later. ", 503);
    }
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{	
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest(App::make('backend_url') . '/login');
		}
	}
});


/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		return App::abort(403, 'Unauthorized action');
	}
});


/*
 * Permission filters using Entrust
 */

// USERS
// Route::filter('read.users', function()
// {
//     if (! Auth::user()->hasAccess('read', User::$permission_type) ) 
//     {
//         return Redirect::to(App::make('backend_url') . '/forbidden');
//     }
// });
// Route::filter('preview.users', function()
// {
//     if (! Auth::user()->hasAccess('preview', User::$permission_type) ) 
//     {
//         return Redirect::to(App::make('backend_url') . '/forbidden');
//     }
// });
// Route::filter('delete.users', function()
// {
//     if (! Auth::user()->hasAccess('delete', User::$permission_type) ) 
//     {
//         return Redirect::to(App::make('backend_url') . '/forbidden');
//     }
// });
// Route::filter('edit.users', function()
// {
//     if (! Auth::user()->hasAccess('edit', User::$permission_type) ) 
//     {
//         return Redirect::to(App::make('backend_url') . '/forbidden');
//     }
// });

// // ROLES
// Route::filter('read.roles', function()
// {
//     if (! Auth::user()->hasAccess('read', Role::$permission_type) ) 
//     {
//         return Redirect::to(App::make('backend_url') . '/forbidden');
//     }
// });
// Route::filter('preview.roles', function()
// {
//     if (! Auth::user()->hasAccess('preview', User::$permission_type) ) 
//     {
//         return Redirect::to(App::make('backend_url') . '/forbidden');
//     }
// });
// Route::filter('delete.roles', function()
// {
//     if (! Auth::user()->hasAccess('delete', User::$permission_type) ) 
//     {
//         return Redirect::to(App::make('backend_url') . '/forbidden');
//     }
// });
// Route::filter('edit.roles', function()
// {
//     if (! Auth::user()->hasAccess('edit', User::$permission_type) ) 
//     {
//         return Redirect::to(App::make('backend_url') . '/forbidden');
//     }
// });

/* 
 * Auth Filter 
 */
Route::filter('auth.check', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			// return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest(App::make('backend_url') . '/login');
		}
	}
	else
	{
		$user = Auth::user(); //get current user

		if(null !== $user->role)
		{
			if($_SERVER["REMOTE_ADDR"] == '123.123.123.123')
			{
				$default_permissions = array(
					'index' => 'read',
					'getIndex' => 'read',
					'postAdd' => 'create',
					'edit' => 'update',
					'getEdit' => 'update',
					'postEdit' => 'update',
					'postSave' => 'save',
					'preview' => 'preview',
					'delete' => 'delete',
					'getDelete' => 'delete'
				);

				$currentAction  = Route::currentRouteAction();	
				// dd($currentAction);

				if(null !== $currentAction)
				{
					$params = explode("@", $currentAction);
					// dd($params);

					$action = $default_permissions[$params[1]];	
					// dd($action);

					$controller = $params[0];
					// dd($controller);

					$model = ucfirst(strtolower(str_replace("Controller", "", $controller)));
					// dd($model);

					$model = new $model;
					if($model)
					{
						$type = $model;
						// dd($type);

					    if (! $user->hasAccess($action, get_class($model)) ) 
					    {
					        return Redirect::to(App::make('backend_url') . '/forbidden');
					    }
					}		
				}
			}
		}
		else
		{
			return Redirect::guest('/');
		}
	}
});


/*
 * Installation filters
 */
Route::filter('install.before', function(){
    if (!Installer::isInstalled()) {
        $current_step = Session::get('install_url') ?: Installer::uri();
        
        return Redirect::to($current_step);
    }
});

Route::filter('ready.before', function(){
    if (Installer::isInstalled()) {
        return Redirect::to('/');
    }
});