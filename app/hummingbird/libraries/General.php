<?php 

/**
 * General functions for Hummingbird3
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */
class General
{
    /**
     *
     * 
     *
     *
     */  
    public static function debugAllowed()
    {
        $ips = array('213.104.183.234', '127.0.0.1');

        return (in_array($_SERVER["REMOTE_ADDR"], $ips)) ? true:false;
    }

    /**
     *
     * 
     *
     *
     */  
    public static function total_execution_time($timestamp = NULL)
    {
        $timestamp = ($timestamp === NULL) ? time():$timestamp;

        return round((microtime(true) - $timestamp) / 60, 3);
    }

    /**
     *
     * Return a placeholder image
     *
     * @param    integer $width Width of placeholder image
     * @param    integer $height Hidth of placeholder image
     * @param    string $text Text to be displayed if needed
     * @return   URL
     *
     */  
    public static function parseHeaders( $headers )
    {
        $head = array();
        foreach( $headers as $k=>$v )
        {
            $t = explode( ':', $v, 2 );
            if( isset( $t[1] ) )
                $head[ trim($t[0]) ] = trim( $t[1] );
            else
            {
                $head[] = $v;
                if( preg_match( "#HTTP/[0-9\.]+\s+([0-9]+)#",$v, $out ) )
                    $head['reponse_code'] = intval($out[1]);
            }
        }
        return $head;
    }

    /**
     *
     * Return a placeholder image
     *
     * @param    integer $width Width of placeholder image
     * @param    integer $height Hidth of placeholder image
     * @param    string $text Text to be displayed if needed
     * @return   URL
     *
     */    
    public static function placeHoldItImage($width = 100, $height = 100, $text = '')
    {
        $url = "http://placehold.it/".$width."x".$height."&text=";

        if($text)
        {
            $url .= urlencode($text);
        }

        return $url;
    }

    public static function lorempixel($width = 100, $height = 100)
    {
        return "http://lorempixel.com/$width/$height/?x=".time();
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */  
    public static function gravatarImage($email, $ext = 'jpg')
    {
        $email = md5 (strtolower( trim( $email ) ) ); // "myemailaddress@example.com"
        return 'http://www.gravatar.com/avatar/' . $email . '.' .$ext;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function findImage($data, Array $fields_to_search, $fallback = null)
    {
        $inline_imgs = FALSE;

        if(null !== $data)
        {
            foreach($fields_to_search as $field)
            {
                $image = '';

                switch($field)
                {
                    case 'content':
                    case 'description':
                        $content = new \DOMDocument();
                        @$content->loadHTML($data->$field);

                        $images = $content->getElementsByTagName('img');

                        if(count($images) > 0)
                        {
                            $inline_imgs = TRUE;
                        }
                        break;
                    default:
                        $image = $data->$field;
                        break;
                }

                switch($inline_imgs)
                {
                    case TRUE:
                        foreach($images as $img)
                        {
                            $image = $img->getAttribute('src');

                            if(strpos($image, 'http') !== false)
                            {
                                if(function_exists('file_get_contents'))
                                {
                                    if(file_get_contents($image)) 
                                    {
                                        $response = General::parseHeaders($http_response_header);

                                        if($response['reponse_code'] == 200) return $image;
                                    }
                                }
                                else
                                {
                                    return $image;
                                }
                            }
                            else
                            {
                                // if(File::exists(base_path() . $image))
                                // {
                                    return 'http://alpha.hummingbirdcms.com/'.$image;
                                // } 
                            }
                        }
                        break;
                    case FALSE:
                        if($image != '')
                        {
                            // if(File::exists(base_path() . $image))
                            // {
                                return 'http://alpha.hummingbirdcms.com/'.$image;
                            // }
                        }
                        break;
                }
            }
        }

        return $fallback;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function username_regex()
    {
        return '/^[a-zA-Z0-9_\\-\\.]*$/';
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function pp($array, $die = false)
    {
        echo '<pre>'.print_r($array, true).'</pre>';

        if($die) die();
    }


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function plugins_path()
    {
        return base_path() . '/plugins';
    }  

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function update_path()
    {
        return base_path() . '/updates';
    }  


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function theme_path($public = TRUE)
    {
        if($public) return Config::get('HummingbirdBase::hummingbird.themesDir');

        return base_path() . '/themes';
    } 


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function frontend_url()
    {
        return Config::get('app.url');
    }    
    
    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function backend_url()
    {
        return Config::get('HummingbirdBase::hummingbird.backendURI') ?: 'cms';
    }

    
    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function get_website_name()
    {
        return Config::get('HummingbirdBase::hummingbird.site_name');
    }


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function get_backend_url($key)
    {
        switch($key)
        {
            case 'forbidden':
                return '/forbidden/';
                break;
        }
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function strpos_array($haystack, $needles, $offset = 0)
    {
        if(!is_array($needles)) $needles = array($needles);

        if(count($needles) > 0)
        {
            foreach($needles as $needle) 
            {
                if(strpos($haystack, $needle, $offset) !== false) return true; // stop on first true result
            }
        }

        return false;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function clean_unicode($string)
    {
        $to_clean = array(
            '%20' => ' ',
            '%26' => '&',
            '%2C' => ',',
            '&amp;' => '&',
            'Â' => ''
        );

        if($string == '') return $string;

        foreach($to_clean as $item_key => $item_value)
        {
            $string = str_replace($item_key, $item_value, $string);
        }

        return $string;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function wrap_text($to_wrap, $wrapping_with = FALSE)
    {
        if(!$wrapping_with) return $to_wrap;

        return $wrapping_with . $to_wrap . $wrapping_with;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function singular_or_plural($counter, $str, $offset = 's')
    {
        if(!$counter OR $counter == 1) return $str;

        return $str.$offset;
    }
}
