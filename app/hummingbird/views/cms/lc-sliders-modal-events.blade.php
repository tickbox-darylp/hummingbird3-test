<div class="modal-data @if(!isset($posted['content_type']) OR $posted['content_type'] != 'custom') hide @endif" data-content="custom">
    <div class="form-group">
        <label for="fields" class="col-sm-2 control-label">Position:</label>
        <div class="col-sm-8">
            <select name="fields" id="fields" class="form-control">
                <option value="">---</option>
            </select>
            <span class="help-block">Select the space you want this data to appear in</span>
        </div>
    </div>

    <div class="form-group">
        <label for="modal_title" class="col-sm-2 control-label">Select?</label>
        <div class="col-sm-10">
            <select name="event-filter-by" id="event-filter-by" class="form-control">
                <option value="">---</option>
                <option value="all">Show all</option>
                <option value="custom">Select events</option>
                <option value="filter">Filter by...</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="modal_title" class="col-sm-2 control-label">Filter by:</label>
        <div class="col-sm-10">
            <select name="event-filter-by" id="event-filter-by" class="form-control">
                <option value="">---</option>
                <option value="all">Categories</option>
                <option value="custom">Select events</option>
                <option value="filter">Filter by...</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="modal_title" class="col-sm-2 control-label">Order by:</label>
        <div class="col-sm-10">
            <select name="event-filter-by" id="event-filter-by" class="form-control">
                <option value="">---</option>
                <option value="random">Random</option>
                <optgroup label="ASC">
                    <option value="id_ASC">ID</option>
                    <option value="start_date_ASC">Start date</option>
                    <option value="finish_date_ASC">Finish date</option>
                </optgroup>
                <optgroup label="DESC">
                    <option value="id_DESC">ID</option>
                    <option value="start_date_DESC">Start date</option>
                    <option value="finish_date_DESC">Finish date</option>
                </optgroup>
            </select>
        </div>
    </div>
</div>