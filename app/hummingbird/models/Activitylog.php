<?php

class Activitylog extends Eloquent 
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'activitylogs';

    protected $guarded = array();
    public $search_field = 'action';
    public static $permission_type = 'logging';
    
    
    /**
     * Get the user that the activity belongs to.
     *
     * @return object
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Create an activity log entry.
     *
     * @param  mixed
     * @return boolean
     */
    public static function log($data = array())
    {
        if (is_object($data)) $data = (array) $data;
        if (is_string($data)) $data = array('action' => $data);

        $activity = new static;
        $activity->user_id      = isset(Auth::user()->id) ? Auth::user()->id : "SYSTEM"; //set the user
        $activity->ip_address   = Request::getClientIp();
        $activity->action       = $data['action'];
        $activity->type         = isset($data['type'])   ? $data['type']   : null;
        $activity->link_id      = isset($data['link_id']) ? $data['link_id'] : null;
        $activity->url          = isset($data['url'])      ? $data['url']      : null;
        $activity->description  = isset($data['description']) ? $data['description'] : null;
        $activity->notes        = isset($data['notes'])     ? $data['notes']     : null;
        $activity->user_agent   = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'No UserAgent';
        $activity->save();

        return true;
    }

    /**
     * Get a shortened version of the user agent with title text of the full user agent.
     *
     * @return string
     */
    public function getUserAgentPreview()
    {
        return substr($this->user_agent, 0, 42) . (strlen($this->user_agent) > 42 ? '<strong title="'.$this->user_agent.'">...</strong>' : '');
    }

    
    public function getItemUrlAttribute($value)
    {
        // get unique field from table
        # select name from db_table
        
        return "/".App::make('backend_url')."/$this->type/edit/$this->link_id";
    }
    
    public function getItemAttribute($value)
    {
        // get unique field from table
        # select name from db_table
        
        // return substr($this->type, 0, -1);
        return $this->type;
    }


    public static function scopeByuser($query, $user_id = null)
    {
        if(null !== $user_id)
        {
            return $query->where('user_id', '=', $user_id);
        }

        return $query;
    }
    
}
