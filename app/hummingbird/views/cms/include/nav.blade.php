<?php /*<!-- 
{{'';$i = 0;}}
@foreach ($nav as $mainsection => $sections)
    <div class="section_nav">
        <h1>{{$mainsection}}</h1>
            @foreach ($sections as $section => $items)
            
                <div class="subnav first">
                    <h1 style="color:#2B3E42;">
                        <a class="subnav_{{$i}}" href="javascript:void(0);" onclick="toggleView({{$i}});">{{$section}}<span>[+]</span></a>
                        <a class="subnav_{{$i}}" href="javascript:void(0);" style='display:none;' onclick="toggleView({{$i}});">{{$section}}<span>[-]</span></a>
                    </h1>

                    <div class="subnav-section subnavlist_{{$i++}}">
                        <ul>
                            @foreach ($items as $item)
                                <li><a href="/{{App::make('backend_url')}}/{{$item->link}}">{{$item->name}}</a></li>
                            @endforeach
                        </ul>         
                    </div>
                </div>
            @endforeach
    </div>

@endforeach

<div class="subnav">            
    <h1><a href="/{{App::make('backend_url')}}/logout" style="color:#666">Log Out</a></h1>
</div>


<style>
    .subnav-section { display: none; }
</style>
 -->
<?php 
// if(isset($help))
// {
//     echo '<h1 style="text-decoration:underline;">Help</h1><p style="font-size:10pt;line-height:125%;">'.$help.'</p>';
// }*/
?>

            <aside id="sidebar">
                <div class="hide sidebar-fixed fixed" style="background-color:grey;width:240px;bottom:0;left:0;height:60px;z-index:100;overflow:hidden;">
                    <div class="nav-button col-md-12" style="font-size:26px;color:white !important;"><a href="/hummingbird/logout" style="color:white !important;   "><i class="fa fa-sign-out"></i></a></div>
                </div>

                <div class="brand">
                    <img src="/themes/hummingbird/default/layout/hb-icon-white.png" /> 
                    <span>Hummingbird</span><span class="bold">CMS</span>
                </div>

                <div class="nav">
                    
<!--                     <div class="menu">
                        <h3>Hummingbird Elements</h3>
                        <ul>
                            <li class="option">
                                <a href="#" class="hasSubMenu"><i class="fa fa-image"></i> <span class="title">UI</span><i class="fa fa-chevron-right"></i></a>
                                <ul class="submenu">
                                    <li><a href="/hummingbird/ui/">General</a></li>
                                    <li><a href="/hummingbird/ui/el/"><i class="fa fa-pencil"></i> <span class="title">Buttons</span></a></li>
                                    <li><a href="/hummingbird/ui/typography/"><i class="fa fa-pencil"></i> <span class="title">Typography</span></a></li>
                                    <li><a href="/hummingbird/ui/widget/"><i class="fa fa-pencil"></i> <span class="title">Widgets</span></a></li>
                                    <li><a href="/hummingbird/ui/slider/"><i class="fa fa-pencil"></i> <span class="title">Sliders</span></a></li>
                                    <li><a href="/hummingbird/ui/grids/"><i class="fa fa-pencil"></i> <span class="title">Grids</span></a></li>
                                    <li><a href="/hummingbird/ui/calendar/"><i class="fa fa-pencil"></i> <span class="title">Calendar</span></a></li>
                                </ul>
                            </li>
                            <li class="option">
                                <a href="#" class="hasSubMenu"><i class="fa fa-image"></i> <span class="title">Fonts/Icons</span><i class="fa fa-chevron-right"></i></a>
                                <ul class="submenu">
                                    <li><a href="/hummingbird/admin-theme/fonts/fontawesome/">Fontawesome</a></li>
                                    <li><a href="/hummingbird/admin-theme/fonts/glyphicons/">Glyphicons</a></li>
                                </ul>
                            </li>
                            <li class="option">
                                <a href="#" class="hasSubMenu"><i class="fa fa-image"></i> <span class="title">Tables</span><i class="fa fa-chevron-right"></i></a>
                                <ul class="submenu">
                                    <li><a href="/hummingbird/admin-theme/tables/">Basic</a></li>
                                    <li><a href="/hummingbird/admin-theme/tables/responsive/">Responsive</a></li>
                                    <li><a href="/hummingbird/admin-theme/tables/advanced/">Advanced</a></li>
                                </ul>
                            </li>
                            <li class="option">
                                <a href="#" class="hasSubMenu"><i class="fa fa-image"></i> <span class="title">Form Components</span><i class="fa fa-chevron-right"></i></a>
                                <ul class="submenu">
                                    <li><a href="/hummingbird/admin-theme/forms/">Elements</a></li>
                                    <li><a href="/hummingbird/admin-theme/forms/components/">Components</a></li>
                                    <li><a href="/hummingbird/admin-theme/forms/wizard/">Wizard</a></li>
                                    <li><a href="/hummingbird/admin-theme/forms/validation/">Elements</a></li>
                                    <li><a href="/hummingbird/admin-theme/forms/pickers/">Pickers</a></li>
                                </ul>
                            </li>
                            <li class="option">
                                <a href="#" class="hasSubMenu"><i class="fa fa-image"></i> <span class="title">Charts</span><i class="fa fa-chevron-right"></i></a>
                                <ul class="submenu">
                                    <li><a href="/hummingbird/admin-theme/charts/morris/">Morris</a></li>
                                    <li><a href="/hummingbird/admin-theme/charts/flot/">Flot</a></li>
                                    <li><a href="/hummingbird/admin-theme/charts/chartjs/">ChartJS</a></li>
                                    <li><a href="/hummingbird/admin-theme/charts/high-charts/">High Charts</a></li>
                                    <li><a href="/hummingbird/admin-theme/charts/c3chart/">C3 Chart</a></li>
                                    <li><a href="/hummingbird/admin-theme/charts/d3/">D3 Data Driven Documents</a></li>
                                </ul>
                            </li>
                            <li class="option">
                                <a href="#" class="hasSubMenu"><i class="fa fa-image"></i> <span class="title">Maps</span><i class="fa fa-chevron-right"></i></a>
                                <ul class="submenu">
                                    <li><a href="/hummingbird/admin-theme/maps/google/">Google</a></li>
                                    <li><a href="/hummingbird/admin-theme/maps/vector/">Vector</a></li>
                                </ul>
                            </li>
                            <li class="option">
                                <a href="#" class="hasSubMenu"><i class="fa fa-image"></i> <span class="title">Pages</span><i class="fa fa-chevron-right"></i></a>
                                <ul class="submenu">
                                    <li><a href="/hummingbird/admin-theme/pages/profile/">Profile</a></li>
                                    <li><a href="/hummingbird/admin-theme/pages/invoice/">Invoice</a></li>
                                    <li><a href="/hummingbird/admin-theme/pages/timeline/">Timeline</a></li>
                                    <li><a href="/hummingbird/admin-theme/pages/404/">404</a></li>
                                    <li><a href="/hummingbird/admin-theme/pages/500/">500</a></li>
                                </ul>
                            </li>
                            <li class="option">
                                <a href="#" class="hasSubMenu"><i class="fa fa-image"></i> <span class="title">Media</span><i class="fa fa-chevron-right"></i></a>
                                <ul class="submenu">
                                    <li><a href="/hummingbird/admin-theme/media/">Library</a></li>
                                    <li><a href="/hummingbird/admin-theme/media/image-crop/">Image Crop</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div> 
 -->
                    <?php $menu_items = NavigationMenu::get_cms_nav(); ?>

                    @if(count($menu_items) > 0)

                        @foreach ($menu_items as $key => $items)
                            @if(count($menu_items[$key]) > 0)
                                <!--CMS SECTION {<?=$key?>}-->
                                <div class="menu">
                                    @if(isset($key) AND $key != '')
                                        <h3><?=$key?> <a class="menu-collapse active white"><i class="fa fa-plus font-size-10"></i></a></h3>
                                    @endif

                                    <ul @if($key != '') style="display:none;" @endif>
                                        <?php 
                                            $home = ($_SERVER["REQUEST_URI"] === '/' . App::make('backend_url') . '/') ? true:false;
                                            ksort($items);
                                        ?>

                                        @foreach($items as $item)
                                            <?php 
                                                $class = ($_SERVER["REQUEST_URI"] === $item['link'] OR $_SERVER["REQUEST_URI"] . '/' === $item['link']) ? 'active':false;
                                            ?>

                                            @if(isset($item['children']) AND count($item['children']) > 0)
                                                <li class="option">
                                                    <a href="#" class="hasSubMenu">@if($item['icon'] != '')<i class="<?=$item['icon']?>"></i> @endif<span class="title">{{$item['label']}}</span><i class="fa fa-chevron-right"></i></a>
                                                    <ul class="submenu" style="display:none;">
                                                        <li><a href="<?=$item['link']?>" class="{{$class}}"><i class="fa fa-angle-right"></i>{{$item['label']}}</a></li>
                                                        
                                                        @foreach($item['children'] as $child)
                                                            <?php 
                                                                $class = ($_SERVER["REQUEST_URI"] === $child['link'] OR $_SERVER["REQUEST_URI"] . '/' === $child['link']) ? 'active':false;
                                                            ?>
                                                            <li><a href="<?=$child['link']?>" class="{{$class}}"><i class="fa fa-angle-right"></i>{{$child['label']}}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @else
                                                <li>
                                                    <a href="<?=$item['link']?>" class="{{$class}}">@if($item['icon'] != '')<i class="<?=$item['icon']?>"></i> @endif<span class="title"><?=$item['label']?></span></a>
                                                </li>
                                            @endif

                                            <?php $class = '';?>
                                        @endforeach

                                        @if($key == 'Content')
                                            <li class="option hide">
                                                <a href="#" class="hasSubMenu"><i class="fa fa-image"></i> <span class="title">Media</span><i class="fa fa-chevron-right"></i></a>
                                                <ul class="submenu" style="display:none;">
                                                    <li><a href="/hummingbird/media/" <?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/media/") !== false) echo 'class="active"';?>><i class="fa fa-angle-right"></i>Manage Media</a></li>
                                                    <li><a href="#"><i class="fa fa-angle-right"></i>Galleries</a></li>
                                                </ul>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            @endif
                        @endforeach
                    @endif


<!--
                    <div class="menu">
                        <ul>
                            <li><a href="/hummingbird/" <?php if($_SERVER["REQUEST_URI"] == '/hummingbird/') echo 'class="active"';?>><i class="fa fa-home"></i> <span class="title">Dashboard</span></a></li>
                            <li><a href="/hummingbird/typography/"<?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/typography/") !== false) echo 'class="active"';?>><i class="fa fa-pencil"></i> <span class="title">Typography</span></a></li>
                            <li><a href="/hummingbird/ui/"<?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/ui/") !== false) echo 'class="active"';?>><i class="fa fa-pencil"></i> <span class="title">UI Elements - General</span></a></li>
                            <li><a href="/hummingbird/ui-el/"<?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/ui-el/") !== false) echo 'class="active"';?>><i class="fa fa-pencil"></i> <span class="title">UI Elements - Buttons</span></a></li>
                            <li><a href="/hummingbird/ui-grids/"<?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/ui-grids/") !== false) echo 'class="active"';?>><i class="fa fa-pencil"></i> <span class="title">UI Elements - Grids</span></a></li>
                        </ul>
                    </div>  

                    <div class="menu">
                        <h3>Content</h3>

                        <ul>
                            <li><a href="/hummingbird/pages/" <?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/pages/") !== false) echo 'class="active"';?>><i class="fa fa-edit"></i> <span class="title">Pages</span></a></li>
                            <li><a href="/hummingbird/templates-new/" <?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/templates-new/") !== false) echo 'class="active"';?>><i class="fa fa-edit"></i> <span class="title">Template Manager</span></a></li>
                            <li><a href="#"><i class="fa fa-th-large"></i> <span class="title">Modules</span></a></li>
                            <li class="option">
                                <a href="#" class="hasSubMenu"><i class="fa fa-image"></i> <span class="title">Media</span><i class="fa fa-chevron-right"></i></a>
                                <ul class="submenu">
                                    <li><a href="/hummingbird/media/" <?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/media/") !== false) echo 'class="active"';?>><i class="fa fa-angle-right"></i>Manage Media</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i>Galleries</a></li>
                                </ul>
                            </li>
                            <li><a href="/hummingbird/categories/" <?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/categories/") !== false) echo 'class="active"';?>><i class="fa fa-sitemap"></i> <span class="title">Categories</span></a></li>
                            <li><a href="/hummingbird/tags/"><i class="fa fa-tags"></i> <span class="title">Tags</span></a></li>
                        </ul>
                    </div>

                    <div class="menu">
                        <h3>Users</h3>

                        <ul>                        
                            <li><a href="/hummingbird/users/" <?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/users/") !== false) echo 'class="active"';?>><i class="fa fa-users"></i> <span class="title">Manage Users</span></a></li>
                            <li><a href="/hummingbird/roles/" <?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/roles/") !== false) echo 'class="active"';?>><i class="fa fa-eye-slash"></i> <span class="title">Manage Roles</span></a></li>
                        </ul>
                    </div>

                    <div class="menu">
                        <h3>Settings</h3>

                        <ul>
                            <li><a href="/hummingbird/settings/" <?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/settings/") !== false) echo 'class="active"';?>><i class="fa fa-gears"></i> <span class="title">Website Settings</span></a>
                            <li><a href="/hummingbird/errors/" <?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/errors/") !== false) echo 'class="active"';?>><i class="fa fa-warning"></i> <span class="title">Error Log</span></a></li>                       
                            <li><a href="/hummingbird/redirections/" <?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/redirections/") !== false) echo 'class="active"';?>><i class="fa fa-repeat"></i> <span class="title">Redirects</span></a></li>
                            <li><a href="/hummingbird/activity-logs/" <?php if(strpos($_SERVER["REQUEST_URI"], "/hummingbird/activity-logs/") !== false) echo 'class="active"';?>><i class="fa fa-history"></i> <span class="title">Activity</span></a></li>                           
                        </ul>
                    </div>-->

                    <div class="menu hide">
                        <h3>Plugins</h3>

                        <ul>
                            <li><a href="/{{App::make('backend_url')}}/events/"><i class="fa fa-ticket"></i> <span class="title">Events</span></a></li>
                            <li><a href="/{{App::make('backend_url')}}/blog/"><i class="fa fa-ticket"></i> <span class="title">Blog</span></a></li>
                            <!-- <li><a href="/{{App::make('backend_url')}}/categories/"><i class="fa fa-ticket"></i> <span class="title">Categories</span></a></li> -->
                            <!-- <li><a href="/{{App::make('backend_url')}}/tags/"><i class="fa fa-ticket"></i> <span class="title">Tags</span></a></li> -->
                            <!-- <li><a href="/{{App::make('backend_url')}}/templates-new/"><i class="fa fa-ticket"></i> <span class="title">Templates</span></a></li> -->
                        </ul>

<!--                         <ul>
                            <li><a href="#"><i class="fa fa-pencil-square"></i> <span class="title">Blog</span></a></li>
                            <li><a href="#"><i class="fa fa-list-alt"></i> <span class="title">Contact Form Builder</span></a></li>
                            <li class="option">
                                <a href="#" class="hasSubMenu"><i class="fa fa-money"></i> <span class="title">Donations</span><i class="fa fa-chevron-right"></i></a>
                                <ul class="submenu">
                                    <li><a href="#"><i class="fa fa-angle-right"></i> Referrers</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i> Transactions</a></li>
                                </ul>
                            </li>
                            <li class="option">
                                <a href="#" class="hasSubMenu"><i class="fa fa-ticket"></i> <span class="title">Events</span><i class="fa fa-chevron-right"></i></a>
                                <ul class="submenu">
                                    <li><a href="#"><i class="fa fa-angle-right"></i> Manage</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i> Categories</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i> Festivals</a></li>
                                </ul>
                            </li>
                            <li><a href="#"><i class="fa fa-list-alt"></i> <span class="title">E-Marketing</span></a></li>
                            <li><a href="#"><i class="fa fa-linux"></i> <span class="title">Job Vacancies</span></a></li>
                            <li class="option">
                                <a href="#" class="hasSubMenu"><i class="fa fa-shopping-cart"></i> <span class="title">Shop</span><i class="fa fa-chevron-right"></i></a>
                                <ul class="submenu">
                                    <li><a href="#"><i class="fa fa-angle-right"></i> Discount Codes</a></li>                      
                                    <li><a href="#"><i class="fa fa-angle-right"></i> Picking List</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i> Products</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i> Reporting</a></li>                        
                                    <li><a href="#"><i class="fa fa-angle-right"></i> Settings</a></li>                        
                                    <li><a href="#"><i class="fa fa-angle-right"></i> Transactions</a></li>
                                </ul>
                            </li>
                            <li><a href="#"><i class="fa fa-map-marker"></i> <span class="title">Venues</span></a></li>
                        </ul> -->
                    <!-- </div>  -->
                </div>
            </aside>